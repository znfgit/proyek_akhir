<?php
	//error_reporting(E_ALL);
	define ('ROOT', dirname(__FILE__));
	define ('D', DIRECTORY_SEPARATOR);
	define ('P', PATH_SEPARATOR); 
	define ('SYSDIR', ROOT.D.'system'.D);

	date_default_timezone_set('Asia/Jakarta');

	require_once 'includes/config.php';

	set_include_path( 
		ROOT.D.'build'.D.'classes'.P.
		ROOT.D.'build'.D.'classes'.D. APPNAME .P.	
		ROOT.D.'build'.D.'classes'.D. APPNAME .D. 'om' .P.
		ROOT.D.'build'.D.'classes'.D. APPNAME .D. 'map' .P.
		ROOT.D.'..'.D.'xond'.P.
		get_include_path()
	);

	require_once 'propel/Propel.php';
	require_once 'PHPExcel/PHPExcel.php';
	require_once 'library/functions.php';
	require_once 'swift/swift_required.php';

	spl_autoload_register("class_autoloader");

	Propel::init(ROOT.D.'build'.D.'conf'.D.DBNAME.'-conf.php');

	@session_start();
?>
<?php
/*
define ('ROOT', dirname(__FILE__));
define ('D', DIRECTORY_SEPARATOR);
define ('P', PATH_SEPARATOR); 
define ('SYSDIR', ROOT.D.'system'.D);

set_include_path( 
	ROOT.D.'build'.D.'classes'.P.
	ROOT.D.'..'.D.'xond'.P.
	get_include_path()
);

//echo ROOT.D.'build'.D.'classes'.P;

require_once 'propel/Propel.php';
require_once 'propel/engine/database/model/NameGenerator.php';
require_once 'PHPExcel/PHPExcel.php';
require_once 'library/functions.php';
require_once 'includes/config.php';

spl_autoload_register("class_autoloader");

Propel::init(ROOT.D.'build'.D.'conf'.D.DBNAME.'-conf.php');

@session_start();
*/
?>