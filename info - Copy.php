<?php

	include 'startup.php';
	error_reporting(E_ALL);
	global $ptkObj, $validasis, $outArr, $fromEmail, $isCorrectLogin, $isWrongPassword, $isWrongNuptk, $isMaintenance;
	
	$isMaintenance = false;
	
	function getGajiPokok($pgId, $mk=0) {
		
		$c = new Criteria();
		$c->add(RGajiPokokPeer::PANGKAT_GOLONGAN_ID, $pgId);		
		$rgps = RGajiPokokPeer::doSelect($c);
		$i = 0;		
		$gp = 0;
		$tmpRgps = false;
		
		foreach ($rgps as $r) {
			//$r = new RGajiPokok();			
			$pass = ($r->getMasaKerja() < $mk);
			if ((!$pass)) {
				if ($r->getMasaKerja() == $mk) {
					$gp = $r->getGajiPokok();
				} else {
					$gp = $tmpRgps->getGajiPokok();
				}
				break;
			}
			$tmpRgps = $r;
			//echo ($pass) ? "belum sampai<br>" : "lewat!!<br>";
			$i++;
		}
		
		if (!$gp && is_object($tmpRgps)) {
			$gp = $tmpRgps->getGajiPokok();
		}
		return nf($gp);
	}
	
	/*
	$pgId = 11;
	$mk = 35;
	$pg = PangkatGolonganPeer::retrieveByPK($pgId);
	
	echo "PG = ".$pg->getNama()." | MK = ".$mk." | Gaji Pokok = ". getGajiPokok($pgId, $mk);
	die;
	*/
	
	
	
		
	$nuptk = $_REQUEST["id"];
	$fromEmail = $_REQUEST["email"]? true : false;
	
	$isCorrectLogin = false;
	$isWrongPassword = false;
	$isWrongNuptk = false;
	
	//session_start();
		
	if (isset($_REQUEST["logout"])) {
		
		//session_destroy();		
		//unset($_SESSION["nuptk"]);
		//unset($_SESSION["password"]);
		//unset($_SESSION["fromEmail"]);	
		
	}
		
	//if (isset($_REQUEST["nuptk"]) || isset($_SESSION["nuptk"])) {
	if (isset($_REQUEST["nuptk"]) || isset($_POST["admin"])) {

		$nuptk = $_REQUEST["nuptk"];
		$tglStr = $_REQUEST["password"];
		
		//$nuptk = $_REQUEST["nuptk"] ? $_REQUEST["nuptk"] : $_SESSION["nuptk"];
		//$tglStr = $_REQUEST["password"] ?  $_REQUEST["password"] : $_SESSION["password"];
		
		$tglStrWithStripes = substr($tglStr, 0, 4)."-".substr($tglStr, 4, 2)."-".substr($tglStr, 6,2);
		
		//echo $nuptk."|".$tglStr."|".$tglStrWithStripes;
		
		$c = new Criteria();
		$c->add(TPtkPeer::NUPTK, $nuptk);
		$ptkObjNuptkOnly = TPtkPeer::doSelectOne($c);
		
		if (!$isAdmin) {
			
			$c->add(TPtkPeer::TGL_LAHIR, $tglStrWithStripes);
			$c->add(TPtkPeer::STATUS, array(NULL, 1), Criteria::IN);		
			$ptkObj = TPtkPeer::doSelectOne($c);
			
			$isAdmin = isset($_REQUEST["admin"]);
		
			if (!is_object($ptkObjNuptkOnly)) {
				//$nrg
			}
			
			if (is_object($ptkObjNuptkOnly)) {
				// Berarti nuptk bener
			} else {
				$isWrongNuptk = true;
			}
			
			if (is_object($ptkObj)) {		
				$isCorrectLogin = true;	
				//$_SESSION["nuptk"] = $ptkObj->getNuptk();
				//$_SESSION["password"] = $_REQUEST["password"];
				//$_SESSION["fromEmail"] = $fromEmail;			
			}
		} else {
			if (is_object($ptkObjNuptkOnly)) {		
				$isCorrectLogin = true;
			}
		}
	}
	
	//print_r($_REQUEST["logout"]); echo "<br>"; print_r($_REQUEST["nuptk"]);   
	//die;

	if ($isMaintenance) {
		include "info_login.php";
		die;
	}
	
	if (!$isCorrectLogin) {
		//echo "belun login";
		if (is_object($ptkObjNuptkOnly)) {			
			$isWrongPassword = true;
		} else {
			$isWrongPassword = false;
		}					
		include "info_login.php";
		die;
	}

	
	/* Prep Module Validasi */
	require_once(dirname(__FILE__).D."modules".D."Validasi".D."Validasi.php");
	$vModule = new ValidasiModule();
	//print_r($ptkObj);
	//print_r($vModule); die;
	//$vModule->cekNuptk($ptkObj);
	
	/* Loop RMappingValidasi utk keperluan no.1 */
	$c = new Criteria();
	$c->add(RMappingValidasiPeer::R_KEPERLUAN_ID, 1);
	$validasis = RMappingValidasiPeer::doSelect($c);
	
	//print_r($validasis); die;
	
	/* Prepare PTK Object, in Array & fieldname form */
	$arrPtk = $ptkObj->toArray(BasePeer::TYPE_FIELDNAME);
	
	$i = 0;
	$arr["no"] = $i++;
	$fieldName = "nama";	
	$arr["fieldName"] = $fieldName;
	$arr["val"] = $arrPtk["$fieldName"];
	$arr["icon"] = "/assets/icons/information.png";
	$arr["status"] = "Info";
	$outArr[] = $arr;
	
	$arr["no"] = $i++;
	$fieldName = "nip";	
	$arr["fieldName"] = $fieldName;
	$arr["val"] = $arrPtk["$fieldName"] ? $arrPtk["$fieldName"] : "-";
	$arr["icon"] = "/assets/icons/information.png";
	$arr["status"] = "Info";
	$outArr[] = $arr;
	
	$arr["no"] = $i++;	
	$fieldName = "nama_kabupaten_kota";	
	$arr["fieldName"] = $fieldName;
	$arr["val"] = $arrPtk["$fieldName"];
	$arr["icon"] = "/assets/icons/information.png";
	$arr["status"] = "Info";
	$outArr[] = $arr;	
	
	$arr["no"] = $i++;	
	$fieldName = "nama_propinsi";	
	$arr["fieldName"] = $fieldName;	
	$arr["val"] = $arrPtk["$fieldName"];
	$arr["icon"] = "/assets/icons/information.png";
	$arr["status"] = "Info";
	$outArr[] = $arr;	
	
	foreach ($validasis as $v) {
		//$v = new RMappingValidasi();
		//echo "<br>".$i."<br>";
		//print_r($v);
		//continue;
		
		$fieldName = $v->getRValidasi()->getObyek();
		$functionName = $v->getRValidasi()->getNamaFungsi();
		
		$okIcon = "accept.png";
		$failIcon = $v->getRDerajatValidasi()->getIcon();
		$failStatus = $v->getRDerajatValidasi()->getNama();
		
		//echo "$fieldName|$functionName<br>";
		
		
		$arr["no"] = $i++;
		$arr["fieldName"] = $fieldName;
		$arr["val"] = $arrPtk["$fieldName"];
		
		if ($functionName) {
			$hasilCek = $vModule->{$functionName}($ptkObj);
			$arr["pass"] = $hasilCek["pass"];
			$arr["reason"] = $hasilCek["reason"]; 
			$arr["icon"] = "/assets/icons/".($hasilCek["pass"] ? $okIcon : $failIcon);
			$arr["status"] = $hasilCek["pass"] ? "OK" : $failStatus;			
		} else {
			$arr["icon"] = "/assets/icons/information.png";
		}
		
		$outArr[] = $arr;
		
	}
	
	
	$arr["no"] = $i++;	
	$fieldName = "gaji_pokok";	
	$arr["fieldName"] = $fieldName;	
	$arr["val"] = "Rp ". getGajiPokok($ptkObj->getPangkatGolonganId(), $ptkObj->getMasaKerjaTahun());
	$arr["icon"] = "/assets/icons/information.png";
	$arr["status"] = "Info";
	$arr["reason"] = "Info"; 
		
	$outArr[] = $arr;
	
	/* Loop RMappingValidasi utk keperluan no.1 */
	$c = new Criteria();
	$c->add(RMappingValidasiPeer::R_KEPERLUAN_ID, 3);
	$validasisProfesi = RMappingValidasiPeer::doSelect($c);
	
	$passAll = true;
	
	foreach ($validasisProfesi as $v) {
		//$v = new RMappingValidasi();
		//echo "<br>".$i."<br>";
		//print_r($v);
		//continue;
		
		$fieldName = $v->getRValidasi()->getObyek();
		$functionName = $v->getRValidasi()->getNamaFungsi();
		
		$okIcon = "accept.png";
		$failIcon = $v->getRDerajatValidasi()->getIcon();
		$failStatus = $v->getRDerajatValidasi()->getNama();
		$severity = $v->getRDerajatValidasiId();
		
		//echo "$fieldName|$functionName<br>";
		
		
		$arr["no"] = $i++;
		$arr["fieldName"] = $fieldName;
		$arr["val"] = $arrPtk["$fieldName"];
		
		if ($functionName) {
			$hasilCek = $vModule->{$functionName}($ptkObj);
			$arr["pass"] = $hasilCek["pass"];
			$arr["reason"] = $hasilCek["reason"]; 
			$arr["icon"] = "/assets/icons/".($hasilCek["pass"] ? $okIcon : $failIcon);
			$arr["status"] = $hasilCek["pass"] ? "OK" : $failStatus;
			$arr["severity"] = $severity;
		} else {
			$arr["icon"] = "/assets/icons/information.png";
		}
		
		$outArrProfesi[] = $arr;
		
	}
	//print_r($outArrProfesi);
	//print_r($outArr);
	//print_r($validasis); die;		
	include "info_tabel.php";
?>
