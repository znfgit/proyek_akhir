<style>
td {
	font-family: Arial Narrow;
	font-size: 10px;
	border-left: thin solid #000000;
	border-top: thin solid #000000;
	padding-left: 3px;
}
</style>
<?php

	include 'startup.php';
	error_reporting(E_ALL);
	
	global $setLimit;

	$setLimit = false;
	
	function microtime_float()
	{
	    list($usec, $sec) = explode(" ", microtime());
	    return ((float)$usec + (float)$sec);
	}
	
	function check_zero($var) {
		return ($var === 0);
	}
			
	function main($kabKotaId, $date=false) {
		global $setLimit;
		
		$time_start = microtime_float();
	
		$kabKotaObj = KabupatenKotaPeer::retrieveByPK($kabKotaId);
		$kabKotaNama = $kabKotaObj->getNama();
		$propinsiObj = $kabKotaObj->getPropinsi();
		
		echo "<br><b>Mengerjakan ($kabKotaId) $kabKotaNama</b><br>";
		
		////////////////////////
		////////////////////////
		/* Get Data PTK */
		////////////////////////
		////////////////////////
		$c = new Criteria();
		$c->add(TPtkPeer::KABUPATEN_KOTA_ID, $kabKotaId);		
		$c->addAscendingOrderByColumn(TPtkPeer::NUPTK);
		
		if ($setLimit) {
			$c->setLimit($setLimit);
		}
		
		try {
			$ptks = TPtkPeer::doSelect($c);
			if ($date) {
				echo "\n<br>Sejak tanggal $date, ditemukan ".sizeof($ptks)." ptk yang terupdate<br>\n";
			} else {
				echo "\n<br>Ditemukan ".sizeof($ptks)." yang akan diupdate<br>\n";
			}
		} catch (Exception $e) {
			print_r($e); die;
		}
		
		foreach ($ptks as $p) {
			$arrPtk[$p->getTPtkId()] = $p;
		}
		//echo "Ditemukan ".sizeof ($arrPtk)." ptk<br>"; die;
		
		/* Get Data Terdaftar */
		$c = new Criteria();
		$c->addJoin(PtkTerdaftarPeer::PTK_ID, PtkPeer::PTK_ID);
		$c->addJoin(PtkPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID);
		$c->addJoin(PtkTerdaftarPeer::TUGAS_PTK_ID, TugasPtkPeer::TUGAS_PTK_ID);		
		$c->add(SekolahPeer::KABUPATEN_KOTA_ID, $kabKotaId);
		$terdaftars = PtkTerdaftarPeer::doSelect($c);
		
		foreach ($terdaftars as $t) {
			$arrTerdaftar[$t->getPtkId()][] = $t;
		}
		
		/* Get Ready */
		$con = Propel::getConnection(TPtkPeer::DATABASE_NAME);
		$con->beginTransaction();

		$ram_used = memory_get_usage()/(1024 * 1024);
		$arrRekapSekolah = array();
		
		echo "<br>Perlu $ram_used MB memory untuk persiapan.<br>";
		
		$jumlahreal = 0;
		$ganda = 0;
		$record = 0;
		
		/* Loop On PTK */		
		foreach ($arrPtk as $p) {
			
			$tPtk = $p;
			//print_r($p); die;
			
			// Cek If NUPTK Dobel //
			if (!$tPtk->getNuptk()) {
				continue;
			}
			
			if ($lastNuptk != $tPtk->getNuptk()) {
				// 0 : != , bikin new T_Ptk_ganda, save 0, simpan 
				//		$lastNuptk = $tptk->getNuptk
				//		$lastPtkId = $tptk->getPtkId 
				// 1 : sama dgn 0 ? bikin new T_Ptk_ganda, 
				//		setPtk->lastNuptk
				//		setDll
				//		last2 JANGAN DISET
				// 2 : sama dgn 0 ?
				//		setPtk->lastNuptk
				//		setDll
				// 3 : !=, bikin baru set $last2 baru
				
				$tg = new TPtkGanda();
				$tg->setTPtkGandaId($tPtk->getTPtkId());
				$tg->setTPtkId($tPtk->getTPtkId());				
				$lastPtkId = $tPtk->getTPtkId();				
				$lastNuptk = $tPtk->getNuptk();				
				$lastNama = $tPtk->getNama();				
				$lastTglLahir = $tPtk->getTglLahir(); 
				$lastNamaSekolah = $tPtk->getNamaSekolah();
				$lastSekolahId = $tPtk->getSekolahId();
				$jumlahreal++;
				unset($isBerganda);
				echo "\n- ".$tPtk->getNuptk().": ". $tPtk->getNama()."<br>";
				
			} else if ($lastNuptk == $tPtk->getNuptk()) {
				
				$tg = new TPtkGanda();
				$tg->setTPtkGandaId($tPtk->getTPtkId());
				$tg->setTPtkId($lastPtkId);
				$isBerganda = 1;
				$ganda++;
				echo "\n |- ".$tPtk->getNuptk().": ". $tPtk->getNama()." (g) <br>";
			}
			$tg->setNuptk($lastNuptk);
			$tg->setNama($tPtk->getNama());			
			$tg->setTglLahir($tPtk->getTglLahir());
			$tg->setNamaSekolah($tPtk->getNamaSekolah());
			$tg->setSekolahId($tPtk->getSekolahId());
			
			$tg->setParentNama($lastNama);
			$tg->setParentTglLahir($lastTglLahir);
			$tg->setParentNamaSekolah($lastNamaSekolah);
			$tg->setParentSekolahId($lastSekolahId);			
			
			$tg->setIsBerganda($isBerganda);
			
			$td = $arrTerdaftar[$tPtk->getTPtkId()];
			//$td = new PtkTerdaftar();
			if (is_object ($td)) {
				$tg->setNamaTugasPtk($td->getTugasPtk()->getNama());
				$tg->setTugasPtkId($td->getTugasPtkId());			
				$tg->setIsInduk($td->getPtkInduk());
			}			
			$tg->save($con);
			
			$record++;
			
			if (($record % 100) == 0) {
				$con->commit();					
			}

		}		
		
		$countNrgAkhir = sizeof($arrNrg);
		$countNrgMatch = $countNrgAwal - $countNrgAkhir;
		
		$time_end = microtime_float();
		$time = $time_end - $time_start;
		$ram_used = memory_get_usage()/(1024 * 1024);
		
		$jumlahreal = 0;
		$ganda = 0;
		$record = 0;
		
		echo "\nTotal {$record} record | $jumlahreal NUPTK | $ganda NUPTK Ganda <br>";		
		echo "\n<br><b><i>Selesai dalam $time detik, memakan ". nf($ram_used)." MB</i></b><br>";		

		unset ($sekolahs, $mengajars, $terdaftars, $nrgs, $arrSekolah, $arrPtk, $arrMengajar, $arrTerdaftar, $arrNrg, $arrBidangStudi);
		
		$con->commit();
	
	}
	

	if ($argv) {
		
		$wilayah = $argv[1];
		$id = $argv[2];
		
		if ($wilayah == "kabupaten_kota_id") {
			$kabKotaId = $id;
		} else if ($wilayah == "propinsi_id") {
			$propId = $id;
		}
	
	} else {
	
		$propId = $_REQUEST["propinsi_id"];
		$kabKotaId = $_REQUEST["kabupaten_kota_id"];
			
	}
	
	if ($kabKotaId) {		
		
		main($kabKotaId);
		
	} else if ($propId) {
		
		$prop = PropinsiPeer::retrieveByPK($propId);
		$kks = $prop->getKabupatenKotas();
		if (!writePropinsiKonversiFile($propId, 0, sizeof($kks))){
			return false;
		} else {
			$filesAllowedToBeDeletedPropinsi[] = $propId;
		}
				
		$i = 1;
		
		foreach ($kks as $k) {
			//$k = new KabupatenKota();	
			$propId = $k->getPrimaryKey();
			main($k->getPrimaryKey());
			writePropinsiKonversiFile($propId, $i, sizeof($kks));
			
			$i++;
			continue;
		}
				
	}
	
	echo "<br>Rekor pemakaian memory ". nf(memory_get_peak_usage()/(1024*1024))." MB<br>";
?>