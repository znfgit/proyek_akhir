<?php
	include 'startup.php'; 
	
	function getTRekap($jn, $kabKotaId, $single){
		error_reporting(E_ERROR);
		
		$server = true;
		if ($server) {
			$enter = "\n";
		} else {
			$enter = "<br>";
		}
		
		// fetch t_ptk
		$c = new Criteria();
		if ($kabKotaId) {
			$c->add(TPtkPeer::KABUPATEN_KOTA_ID, $kabKotaId);
		}
		$c->add(TPtkPeer::NRG, NULL, Criteria::ISNOTNULL);
		//$c->addGroupByColumn(TPtkPeer::NUPTK);
		try {
			$tPtks = TPtkPeer::doSelect($c);
		} catch (Exception $e) {
			print_r($e);
		}
		$arr = array();
		foreach ($tPtks as $t) {
			$n = 1;
			/**
			 * 	nama_tugas_tambahan = Kepala Sekolah (6 jam)
				kode_sertifikasi_bidang_studi_guru_id = 2 (jjm diloskan saja)
			 */
			
			// kondisi 1=memenuhi, 2=tdk memenuhi, 3=belum update data
			if (!$t->getLastUpdate()) {
				$kondisi = 3; 
			} else {
				
				if ($t->getTglPensiun() < date("Y-m-d"))
					$n *= 0;

				// jika selain guru kelas sd
				if ($t->getKodeSertifikasiBidangStudiGuruId() != 2) {
					
					// jika jabatan sebagai kepala sekolah
					if ($t->getJabatanPtkId() == 2) {
						if ($t->getJumlahJamMengajarSesuai() < 6)
							$n *= 0;
					} else {
						// if ($t->getJumlahJamMengajarSesuai() < 24 || $t->getJumlahJamMengajarSesuai() > 60)
						if ($t->getJumlahJamMengajarSesuai() < 24)
							$n *= 0;
					}
				}
					
				if ($n == 1) {
					$kondisi = 1;
				} else if ($n == 0){
					$kondisi = 2;
				}
			}
							
			$arr[$t->getNuptk()]["nrg"] = $t->getNrg();
			$arr[$t->getNuptk()]["nuptk"] = $t->getNuptk();
			$arr[$t->getNuptk()]["kondisi"] = $kondisi;
				
		}
		
		// fecth table nrg per kabkota 
		$c = new Criteria();
		if($kabKotaId){
			$c->add(KabupatenKotaPeer::KABUPATEN_KOTA_ID, $kabKotaId);
		}
		$kabKotas = KabupatenKotaPeer::doSelect($c);
		$affected = 0;
		
		foreach ($kabKotas as $k) {
			echo $k->getPrimaryKey()."-".$k->getNama()."{$enter}{$enter}";
			
			$c = new Criteria();
			$c->add(NrgNewPeer::NRG_JENJANG, 'DIKDAS', Criteria::EQUAL);
			$c->add(NrgNewPeer::NRG_KODEKAB, $k->getKodeSertifikasi(), Criteria::EQUAL);
			$nrgNewes = NrgNewPeer::doSelect($c);
			$count = NrgNewPeer::doCount($c);
			
			$jmlDau = 0;
			$jmlDekon = 0;
			$memenuhiT = 0;
			$tdkMemenuhiT = 0;
			$blmUpdateT = 0;
			$tdkMatchT = 0;
			$memenuhiD = 0;
			$tdkMemenuhiD = 0;
			$blmUpdateD = 0;
			$tdkMatchD = 0;
			
			foreach ($nrgNewes as $n) {
				$nuptk = $n->getNrgNuptk();
				// echo $nrg."=".$arr[$nrg]["nuptk"]."{$enter}";
				
				// Jika matching
				if ($nuptk == $arr[$nuptk]["nuptk"]) {
					// echo $n->getNrgNrg()."=".$arr[$n->getNrgNuptk()]["nrg"]."{$enter}";
					if ($n->getNrgFormatBayar() == "DAU") {
						$jmlDau++;
						if ($arr[$nuptk]["kondisi"] == 1) {
							$memenuhiT++;
						} else if ($arr[$nuptk]["kondisi"] == 2) {
							$tdkMemenuhiT++;
						} else if ($arr[$nuptk]["kondisi"] == 3) {
							$blmUpdateT++;
						}
					} else if ($n->getNrgFormatBayar() == "DEKON") {
						$jmlDekon++;
						if ($arr[$nuptk]["kondisi"] == 1) {
							$memenuhiD++;
						} else if ($arr[$nuptk]["kondisi"] == 2) {
							$tdkMemenuhiD++;
						} else if ($arr[$nuptk]["kondisi"] == 3) {
							$blmUpdateD++;
						}
					}
					
				} else  {
					if ($n->getNrgFormatBayar() == "DAU") {
						$tdkMatchT++;
					} else if ($n->getNrgFormatBayar() == "DEKON") {
						$tdkMatchD++;
					}
				}
			}
			
			echo "Total PTK yg ada di NRG : {$count}{$enter}{$enter}";
			
			echo "DAU{$enter}";
			echo "Memenuhi Syarat : {$memenuhiT}{$enter}";
			echo "Tidak Memenuhi Syarat : {$tdkMemenuhiT}{$enter}";
			echo "Belum Update: {$blmUpdateT}{$enter}{$enter}";
			
			echo "DEKON{$enter}";
			echo "Memenuhi Syarat : {$memenuhiD}{$enter}";
			echo "Tidak Memenuhi Syarat : {$tdkMemenuhiD}{$enter}";
			echo "Belum Update: {$blmUpdateD}{$enter}";
			
			echo "Tidak Match DAU : {$tdkMatchT}{$enter}";
			echo "Tidak Match DEKON : {$tdkMatchD}{$enter}{$enter}{$enter}";

			// insert rekap tunjangan profesi all
			try {
				$belumUpdateNasional = $blmUpdateT + $blmUpdateD + $tdkMatchD + $tdkMatchT;
				
				$nAll = new TRekapValidasi();
				$nAll->setRKeperluanId(3);
				$nAll->setKabupatenKotaId($k->getKabupatenKotaId());
				$nAll->setPropinsiId($k->getPropinsiId());
				$nAll->setJumlahIncomplete($belumUpdateNasional);
				$nAll->setJumlahPass($memenuhiT+$memenuhiD);
				$nAll->setJumlahUnqualified($tdkMemenuhiT+$tdkMemenuhiD);
				if (!$nAll->save())
					die("Gagal Update All ".$k->getPrimaryKey()."-".$k->getNama());
				
				// insert rekap tunjangan profesi DAU
				$nDau = new TRekapValidasi();
				$nDau->setRKeperluanId(7);
				$nDau->setKabupatenKotaId($k->getKabupatenKotaId());
				$nDau->setPropinsiId($k->getPropinsiId());
				$nDau->setJumlahIncomplete($blmUpdateT + $tdkMatchT);
				$nDau->setJumlahPass($memenuhiT);
				$nDau->setJumlahUnqualified($tdkMemenuhiT);
				if (!$nDau->save())
					die("Gagal Update DAU ".$k->getPrimaryKey()."-".$k->getNama());
				
				// insert rekap tunjangan profesi DEKON
				$nDekon = new TRekapValidasi();
				$nDekon->setRKeperluanId(8);
				$nDekon->setKabupatenKotaId($k->getKabupatenKotaId());
				$nDekon->setPropinsiId($k->getPropinsiId());
				$nDekon->setJumlahIncomplete($blmUpdateD + $tdkMatchD);
				$nDekon->setJumlahPass($memenuhiD);
				$nDekon->setJumlahUnqualified($tdkMemenuhiD);
				if (!$nDekon->save())
					die("Gagal Update DEKON ".$k->getPrimaryKey()."-".$k->getNama());
					
			} catch (Exception $err) {
					die("Gagal Update {$err}");
			}
		}
	}
	
	if ($argv) {
		$jn = $argv[1];
		$kabKotaId = $argv[2];
	} else {
		$jn = ($_REQUEST["jn"]) ? $_REQUEST["jn"] : 3;
		$kabKotaId = ($_REQUEST["kabkota"]) ? $_REQUEST["kabkota"] : 146;
	}
	
	getTRekap($jn, $kabKotaId);
	// getTRekap(3, 146);
?>