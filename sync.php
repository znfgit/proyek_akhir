<?php
	error_reporting(E_ALL);
	
	require('startup.php');
	//error_reporting(E_ALL);
	
	
	function executeSqlCommand($sql) {
		try {
			$con = Propel::getConnection(DBNAME);	
			$stmt = $con->prepare($sql); 
			$stmt->execute(); 
			return true;
		} catch (Exception $e) {
			return false;
		}
	}
		
	
	$propinsis = PropinsiPeer::doSelect(new Criteria());
	
	foreach ($propinsis as $p) {
		$progress = TSinkronPropinsiPeer::retrieveByPK($p->getPrimaryKey());
		if (!is_object($progress)) {
			$progress = new TSinkronPropinsi();						
		}
		
		$progress->setWaktuMulai(NULL);
		$progress->setWaktuSelesai(NULL);
		$progress->setNamaProses(NULL);
		$progress->setProgress(0);
		$progress->save();		
	}
	//print_r($propinsis);
	
	foreach ($propinsis as $p) {
		
		echo "\n".$p->getPrimaryKey();
		
		$progress = TSinkronPropinsiPeer::retrieveByPK($p->getPrimaryKey());
		$progress->setWaktuMulai(date("Y-m-d H:i:s"));
		$progress->setWaktuSelesai(NULL);
		$progress->setProgress(0);
		$progress->save();
		
		$execStr = "execute dbo.syncStepByStepRefreshJam ".$p->getPrimaryKey();
		//echo " ".$execStr." ";
		//sleep(1);
		executeSqlCommand($execStr);
		
		$progress->setProgress(100);
		$progress->setWaktuSelesai(date("Y-m-d H:i:s"));
		$progress->save();
		
	}
	
?>