<?php

	function getSize($arr) {
		$tot = 0;
		foreach($arr as $a) {
			if (is_array($a)) {
				$tot += getSize($a);
			}
			if (is_string($a)) {
				$tot += strlen($a);
			}
			if (is_int($a)) {
				$tot += PHP_INT_SIZE;
			}
		}
		return $tot;
	}

	function getGajiPokok($pgId, $mk=0) {
		
		$c = new Criteria();
		$c->add(RGajiPokokPeer::PANGKAT_GOLONGAN_ID, $pgId);		
		$rgps = RGajiPokokPeer::doSelect($c);
		$i = 0;		
		$gp = 0;
		$tmpRgps = false;
		
		foreach ($rgps as $r) {
			//$r = new RGajiPokok();			
			
			$pass = ($r->getMasaKerja() < $mk);
			if ((!$pass)) {
				if ($r->getMasaKerja() == $mk) {
					//print_r($r); die;
					if (is_object($r)) {
						$gp = $r->getGajiPokok();
					}
				} else {
					if (is_object($tmpRgps)) {
						$gp = $tmpRgps->getGajiPokok();
					}
				}
				break;
			}
			$tmpRgps = $r;
			//echo ($pass) ? "belum sampai<br>" : "lewat!!<br>";
			$i++;
		}
		
		if (!$gp && is_object($tmpRgps)) {
			$gp = $tmpRgps->getGajiPokok();
		}
		return nf($gp);
	}
