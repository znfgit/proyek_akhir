simptk.CariWilayah = Ext.extend(Ext.Window, {
	
	/* Standard configs */
	title: 'Pemilihan Wilayah',
	border: true,
	layout: 'fit',
	width: 350,
	height: 130,
	modal: true,	
	bodyStyle:'padding:5px;',
	gridSekolah: null,
	form: null,	
	constructor: function(config){    
		
		if (!this.level_wilayah) {
			this.level_wilayah = 2;
		}
		// Lingkup data
		var lingkupData = new  Ext.form.RadioGroup({			
            fieldLabel: 'Sinkronkan',
            items: [
                {boxLabel: 'Semua Data', name: 'rg-sync', inputValue: 1, checked: true},
                {boxLabel: 'Data Baru Diupdate', name: 'rg-sync', inputValue: 2}
            ]
		});
		
		// Propinsi //
		var propinsiCombo = new simptk.PropinsiCombo({
			anchor: '100%',
			lastQuery: '',
			listeners: {
				select: function(thiscombo, record, index) {
					
					simptk.propinsi_selected = record;
					simptk.propinsi_selected_nama = record.data.Nama;					

					kabkotaCombo.filter(thiscombo.getValue());
					
				}
			}
		});
		
		var kabkotaCombo = new simptk.KabupatenKotaCombo({
			anchor: '100%',
			listeners: {
				select: function(thiscombo, record, index) {
					simptk.kabkota_selected = record;
					simptk.kabkota_selected_nama = record.data.Nama;
				}
			}
		});
		
		var itemsArr = [];
		
		var varHeight;
		if (window.level_wilayah >= 1) {
			itemsArr.push(propinsiCombo);
			//varHeight = 105;
		}
		if (window.level_wilayah >= 2) {
			itemsArr.push(kabkotaCombo);
			//varHeight = 130;
		}
		if (window.level_wilayah >= 3) {
			itemsArr.push(kecamatanCombo);			
			//varHeight = 155;
		}

		var form = new Ext.form.FormPanel({
			baseCls: 'x-plain',			
			labelWidth: 110,
			defaultType: 'textfield',			
			flex: 2,
			margins: {top:10, right:0, bottom:0, left:0},
			items: [
				propinsiCombo,
				kabkotaCombo
			]
		});		
		
		
		Ext.apply(this, {
			layout: 'fit',
			items: [
				form
			],
			buttons: [{
				text: 'Pilih',
				iconCls: 'cek',				
				handler: function(){
					if (!kabkotaCombo.getValue()) {
						Ext.Msg.alert('Error', 'Mohon pilih kab/kota terlebih dahulu');
						return;
					}
					
					var kabupaten_kota_id = kabkotaCombo.getValue();
					
					window.open('/result/'+ kabupaten_kota_id +'.html');
					/*
					window.fireEvent('wilayahselected', window, simptk.kabkota_selected, simptk.propinsi_selected);
					window.close();
					*/
				}
			}]
		});
		
		simptk.CariWilayah.superclass.constructor.call(this);
		
	},
	initComponent: function() {
	   simptk.CariWilayah.superclass.initComponent.call(this);
	   this.addEvents('wilayahselected');
	}
});
