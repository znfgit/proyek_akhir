Ext.onReady(

function () {

    var form = new Ext.form.FormPanel({
        baseCls: 'x-plain',
        columnWidth: 0.99,
        bodyStyle: 'padding-left:10px; padding-top:5px;',
        labelWidth: 55,
        url: 'auth/login.json',
        defaultType: 'textfield',
        items: [
        new Ext.form.Label({
            text: 'Username',
            style: 'font-weight:bold;'
        }), {
            hideLabel: true,
            name: 'username',
            allowBlank: false,
            blankText: 'Username harus di isi',
            width: 150
        },
        new Ext.form.Label({
            text: 'password',
            style: 'font-weight:bold;'
        }), {
            hideLabel: true,
            id: 'login_password',
            name: 'password',
            allowBlank: false,
            blankText: 'password harus di isi',
            inputType: 'password',
            width: 150
        }]
    });

    var win_login = new Ext.Window({
        title: 'Sistem Validasi PTK',
        iconCls: 'logo-tut',
        width: 300,
        height: 217,
        closable: false,
        resizable: false,
        draggable: false,
        plain: true,
        buttonAlign: 'right',
        layout: 'column',
        items: [{
            width: 115,
            html: '<img src="/assets/images/logo_ptk.png">'
        },
        form],
        buttons: [{
            text: '<b>Login</b>',
            iconCls: 'login',
            type: 'submit',
            handler: function () {
                var panel_submit = win_login.getComponent(1);
                if (panel_submit.form.isValid()) {
					var vals = panel_submit.form.getValues();
					console.log (vals.username);
					if (vals.username != 'admin') {
						Ext.Msg.alert('Error', 'Username salah');
						return;
					}
                    panel_submit.form.submit({
                        waitMsg: 'Pemuatan ...',
                        failure: function (form, action) {
                            Ext.MessageBox.alert('Error', action.result.message);
                        },
                        success: function (form, action) {
                            var reason = action.result.message;
							var url = '/default.php';
							Ext.MessageBox.alert('Berhasil', 'Login berhasil, tunggu sebentar..');
							setTimeout('window.location = "' + url + '"',500);
                        }
                    });
                } else {
                    Ext.MessageBox.alert('Error', 'Username & password harus di isi');
                }
            }
        }]
    });

    Ext.getCmp('login_password').on('specialkey',

    function (field, e) {
        if (e.getKey() === e.ENTER) {
            form.getForm().submit({
                method: 'POST',
                waitTitle: 'Connecting',
                waitMsg: 'Pemuatan ...',
                url: 'auth/login.json',
                failure: function (form, action) {
                    Ext.MessageBox.alert('Error', action.result.message);
                },
                success: function (form, action) {
                    var reason = action.result.message;
					var url = '/default.php';
					Ext.MessageBox.alert('Berhasil', 'Login berhasil, tunggu sebentar..');
					setTimeout('window.location = "' + url + '"',500);
                }
            });
        }
    });

    win_login.show();
});