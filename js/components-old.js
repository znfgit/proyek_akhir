simptk.RecordSetTahunAjaran = [
    { name: 'TahunAjaranId', type: 'float'  } ,
    { name: 'Kode', type: 'string'  } ,
    { name: 'Nama', type: 'string'  }     
];

simptk.lookupStoreTahunAjaran = new Ext.data.Store({
    reader: new Ext.data.JsonReader({
        id: 'TahunAjaranId',
        root: 'rows',
        totalProperty: 'results',
        fields: simptk.RecordSetTahunAjaran        
    }),
    // url: './TahunAjaran/fetch.json',
    autoLoad: true,        
    url: './data/TahunAjaran.json',
    
    sortInfo: {field: 'TahunAjaranId', direction: "ASC"}
});

simptk.Tahun = Ext.extend(Ext.form.ComboBox, {
	store: simptk.lookupStoreTahunAjaran,
	displayField:'TahunAjaranId',
	valueField: 'TahunAjaranId',
	fieldLabel: 'Tahun Lulus',
	typeAhead: true,
	mode: 'local',
	forceSelection: true,
	triggerAction: 'all',
	emptyText:'Tahun Lulus...',
	selectOnFocus:true,
	width: 100,
	listeners : {
		select: function(thiscombo, record, index) {
			simptk.tahun = thiscombo.getValue();
		}
	},
	initComponent: function(){
		simptk.Tahun.superclass.initComponent.call(this);
	}
});

// Propinsi //
simptk.RecordSetPropinsi = [
	{ name: 'PropinsiId', type: 'int'  } ,
	{ name: 'NegaraId', type: 'float'  } ,
	{ name: 'Kode', type: 'string'  } ,
	{ name: 'KodeNuptk', type: 'string'  } ,
	{ name: 'KodeSertifikasi', type: 'string'  } ,
	{ name: 'Nama', type: 'string'  } ,
	{ name: 'Status', type: 'float'  }	 
];

//simptk.RecordObjPropinsi = Ext.data.Record.create(simptk.RecordSetPropinsi);
simptk.RecordPropinsi = [
	[1, 1, 11, '', '', 'Aceh', 1],
	[2, 1, 12, '', '', 'Sumatera Utara', 1],
	[3, 1, 13, '', '', 'Sumatera Barat', 1],
	[4, 1, 14, '', '', 'Riau', 1],
	[5, 1, 15, '', '', 'Jambi', 1],
	[6, 1, 16, '', '', 'Sumatera Selatan', 1],
	[7, 1, 17, '', '', 'Bengkulu', 1],
	[8, 1, 18, '', '', 'Lampung', 1],
	[9, 1, 19, '', '', 'Kepulauan Bangka Belitung', 1],
	[10, 1, 21, '', '', 'Kepulauan Riau', 1],
	[11, 1, 31, '', '', 'D.K.I. Jakarta', 1],
	[12, 1, 32, '', '', 'Jawa Barat', 1],
	[13, 1, 33, '', '', 'Jawa Tengah', 1],
	[14, 1, 34, '', '', 'Daerah Istimewa Yogyakarta', 1],
	[15, 1, 35, '', '', 'Jawa Timur', 1],
	[16, 1, 36, '', '', 'Banten', 1],
	[17, 1, 51, '', '', 'Bali', 1],
	[18, 1, 52, '', '', 'Nusa Tenggara Barat', 1],
	[19, 1, 53, '', '', 'Nusa Tenggara Timur', 1],
	[20, 1, 61, '', '', 'Kalimantan Barat', 1],
	[21, 1, 62, '', '', 'Kalimantan Tengah', 1],
	[22, 1, 63, '', '', 'Kalimantan Selatan', 1],
	[23, 1, 64, '', '', 'Kalimantan Timur', 1],
	[24, 1, 71, '', '', 'Sulawesi Utara', 1],
	[25, 1, 72, '', '', 'Sulawesi Tengah', 1],
	[26, 1, 73, '', '', 'Sulawesi Selatan', 1],
	[27, 1, 74, '', '', 'Sulawesi Tenggara', 1],
	[28, 1, 75, '', '', 'Gorontalo', 1],
	[29, 1, 76, '', '', 'Sulawesi Barat', 1],
	[30, 1, 81, '', '', 'Maluku', 1],
	[31, 1, 82, '', '', 'Maluku Utara', 1],
	[32, 1, 91, '', '', 'Papua', 1],
	[33, 1, 92, '', '', 'Papua Barat', 1]
];

//var lookupStorePropinsi = new Ext.data.ArrayStore({
//simptk.PropinsiStore = Ext.extend(Ext.data.ArrayStore, {
simptk.PropinsiStore = new Ext.data.ArrayStore({
	autoLoad: true,
	fields: simptk.RecordSetPropinsi,
	data : simptk.RecordPropinsi
});

simptk.PropinsiCombo = Ext.extend(Ext.form.ComboBox, {
	
	fieldLabel: 'Propinsi',
	emptyText: 'Pilih Propinsi...',
	anchor: '100%',
	
	constructor: function(config){
		Ext.apply(this, config);
		
		Ext.apply(this, {			
			//store: lookupStorePropinsi,
			store: simptk.PropinsiStore,
			displayField: 'Nama',
			valueField: 'PropinsiId',
			fieldLabel: 'Propinsi',
			typeAhead: true,
			mode: 'local',
			triggerAction: 'all',			
			selectOnFocus: true,
			maxHeight: 200
		});
		
		simptk.PropinsiCombo.superclass.constructor.call(this);
		
		if (simptk.propinsi_selected) {
			var propId = simptk.propinsi_selected.get('PropinsiId');
			this.setValue(propId, true);
		}
		if (simptk.session_propinsi_id) {
			this.disable();
		}

	}

});

Ext.reg('propinsicombo', simptk.PropinsiCombo); 

// Kabkota //
simptk.RecordSetKabupatenKota = [
	{ name: 'KabupatenKotaId', type: 'int'  } ,
	{ name: 'PropinsiId', type: 'int'  } ,
	{ name: 'Kode', type: 'string'  } ,
	{ name: 'KodeNuptk', type: 'string'  } ,
	{ name: 'KodeSertifikasi', type: 'string'  } ,
	{ name: 'Nama', type: 'string'  } ,
	{ name: 'Type', type: 'float'  } ,
	{ name: 'Status', type: 'float'  } ,
	{ name: 'LuasWilayah', type: 'string' },
	{ name: 'JumlahPenduduk', type: 'string'  }	,
	{ name: 'Keterangan', type: 'string' }
];

simptk.RecordKabupatenKota = [
	[1, 1, '01', '0160', '0607', 'Kab. Aceh Selatan', '', 1, 3841, 218230, ''],
	[2, 1, '02', '0161', '0608', 'Kab. Aceh Tenggara', '', 1, 4231, 170099, ''],
	[3, 1, '03', '0162', '0604', 'Kab. Aceh Timur', '', 1, 6286, 362610, ''],
	[4, 1, '04', '0163', '0605', 'Kab. Aceh Tengah', '', 1, 4318, 194235, ''],
	[5, 1, '05', '0164', '0606', 'Kab. Aceh Barat', '', 1, 2927, 170001, ''],
	[6, 1, '06', '0205', '0601', 'Kab. Aceh Besar', '', 1, 2969, 318949, ''],
	[7, 1, '07', '0206', '0602', 'Kab. Pidie', '', 1, 3086, 393921, ''],
	[8, 1, '08', '0207', '0603', 'Kab. Aceh Utara', '', 1, 3236, 534411, ''],
	[9, 1, '09', '0208', '0611', 'Kab. Simeulue', '', 1, 2051, 82064, ''],
	[10, 1, '10', '0210', '0613', 'Kab. Aceh Singkil', '', 1, 2185, 109125, ''],
	[11, 1, '11', '0211', '0612', 'Kab. Bireuen', '', 1, 1901, 365973, ''],
	[12, 1, '12', '0212', '0617', 'Kab. Aceh Barat Daya', '', 1, 1490, 117727, ''],
	[13, 1, '13', '0214', '0618', 'Kab. Gayo Lues', '', 1, 5719, 80351, ''],
	[14, 1, '14', '0215', '0616', 'Kab. Aceh Jaya', '', 1, 3812, 86590, ''],
	[15, 1, '15', '0216', '0615', 'Kab. Nagan Raya', '', 1, 3363, 122948, ''],
	[16, 1, '16', '0217', '0614', 'Kab. Aceh Tamiang', '', 1, 1956, 264626, ''],
	[17, 1, '17', '0218', '0619', 'Kab. Bener Meriah', '', 1, 1454, 125075, ''],
	[18, 1, '18', '0219', '0620', 'Kab. Pidie Jaya', '', 1, 1073, 134794, 'UU No. 7/2007'],
	[19, 1, '71', '0220', '0661', 'Kota Banda Aceh', 1, 1, 61, 168551, ''],
	[20, 1, '72', '0221', '0660', 'Kota Sabang', 1, 1, 153, 35073, ''],
	[21, 1, '73', '0222', '0662', 'Kota Lhokseumawe', 1, 1, 181, 169507, ''],
	[22, 1, '74', '0223', '0663', 'Kota Langsa', 1, 1, 262, 177584, ''],
	[23, 1, '75', '0260', '0664', 'Kota Subulussalam', 1, 1, 1391, 74497, 'UU No. 8/2007'],
	[24, 2, '01', '0261', '0709', 'Kab. Tapanuli Tengah', '', 1, '', '', ''],
	[25, 2, '02', '0262', '0708', 'Kab. Tapanuli Utara', '', 1, '', '', ''],
	[26, 2, '03', '0263', '0710', 'Kab. Tapanuli Selatan', '', 1, '', '', ''],
	[27, 2, '04', '0265', '0711', 'Kab. Nias', '', 1, '', '', ''],
	[28, 2, '05', '0266', '0702', 'Kab. Langkat', '', 1, '', '', ''],
	[29, 2, '06', '0267', '0703', 'Kab. Karo', '', 1, '', '', ''],
	[30, 2, '07', '0268', '0701', 'Kab. Deli Serdang', '', 1, '', '', ''],
	[31, 2, '08', '0269', '0704', 'Kab. Simalungun', '', 1, '', '', ''],
	[32, 2, '09', '0301', '0706', 'Kab. Asahan', '', 1, '', '', ''],
	[33, 2, '10', '0302', '0707', 'Kab. Labuhan Batu', '', 1, '', '', ''],
	[34, 2, '11', '0303', '0705', 'Kab. Dairi', '', 1, '', '', ''],
	[35, 2, '12', '0304', '0716', 'Kab. Toba Samosir', '', 1, '', '', ''],
	[36, 2, '13', '0305', '0715', 'Kab. Mandailing Natal', '', 1, '', '', ''],
	[37, 2, '14', '0306', '0717', 'Kab. Nias Selatan', '', 1, '', '', ''],
	[38, 2, '15', '0307', '0718', 'Kab. Pakpak Bharat', '', 1, '', '', ''],
	[39, 2, '16', '0308', '0719', 'Kab. Humbang Hasundutan', '', 1, '', '', ''],
	[40, 2, '17', '0309', '0720', 'Kab. Samosir', '', 1, '', '', ''],
	[41, 2, '18', '0310', '0721', 'Kab. Serdang Bedagai', '', 1, '', '', ''],
	[42, 2, '19', '0311', '0722', 'Kab. Batubara', '', 1, '', '', 'UU No. 5/2007'],
	[43, 2, '20', '0312', '0724', 'Kab. Padang Lawas Utara', '', 1, '', '', 'UU No. 37/2007'],
	[44, 2, '21', '0313', '0723', 'Kab. Padang Lawas', '', 1, '', '', 'UU No. 38/2007'],
	[45, 2, '22', '0314', '0726', 'Kab. Labuhanbatu Selatan', '', 1, '', '', 'UU No. 22/2008'],
	[46, 2, '23', '0315', '0725', 'Kab. Labuhanbatu Utara', '', 1, '', '', 'UU No. 23/2008'],
	[47, 2, '24', '0316', '0728', 'Kab. Nias Utara', '', 1, '', '', 'UU No. 45/2008'],
	[48, 2, '25', '0317', '0727', 'Kab. Nias Barat', '', 1, '', '', 'UU No. 46/2008'],
	[49, 2, '71', '0318', '0760', 'Kota Medan', 1, 1, '', '', ''],
	[50, 2, '72', '0319', '0763', 'Kota Pematang Siantar', 1, 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[51, 2, '73', '0320', '0765', 'Kota Sibolga', 1, 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[52, 2, '74', '0321', '0764', 'Kota Tanjung Balai', 1, 1, '', '', ''],
	[53, 2, '75', '0322', '0761', 'Kota Binjai', 1, 1, '', '', ''],
	[54, 2, '76', '0323', '0762', 'Kota Tebing Tinggi', 1, 1, '', '', ''],
	[55, 2, '77', '0324', '0766', 'Kota Padang Sidempuan', 1, 1, '', '', ''],
	[56, 2, '78', '0325', '0767', 'Kota Gunung Sitoli', 1, 1, '', '', 'UU.No.46/2008'],
	[57, 3, '01', '0326', '0806', 'Kab. Pesisir Selatan', '', 1, '', '', ''],
	[58, 3, '02', '0327', '0804', 'Kab. Solok', '', 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[59, 3, '03', '0328', '0808', 'Kab. Sw.Lunto/Sijunjung', '', 1, '', '', 'Perubahan nama kabupaten sawahlunto/sijunjung PP.No. 25/2008'],
	[60, 3, '04', '0329', '0807', 'Kab. Tanah Datar', '', 1, '', '', ''],
	[61, 3, '05', '0360', '0805', 'Kab. Padang Pariaman', '', 1, '', '', 'Pemindahan ibukota PP.No. 79/2008'],
	[62, 3, '06', '0361', '0801', 'Kab. Agam', '', 1, '', '', ''],
	[63, 3, '07', '0362', '0803', 'Kab. Lima Puluh Kota', '', 1, '', '', ''],
	[64, 3, '08', '0363', '0802', 'Kab. Pasaman', '', 1, '', '', ''],
	[65, 3, '09', '0364', '0810', 'Kab. Kepulauan Mentawai', '', 1, '', '', ''],
	[66, 3, '10', '0365', '0812', 'Kab. Dharmasraya', '', 1, '', '', ''],
	[67, 3, '11', '0401', '0811', 'Kab. Solok Selatan', '', 1, '', '', ''],
	[68, 3, '12', '0402', '0813', 'Kab. Pasaman Barat', '', 1, '', '', ''],
	[69, 3, '71', '0403', '0861', 'Kota Padang', 1, 1, '', '', ''],
	[70, 3, '72', '0404', '0864', 'Kota Solok', 1, 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[71, 3, '73', '0460', '0863', 'Kota Sawahlunto', 1, 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[72, 3, '74', '0501', '0862', 'Kota Padang Panjang', 1, 1, '', '', ''],
	[73, 3, '75', '0502', '0860', 'Kota Bukittinggi', 1, 1, '', '', ''],
	[74, 3, '76', '0503', '0865', 'Kota Payakumbuh', 1, 1, '', '', ''],
	[75, 3, '77', '0504', '0866', 'Kota Pariaman', 1, 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[76, 4, '01', '0505', '0901', 'Kab. Kampar', '', 1, '', '', ''],
	[77, 4, '02', '0506', '0904', 'Kab. Indragiri Hulu', '', 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[78, 4, '03', '0507', '0902', 'Kab. Bengkalis', '', 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[79, 4, '04', '0508', '0905', 'Kab. Indragiri Hilir', '', 1, '', '', ''],
	[80, 4, '05', '0509', '0908', 'Kab. Pelalawan', '', 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[81, 4, '06', '0510', '0909', 'Kab. Rokan Hulu', '', 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[82, 4, '07', '0511', '0910', 'Kab. Rokan Hilir', '', 1, '', '', ''],
	[83, 4, '08', '0512', '0911', 'Kab. Siak', '', 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[84, 4, '09', '0513', '0914', 'Kab. Kuantan Singingi', '', 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[85, 4, '10', '0514', '0915', 'Kab. Kepulauan Meranti', '', 1, '', '', 'UU No. 12/2009'],
	[86, 4, '71', '0515', '0960', 'Kota Pekan Baru', 1, 1, '', '', ''],
	[87, 4, '72', '0516', '0962', 'Kota Dumai', 1, 1, '', '', ''],
	[88, 5, '01', '0517', '1005', 'Kab. Kerinci', '', 1, '', '', ''],
	[89, 5, '02', '0518', '1009', 'Kab. Merangin', '', 1, '', '', ''],
	[90, 5, '03', '0519', '', 'Kab. Sorolangun', '', 1, '', '', ''],
	[91, 5, '04', '0520', '1001', 'Kab. Batang Hari', '', 1, '', '', ''],
	[92, 5, '05', '0521', '1007', 'Kab. Muaro Jambi', '', 1, '', '', 'Data luas wil. Berdasarkan UU.No 54/1999'],
	[93, 5, '06', '0522', '1004', 'Kab. Tanjung Jabung Barat', '', 1, '', '', 'Data luas wil. Berdasarkan UU.No 54/1999'],
	[94, 5, '07', '0523', '1008', 'Kab. Tanjung Jabung Timur', '', 1, '', '', ''],
	[95, 5, '08', '0524', '1002', 'Kab. Bungo', '', 1, '', '', 'Data luas wil. Berdasarkan UU.No 54/1999'],
	[96, 5, '09', '0525', '1006', 'Kab. Tebo', '', 1, '', '', ''],
	[97, 5, '71', '0526', '1060', 'Kota Jambi', 1, 1, '', '', ''],
	[98, 5, '72', '0527', '1061', 'Kota Sungai Penuh', 1, 1, '', '', 'UU.No 25/2008'],
	[99, 6, '01', '0528', '1103', 'Kab. Ogan Komering Ulu', '', 1, '', '', 'Data luas wil. Berdasarkan UU.No 37/2003'],
	[100, 6, '02', '0529', '1102', 'Kab. Ogan Komering Ilir', '', 1, '', '', 'Data luas wil. Berdasarkan UU.No 37/2003'],
	[101, 6, '03', '0560', '1104', 'Kab. Muara Enim', '', 1, '', '', ''],
	[102, 6, '04', '0561', '1105', 'Kab. Lahat', '', 1, '', '', ''],
	[103, 6, '05', '0562', '1106', 'Kab. Musi Rawas', '', 1, '', '', ''],
	[104, 6, '06', '0563', '1101', 'Kab. Musi Banyuasin', '', 1, '', '', 'Data luas wil. Berdasarkan UU.No 6/2002'],
	[105, 6, '07', '0564', '1107', 'Kab. Banyuasin', '', 1, '', '', ''],
	[106, 6, '08', '0565', '1108', 'Kab. Oku Timur', '', 1, '', '', ''],
	[107, 6, '09', '0566', '1109', 'Kab. Oku Selatan', '', 1, '', '', ''],
	[108, 6, '10', '0567', '1110', 'Kab. Ogan Ilir', '', 1, '', '', ''],
	[109, 6, '11', '0568', '1111', 'Kab. Empat Lawang', '', 1, '', '', 'UU.No 1/2007'],
	[110, 6, '71', '0601', '1160', 'Kota Palembang', 1, 1, '', '', ''],
	[111, 6, '74', '0602', '1163', 'Kota Pagar Alam', 1, 1, '', '', ''],
	[112, 6, '77', '0603', '1162', 'Kota Lubuk Linggau', 1, 1, '', '', 'Data luas wil. Berdasarkan UU.No 7/2001'],
	[113, 6, '80', '0604', '1161', 'Kota Prabumulih', 1, 1, '', '', ''],
	[114, 7, '01', '0605', '2603', 'Kab. Bengkulu Selatan', '', 1, '', '', ''],
	[115, 7, '02', '0606', '2602', 'Kab. Rejang Lebong', '', 1, '', '', ''],
	[116, 7, '03', '0607', '2601', 'Kab. Bengkulu Utara', '', 1, '', '', ''],
	[117, 7, '04', '0608', '2607', 'Kab. Kaur', '', 1, '', '', ''],
	[118, 7, '05', '0611', '2608', 'Kab. Seluma', '', 1, '', '', ''],
	[119, 7, '06', '0612', '2604', 'Kab. Muko Muko', '', 1, '', '', ''],
	[120, 7, '07', '0613', '2606', 'Kab. Lebong', '', 1, '', '', ''],
	[121, 7, '08', '0614', '2605', 'Kab. Kepahiang', '', 1, '', '', ''],
	[122, 7, '09', '0615', '2609', 'Kab. Bengkulu Tengah', '', 1, '', '', 'UU.No. 24/2008'],
	[123, 7, '71', '0616', '2660', 'Kota Bengkulu', 1, 1, '', '', ''],
	[124, 8, '01', '0617', '1201', 'Kab. Lampung Selatan', '', 1, '', '', ''],
	[125, 8, '02', '0618', '1202', 'Kab. Lampung Tengah', '', 1, '', '', 'Data luas wil. Berdasarkan UU.No 12/1999'],
	[126, 8, '03', '0619', '1203', 'Kab. Lampung Utara', '', 1, '', '', 'Data luas wil. Berdasarkan UU.No 12/1999'],
	[127, 8, '04', '0622', '1204', 'Kab. Lampung Barat', '', 1, '', '', ''],
	[128, 8, '05', '0660', '1205', 'Kab. Tulang Bawang', '', 1, '', '', ''],
	[129, 8, '06', '0661', '1206', 'Kab. Tanggamus', '', 1, '', '', ''],
	[130, 8, '07', '0662', '1207', 'Kab. Lampung Timur', '', 1, '', '', ''],
	[131, 8, '08', '0663', '1208', 'Kab. Way Kanan', '', 1, '', '', 'Data luas wil. Berdasarkan UU.No 12/1999'],
	[132, 8, '09', '0664', '1209', 'Kab. Pesawaran', '', 1, '', '', 'UU No.33/2008'],
	[133, 8, '10', '0701', '1211', 'Kab. Pringsewu', '', 1, '', '', 'UU No.48/2008'],
	[134, 8, '11', '0702', '1210', 'Kab. Mesuji', '', 1, '', '', 'UU No.49/2008'],
	[135, 8, '12', '0703', '1212', 'Kab. Tulang Bawang Barat', '', 1, '', '', 'UU No.50/2008'],
	[136, 8, '71', '0704', '1260', 'Kota Bandar Lampung', 1, 1, '', '', ''],
	[137, 8, '72', '0705', '1261', 'Kota Metro', 1, 1, '', '', 'Data luas wil. Berdasarkan UU.No 12/1999'],
	[138, 9, '01', '0706', '2901', 'Kab. Bangka', '', 1, '', '', ''],
	[139, 9, '02', '0707', '2902', 'Kab. Belitung', '', 1, '', '', ''],
	[140, 9, '03', '0708', '2905', 'Kab. Bangka Selatan', '', 1, '', '', ''],
	[141, 9, '04', '0709', '2903', 'Kab. Bangka Tengah', '', 1, '', '', ''],
	[142, 9, '05', '0710', '2904', 'Kab. Bangka Barat', '', 1, '', '', ''],
	[143, 9, '06', '0711', '2906', 'Kab. Belitung Timur', '', 1, '', '', ''],
	[144, 9, '71', '0715', '2960', 'Kota Pangkal Pinang', 1, 1, '', '', ''],
	[145, 10, '01', '0716', '3101', 'Kab. Bintan', '', 1, '', '', 'Perubahan Nama Kab.Kepri No.5/2006'],
	[146, 10, '02', '0717', '3102', 'Kab. Karimun', '', 1, '', '', ''],
	[147, 10, '03', '0718', '3103', 'Kab. Natuna', '', 1, '', '', ''],
	[148, 10, '04', '0719', '3104', 'Kab. Lingga', '', 1, '', '', ''],
	[149, 10, '05', '0720', '3105', 'Kab. Kepulauan Anambas', '', 1, '', '', 'UU No.33/2008'],
	[150, 10, '71', '0721', '3160', 'Kota Batam', 1, 1, '', '', ''],
	[151, 10, '72', '0722', '3161', 'Kota Tanjung Pinang', 1, 1, '', '', ''],
	[152, 11, '01', '0723', '0101', 'Kab. Kep. Seribu', '', 1, '', '', 'UU No.29/2007'],
	[153, 11, '71', '', '0160', 'Kodya Jakarta Pusat', 1, 1, '', '', 'UU No.29/2007'],
	[154, 11, '72', '', '0161', 'Kodya Jakarta Utara', 1, 1, '', '', 'UU No.29/2007'],
	[155, 11, '73', '', '0162', 'Kodya Jakarta Barat', 1, 1, '', '', 'UU No.29/2007'],
	[156, 11, '74', '', '0163', 'Kodya Jakarta Selatan', 1, 1, '', '', 'UU No.29/2007'],
	[157, 11, '75', '', '0164', 'Kodya Jakarta Timur', 1, 1, '', '', 'UU No.29/2007'],
	[158, 12, '01', '0760', '0205', 'Kab. Bogor', '', 1, '', '', ''],
	[159, 12, '02', '0761', '0206', 'Kab. Sukabumi', '', 1, '', '', ''],
	[160, 12, '03', '0762', '0207', 'Kab. Cianjur', '', 1, '', '', ''],
	[161, 12, '04', '0763', '0208', 'Kab. Bandung', '', 1, '', '', ''],
	[162, 12, '05', '0764', '0211', 'Kab. Garut', '', 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[163, 12, '06', '0765', '0212', 'Kab. Tasikmalaya', '', 1, '', '', ''],
	[164, 12, '07', '0766', '0214', 'Kab. Ciamis', '', 1, '', '', ''],
	[165, 12, '08', '0767', '0215', 'Kab. Kuningan', '', 1, '', '', ''],
	[166, 12, '09', '0801', '0217', 'Kab. Cirebon', '', 1, '', '', ''],
	[167, 12, '10', '0802', '0216', 'Kab. Majalengka', '', 1, '', '', ''],
	[168, 12, '11', '0803', '0210', 'Kab. Sumedang', '', 1, '', '', ''],
	[169, 12, '12', '0804', '0218', 'Kab. Indramayu', '', 1, '', '', ''],
	[170, 12, '13', '0805', '0219', 'Kab. Subang', '', 1, '', '', ''],
	[171, 12, '14', '0806', '0220', 'Kab. Purwakarta', '', 1, '', '', ''],
	[172, 12, '15', '0807', '0221', 'Kab. Karawang', '', 1, '', '', ''],
	[173, 12, '16', '0808', '0222', 'Kab. Bekasi', '', 1, '', '', ''],
	[174, 12, '17', '0809', '0223', 'Kab. Bandung Barat', '', 1, '', '', 'UU No.12/2007'],
	[175, 12, '71', '0810', '0261', 'Kota. Bogor', 1, 1, '', '', ''],
	[176, 12, '72', '0811', '0262', 'Kota Sukabumi', 1, 1, '', '', ''],
	[177, 12, '73', '0812', '0260', 'Kota Bandung', 1, 1, '', '', ''],
	[178, 12, '74', '0860', '0263', 'Kota Cirebon', 1, 1, '', '', ''],
	[179, 12, '75', '0861', '0265', 'Kota Bekasi', 1, 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[180, 12, '76', '0862', '0266', 'Kota Depok', 1, 1, '', '', ''],
	[181, 12, '77', '0863', '0267', 'Kota Cimahi', 1, 1, '', '', ''],
	[182, 12, '78', '0864', '0268', 'Kota Tasikmalaya', 1, 1, '', '', ''],
	[183, 12, '79', '0865', '0269', 'Kota Banjar', 1, 1, '', '', ''],
	[184, 13, '01', '0866', '0301', 'Kab. Cilacap', '', 1, '', '', ''],
	[185, 13, '02', '0901', '0302', 'Kab. Banyumas', '', 1, '', '', ''],
	[186, 13, '03', '0902', '0303', 'Kab. Purbalingga', '', 1, '', '', ''],
	[187, 13, '04', '0904', '0304', 'Kab. Banjarnegara', '', 1, '', '', ''],
	[188, 13, '05', '0905', '0305', 'Kab. Kebumen', '', 1, '', '', ''],
	[189, 13, '06', '0908', '0306', 'Kab. Purworejo', '', 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[190, 13, '07', '0909', '0307', 'Kab. Wonosobo', '', 1, '', '', ''],
	[191, 13, '08', '0910', '0308', 'Kab. Magelang', '', 1, '', '', ''],
	[192, 13, '09', '0911', '0309', 'Kab. Boyolali', '', 1, '', '', ''],
	[193, 13, '10', '0914', '0310', 'Kab. Klaten', '', 1, '', '', ''],
	[194, 13, '11', '0915', '0311', 'Kab. Sukoharjo', '', 1, '', '', ''],
	[195, 13, '12', '0960', '0312', 'Kab. Wonogiri', '', 1, '', '', ''],
	[196, 13, '13', '0962', '0313', 'Kab. Karanganyar', '', 1, '', '', ''],
	[197, 13, '14', '1001', '0314', 'Kab. Sragen', '', 1, '', '', ''],
	[198, 13, '15', '1002', '0315', 'Kab. Grobogan', '', 1, '', '', ''],
	[199, 13, '16', '1003', '0316', 'Kab. Blora', '', 1, '', '', ''],
	[200, 13, '17', '1004', '0317', 'Kab. Rembang', '', 1, '', '', ''],
	[201, 13, '18', '1005', '0318', 'Kab. Pati', '', 1, '', '', ''],
	[202, 13, '19', '1006', '0319', 'Kab. Kudus', '', 1, '', '', ''],
	[203, 13, '20', '1007', '0320', 'Kab. Jepara', '', 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[204, 13, '21', '1008', '0321', 'Kab. Demak', '', 1, '', '', ''],
	[205, 13, '22', '1009', '0322', 'Kab. Semarang', '', 1, '', '', ''],
	[206, 13, '23', '1060', '0323', 'Kab. Temanggung', '', 1, '', '', ''],
	[207, 13, '24', '1061', '0324', 'Kab. Kendal', '', 1, '', '', ''],
	[208, 13, '25', '1101', '0325', 'Kab. Batang', '', 1, '', '', ''],
	[209, 13, '26', '1102', '0326', 'Kab. Pekalongan', '', 1, '', '', ''],
	[210, 13, '27', '1103', '0327', 'Kab. Pemalang', '', 1, '', '', ''],
	[211, 13, '28', '1104', '0328', 'Kab. Tegal', '', 1, '', '', ''],
	[212, 13, '29', '1105', '0329', 'Kab. Brebes', '', 1, '', '', ''],
	[213, 13, '71', '1106', '0360', 'Kota Magelang', 1, 1, '', '', ''],
	[214, 13, '72', '1107', '0361', 'Kota Surakarta', 1, 1, '', '', ''],
	[215, 13, '73', '1108', '0362', 'Kota Salatiga', 1, 1, '', '', ''],
	[216, 13, '74', '1109', '0363', 'Kota Semarang', 1, 1, '', '', ''],
	[217, 13, '75', '1110', '0364', 'Kota Pekalongan', 1, 1, '', '', ''],
	[218, 13, '76', '1111', '0365', 'Kota Tegal', 1, 1, '', '', ''],
	[220, 14, '01', '1160', '0404', 'Kab. Kulon Progo', '', 1, '', '', ''],
	[221, 14, '02', '1161', '0401', 'Kab. Bantul', '', 1, '', '', ''],
	[222, 14, '03', '1162', '0403', 'Kab. Gunung Kidul', '', 1, '', '', ''],
	[223, 14, '04', '1163', '0402', 'Kab. Sleman', '', 1, '', '', ''],
	[224, 14, '71', '1201', '0460', 'Kota Yogyakarta', 1, 1, '', '', ''],
	[225, 15, '01', '1205', '0512', 'Kab. Pacitan', '', 1, '', '', ''],
	[226, 15, '02', '1206', '0511', 'Kab. Ponorogo', '', 1, '', '', ''],
	[227, 15, '03', '1208', '0517', 'Kab. Trenggalek', '', 1, '', '', ''],
	[228, 15, '04', '1209', '0516', 'Kab. Tulungagung', '', 1, '', '', ''],
	[229, 15, '05', '1211', '0515', 'Kab. Blitar', '', 1, '', '', 'Pemindahan Ibukota PP No. 3/2010'],
	[230, 15, '06', '1212', '0513', 'Kab. Kediri', '', 1, '', '', ''],
	[231, 15, '07', '1261', '0518', 'Kab. Malang', '', 1, '', '', 'Pemindahan Ibukota PP No. 18/2010'],
	[232, 15, '08', '1301', '0521', 'Kab. Lumajang', '', 1, '', '', ''],
	[233, 15, '09', '1302', '0524', 'Kab. Jember', '', 1, '', '', ''],
	[234, 15, '10', '1260', '0525', 'Kab. Banyuwangi', '', 1, '', '', ''],
	[235, 15, '11', '1204', '0522', 'Kab. Bondowoso', '', 1, '', '', ''],
	[236, 15, '12', '1202', '0523', 'Kab. Situbondo', '', 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[237, 15, '13', '1207', '0520', 'Kab. Probolinggo', '', 1, '', '', 'Pemindahan Ibukota PP No. 2/2010'],
	[238, 15, '14', '1203', '0519', 'Kab. Pasuruan', '', 1, '', '', ''],
	[239, 15, '15', '1210', '0502', 'Kab. Sidoarjo', '', 1, '', '', ''],
	[240, 15, '16', '1303', '0503', 'Kab. Mojokerto', '', 1, '', '', ''],
	[241, 15, '17', '1304', '0504', 'Kab. Jombang', '', 1, '', '', ''],
	[242, 15, '18', '1305', '0514', 'Kab. Nganjuk', '', 1, '', '', ''],
	[243, 15, '19', '1306', '0508', 'Kab. Madiun', '', 1, '', '', ''],
	[244, 15, '20', '1307', '0510', 'Kab. Magetan', '', 1, '', '', ''],
	[245, 15, '21', '1308', '0509', 'Kab. Ngawi', '', 1, '', '', ''],
	[246, 15, '22', '1309', '0505', 'Kab. Bojonegoro', '', 1, '', '', ''],
	[247, 15, '23', '1310', '0506', 'Kab. Tuban', '', 1, '', '', ''],
	[248, 15, '24', '1311', '0507', 'Kab. Lamongan', '', 1, '', '', ''],
	[249, 15, '25', '1312', '0501', 'Kab. Gresik', '', 1, '', '', ''],
	[250, 15, '26', '1360', '0529', 'Kab. Bangkalan', '', 1, '', '', ''],
	[251, 15, '27', '1361', '0527', 'Kab. Sampang', '', 1, '', '', ''],
	[252, 15, '28', '1401', '0526', 'Kab. Pamekasan', '', 1, '', '', ''],
	[253, 15, '29', '1402', '0528', 'Kab. Sumenep', '', 1, '', '', ''],
	[254, 15, '71', '1403', '0563', 'Kota Kediri', 1, 1, '', '', ''],
	[255, 15, '72', '1404', '0565', 'Kota Blitar', 1, 1, '', '', ''],
	[256, 15, '73', '1405', '0561', 'Kota Malang', 1, 1, '', '', ''],
	[257, 15, '74', '1406', '0567', 'Kota Probolinggo', 1, 1, '', '', ''],
	[258, 15, '75', '1407', '0566', 'Kota Pasuruan', 1, 1, '', '', ''],
	[259, 15, '76', '1408', '0564', 'Kota Mojokerto', 1, 1, '', '', ''],
	[260, 15, '77', '1409', '0562', 'Kota Madiun', 1, 1, '', '', ''],
	[261, 15, '78', '1410', '0560', 'Kota Surabaya', 1, 1, '', '', ''],
	[262, 15, '79', '1411', '0568', 'Kota Batu', 1, 1, '', '', ''],
	[263, 16, '01', '1412', '2801', 'Kab. Pandeglang', '', 1, '', '', ''],
	[264, 16, '02', '1413', '2802', 'Kab. Lebak', '', 1, '', '', ''],
	[265, 16, '03', '1460', '2803', 'Kab. Tangerang', '', 1, '', '', ''],
	[266, 16, '04', '1501', '2804', 'Kab. Serang', '', 1, '', '', ''],
	[267, 16, '71', '1502', '2861', 'Kota Tangerang', 1, 1, '', '', ''],
	[268, 16, '72', '1503', '2860', 'Kota Cilegon', 1, 1, '', '', ''],
	[269, 16, '73', '1504', '2862', 'Kota Serang', 1, 1, '', '', 'UU No. 32/2007'],
	[270, 16, '74', '1505', '2863', 'Kota Tangerang Selatan', 1, 1, '', '', 'UU No. 51/2008'],
	[271, 17, '01', '1506', '2202', 'Kab. Jembrana', '', 1, '', '', ''],
	[272, 17, '02', '1507', '2203', 'Kab. Tabanan', '', 1, '', '', 'Pemindahan Ibukota PP No. 67/2009'],
	[273, 17, '03', '1508', '2204', 'Kab. Badung', '', 1, '', '', ''],
	[274, 17, '04', '1509', '2205', 'Kab. Gianyar', '', 1, '', '', ''],
	[275, 17, '05', '1510', '2206', 'Kab. Klungkung', '', 1, '', '', ''],
	[276, 17, '06', '1511', '2207', 'Kab. Bangli', '', 1, '', '', ''],
	[277, 17, '07', '1560', '2208', 'Kab. Karangasem', '', 1, '', '', ''],
	[278, 17, '08', '1561', '2201', 'Kab. Buleleng', '', 1, '', '', ''],
	[279, 17, '71', '1601', '2260', 'Kota Denpasar', 1, 1, '', '', ''],
	[280, 18, '01', '1602', '2301', 'Kab. Lombok Barat', '', 1, '', '', ''],
	[281, 18, '02', '1603', '2302', 'Kab. Lombok Tengah', '', 1, '', '', ''],
	[282, 18, '03', '1604', '2303', 'Kab. Lombok Timur', '', 1, '', '', ''],
	[283, 18, '04', '1607', '2304', 'Kab. Sumbawa', '', 1, '', '', ''],
	[284, 18, '05', '1608', '2305', 'Kab. Dompu', '', 1, '', '', ''],
	[285, 18, '06', '1609', '2306', 'Kab. Bima', '', 1, '', '', 'Pemindahan ibukota PP No. 31/2008'],
	[286, 18, '07', '1610', '2307', 'Kab. Sumbawa Barat', '', 1, '', '', ''],
	[287, 18, '08', '1611', '2308', 'Kab. Lombok Utara', '', 1, '', '', 'UU No. 26/2008'],
	[288, 18, '71', '1614', '2360', 'Kota Mataram', 1, 1, '', '', ''],
	[289, 18, '72', '1660', '2361', 'Kota Bima', 1, 1, '', '', ''],
	[290, 19, '01', '1661', '2401', 'Kab. Kupang', '', 1, '', '', ''],
	[291, 19, '02', '1662', '2403', 'Kab. Timor Tengah Selatan', '', 1, '', '', ''],
	[292, 19, '03', '1663', '2404', 'Kab. Timor Tengah Utara', '', 1, '', '', ''],
	[293, 19, '04', '1701', '2405', 'Kab. Belu', '', 1, '', '', ''],
	[294, 19, '05', '1702', '2406', 'Kab. Alor', '', 1, '', '', ''],
	[295, 19, '06', '1703', '2407', 'Kab. Flores Timur', '', 1, '', '', ''],
	[296, 19, '07', '1704', '2408', 'Kab. Sikka', '', 1, '', '', ''],
	[297, 19, '08', '1705', '2409', 'Kab. Ende', '', 1, '', '', ''],
	[298, 19, '09', '1706', '2410', 'Kab. Ngada', '', 1, '', '', ''],
	[299, 19, '10', '1707', '2411', 'Kab. Manggarai', '', 1, '', '', ''],
	[300, 19, '11', '1708', '2412', 'Kab. Sumba Timur', '', 1, '', '', ''],
	[301, 19, '12', '1709', '2413', 'Kab. Sumba Barat', '', 1, '', '', ''],
	[302, 19, '13', '1710', '2414', 'Kab. Lembata', '', 1, '', '', ''],
	[303, 19, '14', '1711', '2415', 'Kab. Rote Ndao', '', 1, '', '', ''],
	[304, 19, '15', '1712', '2416', 'Kab. Manggarai Barat', '', 1, '', '', ''],
	[305, 19, '16', '1760', '2417', 'Kab. Nagekeo', '', 1, '', '', 'UU No. 2/2007'],
	[306, 19, '17', '1761', '2418', 'Kab. Sumba Tengah', '', 1, '', '', 'UU No. 3/2007'],
	[307, 19, '18', '1762', '2419', 'Kab. Sumba Barat Daya', '', 1, '', '', 'UU No. 16/2007'],
	[308, 19, '19', '1763', '2420', 'Kab. Manggarai Timur', '', 1, '', '', 'UU No. 36/2007'],
	[309, 19, '20', '1801', '2421', 'Kab. Sabu Raijua', '', 1, '', '', 'UU No. 51/2007'],
	[310, 19, '71', '1802', '2460', 'Kota Kupang', 1, 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[311, 20, '01', '1803', '1301', 'Kab. Sambas', '', 1, '', '', ''],
	[312, 20, '02', '1804', '1302', 'Kab. Pontianak', '', 1, '', '', ''],
	[313, 20, '03', '1805', '1303', 'Kab. Sanggau', '', 1, '', '', ''],
	[314, 20, '04', '1806', '1306', 'Kab. Ketapang', '', 1, '', '', ''],
	[315, 20, '05', '1807', '1304', 'Kab. Sintang', '', 1, '', '', ''],
	[316, 20, '06', '1808', '1305', 'Kab. Kapuas Hulu', '', 1, '', '', ''],
	[317, 20, '07', '1809', '1308', 'Kab. Bengkayang', '', 1, '', '', ''],
	[318, 20, '08', '1810', '1309', 'Kab. Landak', '', 1, '', '', ''],
	[319, 20, '09', '1860', '1310', 'Kab. Sekadau', '', 1, '', '', ''],
	[320, 20, '10', '1901', '1311', 'Kab. Melawai', '', 1, '', '', 'Luas wil koreksi dari Gubernur Surat No. 140/Pem-D Juni 2006'],
	[321, 20, '11', '1902', '1312', 'Kab. Kayong Utara', '', 1, '', '', 'UU No. 6/2007'],
	[322, 20, '12', '1903', '', 'Kab. Kubu Raya', '', 1, '', '', 'UU No. 35/2007'],
	[323, 20, '71', '1904', '1360', 'Kota Pontianak', 1, 1, '', '', ''],
	[324, 20, '72', '1905', '1361', 'Kota Singkawang', 1, 1, '', '', ''],
	[325, 21, '01', '1906', '1405', 'Kab. Kotawaringin Barat', '', 1, '', '', ''],
	[326, 21, '02', '1907', '1404', 'Kab. Kotawaringin Timur', '', 1, '', '', ''],
	[327, 21, '03', '1908', '1401', 'Kab. Kapuas', '', 1, '', '', ''],
	[328, 21, '04', '1909', '1402', 'Kab. Barito Selatan', '', 1, '', '', ''],
	[329, 21, '05', '1910', '1403', 'Kab. Barito Utara', '', 1, '', '', ''],
	[330, 21, '06', '1911', '1406', 'Kab. Katingan', '', 1, '', '', ''],
	[331, 21, '07', '1912', '1407', 'Kab. Seruyan', '', 1, '', '', ''],
	[332, 21, '08', '1913', '1408', 'Kab. Sukamara', '', 1, '', '', ''],
	[333, 21, '09', '1914', '1409', 'Kab. Lamandau', '', 1, '', '', ''],
	[334, 21, '10', '1915', '1410', 'Kab. Gunung Mas', '', 1, '', '', ''],
	[335, 21, '11', '1916', '1411', 'Kab. Pulang Pisau', '', 1, '', '', ''],
	[336, 21, '12', '1917', '1412', 'Kab. Murung Raya', '', 1, '', '', ''],
	[337, 21, '13', '1918', '1413', 'Kab. Barito Timur', '', 1, '', '', ''],
	[338, 21, '71', '1924', '1460', 'Kota Palangkaraya', 1, 1, '', '', ''],
	[339, 22, '01', '1926', '1502', 'Kab. Tanah Laut', '', 1, '', '', ''],
	[340, 22, '02', '1927', '1509', 'Kab. Kotabaru', '', 1, '', '', ''],
	[341, 22, '03', '1960', '1501', 'Kab. Banjar', '', 1, '', '', ''],
	[342, 22, '04', '1961', '1503', 'Kab. Barito Kuala', '', 1, '', '', ''],
	[343, 22, '05', '1962', '1504', 'Kab. Tapin', '', 1, '', '', ''],
	[344, 22, '06', '2001', '1505', 'Kab. Hulu Sungai Selatan', '', 1, '', '', ''],
	[345, 22, '07', '2002', '1506', 'Kab. Hulu Sungai Tengah', '', 1, '', '', ''],
	[346, 22, '08', '2003', '1507', 'Kab. Hulu Sungai Utara', '', 1, '', '', ''],
	[347, 22, '09', '2004', '1508', 'Kab. Tabalong', '', 1, '', '', ''],
	[348, 22, '10', '2005', '1511', 'Kab. Tanah Bumbu', '', 1, '', '', ''],
	[349, 22, '11', '2006', '1510', 'Kab. Balangan', '', 1, '', '', ''],
	[350, 22, '71', '2007', '1560', 'Kota Banjarmasin', 1, 1, '', '', ''],
	[351, 22, '72', '2008', '1561', 'Kota Banjarbaru', 1, 1, '', '', ''],
	[352, 23, '01', '2009', '1601', 'Kab. Paser', '', 1, '', '', 'Perubahan Nama Kab. Pasir menjadi Paser, PP No. 49/2007'],
	[353, 23, '02', '2010', '1602', 'Kab. Kutai Kertanegara', '', 1, '', '', ''],
	[354, 23, '03', '2060', '1603', 'Kab. Berau', '', 1, '', '', ''],
	[355, 23, '04', '2061', '1604', 'Kab. Bulungan', '', 1, '', '', ''],
	[356, 23, '05', '2101', '1608', 'Kab. Nunukan', '', 1, '', '', ''],
	[357, 23, '06', '2102', '1607', 'Kab. Malinau', '', 1, '', '', ''],
	[358, 23, '07', '2103', '1609', 'Kab. Kutai Barat', '', 1, '', '', ''],
	[359, 23, '08', '2104', '1610', 'Kab. Kutai Timur', '', 1, '', '', ''],
	[360, 23, '09', '2105', '1611', 'Kab. Penajam Paser Utara', '', 1, '', '', ''],
	[361, 23, '10', '2106', '1612', 'Kab. Tana Tidung', '', 1, '', '', 'UU No. 34/2007'],
	[362, 23, '70', '2107', '1661', 'Kota Balikpapan', 1, 1, '', '', ''],
	[363, 23, '72', '2109', '1660', 'Kota Samarinda', 1, 1, '', '', ''],
	[364, 23, '73', '2108', '1662', 'Kota Tarakan', 1, 1, '', '', ''],
	[365, 23, '74', '2160', '1663', 'Kota Bontang', 1, 1, '', '', ''],
	[366, 24, '01', '2161', '1701', 'Kab. Bolaang Mongondow', '', 1, '', '', ''],
	[367, 24, '02', '2200', '1702', 'Kab. Minahasa', '', 1, '', '', ''],
	[368, 24, '03', '2201', '1703', 'Kab. Kepulauan Sangihe', '', 1, '', '', ''],
	[369, 24, '04', '2202', '1704', 'Kab. Kepulauan Talaud', '', 1, '', '', ''],
	[370, 24, '05', '2203', '1705', 'Kab. Minahasa Selatan', '', 1, '', '', ''],
	[371, 24, '06', '2204', '1706', 'Kab. Minahasa Utara', '', 1, '', '', ''],
	[372, 24, '07', '2205', '1710', 'Kab. Minahasa Tenggara', '', 1, '', '', 'UU No. 9/2007'],
	[373, 24, '08', '2206', '1708', 'Kab. Bolmong Utara', '', 1, '', '', 'UU No. 10/2007'],
	[374, 24, '09', '2207', '1709', 'Kab. Kep. Sitaro', '', 1, '', '', 'UU No. 15/2007'],
	[375, 24, '10', '2208', '1711', 'Kab. Bolmong Timur', '', 1, '', '', 'UU No. 29/2008'],
	[376, 24, '11', '2209', '1712', 'Kab. Bolmong Selatan', '', 1, '', '', 'UU No. 30/2008'],
	[377, 24, '71', '223', '1760', 'Kota Manado', 1, 1, '', '', ''],
	[378, 24, '72', '2301', '1761', 'Kota Bitung', 1, 1, '', '', ''],
	[379, 24, '73', '2302', '1762', 'Kota Tomohon', 1, 1, '', '', ''],
	[380, 24, '74', '2303', '1763', 'Kota Kotamobagu', 1, 1, '', '', 'UU No. 4/2007'],
	[381, 25, '01', '2304', '1804', 'Kab. Banggai', '', 1, '', '', 'Data luas Wil berdasarkan UU No. 51/1999'],
	[382, 25, '02', '2305', '1803', 'Kab. Poso', '', 1, '', '', ''],
	[383, 25, '03', '2306', '1802', 'Kab. Donggala', '', 1, '', '', ''],
	[384, 25, '04', '2307', '1806', 'Kab. Toli Toli', '', 1, '', '', ''],
	[385, 25, '05', '2308', '1805', 'Kab. Buol', '', 1, '', '', ''],
	[386, 25, '06', '2360', '1807', 'Kab. Morowali', '', 1, '', '', ''],
	[387, 25, '07', '2361', '1801', 'Kab. Banggai Kepulauan', '', 1, '', '', ''],
	[388, 25, '08', '2401', '1808', 'Kab. Parigi Moutong', '', 1, '', '', ''],
	[389, 25, '09', '2403', '1809', 'Kab. Tojo Una Una', '', 1, '', '', ''],
	[390, 25, '10', '2404', '1810', 'Kab. Sigi', '', 1, '', '', 'UU No. 27/2008'],
	[391, 25, '70', '2405', '1860', 'Kota Palu', 1, 1, '', '', ''],
	[392, 26, '01', '2406', '1913', 'Kab. Kepulauan Selayar', '', 1, '', '', 'Perubahan nama Kab PP.No. 59/2008'],
	[393, 26, '02', '2407', '1911', 'Kab. Bulukumba', '', 1, '', '', ''],
	[394, 26, '03', '2408', '1910', 'Kab. Bantaeng', '', 1, '', '', ''],
	[395, 26, '04', '2409', '1905', 'Kab. Jeneponto', '', 1, '', '', ''],
	[396, 26, '05', '2410', '1904', 'Kab. Takalar', '', 1, '', '', ''],
	[397, 26, '06', '2411', '1903', 'Kab. Gowa', '', 1, '', '', ''],
	[398, 26, '07', '2412', '1912', 'Kab. Sinjai', '', 1, '', '', ''],
	[399, 26, '08', '2413', '1907', 'Kab. Bone', '', 1, '', '', ''],
	[400, 26, '09', '2414', '1901', 'Kab. Maros', '', 1, '', '', ''],
	[401, 26, '10', '2415', '1902', 'Kab. Pangkajene Kep.', '', 1, '', '', ''],
	[402, 26, '11', '2416', '1906', 'Kab. Barru', '', 1, '', '', ''],
	[403, 26, '12', '2417', '1909', 'Kab. Soppeng', '', 1, '', '', ''],
	[404, 26, '13', '2418', '1908', 'Kab. Wajo', '', 1, '', '', ''],
	[405, 26, '14', '2419', '1915', 'Kab. Sidenreng Rappang', '', 1, '', '', ''],
	[406, 26, '15', '2420', '1914', 'Kab. Pinrang', '', 1, '', '', ''],
	[407, 26, '16', '2421', '1916', 'Kab. Enrekang', '', 1, '', '', ''],
	[408, 26, '17', '2460', '1917', 'Kab. Luwu', '', 1, '', '', ''],
	[409, 26, '18', '2501', '1918', 'Kab. Tana Toraja', '', 1, '', '', ''],
	[410, 29, '', '2502', '3303', 'Kab. Polewali Mamasa', '', '', '', '', 'Menjadi wil.Prov.Sulbar UU No. 26/2004'],
	[411, 29, '05', '2503', '3305', 'Kab. Majene', '', 1, '', '', 'Menjadi wil.Prov.Sulbar UU No. 26/2004'],
	[412, 29, '02', '2507', '3301', 'Kab. Mamuju', '', 1, '', '', 'Menjadi wil.Prov.Sulbar UU No. 26/2004'],
	[413, 26, '22', '2508', '1924', 'Kab. Luwu Utara', '', 1, '', '', ''],
	[414, 29, '03', '2509', '3304', 'Kab. Mamasa', '', 1, '', '', 'Menjadi wil.Prov.Sulbar UU No. 26/2004'],
	[415, 26, '23', '2510', '1926', 'Kab. Luwu Timur', '', 1, '', '', ''],
	[416, 29, '01', '2511', '3302', 'Kab. Mamuju Utara', '', 1, '', '', 'Menjadi wil.Prov.Sulbar UU No. 26/2004'],
	[417, 26, '26', '2512', '1927', 'Kab. Toraja Utara', '', 1, '', '', 'UU No. 28/2008'],
	[418, 26, '70', '2513', '1960', 'Kota Makasar', 1, 1, '', '', ''],
	[419, 26, '72', '2514', '1961', 'Kota Pare Pare', 1, 1, '', '', ''],
	[420, 26, '73', '2515', '1962', 'Kota Palopo', 1, 1, '', '', 'Data luas wil.berdasarkan GIS Pusat,2007'],
	[421, 27, '01', '2516', '2004', 'Kab. Kolaka', '', 1, '', '', ''],
	[422, 27, '02', '2517', '2001', 'Kab. Konawe', '', 1, '', '', ''],
	[423, 27, '03', '2518', '2002', 'Kab. Muna', '', 1, '', '', ''],
	[424, 27, '04', '2519', '2003', 'Kab. Buton', '', 1, '', '', ''],
	[425, 27, '05', '2520', '2005', 'Kab. Konawe Selatan', '', 1, '', '', ''],
	[426, 27, '06', '2522', '2007', 'Kab. Bombana', '', 1, '', '', ''],
	[427, 27, '07', '2523', '2006', 'Kab. Wakatobi', '', 1, '', '', ''],
	[428, 27, '08', '2524', '2008', 'Kab. Kolaka Utara', '', 1, '', '', ''],
	[429, 27, '09', '2525', '2009', 'Kab. Konawe Utara', '', 1, '', '', 'UU No. 13/2007'],
	[430, 27, '10', '2526', '2010', 'Kab. Buton Utara', '', 1, '', '', 'UU No. 14/2007'],
	[431, 27, '70', '2527', '2060', 'Kota Kendari', 1, 1, '', '', ''],
	[432, 27, '72', '2528', '2061', 'Kota Bau Bau', 1, 1, '', '', ''],
	[433, 28, '01', '2530', '3002', 'Kab. Gorontalo', '', 1, '', '', ''],
	[434, 28, '02', '2534', '3001', 'Kab. Boalemo', '', 1, '', '', 'Data luas wil.berdasarkan GIS Pusat,2007'],
	[435, 28, '03', '2535', '3004', 'Kab. Bone Bolango', '', 1, '', '', ''],
	[436, 28, '04', '2536', '3003', 'Kab. Pahuwato', '', 1, '', '', ''],
	[437, 28, '05', '2560', '3005', 'Kab. Gorontalo Utara', '', 1, '', '', 'UU No. 11/2007'],
	[438, 28, '71', '2601', '3060', 'Kota Gorontalo', 1, 1, '', '', 'Data luas wil.berdasarkan GIS Pusat,2007'],
	[440, 29, '04', '', '', 'Kab. Polewali Mandar', '', 1, '', '', 'Perubahan Nama Kab. Polewali Mamasa, PP No. 74/2005'],
	[441, 29, '', '2603', '3305', 'Kab. Majene', '', '', '', '', ''],
	[442, 30, '01', '2604', '2101', 'Kab. Maluku Tengah', '', 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[443, 30, '02', '2605', '2102', 'Kab. Maluku Tenggara', '', 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[444, 30, '03', '2606', '2104', 'Kab. Maluku Tenggara Barat', '', 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[445, 30, '04', '2607', '2103', 'Kab. Buru', '', 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[446, 30, '05', '2608', '2106', 'Kab. Seram Bagian Timur', '', 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[447, 30, '06', '2609', '2105', 'Kab. Seram Bagian Barat', '', 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[448, 30, '07', '2660', '2107', 'Kab. Kepulauan Aru', '', 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[449, 30, '08', '2701', '2109', 'Kab. Maluku Barat Daya', '', 1, '', '', 'UU No. 31/2008'],
	[450, 30, '09', '2702', '2108', 'Kab. Buru Selatan', '', 1, '', '', 'UU No. 32/2009'],
	[451, 30, '71', '2703', '2160', 'Kota Ambon', 1, 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[452, 30, '72', '2704', '2161', 'Kota Tual', 1, 1, '', '', 'UU No. 31/2007'],
	[453, 31, '01', '2705', '2703', 'Kab. Halmahera Barat', '', 1, '', '', ''],
	[454, 31, '02', '2706', '2702', 'Kab. Halmahera Tengah', '', 1, '', '', ''],
	[455, 31, '03', '2707', '2704', 'Kab. Halmahera Utara', '', 1, '', '', ''],
	[456, 31, '04', '2760', '2705', 'Kab. Halmahera Selatan', '', 1, '', '', ''],
	[457, 31, '05', '2761', '2707', 'Kab. Kepulauan Sula', '', 1, '', '', ''],
	[458, 31, '06', '2801', '2706', 'Kab. Halmahera Timur', '', 1, '', '', ''],
	[459, 31, '07', '2802', '2708', 'Kab. Pulau Morotai', '', 1, '', '', 'UU No. 53/2008'],
	[460, 31, '71', '2803', '2760', 'Kota Ternate', 1, 1, '', '', ''],
	[461, 31, '72', '2804', '2761', 'Kota Tidore Kepulauan', 1, 1, '', '', ''],
	[462, 32, '01', '2860', '2507', 'Kab. Merauke', '', 1, '', '', ''],
	[463, 32, '02', '2861', '2508', 'Kab. Jayawijaya', '', 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[464, 32, '03', '2862', '', 'Kab. Jayapura', '', 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[465, 32, '04', '2863', '2509', 'Kab. Nabire', '', 1, '', '', ''],
	[466, 32, '05', '2901', '2503', 'Kab. Kepulauan Yapen', '', 1, '', '', 'Perubahan nama kab.Yapen Waropen PP No.40/2008'],
	[467, 32, '06', '2902', '2502', 'Kab. Biak Numfor', '', 1, '', '', ''],
	[468, 32, '07', '2903', '2511', 'Kab. Puncak Jaya', '', 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[469, 32, '08', '2904', '2510', 'Kab. Paniai', '', 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[470, 32, '09', '2905', '2512', 'Kab. Mimika', '', 1, '', '', ''],
	[471, 32, '10', '2906', '2519', 'Kab. Sarmi', '', 1, '', '', 'Termasuk Jml.Penduduk Kab. Mamberamo Raya (kab. Pemekaran),data luas wil. Berdasarkan UU No. 19/2007'],
	[472, 32, '11', '2960', '2520', 'Kab. Keerom', '', 1, '', '', ''],
	[473, 32, '12', '3001', '2517', 'Kab. Pegunungan Bintang', '', 1, '', '', ''],
	[474, 32, '13', '3002', '2516', 'Kab. Yahukimo', '', 1, '', '', ''],
	[475, 32, '14', '3003', '2518', 'Kab. Tolikara', '', 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[476, 32, '15', '3004', '2526', 'Kab. Waropen', '', 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[477, 32, '16', '3005', '2513', 'Kab. Boven Digoel', '', 1, '', '', ''],
	[478, 32, '17', '3060', '2514', 'Kab. Mappi', '', 1, '', '', ''],
	[479, 32, '18', '3101', '2515', 'Kab. Asmat', '', 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[480, 32, '19', '3102', '2527', 'Kab. Supiori', '', 1, '', '', 'Data luas wil. Berdasarkan GIS Pusat, 2007'],
	[481, 32, '20', '3103', '2528', 'Kab. Mamberamo Raya', '', 1, '', '', 'UU No. 19/2007'],
	[482, 32, '21', '3104', '2529', 'Kab. Mamberamo Tengah', '', 1, '', '', 'UU No. 3/2008'],
	[483, 32, '22', '3105', '2531', 'Kab. Yalimo', '', 1, '', '', 'UU No. 4/2008'],
	[484, 32, '23', '3160', '2534', 'Kab. Lanny Jaya', '', 1, '', '', 'UU No. 5/2008'],
	[485, 32, '24', '3161', '2530', 'Kab. Nduga', '', 1, '', '', 'UU No. 6/2008'],
	[486, 32, '25', '3201', '2532', 'Kab. Puncak', '', 1, '', '', 'UU No. 7/2008'],
	[487, 32, '26', '3202', '2533', 'Kab. Dogiyai', '', 1, '', '', 'UU No. 8/2008'],
	[488, 32, '27', '3203', '2536', 'Kab. Intan Jaya', '', 1, '', '', 'UU No. 55/2008'],
	[489, 32, '28', '3204', '2535', 'Kab. Deiyai', '', 1, '', '', 'UU No. 55/2008'],
	[490, 32, '71', '3205', '2560', 'Kota Jayapura', 1, 1, '', '', ''],
	[491, 33, '01', '3206', '3202', 'Kab. Sorong', '', 1, '', '', ''],
	[492, 33, '02', '3207', '3203', 'Kab. Manokwari', '', 1, '', '', ''],
	[493, 33, '03', '3208', '3201', 'Kab. Fak Fak', '', 1, '', '', ''],
	[494, 33, '04', '3209', '3205', 'Kab. Sorong Selatan', '', 1, '', '', 'Termasuk Data Jumlah Penduduk Kab. Maybrat'],
	[495, 33, '05', '3210', '3206', 'Kab. Raja Ampat', '', 1, '', '', ''],
	[496, 33, '06', '3260', '3207', 'Kab. Teluk Bentuni', '', 1, '', '', ''],
	[497, 33, '07', '3301', '3208', 'Kab. Teluk Wondama', '', 1, '', '', ''],
	[498, 33, '08', '3302', '3204', 'Kab. Kaimana', '', 1, '', '', ''],
	[499, 33, '09', '3303', '3209', 'Kab. Tambrauw', '', 1, '', '', 'UU No. 56/2008'],
	[500, 33, '10', '3304', '3210', 'Kab. Maybrat', '', 1, '', '', 'UU No. 13/2009, Data Jumlah Penduduk masihTergabung di kab. Sorong Selatan'],
	[501, 33, '71', '3305', '3260', 'Kota Sorong', 1, 1, '', '', '']
];

//simptk.KabupatenKotaStore = Ext.extend(Ext.data.ArrayStore, {
simptk.KabupatenKotaStore = new Ext.data.ArrayStore({
	autoLoad: true,
	fields: simptk.RecordSetKabupatenKota,
	data : simptk.RecordKabupatenKota			
});

simptk.KabupatenKotaCombo = Ext.extend(Ext.form.ComboBox, {
	
	fieldLabel: 'Kab/Kota',
	emptyText: 'Pilih Kabupaten/Kota...',
	anchor: '100%',
	
	filter: function(propId) {
		
		this.store.filterBy(function(record, id){						
			if (record.data.PropinsiId == propId) {
				//console.log(propId + '|' + record.data.PropinsiId + "| Tampil");
				return true;
			} else  {
				//console.log(propId + '|' + record.data.PropinsiId + "| Tidak Tampil");
				return false;
			}
		});	

	},
	
	constructor: function(config){
		Ext.apply(this, config);
		
		Ext.apply(this, {			
			//store: lookupStoreKabupatenKota,
			store: simptk.KabupatenKotaStore,
			displayField: 'Nama',
			valueField: 'KabupatenKotaId',
			fieldLabel: 'Kab/Kota',
			typeAhead: true,
			mode: 'local',
			triggerAction: 'all',			
			selectOnFocus: true,			
			maxHeight: 200,
			lastQuery: ''
		});

		simptk.KabupatenKotaCombo.superclass.constructor.call(this);
		
		if (simptk.kabkota_selected) {
			var kabkotaId = simptk.kabkota_selected.get('KabupatenKotaId');
			this.setValue(kabkotaId, true);
		}
		
		if (simptk.session_kabupaten_kota_id) {
			this.disable();
		} 
	}
});

Ext.reg('kabupatenkotacombo', simptk.KabupatenKotaCombo); 


simptk.RecordSetKelulusan = [
	{ name: 'NrgId', type: 'float'  } ,
    { name: 'Nrg', type: 'string'  } ,
    { name: 'Nama', type: 'string'  } ,
    { name: 'TempatTugas', type: 'string'  } ,
    { name: 'KabKota', type: 'string'  } ,
    { name: 'Propinsi', type: 'string'  } ,
    { name: 'NoPeserta', type: 'string'  } ,
    { name: 'Nuptk', type: 'string'  } ,
    { name: 'Kementerian', type: 'string'  },
    { name: 'TahunLulus', type: 'string'  }
];

simptk.DataKelulusan = [
	[1, '021340952007', 'Muhammad Khudzaifah, S.T', 'MA At - Thohiriyah Ngantru', 'Kab. Tulungagung', 'Jawa Timur', '1', '3544755658200012', 'Kemenag', '2010'],
	[2, '021539862004', 'IMAM MASNGUDI, Drs.', 'MA Bustanul Ulum Sumbergempol', 'Kab. Tulungagung', 'Jawa Timur', '2', '5942737639200042', 'Kemenag', '2010'],
	[3, '021635897005', 'NURWULAN AGUSTIN, Dra', 'MA Bustanul Ulum Sumbergempol', 'Kab. Tulungagung', 'Jawa Timur', '3', '9134744646300043', 'Kemenag', '2010'],
	[4, '022689392001', 'Drs.MOCHAMAD RUM WAHYUDI', 'MA Darul Hikmah', 'Kab. Tulungagung', 'Jawa Timur', '4', '9261743646200043', 'Kemenag', '2010'],
	[5, '021435402016', 'SURATMI, S.Pd', 'MA DIPONEGORO Bandung', 'Kab. Tulungagung', 'Jawa Timur', '5', '1733745649300022', 'Kemenag', '2010'],
	[6, '021786432009', 'MUKTI ALI, S.Ag.', 'MA DIPONEGORO Bandung', 'Kab. Tulungagung', 'Jawa Timur', '6', '2435751653200023', 'Kemenag', '2009'],
	[7, '021686397004', 'Drs. H. M. ZAENUDIN,M.Ag', 'MAN 1 Tulungagung', 'Kab. Tulungagung', 'Jawa Timur', '7', '3235744647200043', 'Kemenag', '2009'],
	[8, '021335917003', 'SHOKHIBUL AKHWALI, S.Pd', 'MAN 1 Tulungagung', 'Kab. Tulungagung', 'Jawa Timur', '8', '4534748650200062', 'Kemenag', '2009'],
	[9, '022289897001', 'Drs. NANANG ASHARI', 'MAN 2 Tulungagung', 'Kab. Tulungagung', 'Jawa Timur', '9', '0353744648200023', 'Kemenag', '2009'],
	[10, '021686452010', 'NUR ALIFAH, S.Pd', 'MAN 2 Tulungagung', 'Kab. Tulungagung', 'Jawa Timur', '10', '2235755657300033', 'Kemenag', '2009'],
	[11, '064435051003', 'Susiati, Dra', 'SDN 2 SIDOKUMPUL', 'Kab. Gresik', 'Jawa Timur', '06050102700001', '8544737641300013', 'Kemdikbud', '2008'],
	[12, '064025063001', 'Siti Zulaichah, S.pd', 'SDN 7 SIDOKUMPUL', 'Kab. Gresik', 'Jawa Timur', '06050102700002', '7543739641300033', 'Kemdikbud', '2008'],
	[13, '064635051005', 'Tri Peni Handayani, S.pd', 'SDN BEDILAN', 'Kab. Gresik', 'Jawa Timur', '06050102700003', '9546737640300013', 'Kemdikbud', '2008'],
	[14, '065626047002', 'Zahrotul Ulfah, S.pd', 'SDN LUMPUR', 'Kab. Gresik', 'Jawa Timur', '06050102700004', '1656733634300012', 'Kemdikbud', '2008'],
	[15, '064332046002', 'Isnaningsih, S.pd', 'SDN PETROKIMIA', 'Kab. Gresik', 'Jawa Timur', '06050102700005', '2243732634300033', 'Kemdikbud', '2008'],
	[16, '065134051001', 'Sri Supartiyani, S.pd', 'SDN 5 Sidokumpul', 'Kab. Gresik', 'Jawa Timur', '06050102700006', '4451737640300013', 'Kemdikbud', '2007'],
	[17, '063830046004', 'Astuti Sri Utami, S.pd', 'SDN 2 TLOOPATUT', 'Kab. Gresik', 'Jawa Timur', '06050102700008', '0038732634300023', 'Kemdikbud', '2007'],
	[18, '064631054007', 'Hj. Dwi Ratnaningtyas,s.pd', 'SDN SUKORAME', 'Kab. Gresik', 'Jawa Timur', '06050102700009', '0146739641300053', 'Kemdikbud', '2007'],
	[19, '064334052006', 'Titik Setyo Rahayu, S.pd', 'SDN 3 SIDOKUMPUL', 'Kab. Gresik', 'Jawa Timur', '06050102700010', '1443738640300023', 'Kemdikbud', '2007'],
	[20, '064335053003', 'Titik Hidayati', 'SDN BEDILAN', 'Kab. Gresik', 'Jawa Timur', '06050102700011', '8543739641300033', 'Kemdikbud', '2007']
];

simptk.StoreKelulusan = new Ext.data.Store({
    reader: new Ext.data.JsonReader({
        id: 'NrgId',
        root: 'rows',
        totalProperty: 'results',
        fields: simptk.RecordSetKelulusan        
    }),
    //url: './Pengguna/fetch.json',
    url: './TunjanganProfesi/fetchNrg.json',
    autoLoad: false,        
	sortInfo: {field: 'NrgId', direction: "ASC"}
});

simptk.RecordSetPraSk = [
	{ name: 'PraSkId', type: 'float'  } ,
    { name: 'Nama', type: 'string'  } ,
    { name: 'TempatTugas', type: 'string'  } ,
    { name: 'JJMT', type: 'int'  } ,
    { name: 'JJMS', type: 'int'  } ,
    { name: 'TugasTambahan', type: 'string'  } ,
    { name: 'JTT', type: 'int'  } ,
    { name: 'TJJMS', type: 'int'  } ,
    { name: 'Syarat', type: 'int'  } ,
    { name: 'Status', type: 'int'  } ,
    { name: 'Nuptk', type: 'string'  } ,
    { name: 'Nrg', type: 'string'  } ,
    { name: 'TahunLulus', type: 'string'  } ,
    { name: 'StatusPns', type: 'int'  } ,
    { name: 'Jenis', type: 'String'  } ,
    { name: 'Jenjang', type: 'String'  } ,
    { name: 'Proses', type: 'int'  } ,
    { name: 'Norek', type: 'string'  } ,
    { name: 'NamaBank', type: 'string'  } ,
    { name: 'AnBank', type: 'string'  } ,
];

simptk.RecordPraSk = [
	[1, 'Muhammad Khudzaifah, S.T', 'MA At - Thohiriyah Ngantru', 24, 24, '', 0, 0, 1, 1, '3544755658200012', '021340952007', 2010, 1, 'DEKON', 'DIKDAS', 1],
	[1, 'Muhammad Khudzaifah, S.T', 'MA At - Thohiriyah Ngantru', 24, 24, '', 0, 0, 1, 1, '3544755658200012', '021340952007', 2010, 1, 'DEKON', 'DIKDAS', 1]
];

simptk.StorePraSk = new Ext.data.ArrayStore({
	fields: simptk.RecordSetPraSk,
	data : simptk.RecordPraSk,
	autoLoad: false
});



simptk.CheckboxGroupValidasi = Ext.extend( Ext.form.CheckboxGroup, {	
	
	constructor: function(config){ 	
		
		Ext.apply(this, {
			autoWidth: true,
			itemCls: 'x-check-group-alt',							
			columns: 2,
			fieldLabel: ' ',
			labelSeparator: ' ',
			style: "border-bottom: 1px dashed #bbbbbb; padding-bottom: 4px;",
			items: [
				{boxLabel: 'Tidak Valid', name: 'cb-col-1'},
				{boxLabel: 'Valid', name: 'cb-col-2', checked: true}								
			]
		});
		
		Ext.apply(this, config);

		simptk.CheckboxGroupValidasi.superclass.constructor.call(this);
	}
});	

Ext.reg('checkboxgroupvalidasi', simptk.CheckboxGroupValidasi); 

simptk.ComponentSpacer = Ext.extend( Ext.Component, {
	
	constructor: function(config){ 
		
		Ext.apply(this, {
			fieldLabel: '&nbsp;',
			labelSeparator: ' '
		});
		
		Ext.apply(this, config);

		simptk.ComponentSpacer.superclass.constructor.call(this);
	}
});	

Ext.reg('componentspacer', simptk.ComponentSpacer); 


/*
 * Cari Sekolah
 * Pencarian Sekolah dengan Window
 */
simptk.CariSekolah = Ext.extend(Ext.Window, {
	
	/* Standard configs */
	title: 'Pencarian Sekolah',
	border: true,
	layout: 'fit',
	width: 650,
	height: 370,
	modal: true,	
	bodyStyle:'padding:5px;',
	gridSekolah: null,
	form: null,	
	constructor: function(config){    
		
		Ext.apply(this, config);
		var window = this;
		
		// Propinsi //
		simptk.RecordSetPropinsi = [
			{ name: 'PropinsiId', type: 'float'  } ,
			{ name: 'NegaraId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  } ,
			{ name: 'Status', type: 'float'  }	 
		];
		
		simptk.RecordObjPropinsi = Ext.data.Record.create(simptk.RecordSetPropinsi);
		
		simptk.LookupStorePropinsi = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'PropinsiId',
				root: 'rows',
				totalProperty: 'results',
				fields: simptk.RecordSetPropinsi		
			}),
			//url: './Propinsi/fetch.json',
			autoLoad: true,		
			url: './data/Propinsi.json',			
			sortInfo: {field: 'PropinsiId', direction: "ASC"}
		});

		// Kabkota //
		simptk.RecordSetKabupatenKota = [
			{ name: 'KabupatenKotaId', type: 'float'  } ,
			{ name: 'PropinsiId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  } ,
			{ name: 'Type', type: 'float'  } ,
			{ name: 'Status', type: 'float'  } ,
			{ name: 'Keterangan', type: 'string'  }	 
		];
		
		simptk.RecordObjKabupatenKota = Ext.data.Record.create(simptk.RecordSetKabupatenKota);
		
		simptk.StoreKabupatenKota = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'KabupatenKotaId',
				root: 'rows',
				totalProperty: 'results',
				fields: simptk.RecordSetKabupatenKota		
			}),
			//url: './KabupatenKota/fetch.json',
			autoLoad: false,		
			url: './data/KabupatenKota.json',			
			sortInfo: {field: 'KabupatenKotaId', direction: "ASC"}
		});
		
		// Sekolah //
		simptk.RecordSetSekolah = [
			{ name: 'SekolahId', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  } ,
			{ name: 'Nss', type: 'string'  } ,
			{ name: 'Npsn', type: 'string'  } ,
			{ name: 'JenisSekolahId', type: 'float'  } ,
			{ name: 'AlamatJalan', type: 'string'  } ,
			{ name: 'Rt', type: 'string'  } ,
			{ name: 'Rw', type: 'string'  } ,
			{ name: 'NamaDusun', type: 'string'  } ,
			{ name: 'NamaDesaKelurahan', type: 'string'  } ,
			{ name: 'KecamatanId', type: 'float'  } ,
			{ name: 'NamaKecamatan', type: 'string'  } ,
			{ name: 'KabupatenKotaId', type: 'float'  } ,
			{ name: 'NamaKabupatenKota', type: 'string'  } ,
			{ name: 'NamaPropinsi', type: 'string'  } ,
			{ name: 'KodePos', type: 'string'  } ,
			{ name: 'KategoriWilayahId', type: 'float'  } ,
			{ name: 'Lintang', type: 'string'  } ,
			{ name: 'Bujur', type: 'string'  } ,
			{ name: 'NomorTelepon', type: 'string'  } ,
			{ name: 'NomorFax', type: 'string'  } ,
			{ name: 'AksesInternetId', type: 'float'  } ,
			{ name: 'IspId', type: 'float'  } ,
			{ name: 'Email', type: 'string'  } ,
			{ name: 'Website', type: 'string'  } ,
			{ name: 'StatusSekolah', type: 'float'  } ,
			{ name: 'StatusKepemilikanId', type: 'float'  } ,
			{ name: 'SkPendirianSekolah', type: 'string'  } ,
			{ name: 'TanggalSkPendirian', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'SkIzinOperasional', type: 'string'  } ,
			{ name: 'TanggalSkIzinOperasional', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'AkreditasiId', type: 'float'  } ,
			{ name: 'NoSkAkreditasi', type: 'string'  } ,
			{ name: 'TanggalSkAkreditasi', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'StatusMutuId', type: 'float'  } ,
			{ name: 'SertifikasiIsoId', type: 'float'  } ,
			{ name: 'WaktuPenyelenggaraanId', type: 'float'  } ,
			{ name: 'GugusSekolahId', type: 'float'  } ,
			{ name: 'KategoriSekolahId', type: 'float'  } ,
			{ name: 'NoRekening', type: 'string'  } ,
			{ name: 'NamaBank', type: 'string'  } ,
			{ name: 'RekeningAtasNama', type: 'string'  } ,
			{ name: 'Mbs', type: 'float'  } ,
			{ name: 'SumberListrikId', type: 'float'  } ,
			{ name: 'DayaListrik', type: 'float'  } ,
			{ name: 'YayasanId', type: 'float'  }	 
		];
		
		simptk.RecordObjSekolah = Ext.data.Record.create(simptk.RecordSetSekolah);
		
		simptk.StoreSekolah = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'SekolahId',
				root: 'rows',
				totalProperty: 'results',
				fields: simptk.RecordSetSekolah		
			}),
			url: './Sekolah/fetchSekolah.json',
			autoLoad: false,			
			sortInfo: {field: 'SekolahId', direction: "ASC"}
		});		
		
		
		var propinsiCombo = new Ext.form.ComboBox({
			store: simptk.LookupStorePropinsi,
			displayField: 'Nama',
			valueField: 'PropinsiId',
			fieldLabel: 'Propinsi',
			typeAhead: true,
			mode: 'local',
			triggerAction: 'all',
			emptyText: 'Pilih Propinsi...',
			selectOnFocus: true,
			anchor: '100%',
			listeners : {
				select: function(thiscombo, record, index) {
					//Ext.Msg.alert('Info ', thiscombo.getValue());
					kabkotaCombo.clearValue();
					simptk.StoreKabupatenKota.reload({
						params: {
							query: thiscombo.getValue(), 
							query_column: 'propinsi_id', 
							query_type: 'EQUAL'
						}
					})
				}
			}	
		});

		var kabkotaCombo = new Ext.form.ComboBox({
			store: simptk.StoreKabupatenKota,
			displayField: 'Nama',
			valueField: 'KabupatenKotaId',
			fieldLabel: 'Kab/Kota',
			typeAhead: true,
			mode: 'local',
			triggerAction: 'all',
			emptyText: 'Pilih Kabupaten Kota...',
			selectOnFocus: true,
			anchor: '100%',
			listeners : {
				select: function(thiscombo, record, index) {
					//Ext.Msg.alert('Info ', thiscombo.getValue());					
					/*
					simptk.StoreSekolah.reload({
						params: {						
							kabkota: thiscombo.getValue(), 
							//jenissekolah: jenisSekolahCombo.getValue(),
							//statussekolah: statusSekolahCombo.getValue(),
							start: 0, 
							limit: 20							
						}
					});
					*/
				}
			}
		});

		var searchSekolahTextField = new Ext.form.TextField({
			fieldLabel: 'Sekolah',
			emptyText: 'Cari nama sekolah..',
			enableKeyEvents: true,
			anchor: '100%',
			listeners: {
				keypress : function(field, e) {					
					if ((e.getKey() == Ext.EventObject.ENTER) || (e.getKey() == Ext.EventObject.TAB)) {

						simptk.StoreSekolah.reload({
							params: {						
								kabkota: kabkotaCombo.getValue(), 
								//jenissekolah: jenisSekolahCombo.getValue(),
								//statussekolah: statusSekolahCombo.getValue(),
								jenissekolah: window.jenjang_id,
								namasekolah: searchSekolahTextField.getValue(),
								start: 0, 
								limit: 20							
							}
						});
					}
				}
			}
		});
		
		var form = new Ext.form.FormPanel({
			baseCls: 'x-plain',			
			labelWidth: 110,
			defaultType: 'textfield',			
			flex: 2,
			margins: {top:10, right:0, bottom:0, left:0},
			items: [
				propinsiCombo,
				kabkotaCombo,
				searchSekolahTextField
			]
		});		
		
		var gridSekolah = new Ext.grid.GridPanel({
			flex: 4,
			sm: new Ext.grid.RowSelectionModel({
				listeners: {
					rowselect: function( thisSm, index, record ) {
						//propertyStore.reload({params:{id:record.data.SekolahId}});						
						//propertyGrid.setSource(record.data);
						//simptk.StorePrasarana.reload({params:{
						//	query: record.data.SekolahId, 
						//	query_column: 'sekolah_id', 
						//	query_type: 'EQUAL'							
						//}});
						//simptk.StorePesertaDidik.reload({params:{start:0,limit:20}});
						//simptk.StorePtk.reload({params:{start:0,limit:20}});																			
					}
				}
			}),
			ds: simptk.StoreSekolah,
			columns : [{				
				header: 'Sekolah ID',
				width: 5,
				//renderer: Xond.format.idMoney,
				align: 'center',
				hidden: true, 
				sortable: true, 
				dataIndex: 'SekolahId'
			},{				
				header: 'Nama Sekolah',
				width: 200,
				sortable: true, 
				dataIndex: 'Nama'
			},{				
				header: 'NPSN',
				width: 100,
				sortable: true, 
				dataIndex: 'Npsn'
			},{				
				header: 'Kecamatan',
				width: 120,
				sortable: true, 
				dataIndex: 'NamaKecamatan'
			},{	
				header: 'Kab/Kota',
				width: 120,
				sortable: true, 
				dataIndex: 'NamaKabupatenKota'
			}],
			bbar: new Ext.PagingToolbar({
				pageSize	: 20,
    			store		: simptk.StoreSekolah,
				paramNames  : {start: 'start', limit: 'limit'},
    			displayInfo	: true,
    			displayMsg	: 'Menampilkan {0} - {1} dari {2}',
    			emptyMsg	: "Data tidak ditemukan"
			})
		});	
		
		simptk.StoreSekolah.on({
			'beforeload':{
				fn: function(store, options){
					Ext.apply(options.params, {
							kabkota: kabkotaCombo.getValue(), 
							jenissekolah: window.jenjang_id,
							namasekolah: searchSekolahTextField.getValue()
					});
				}				
			}
		});

		Ext.apply(this, {
			layout: 'vbox',
			items: [
				form,
				gridSekolah
			],
			buttons: [{
				text: 'Pilih',
				iconCls: 'cek',				
				handler: function(){
					
					var sekolah_selected = gridSekolah.getSelectionModel().getSelected();
					simptk.sekolah_selected = sekolah_selected;
				
					//window.fireEvent('rasioloaded', window);
					window.fireEvent('sekolahselected', window, sekolah_selected);			
					window.close();
				}
			}]
		});
		
		simptk.CariSekolah.superclass.constructor.call(this);
		
	},
	initComponent: function() {
	   simptk.CariSekolah.superclass.initComponent.call(this);
	   this.addEvents('sekolahselected');
	   this.addEvents('rasioloaded');
	}
});
