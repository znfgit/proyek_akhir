simptk.Sekolah = Ext.extend(Ext.Window, {
	/* Standard configs */
	title: 'Sekolah',	
	//bodyStyle: 'padding: 5px;',
	//layout: 'fit',
	layout: {
		type:'fit'	//,align:'stretch'
	},
	width: 800,
	height: 500,
	modal: true,
	
	constructor: function(config){
		var theWindow = this;
		
		Ext.apply(this, config);
		
		// Extend timeout for all Ext.Ajax.requests to 90 seconds.  
		// Ext.Ajax is a singleton, this statement will extend the timeout  
		// for all subsequent Ext.Ajax calls.  
		Ext.Ajax.timeout = 3600000;
		
		simptk.RecordSetTPtk = [
			{ name: 'TPtkId', type: 'string'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  } ,
			{ name: 'JenisKelamin', type: 'float'  } ,
			{ name: 'IjazahTerakhirId', type: 'float'  } ,
			{ name: 'TahunIjazahTerakhir', type: 'float'  } ,
			{ name: 'GelarAkademikDepanId', type: 'float'  } ,
			{ name: 'GelarAkademikBelakangId', type: 'float'  } ,
			{ name: 'NiyNigk', type: 'string'  } ,
			{ name: 'Nuptk', type: 'string'  } ,
			{ name: 'TempatLahir', type: 'string'  } ,
			{ name: 'TglLahir', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'Nik', type: 'string'  } ,
			{ name: 'AgamaId', type: 'float'  } ,
			{ name: 'StatusPerkawinan', type: 'float'  } ,
			{ name: 'JumlahAnak', type: 'float'  } ,
			{ name: 'NamaIbuKandung', type: 'string'  } ,
			{ name: 'AlamatJalan', type: 'string'  } ,
			{ name: 'Rt', type: 'string'  } ,
			{ name: 'Rw', type: 'string'  } ,
			{ name: 'NamaDesaKelurahan', type: 'string'  } ,
			{ name: 'KodePos', type: 'string'  } ,
			{ name: 'KecamatanId', type: 'float'  } ,
			{ name: 'KabupatenKotaId', type: 'float'  } ,
			{ name: 'NoTeleponRumah', type: 'string'  } ,
			{ name: 'NoHp', type: 'string'  } ,
			{ name: 'Email', type: 'string'  } ,
			{ name: 'StatusKepegawaianId', type: 'float'  } ,
			{ name: 'TmtSekolah', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'JabatanPtkId', type: 'float'  } ,
			{ name: 'TmtJabatan', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'JabatanPtkSebelumnyaId', type: 'float'  } ,
			{ name: 'SertifikasiJabatan', type: 'float'  } ,
			{ name: 'TahunSertifikasiJabatan', type: 'float'  } ,
			{ name: 'NomorSertifikat', type: 'string'  } ,
			{ name: 'Nip', type: 'string'  } ,
			{ name: 'TmtPns', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'PangkatGolonganId', type: 'float'  } ,
			{ name: 'TmtPangkatGol', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'KodeSertifikasiBidangStudiGuruId', type: 'float'  } ,
			{ name: 'KodeProgramKeahlianLaboranId', type: 'float'  } ,
			{ name: 'SudahLisensiKepalaSekolah', type: 'float'  } ,
			{ name: 'JenjangKepengawasanId', type: 'float'  } ,
			{ name: 'PengawasBidangRumpunId', type: 'float'  } ,
			{ name: 'PengawasMapelId', type: 'float'  } ,
			{ name: 'JumlahSekolahBinaan', type: 'float'  } ,
			{ name: 'PernahMengikutiDiklatKepengawasan', type: 'float'  } ,
			{ name: 'NamaSuamiIstri', type: 'string'  } ,
			{ name: 'PekerjaanId', type: 'float'  } ,
			{ name: 'NipSuamiIstri', type: 'string'  } ,
			{ name: 'SekolahId', type: 'string'  } ,
			{ name: 'SimNuptk', type: 'string'  } ,
			{ name: 'Status', type: 'float'  } ,
			{ name: 'StatusData', type: 'float'  } ,
			{ name: 'JabatanFungsional', type: 'float'  } ,
			{ name: 'LembagaPengangkat', type: 'float'  } ,
			{ name: 'NomorSkPengangkatan', type: 'string'  } ,
			{ name: 'TmtPengangkatan', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'SumberGaji', type: 'float'  } ,
			{ name: 'NomorSkKgb', type: 'string'  } ,
			{ name: 'TmtKgb', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'Nrg', type: 'string'  } ,
			{ name: 'NamaBidangStudiSertifikasi', type: 'string'  } ,
			{ name: 'BidangStudiSertifikasiId', type: 'float'  } ,
			{ name: 'TglLulusSertifikasi', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'NamaSekolah', type: 'string'  } ,
			{ name: 'StatusSekolah', type: 'float'  } ,
			{ name: 'NamaJenisSekolah', type: 'string'  } ,
			{ name: 'JenisSekolahId', type: 'float'  } ,
			{ name: 'NamaKabupatenKota', type: 'string'  } ,
			{ name: 'KabupatenKotaSekolahId', type: 'float'  } ,
			{ name: 'NamaPropinsi', type: 'string'  } ,
			{ name: 'PropinsiSekolahId', type: 'float'  } ,
			{ name: 'NamaPangkatGolongan', type: 'string'  } ,
			{ name: 'NamaStatusKepegawaian', type: 'string'  } ,
			{ name: 'NamaTugasTambahan', type: 'string'  } ,
			{ name: 'TugasTambahanId', type: 'float'  } ,
			{ name: 'JamTugasTambahan', type: 'float'  } ,
			{ name: 'JumlahJamMengajar', type: 'float'  } ,
			{ name: 'JumlahJamMengajarSesuai', type: 'float'  } ,
			{ name: 'JumlahJamMengajarSesuaiKtsp', type: 'float'  } ,
			{ name: 'TotalJamMengajarSesuai', type: 'float'  } ,
			{ name: 'MasaKerjaTahun', type: 'string'  } ,
			{ name: 'MasaKerjaBulan', type: 'string'  } ,
			{ name: 'TglPensiun', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'SedangKuliah', type: 'string'  } ,
			{ name: 'BertugasDiWilayahTerpencil', type: 'string'  }     
		];

		simptk.RecordObjTPtk = Ext.data.Record.create(simptk.RecordSetTPtk);

		simptk.StoreTPtk = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'TPtkId',
				root: 'rows',
				totalProperty: 'results',
				fields: simptk.RecordSetTPtk        
			}),
			//url: './TPtk/fetch.json',
			autoLoad: false,        
			url: './Sekolah/fetchTPtk.json',
			
			sortInfo: {field: 'TPtkId', direction: "ASC"}
		});

		simptk.GridSekolah = new Ext.grid.EditorGridPanel({
			id: 'grid-sekolah',
			store: simptk.StoreTPtk,
			recDef: simptk.RecordSetTPtk,
			recObj: simptk.RecordObjTPtk,
			useForm: false,
			loadMask: true,
			tbar: [{
				text: 'Pilih Sekolah',
				tooltip: 'Klik untuk memilih Sekolah',
				handler: function(){
					var win = new simptk.CariSekolah();
					win.show();
				},
				scope: this,
				iconCls: 'zoom'
			},'-',{
				text: 'Download Laporan Koreksi Data',
				tooltip: 'Klik untuk mengunduh dalam format excel',
				handler: function(){
					if (!simptk.sekolah_selected.id) {
						Ext.Msg.alert('Error', 'Silakan pilih sekolah terlebih dahulu');
						return false;
					}
					
					var sekolah_id = simptk.sekolah_selected.id;
					// console.log(sekolah_id);
					window.location.assign('/UnduhRekap/unduhLaporanKoreksiData.xls/'+ sekolah_id);
					
				},
				scope: this,
				iconCls: 'xls'
			},'->',{
				text: 'Refresh',
				iconCls: 'refresh',
				handler: function(){
					simptk.StoreTPtk.reload();
				}	
			}],
			columns : [{                
                header: 'Nama',
                width: 200,
                sortable: true, 
                dataIndex: 'Nama'

            },{                
                header: 'Niy/Nigk',
                width: 125,
                sortable: true, 
                dataIndex: 'NiyNigk'

            },{                
                header: 'Nuptk',
                width: 125,
                sortable: true, 
                dataIndex: 'Nuptk'

            },{                
                header: 'Tempat Lahir',
                width: 150,
                sortable: true, 
                dataIndex: 'TempatLahir'

            },{                
                header: 'Tgl Lahir',
                width: 100,
                renderer: Ext.util.Format.dateRenderer('Y/m/d'),
                sortable: true, 
                dataIndex: 'TglLahir'

            },{                
                header: 'Nik',
                width: 125,
                sortable: true, 
                dataIndex: 'Nik'

            },{                
                header: 'TMT Sekolah',
                width: 100,
                renderer: Ext.util.Format.dateRenderer('Y/m/d'),
                sortable: true, 
                dataIndex: 'TmtSekolah'

            },{                
                header: 'TMT Jabatan',
                width: 100,
                renderer: Ext.util.Format.dateRenderer('Y/m/d'),
                sortable: true, 
                dataIndex: 'TmtJabatan'

            },{                
                header: 'Sertifikasi Jabatan',
                width: 50,
                sortable: true, 
                dataIndex: 'SertifikasiJabatan'

            },{                
                header: 'Tahun Sertifikasi Jabatan',
                width: 80,
                sortable: true, 
                dataIndex: 'TahunSertifikasiJabatan'

            },{                
                header: 'NomorSertifikat',
                width: 125,
                sortable: true, 
                dataIndex: 'NomorSertifikat'

            },{                
                header: 'Nip',
                width: 125,
                sortable: true, 
                dataIndex: 'Nip'

            },{                
                header: 'TMT Pns',
                width: 100,
                renderer: Ext.util.Format.dateRenderer('Y/m/d'),
                sortable: true, 
                dataIndex: 'TmtPns'

            },{                
                header: 'TMT Pangkat Gol',
                width: 100,
                renderer: Ext.util.Format.dateRenderer('Y/m/d'),
                sortable: true, 
                dataIndex: 'TmtPangkatGol'

            },{                
                header: 'Sudah Lisensi Kepala Sekolah',
                width: 50,
                sortable: true, 
                dataIndex: 'SudahLisensiKepalaSekolah'

            },{                
                header: 'Jumlah Sekolah Binaan',
                width: 60,
                sortable: true, 
                dataIndex: 'JumlahSekolahBinaan'

            },{                
                header: 'Pernah Mengikuti Diklat Kepengawasan',
                width: 60,
                sortable: true, 
                dataIndex: 'PernahMengikutiDiklatKepengawasan'

            },{                
                header: 'SimNuptk',
                width: 125,
                sortable: true, 
                dataIndex: 'SimNuptk'

            },{                
                header: 'Status',
                width: 80,
                sortable: true, 
                dataIndex: 'Status'

            },{                
                header: 'StatusData',
                width: 80,
                sortable: true, 
                dataIndex: 'StatusData'

            },{                
                header: 'Lembaga Pengangkat',
                width: 100,
                sortable: true, 
                dataIndex: 'LembagaPengangkat'

            },{                
                header: 'NomorSkPengangkatan',
                width: 125,
                sortable: true, 
                dataIndex: 'NomorSkPengangkatan'

            },{                
                header: 'TMT Pengangkatan',
                width: 100,
                renderer: Ext.util.Format.dateRenderer('Y/m/d'),
                sortable: true, 
                dataIndex: 'TmtPengangkatan'

            },{                
                header: 'Nomor Sk Kgb',
                width: 125,
                sortable: true, 
                dataIndex: 'NomorSkKgb'

            },{                
                header: 'TMT Kgb',
                width: 100,
                renderer: Ext.util.Format.dateRenderer('Y/m/d'),
                sortable: true, 
                dataIndex: 'TmtKgb'

            },{                
                header: 'Nrg',
                width: 125,
                sortable: true, 
                dataIndex: 'Nrg'

            },{                
                header: 'Bidang Studi Sertifikasi',
                width: 125,
                sortable: true, 
                dataIndex: 'NamaBidangStudiSertifikasi'

            },{                
                header: 'Tgl Lulus Sertifikasi',
                width: 100,
                renderer: Ext.util.Format.dateRenderer('Y/m/d'),
                sortable: true, 
                dataIndex: 'TglLulusSertifikasi'

            },{                
                header: 'Nama Sekolah',
                width: 200,
                sortable: true, 
                dataIndex: 'NamaSekolah'

            },{                
                header: 'Status Sekolah',
                width: 80,
                sortable: true, 
                dataIndex: 'StatusSekolah'

            },{                
                header: 'Jenis Sekolah',
                width: 80,
                sortable: true, 
                dataIndex: 'NamaJenisSekolah'

            },{                
                header: 'Kabupaten/Kota',
                width: 125,
                sortable: true, 
                dataIndex: 'NamaKabupatenKota'

            },{                
                header: 'Propinsi',
                width: 125,
                sortable: true, 
                dataIndex: 'NamaPropinsi'

            },{                
                header: 'Pangkat Golongan',
                width: 100,
                sortable: true, 
                dataIndex: 'NamaPangkatGolongan'

            },{                
                header: 'Status Kepegawaian',
                width: 100,
                sortable: true, 
                dataIndex: 'NamaStatusKepegawaian'

            },{                
                header: 'Tugas Tambahan',
                width: 125,
                sortable: true, 
                dataIndex: 'NamaTugasTambahan'

            },{                
                header: 'Jam Tugas Tambahan',
                width: 80,
                sortable: true, 
                dataIndex: 'JamTugasTambahan'

            },{                
                header: 'Jumlah Jam Mengajar',
                width: 80,
                sortable: true, 
                dataIndex: 'JumlahJamMengajar'

            },{                
                header: 'Jumlah Jam Mengajar Sesuai',
                width: 80,
                sortable: true, 
                dataIndex: 'JumlahJamMengajarSesuai'

            },{                
                header: 'Jumlah Jam Mengajar Sesuai Ktsp',
                width: 80,
                sortable: true, 
                dataIndex: 'JumlahJamMengajarSesuaiKtsp'

            },{                
                header: 'Total Jam Mengajar Sesuai',
                width: 80,
                sortable: true, 
                dataIndex: 'TotalJamMengajarSesuai'

            },{                
                header: 'Masa Kerja Tahun',
                width: 60,
                sortable: true, 
                dataIndex: 'MasaKerjaTahun'

            },{                
                header: 'Masa Kerja Bulan',
                width: 60,
                sortable: true, 
                dataIndex: 'MasaKerjaBulan'

            },{                
                header: 'Tgl Pensiun',
                width: 100,
                renderer: Ext.util.Format.dateRenderer('Y/m/d'),
                sortable: true, 
                dataIndex: 'TglPensiun'

            },{                
                header: 'Sedang Kuliah',
                width: 50,
                sortable: true, 
                dataIndex: 'SedangKuliah'

            },{                
                header: 'Bertugas Di Wilayah Terpencil',
                width: 50,
                sortable: true, 
                dataIndex: 'BertugasDiWilayahTerpencil'

            }],
			bbar: new Ext.PagingToolbar({
				pageSize	: 20,
    			store		: simptk.StoreTPtk,
				paramNames  : {start: 'start', limit: 'limit'},
    			displayInfo	: true,
    			displayMsg	: 'Menampilkan {0} - {1} dari {2}',
    			emptyMsg	: "Data tidak ditemukan"
			})
		});
		
		Ext.apply(this, {
			items: simptk.GridSekolah
		});
		
		simptk.Sekolah.superclass.constructor.call(this);
	}
});