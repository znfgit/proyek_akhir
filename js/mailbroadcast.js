
simptk.MailBroadcast = Ext.extend(Ext.Window, {
	/* Standard configs */
	title: 'Broadcast E-mail',
	border: true,
	//bodyStyle: 'padding: 5px;',
	//layout: 'vbox',
	//align:'stretch',
	layout: {
		type:'vbox',			
		align:'stretch'
	},
	width: 600,
	height: 400,
	modal: true,
	viewConfig: {forceFit: true},
	
	/* Custom configs */
	constructor: function(config){
		
		var thiswindow = this;
		
		var tujuanEmail = [
			['1','Semua Operator'],
			['2','Operator SIM Tunjangan Profesi'],
			['3','Operator SIM Aneka Tunjangan'],
			['4','Operator SIMPAK'],
			['5','Operator SIM Jabfung'],
			['6','Operator SIM Rasio'],
			['7','Semua Guru'],
			['7','Guru Telah Sertifikasi'],
			['8','Guru Belum Sertifikasi'],
			['9','Guru Daerah Khusus'],
			['10','Test']
		];
		
		var storeTujuanEmail = new Ext.data.SimpleStore({
            fields: ['no', 'nama'],
            data: tujuanEmail,
            sortInfo: {field: 'no', direction: 'ASC'}
        });
		
		var form = new Ext.form.FormPanel ({		
			baseCls: 'x-plain',
			url: '/Mail/send.php',
			//margins: {top:10, right:0, bottom:0, left:0},
			defaultType: 'textfield',	
			autoScroll: true, 
			
			items: [{
				xtype: 'fieldset',
				//frame: false,
				layout: 'form',
				items: [{
					xtype: 'hidden',
					name: 'PenerimaId'
				},{
					xtype: 'textfield',
					fieldLabel: 'From',
					name: 'From',
					anchor: '95%',
					value: 'dit.p2tk.dikdas@p2tkdikdas.kemdikbud.go.id',
					allowBlank: false
				},{	
					allowBlank:false,
					//msgTarget: 'title',
					xtype: 'superboxselect',
					fieldLabel: 'To',
					resizable: true,
					name: 'To[]',
					anchor:'95%',
					store: storeTujuanEmail,
					mode: 'local',
					displayField: 'nama',
					//displayFieldTpl: '{state} ({abbr})',
					valueField: 'no',
					navigateItemsWithTab: false
				},{
					xtype: 'textfield',
					fieldLabel: 'Cc',
					name: 'Cc',
					anchor: '95%',
					allowBlank: false
				},{
					xtype: 'textfield',
					fieldLabel: 'Subject',
					name: 'Subject',
					anchor: '95%',
					allowBlank: false
				}]		
			},{
				xtype: 'panel',
				layout: 'fit',
				border: false,
				items: [{
					xtype: 'htmleditor',
					//fieldLabel: 'From',
					name: 'Body',
					anchor: '95%',
					allowBlank: false			
				}]
			}]
		});
		
		Ext.apply(this, config);

		Ext.apply(this, {
			form: form,
			items:  [				
				form
			],
			buttons: [{
				text: 'Broadcast',
				iconCls: 'arrow_out',
				handler: function(){
					form.getForm().submit({
						success: function(form, action) {
						   Ext.Msg.alert('Success', action.result.message);
						},
						failure: function(form, action) {
							Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
						}
					});
				}
			},{
				text: 'Cancel',
				iconCls: 'logout',
				handler: function(){
					thiswindow.close();
				}
			}]
		});
		
		simptk.Rekap.superclass.constructor.call(this);

	}
});