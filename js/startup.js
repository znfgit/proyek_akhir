Ext.namespace('simptk');

simptk.sekolah_selected = "";
simptk.sekolah_darurat = false;

// Dalam bentuk objek
simptk.kecamatan_selected = "";
simptk.kabkota_selected = "";
simptk.propinsi_selected = "";
simptk.nasional_selected = false;

// String lah
simptk.kecamatan_selected_nama = "";
simptk.kabkota_selected_nama = "";
simptk.propinsi_selected_nama = "";

// Yang ini int aja
simptk.bidang_studi_selected = "";
simptk.jenjang_selected = "";
//simptk.jenjang_selected = "5";
simptk.rsg_selected = "Eksisting";
simptk.bk_selected = "24";
simptk.rsgbk_selected = "150";
simptk.filter_pns = 1;
simptk.filter_pensiun = 1;

simptk.smt = "2";
simptk.tahun = "2011";

simptk.filter_jenis_lingkup_bantuan = "0";		//0 semua, 1 dau, 2 dekon
simptk.filter_syarat_24_jam = "0";			//0 semua, 1 terpenuhi, 2 tidak

