
simptk.Rekap = Ext.extend(Ext.Window, {
	/* Standard configs */
	title: 'Rekap Data',
	border: true,
	//bodyStyle: 'padding: 5px;',
	//layout: 'vbox',
	//align:'stretch',
	layout: {
		type:'vbox',			
		align:'stretch'
	},
	width: 800,
	height: 500,
	modal: true,
	viewConfig: {forceFit: true},
	
	/* Custom configs */
	// gridRekap: null, 
	// ds: null,
	// cm: null,
	//jenjang: null,
	expandAll: function(){
		this.grid.getStore().expandAll();
	},
	expandProp: function() {
		var store = this.grid.getStore();
		store.each(function(r){
			if (r.data.Level == 1) {
				// console.log (r.data.Nama);
				store.expandNode(r);
			}
		});
	},
	collapseAll: function(){
		this.grid.getStore().collapseAll();
	},
	
	updateProgress: function(rec, start) {
		var thewindow = this;
		var thegrid = this.grid;
		
		var id = 0;		
		var level = rec.data.Level;
		if (level == 2) {
			id = rec.data.PropinsiId;
		} else {
			id = rec.data.KabupatenKotaId;
		}
			
		Ext.Ajax.request({
			
			waitMsg: 'Menyimpan...',						
			url: 'Sinkronisasi/getProgressSinkronisasi.json',					
			method: 'POST',					
			params: {
				id: id,
				level: level,
				start: start ? '1' : '0'
			},
			failure:function(response,options){						   
				Ext.Msg.alert('Warning','Response : ' + response );
			},
			success: function(response,options){							
				
				var json = Ext.util.JSON.decode(response.responseText);			
				
				if (json.success) {	
					
					var start = json.start;
					var progress = json.progress;
					var total = 0;
					var persen = 0;
					
					if (progress) {						
						total = json.total;					
						if (total > 0) {
							persen = (progress * 100)/total;
							persen = Math.round(persen);
						} else {
							persen = 0;
						}
						rec.set('Progress', persen);
					}
					
					setTimeout(function(){
						thewindow.updateProgress(rec, start);
					}, 1000);
					
				} else {
					rec.set('Progress', 100);
					thegrid.getStore().reload();
					
					setTimeout(function(){
						thewindow.expandProp();
					}, 2000);
					
					setTimeout(function(){
						thegrid.getStore().expandNode(rec);
					}, 3000);
					
				}
			}
			
		});
	},
	
	constructor: function(config){
		
		var thiswindow = this;
		
		Ext.apply(this, config);
		
		/* Main Record Set */		
		simptk.RecordSetTRekapValidasi= [
			{ name: 'RKeperluanId', type: 'float'  } ,
			{ name: '_id', type: 'int' },
			{ name: '_parent', type: 'auto' },
			{ name: '_is_leaf', type: 'bool' },
			{ name: 'Level', type: 'int'  } ,
			{ name: 'PropinsiId', type: 'int'  } ,
			{ name: 'KabupatenKotaId', type: 'int'  } ,
			{ name: 'Nama', type: 'string'  } ,
			{ name: 'JumlahIncomplete', type: 'float'  } ,
			{ name: 'JumlahPass', type: 'float' } ,
			{ name: 'JumlahUnqualified', type: 'float' },
			{ name: 'Progress', type: 'float' }
		];		
		
		simptk.RecordObjTRekapValidasi= Ext.data.Record.create(simptk.RecordSetTRekapValidasi);
		
		var storeTRekapValidasi = new Ext.ux.maximgb.tg.AdjacencyListStore({
			reader: new Ext.data.JsonReader({
				//id: 'TRekapValidasiId',
				id: '_id',
				root: 'rows',
				totalProperty: 'results',
				fields: simptk.RecordSetTRekapValidasi
			}),
			url: './Sinkronisasi/getRekap.json',
			autoLoad: false,
			sortInfo: {field: '_id', direction: "ASC"}
		});
		
		simptk.pilihtunjangan = 3;
		
		function reloadRekapValidasi() {
			storeTRekapValidasi.reload({
				params: {
					pilihtunjangan: simptk.pilihtunjangan,
				}
			});
			setTimeout(function(){
				thiswindow.expandProp();
			},500);
		};
		
		var grid = new Ext.ux.maximgb.tg.EditorGridPanel({
			flex: 4,
			loadMask: true,
			sm: new Ext.grid.RowSelectionModel(),
			tbar: [{
				text: 'Tunjangan Profesi',	
				id: 'buttonpilihtunjangan',
				group: 'pilihtunjangan',
				iconCls: 'revisi',
				menu: {        // <-- submenu by nested config object
					items: [/*{
							text: '-- Semua --',
							checked: true,
							group: 'pilihtunjangan',
							checkHandler: function(){
								btn = Ext.getCmp('buttonpilihtunjangan');
								btn.setText('-- Semua --');
								simptk.pilihtunjangan = 3;
							}
						}, */{
							text: 'Tunjangan Profesi',
							group: 'pilihtunjangan',
							menu: [{
								text: 'Tunjangan Profesi',
								checked: true,
								group: 'pilihtunjangan',
								checkHandler: function(){
									btn = Ext.getCmp('buttonpilihtunjangan');
									btn.setText('Tunjangan Profesi');
									simptk.pilihtunjangan = 3;
								}
							},{
								text: 'Tunjangan Profesi DAU',
								checked: false,
								group: 'pilihtunjangan',
								checkHandler: function(){
									btn = Ext.getCmp('buttonpilihtunjangan');
									btn.setText('Tunjangan Profesi DAU');
									simptk.pilihtunjangan = 7;
								}
							},{
								text: 'Tunjangan Profesi DEKON',
								checked: false,
								group: 'pilihtunjangan',
								checkHandler: function(){
									btn = Ext.getCmp('buttonpilihtunjangan');
									btn.setText('Tunjangan Profesi DEKON');
									simptk.pilihtunjangan = 8;
								}
							}]
							
						}, {
							text: 'Tunjangan Fungsional',
							checked: false,
							group: 'pilihtunjangan',
							checkHandler: function(){
								btn = Ext.getCmp('buttonpilihtunjangan');
								btn.setText('Tunjangan Fungsional');
								simptk.pilihtunjangan = 4;
							}
						}, {
							text: 'Tunjangan Kualifikasi',
							checked: false,
							group: 'pilihtunjangan',
							checkHandler: function(){
								btn = Ext.getCmp('buttonpilihtunjangan');
								btn.setText('Tunjangan Kualifikasi');
								simptk.pilihtunjangan = 5;
							}
						}, {
							text: 'Tunjangan Khusus',
							checked: false,
							group: 'pilihtunjangan',
							checkHandler: function(){
								btn = Ext.getCmp('buttonpilihtunjangan');
								btn.setText('Tunjangan Khusus');
								simptk.pilihtunjangan = 6;
							}
						}
					]
				}
			},'-',{
				text: 'Muat Data',
				iconCls: 'refresh',
				handler : function(btn) {
					reloadRekapValidasi();
				}
		/*},'-',{
				text: 'Update JJM',
				iconCls: 'asterisk_yellow',
				handler : function(btn) {
					Ext.Ajax.request({
						waitMsg: 'Menyimpan...',			
						url: 'Sinkronisasi/refreshJjm.json',
						method: 'POST',					
						params: {
							rows: jsonData
						},	
						failure:function(response,options){						   
							Ext.Msg.alert('Warning','Response : ' + response );
						},
						success: function(response,options){
							var json = Ext.util.JSON.decode(response.responseText);
								
							if (json.success) {					
									//win.hide();
									grid.getStore().removeAll();
									grid.getStore().reload();
									Ext.Msg.alert('OK', json.message);
									
							} else if (json.success == false){
									Ext.Msg.alert('Info', json.message);
							}
						
						}				
					}); 

				}
			/*
			},'-',{
				text: 'Sinkronisasi',
				iconCls: 'asterisk_yellow',
				handler : function(btn) {
					var sel = grid.getSelectionModel().getSelected();
					if (!sel) {
						Ext.Msg.alert('Error', 'Mohon pilih salah satu wilayah terlebih dahulu');
						return;
					}
					if (sel.data.Level == 1) {
						Ext.Msg.alert('Error', 'Mohon pilih Kab/Kota atau Propinsi');
						return;
					}	
					
					var skup = "";
					var id = "";
					var wilayah = "";
					
					if (sel.data.Level == 2) {
						skup = "propinsi_id";						
						id = sel.data.PropinsiId;
						wilayah = "Propinsi ";
					}
					
					if (sel.data.Level == 3) {
						skup = "kabupaten_kota_id";
						id = sel.data.KabupatenKotaId;
					}
					
					Ext.Ajax.request({
						waitMsg: 'Menyimpan...',						
						url: '/sinkronisasi_bg.php?' + skup + '=' + id,
						method: 'POST',
						failure:function(response,options){						   
							Ext.Msg.alert('Warning','Response : ' + response );
						},
						success: function(response,options){
							var json = Ext.util.JSON.decode(response.responseText);						
							if (json.success) {
								Xond.msg('Ok', 'Sinkronisasi dimulai untuk ' + wilayah + sel.data.Nama + '. Tunggu sebentar sampai indikator % mulai.' );
							}
							setTimeout(function(){
								thiswindow.updateProgress(sel, true); 		// True to start
							}, 2000);							
						}
					});
					
				}
			*/
			},'->',{
				text: 'Ouput HTML',
				iconCls: 'showlist',
				handler : function(btn) {
					
					var sel = grid.getSelectionModel().getSelected();
					if (!sel) {
						Ext.Msg.alert('Error', 'Mohon pilih salah satu wilayah terlebih dahulu');
						return;
					}
					if (sel.data.Level != 3) {
						Ext.Msg.alert('Error', 'Mohon pilih Kab/Kota terlebih dahulu');
						return false;
					}	
					
					var kabupaten_kota_id = sel.data.KabupatenKotaId;
					window.open('/result/'+ kabupaten_kota_id +'.html');
					// window.location.assign('/UnduhRekap/unduhRekapNasional.xls/'+ simptk.pilihtunjangan);
				}
			},'-',{
				text: 'Unduh Rekap',
				iconCls: 'xls',
				handler : function(btn) {
					window.location.assign('/UnduhRekap/unduhRekapNasional.xls/'+ simptk.pilihtunjangan);
				}
			}],			
			ds: storeTRekapValidasi,
			//"RKeperluanId", "_id", "_parent", "Level", "KabupatenKotaId", "PropinsiId", "Nama", "JumlahIncomplete", "JumlahPass", "JumlahUnqualified"
			
			columns : [{                
                /*header: 'No',
                width: 80,
                sortable: true, 
				hidden: true,
                dataIndex: 'RFormDupakId'
            },{ */               
                header: 'Wilayah',
                width: 250,				
                sortable: true, 
				renderer: function(v, params, record) {
					//var val = record.data.Kode + ". " + v;
					//return '<div style="white-space:normal !important;">'+ val +'</div>';
					return v;
				},
                dataIndex: 'Nama'
            },{  
                header: 'Belum Update Data',
                width: 120,
                sortable: true,
				align: 'right',
				renderer: Ext.util.Format.formatNumber,
                dataIndex: 'JumlahIncomplete'
            },{  
                header: 'Memenuhi Syarat',
                width: 120,
				align: 'right',
                renderer: Ext.util.Format.formatNumber,
                sortable: true, 
                dataIndex: 'JumlahPass'
            },{                
                header: 'Tidak Memenuhi',
                width: 110,
				align: 'right',
                renderer: Ext.util.Format.formatNumber,
                sortable: true, 
                dataIndex: 'JumlahUnqualified'
            },{                
                header: 'Total',
                width: 110,
				align: 'right',
                // renderer: Ext.util.Format.formatNumber,
                sortable: true, 
				renderer: function(v, params, record) {
					var total = record.data.JumlahPass + record.data.JumlahIncomplete + record.data.JumlahUnqualified;
					return Ext.util.Format.formatNumber(total);
				},
				dataIndex: 'JumlahPass'
			}, new Ext.ux.grid.ProgressColumn({
					header : " ",
					dataIndex : 'Progress',
					width : 50,
					textPst : '%', 			// string added to the end of the cell value (defaults to '%')
					actionEvent: 'click', 	// click event (defaults to 'dblclick')
					colored : false		// True for pretty colors, false for just blue (defaults to false)
				})
			]
			
		});	
		
		Ext.apply(this, {
			grid: grid,
			items:  [				
				grid
			]
		});
		
		simptk.Rekap.superclass.constructor.call(this);

	}
});