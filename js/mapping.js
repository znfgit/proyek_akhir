
simptk.Mapping = Ext.extend(Ext.Window, {
	
	/* Standard configs */
	title: 'Mapping Data Bidang Studi Sertifikasi',
	border: true,
	layout: {
		type:'vbox',			
		align:'stretch'
	},
	width: 800,
	height: 560,
	modal: true,
	viewConfig: {forceFit: true},
	
	updateProgress: function(rec, start) {
		
		var thewindow = this;
		var thegrid = this.grid;

		Ext.Ajax.request({
			
			//waitMsg: 'Menyimpan...',						
			url: 'Mapping/getProgress.json',					
			method: 'POST',					
			failure:function(response,options){						   
				Ext.Msg.alert('Warning','Response : ' + response );
			},
			success: function(response,options){							
				
				var json = Ext.util.JSON.decode(response.responseText);			
				
				if (json.success) {	
					
					var progress = json.progress;
					var nama_proses = json.nama;

					if (progress < 100) {
						
						//Ext.MessageBox.updateProgress(json.progress/100, json.progress +'% selesai');
						Ext.MessageBox.updateProgress(json.progress/100, json.progress +'% ' + ' (' + nama_proses + ')');
						
						setTimeout(function(){
							thewindow.updateProgress(rec, start);
						}, 3000);
						
					} else {
						
						Ext.MessageBox.updateProgress(json.progress/100, json.progress +'% selesai');
						
						setTimeout(function(){
							Ext.MessageBox.hide();
							Ext.Msg.alert('Selesai', 'JJM Telah Terupdate');
						}, 500);
					
					}					
				}

			}
			
		});
	},	
	
	/* Custom configs */
	constructor: function(config){
		
		var thiswindow = this;
		
		Ext.apply(this, config);

		var ds = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'KodeLama',
				root: 'rows',				
				totalProperty: 'results',
				fields: [
					{ name: 'KodeLama', type: 'string'},
					{ name: 'NamaLama', type: 'string'},
					{ name: 'JumlahGuru', type: 'int'},
					{ name: 'VersiTahun', type: 'int'},
					{ name: '020', type: 'int'}, 
					{ name: '027', type: 'int'}, 
					{ name: '800', type: 'int'}, 
					{ name: '127', type: 'int'}, 
					{ name: '130', type: 'int'}, 
					{ name: '134', type: 'int'}, 
					{ name: '137', type: 'int'}, 
					{ name: '140', type: 'int'}, 
					{ name: '143', type: 'int'}, 
					{ name: '217', type: 'int'}, 
					{ name: '220', type: 'int'}, 
					{ name: '157', type: 'int'}, 
					{ name: '154', type: 'int'}, 
					{ name: '180', type: 'int'}, 
					{ name: '156', type: 'int'}, 
					{ name: '224', type: 'int'}, 
					{ name: '227', type: 'int'}, 
					{ name: '097', type: 'int'}, 
					{ name: '100', type: 'int'}, 
					{ name: '190', type: 'int'}, 
					{ name: '184', type: 'int'}, 
					{ name: '187', type: 'int'}, 
					{ name: '210', type: 'int'}, 
					{ name: '214', type: 'int'}, 
					{ name: '215', type: 'int'}, 
					{ name: '207', type: 'int'}, 
					{ name: '204', type: 'int'}, 
					{ name: '167', type: 'int'}, 
					{ name: '160', type: 'int'}, 
					{ name: '164', type: 'int'}, 
					{ name: '170', type: 'int'}, 
					{ name: '174', type: 'int'}, 
					{ name: '330', type: 'int'}, 
					{ name: '331', type: 'int'}, 
					{ name: '810', type: 'int'}, 
					{ name: '062', type: 'int'}, 
					{ name: '063', type: 'int'}, 
					{ name: '068', type: 'int'}, 
					{ name: '069', type: 'int'},
					{ name: 'JumlahTercentang', type: 'int'}
				]
			}),
			url: './Mapping/fetch.json',
			autoLoad: false,						
			sortInfo: {field: 'KodeLama', direction: "ASC"}
		});
		
		//colModel: new Ext.ux.grid.LockingColumnModel([
		//{header: 'Company',      width: 160, sortable: true, dataIndex: 'company', locked: true, id:'company'},
		//Ext.grid.ColumnModel({ 
		
		var cm = new Ext.ux.grid.LockingColumnModel({
			defaults: {
				sortable: true // columns are not sortable by default		   
			},
			columns : [{				
				header: 'Kode',
				width: 60,			
				align: 'center',
				tooltip: 'Kode',						
				sortable: true, 				
				locked: true,
				dataIndex: 'KodeLama'
			},{				
				header: 'Bidang Studi Sertifikasi',
				width: 200,			
				sortable: true, 
				locked: true,
				dataIndex: 'NamaLama'
			},{				
				header: 'Jml.Guru',
				width: 80,			
				sortable: true, 
				locked: true,
				align: 'right',
				renderer: Ext.util.Format.formatNumber,
				bodyStyle: 'margin-left: 10px',
				dataIndex: 'JumlahGuru'
			},{				
				header: 'Jml.Mapping',
				width: 80,			
				sortable: true, 
				locked: true,
				align: 'right',
				dataIndex: 'JumlahTercentang'
			},{
				xtype: 'checkcolumn',
				header: '020 Guru Kelas PAUD',
				width: 130,
				tooltip: 'Guru Kelas PAUD',
				sortable: true, 
				dataIndex: '020'
			},{
				xtype: 'checkcolumn',
				header: '027 Guru Kelas SD/MI',
				width: 130,
				tooltip: 'Guru Kelas SD/MI',
				sortable: true, 
				dataIndex: '027'
			},{
				xtype: 'checkcolumn',
				header: '062 Muatan Lokal Bahasa Daerah',
				width: 130,
				tooltip: 'Muatan Lokal Bahasa Daerah',
				sortable: true, 
				dataIndex: '062'
			},{
				xtype: 'checkcolumn',
				header: '063 Muatan Lokal Potensi Daerah',
				width: 130,
				tooltip: 'Muatan Lokal Potensi Daerah',
				sortable: true, 
				dataIndex: '063'
			},{
				xtype: 'checkcolumn',
				header: '068 Muatan Lokal',
				width: 130,
				tooltip: 'Muatan Lokal',
				sortable: true, 
				dataIndex: '068'
			},{
				xtype: 'checkcolumn',
				header: '069 lainnya',
				width: 130,
				tooltip: 'lainnya',
				sortable: true, 
				dataIndex: '069'
			},{
				xtype: 'checkcolumn',
				header: '097 Ilmu Pengetahuan Alam (IPA)',
				width: 130,
				tooltip: 'Ilmu Pengetahuan Alam (IPA)',
				sortable: true, 
				dataIndex: '097'
			},{
				xtype: 'checkcolumn',
				header: '100 Ilmu Pengetahuan Sosial (IPS)',
				width: 130,
				tooltip: 'Ilmu Pengetahuan Sosial (IPS)',
				sortable: true, 
				dataIndex: '100'
			},{
				xtype: 'checkcolumn',
				header: '127 Pendidikan Agama Islam',
				width: 130,
				tooltip: 'Pendidikan Agama Islam',
				sortable: true, 
				dataIndex: '127'
			},{
				xtype: 'checkcolumn',
				header: '130 Pendidikan Agama Katholik',
				width: 130,
				tooltip: 'Pendidikan Agama Katholik',
				sortable: true, 
				dataIndex: '130'
			},{
				xtype: 'checkcolumn',
				header: '134 Pendidikan Agama Kristen',
				width: 130,
				tooltip: 'Pendidikan Agama Kristen',
				sortable: true, 
				dataIndex: '134'
			},{
				xtype: 'checkcolumn',
				header: '137 Pendidikan Agama Hindu',
				width: 130,
				tooltip: 'Pendidikan Agama Hindu',
				sortable: true, 
				dataIndex: '137'
			},{
				xtype: 'checkcolumn',
				header: '140 Pendidikan Agama Budha',
				width: 130,
				tooltip: 'Pendidikan Agama Budha',
				sortable: true, 
				dataIndex: '140'
			},{
				xtype: 'checkcolumn',
				header: '143 Pendidikan Agama Konghucu',
				width: 130,
				tooltip: 'Pendidikan Agama Konghucu',
				sortable: true, 
				dataIndex: '143'
			},{
				xtype: 'checkcolumn',
				header: '154 Pendidikan Kewarganegaraan (PKn)',
				width: 130,
				tooltip: 'Pendidikan Kewarganegaraan (PKn)',
				sortable: true, 
				dataIndex: '154'
			},{
				xtype: 'checkcolumn',
				header: '156 Bahasa Indonesia',
				width: 130,
				tooltip: 'Bahasa Indonesia',
				sortable: true, 
				dataIndex: '156'
			},{
				xtype: 'checkcolumn',
				header: '157 Bahasa Inggris',
				width: 130,
				tooltip: 'Bahasa Inggris',
				sortable: true, 
				dataIndex: '157'
			},{
				xtype: 'checkcolumn',
				header: '160 Bahasa Jerman',
				width: 130,
				tooltip: 'Bahasa Jerman',
				sortable: true, 
				dataIndex: '160'
			},{
				xtype: 'checkcolumn',
				header: '164 Bahasa Perancis',
				width: 130,
				tooltip: 'Bahasa Perancis',
				sortable: true, 
				dataIndex: '164'
			},{
				xtype: 'checkcolumn',
				header: '167 Bahasa Arab',
				width: 130,
				tooltip: 'Bahasa Arab',
				sortable: true, 
				dataIndex: '167'
			},{
				xtype: 'checkcolumn',
				header: '170 Bahasa Jepang',
				width: 130,
				tooltip: 'Bahasa Jepang',
				sortable: true, 
				dataIndex: '170'
			},{
				xtype: 'checkcolumn',
				header: '174 Bahasa Mandarin',
				width: 130,
				tooltip: 'Bahasa Mandarin',
				sortable: true, 
				dataIndex: '174'
			},{
				xtype: 'checkcolumn',
				header: '180 Matematika',
				width: 130,
				tooltip: 'Matematika',
				sortable: true, 
				dataIndex: '180'
			},{
				xtype: 'checkcolumn',
				header: '184 Fisika',
				width: 130,
				tooltip: 'Fisika',
				sortable: true, 
				dataIndex: '184'
			},{
				xtype: 'checkcolumn',
				header: '187 Kimia',
				width: 130,
				tooltip: 'Kimia',
				sortable: true, 
				dataIndex: '187'
			},{
				xtype: 'checkcolumn',
				header: '190 Biologi',
				width: 130,
				tooltip: 'Biologi',
				sortable: true, 
				dataIndex: '190'
			},{
				xtype: 'checkcolumn',
				header: '204 Sejarah',
				width: 130,
				tooltip: 'Sejarah',
				sortable: true, 
				dataIndex: '204'
			},{
				xtype: 'checkcolumn',
				header: '207 Geografi',
				width: 130,
				tooltip: 'Geografi',
				sortable: true, 
				dataIndex: '207'
			},{
				xtype: 'checkcolumn',
				header: '210 Ekonomi',
				width: 130,
				tooltip: 'Ekonomi',
				sortable: true, 
				dataIndex: '210'
			},{
				xtype: 'checkcolumn',
				header: '214 Sosiologi',
				width: 130,
				tooltip: 'Sosiologi',
				sortable: true, 
				dataIndex: '214'
			},{
				xtype: 'checkcolumn',
				header: '215 Antropologi',
				width: 130,
				tooltip: 'Antropologi',
				sortable: true, 
				dataIndex: '215'
			},{
				xtype: 'checkcolumn',
				header: '217 Seni Budaya',
				width: 130,
				tooltip: 'Seni Budaya',
				sortable: true, 
				dataIndex: '217'
			},{
				xtype: 'checkcolumn',
				header: '220 Pendidikan Jasmani dan Kesehatan',
				width: 130,
				tooltip: 'Pendidikan Jasmani dan Kesehatan',
				sortable: true, 
				dataIndex: '220'
			},{
				xtype: 'checkcolumn',
				header: '224 Teknologi Informasi dan Komunikasi (TIK)',
				width: 130,
				tooltip: 'Teknologi Informasi dan Komunikasi (TIK)',
				sortable: true, 
				dataIndex: '224'
			},{
				xtype: 'checkcolumn',
				header: '227 Keterampilan',
				width: 130,
				tooltip: 'Keterampilan',
				sortable: true, 
				dataIndex: '227'
			},{
				xtype: 'checkcolumn',
				header: '330 Keterampilan Komputer dan Pengelolaan Informasi (K',
				width: 130,
				tooltip: 'Keterampilan Komputer dan Pengelolaan Informasi (K',
				sortable: true, 
				dataIndex: '330'
			},{
				xtype: 'checkcolumn',
				header: '331 Kewirausahaan',
				width: 130,
				tooltip: 'Kewirausahaan',
				sortable: true, 
				dataIndex: '331'
			},{
				xtype: 'checkcolumn',
				header: '800 Guru Kelas SDLB',
				width: 130,
				tooltip: 'Guru Kelas SDLB',
				sortable: true, 
				dataIndex: '800'
			},{
				xtype: 'checkcolumn',
				header: '810 Bimbingan dan Konseling (Konselor)',
				width: 130,
				tooltip: 'Bimbingan dan Konseling (Konselor)',
				sortable: true, 
				dataIndex: '810'
			},{				
				header: 'Tahun',
				width: 80,
				tooltip: 'lainnya',
				sortable: true, 
				align: 'center',
				dataIndex: 'VersiTahun'
			}]
		});
		
		/*
		var groupRow = [
			{header: 'Sertifikasi', colspan: 2, align: 'center'},
			{header: 'Mata Pelajaran', colspan: 2, align: 'center'}
		];
		
		var group = new Ext.ux.grid.ColumnHeaderGroup({
			rows: groupRow
		});
		*/
		
		var quotaGroupRow = [
			{header: 'Sertifikasi', colspan: 2, align: 'center'},
			{header: 'Mata Pelajaran', colspan: 2, align: 'center'}
		];
		
		//var group = new Ext.ux.grid.ColumnHeaderGroup({
		var group = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [quotaGroupRow]
		});
		
		var filterTahun = new Xond.grid.ReferenceComboBox({
			xtype: 'referencecombobox',
			name: 'FilterTahun',
			width: 80,
			options: [[2006,'2006'],[2007,'2007'],[2008,'2008'],[2009,'2009'],[2010,'2010'],[2011,'2011'],[2012,'2012']],
			idField: 'FilterTahun',
			anchor: '100%',
			emptyText: 'Pilih Tahun...',
			optionsField: 'Nama'
		});

		thiswindow.ds = ds;
		thiswindow.cm = cm;
		
		var grid = new Ext.grid.EditorGridPanel({
			ds: ds,
			cm: cm,
			flex: 3,
			//plugins: group,
			view: new Ext.ux.grid.LockingGridView(),
			//width: 300,
			anchor: '100%',
			//height: 220,
			frame: false,
			//title: 'Daftar Kasus',
			iconCls: 'icon-grid',
			//renderTo: 'browse_detil',
			stripeRows: true,
			loadMask: true,
			region: 'center',
			sm : new Ext.grid.RowSelectionModel({
				singleSelect : true
			}),
			tbar: [{
				text: 'Simpan',
				iconCls: 'save',
				handler: function(){
					
					var jsonData = "[";
					var store = grid.getStore();
					var countDirty = 0;
					
					for(i=0;i < store.getCount(); i++) {
						record = store.getAt(i);
						if (record.dirty == true) {
							jsonData += Ext.util.JSON.encode(record.data) + ",";
							countDirty++;
						}
					}
					jsonData = jsonData.substring(0,jsonData.length-1) + "]";
					
					if (countDirty < 1) {
						Ext.Msg.alert('Info', 'Tidak ada perubahan');
						return;
					}
					
					Ext.Ajax.request({
						waitMsg: 'Menyimpan...',			
						url: 'Mapping/save.json',					
						method: 'POST',					
						params: {
							rows: jsonData
						},	
						failure:function(response,options){						   
							Ext.Msg.alert('Warning','Response : ' + response );
						},
						success: function(response,options){
							var json = Ext.util.JSON.decode(response.responseText);
								
							if (json.success) {					
									//win.hide();
									grid.getStore().removeAll();
									grid.getStore().reload();
									Ext.Msg.alert('OK', json.message);
									
							} else if (json.success == false){
									Ext.Msg.alert('Info', json.message);
							}
						
						}				
					}); 
				}
			},'-',{
				
				text: 'Refresh Jumlah Guru',
				iconCls: 'refresh',
				handler: function() {
					
					Ext.Ajax.request({
						waitMsg: 'Menyimpan...',			
						url: 'Mapping/refreshJumlahGuru.json',					
						method: 'POST',					
						failure:function(response,options){						   
							Ext.Msg.alert('Warning','Response : ' + response );
						},
						success: function(response,options){
							var json = Ext.util.JSON.decode(response.responseText);
								
							if (json.success) {					
									//win.hide();
									grid.getStore().removeAll();
									grid.getStore().reload();
									Ext.Msg.alert('OK', json.message);
									
							} else if (json.success == false){
									Ext.Msg.alert('Info', json.message);
							}
						
						}				
					}); 

				}
				
			},'-',{
				
				text: 'Cari kolom',
				iconCls: 'zoom',
				handler: function() {
					//console.log(thiswindow.cm);
					//thiswindow.cm.columns.each(function(col){
					//cm.setHidden(cm.findColumnIndex(''), true);	
					//});
					//Ext.MessageBox.prompt('Cari Kolom', 'Masukkan kode matpel (3 digit):', function(btn, txt){
					Ext.MessageBox.prompt('Cari Kolom', 'Masukkan teks pencarian:', function(btn, txt){
						
						//var colIndex = thiswindow.cm.findColumnIndex(txt);
						
						if (txt != "") {					
						
							//if (!colIndex) {
							//	Ext.Msg.alert('Error', 'Kolom tidak ditemukan');
							//	return;
							//}
							var count = thiswindow.cm.getColumnCount();
							for (var i = 3; i < (count-1); i++ ) {
								//console.log(thiswindow.cm.getColumnId(i));
								/*
								if (i != colIndex) {
									cm.setHidden(i, true);
								} else {
									cm.setHidden(i, false);
								}
								*/
								var col = thiswindow.cm.getColumnById(i);
								//console.log(col);
								if (col.header.match(new RegExp(txt, "i"))) {
									//cm.setHidden(i, true);
									cm.setHidden(i, false);
								} else {									
									cm.setHidden(i, true);
								}
								
							}
						
						} else {
							
							var count = thiswindow.cm.getColumnCount();
							for (var i = 3; i < count; i++ ) {
								//console.log(thiswindow.cm.getColumnId(i));
								cm.setHidden(i, false);								
							}
							
						}
						
					});
					
					
				}
			},'-',{
				text: 'Reset',
				iconCls: 'asterisk_yellow',
				handler: function(){
					var count = thiswindow.cm.getColumnCount();
					for (var i = 3; i < count; i++ ) {
						//console.log(thiswindow.cm.getColumnId(i));
						cm.setHidden(i, false);								
					}
				}
			}, '-', {
				
				text: 'Update JJM',
				iconCls: 'refresh',
				handler: function(){
					
					Ext.Ajax.request({
			
						//waitMsg: 'Menyimpan...',						
						url: '/start_bg.php',					
						method: 'POST',					
						failure:function(response,options){						   
							Ext.Msg.alert('Warning','Response : ' + response );
						},
						success: function(response,options){							
				
							Ext.MessageBox.show({
							   title: 'Mohon Tunggu',
							   msg: 'Mengupdate JJM...',
							   progressText: 'Memulai...',
							   width:300,
							   progress:true,
							   closable:false,
							   animEl: 'mb6'
							});							
							
							thiswindow.updateProgress();					
							
						}
					});
				}
			}, '-', filterTahun ,'->',{
				text: 'Muat',
				iconCls: 'refresh',
				handler: function() {					
					thiswindow.ds.reload();
				}
			}]
			/*
			bbar: [
				new Ext.PagingToolbar({
					pageSize	: 100,
					store		: thiswindow.ds,
					paramNames  : {start: 'start', limit: 'limit'},
					displayInfo	: true,
					displayMsg	: 'Menampilkan {0} - {1} dari {2}',
					emptyMsg	: "Data tidak ditemukan"
				})
			]
			*/
		});
		
		
		Ext.apply(this, {
			grid: grid,
			items:  [				
				grid
			]
		});
		
		thiswindow.ds.load({
			tahun: 2012
		});
		filterTahun.setValue(2012);
		
		thiswindow.ds.on({
			'beforeload':{
				fn: function(store, options){
					Ext.apply(options.params, {						
						tahun: filterTahun.getValue()
					});
				}
			}
		});

		
		simptk.Mapping.superclass.constructor.call(this);

	}
});