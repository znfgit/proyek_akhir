Ext.onReady(function(){
	
	Ext.Ajax.timeout = 600000;
	setTimeout(function(){
		//simptk.session_propinsi_id = 10;
		//simptk.session_kabupaten_kota_id = 146;
		//simptk.session_sekolah = '';		
		//simptk.kecamatan_selected = "";
		//simptk.kabkota_selected = "";
		//simptk.propinsi_selected = "";
		// console.log(simptk.session_propinsi_id);
		// console.log(simptk.session_kabupaten_kota_id);
		if (simptk.session_propinsi_id) {
			var propIdx = simptk.PropinsiStore.find('PropinsiId', simptk.session_propinsi_id);
			var propRec = simptk.PropinsiStore.getAt(propIdx);
			simptk.propinsi_selected = propRec;
		}
		if (simptk.session_kabupaten_kota_id) {			
			var kabkotaIdx = simptk.KabupatenKotaStore.find('KabupatenKotaId', simptk.session_kabupaten_kota_id);
			var kabkotaRec = simptk.KabupatenKotaStore.getAt(kabkotaIdx);
			simptk.kabkota_selected = kabkotaRec;
		}
	}, 1000);
	
	var panel = new Ext.Panel({
		title: 'Sistem Validasi Data Guru - Dit.P2TK Dikdas',
		tbar: [{
			xtype: 'buttongroup',
			columns: 1,
			title: 'Home',
			items: [{
				text: 'Beranda',
				scale: 'large',
				rowspan: 3, 
				iconCls: 'home',
				iconAlign: 'top',
				cls: 'x-btn-as-arrow'					
			}]
		},{
			xtype: 'buttongroup',
			columns: 4,
			title: 'Manajemen',
			items: [{
				text: 'Rekap & Sync',
				scale: 'large',
				rowspan: 3, 
				iconCls: 'view-refresh-6',
				iconAlign: 'top',
				cls: 'x-btn-as-arrow',
				arrowAlign:'bottom',
				handler: function(){
					//Ext.Msg.alert('Hai', 'Test');
					//var win = new simptk.KelulusanGuru();
					//var win = new Ext.Window();
					//win.show();
					var win = new simptk.Rekap();
					win.show();					
				}
			},{
				text: 'E-mail Broadcast',
				scale: 'large',
				rowspan: 3, 
				iconCls: 'mail-message',
				iconAlign: 'top',
				cls: 'x-btn-as-arrow',
				arrowAlign:'bottom',
				handler: function(){
					//var win = new simptk.Mapping();
					//win.show();					
					var win = new simptk.MailBroadcast();
					win.show();
				}
			},{
				text: 'Mapping Bid.Studi',
				scale: 'large',
				rowspan: 3, 
				iconCls: 'checklist-icon',
				iconAlign: 'top',
				cls: 'x-btn-as-arrow',
				arrowAlign:'bottom',
				handler: function(){
					var win = new simptk.Mapping();
					win.show();					
				}
			},{
				text: 'Sekolah',
				scale: 'large',
				rowspan: 3, 
				iconCls: 'logo-sekolah',
				iconAlign: 'top',
				cls: 'x-btn-as-arrow',
				arrowAlign: 'bottom',
				handler: function(){
					//var win = new simptk.OptimalisasiGuruSekolah({
					//	skup: 'Kabupaten/Kota'
					//});
					//win.show();
					var win = new simptk.Sekolah();
					win.show();
				}		
			},{
				text: 'Guru',
				scale: 'large',
				rowspan: 3, 
				iconCls: 'logo-guru',
				iconAlign: 'top',
				cls: 'x-btn-as-arrow',
				arrowAlign:'bottom',
				text: 'Guru Sekolah',
				handler: function(){
					//var win = new simptk.OptimalisasiGuruSekolah({
					//	skup: 'Kabupaten/Kota'
					//});
					//win.show();
				}		
			},{
				text: 'Report',
				scale: 'large',
				rowspan: 3, 
				iconCls: 'report_nas',
				iconAlign: 'top',
				cls: 'x-btn-as-arrow',
				arrowAlign:'bottom',
				text: 'Report',
				handler: function(){
					var win = new simptk.ViewViewer({				
					});
					win.show();
				}		
			}/*,{
				text: 'Output HTML',
				scale: 'large',
				rowspan: 3, 
				iconCls: 'tbantuan-daftar',
				iconAlign: 'top',
				cls: 'x-btn-as-arrow',
				arrowAlign:'bottom',
				handler: function(){
					var win = new simptk.CariWilayah();
					win.on('wilayahselected', function(win, kabkota_selected, propinsi_selected){
						//updateInfo(sekolah, sekolah.data.NamaKabupatenKota, sekolah.data.NamaPropinsi, false, false, false, false, false);
					});
					win.show();					
					// window.open('http://116.66.201.163:8083/result/'+ kabupaten_kota_id +'.html');
				}
			}*/]
		},{
			xtype: 'buttongroup',
			columns: 1,
			title: 'Aplikasi',
			items: [{
				text: 'Logout',
				scale: 'large',
				rowspan: 3, 
				iconCls: 'application-exit',
				iconAlign: 'top',
				cls: 'x-btn-as-arrow',
				handler: function(){
					document.location = '/auth/logout.php';
				}
			}]
		}]
	});

	var viewport = new Ext.Viewport({
		layout:'fit',
		items:[
			panel	
		 ]
	});
});