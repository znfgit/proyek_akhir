
var recordSetViewViewer = [
	{ name: 'TViewId', type: 'string'  } ,
	{ name: 'Kode', type: 'string'  } ,
	{ name: 'Nama', type: 'string'  }   
];
		
var storeViewViewer = new Ext.data.Store({
	reader: new Ext.data.JsonReader({
		id: 'TViewId',
		root: 'rows',
		totalProperty: 'results',
		fields: recordSetViewViewer
	}),
	url: './ViewViewer/fetch.json',
	autoLoad: false
});

var cmViewViewer = new Ext.grid.ColumnModel({ 
	defaults: {
		sortable: true // columns are not sortable by default		   
	},
	columns : [{				
		header: 'TViewId',
		width: 40,			
		align: 'center',
		sortable: true, 
		hidden: false,
		dataIndex: 'TViewId'
	},{				
		header: 'Nama',
		width: 100,
		tooltip: 'Nama',
		sortable: true, 
		dataIndex: 'Nama'
	}]
});

simptk.ViewViewer = Ext.extend(Ext.Window, {
	/* Standard configs */
	title: 'Report',
	border: true,
	layout: {
		type: 'fit'
			//,		align: 'stretch'
	},
	width: 600,
	height: 400,
	modal: true,
	viewConfig: {forceFit: true},
	
	constructor: function(config){
		
		var thiswindow = this;
		
		Ext.apply(this, config);

		simptk.RecordSetTViews = [
			{ name: 'TViewsId', type: 'float'  } ,
			{ name: 'Nama', type: 'string'  } ,
			{ name: 'Deskripsi', type: 'string'  }     
		];

		simptk.RecordObjTViews = Ext.data.Record.create(simptk.RecordSetTViews);

		simptk.StoreTViews = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'TViewsId',
				root: 'rows',
				totalProperty: 'results',
				fields: simptk.RecordSetTViews        
			}),
			autoLoad: true,        
			url: './data/TViews.json',
			sortInfo: {field: 'TViewsId', direction: "ASC"}
		});
		
		simptk.ViewsCombo = Ext.extend(Ext.form.ComboBox, {
			store: simptk.StoreTViews,
			displayField: 'Deskripsi',
			valueField: 'TViewsId',			
			typeAhead: true,
			mode: 'local',
			forceSelection: true,
			triggerAction: 'all',
			emptyText: 'Pilih Laporan ...',
			selectOnFocus:true,
			width: 100,
			initComponent: function(){
				simptk.Tahun.superclass.initComponent.call(this);
			}
		});
		Ext.reg('viewscombo', simptk.ViewsCombo); 
							
		var grid = new Ext.grid.GridPanel({
			flex: 1,			
			store: storeViewViewer,
			cm: cmViewViewer,
			viewConfig: {
				forceFit: true
			},
			tbar: [{
				text: 'Pilih Laporan : ',
				enabled: false
			}, {
				xtype: 'viewscombo',
				width: 300,
				listeners : {
					select: function(thiscombo, record, index) {
						//
						Ext.Ajax.request({
							scope: window,
							waitMsg: 'Menyimpan...',						
							url: 'ViewViewer/getStore.json',
							method: 'POST',		
							params: {
								view_id: thiscombo.getValue()
							},
							failure:function(response,options){						   
								Ext.Msg.alert('Warning','Response : ' + response );
							},
							success: function(response,options){
								
								var json = Ext.util.JSON.decode(response.responseText);
								//console.log(json.record_set);
								//console.log(json.columns);
								
								recordSetViewViewer = json.record_set;
								storeViewViewer = new Ext.data.Store({
									reader: new Ext.data.JsonReader({
										id: json.id,
										root: 'rows',
										totalProperty: 'results',
										fields: recordSetViewViewer
									}),
									autoLoad: true,        
									url: 'ViewViewer/getData.json',
									sortInfo: {field: json.id, direction: "ASC"}
								});
								
								cmViewViewer = new Ext.grid.ColumnModel({ 
									defaults: {
										sortable: true // columns are not sortable by default		   
									},
									columns: json.columns
								});
								
								grid.reconfigure(storeViewViewer, cmViewViewer);
							}
						});
						
					}
				}				
			}]
		});
		
		Ext.apply(this, {
			grid: grid,
			items:  [				
				grid
			]
		});
		
		simptk.ViewViewer.superclass.constructor.call(this);

	}
});