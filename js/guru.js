simptk.ValidasiGuru = Ext.extend(Ext.Window, {
	/* Standard configs */
	title: 'Data Pra Penilaian Angka Kredit',
	border: true,
	//bodyStyle: 'padding: 5px;',
	//layout: 'vbox',
	//align:'stretch',
	layout: {
		type:'vbox',			
		align:'stretch'
	},
	width: 1024,
	height: 500,
	modal: true,
	viewConfig: {forceFit: true},
	
	/* Custom configs */
	// gridValidasiGuru: null, 
	// ds: null,
	// cm: null,
	//jenjang: null,
	
	constructor: function(config){		
		
		var window = this;
				
		Ext.apply(this, config);
		
		//function updateInfoPanel(ptk_penuhi_syarat, ptk_tidak_penuhi_syarat, ptk_hasil_matching, ptk_berdasarkan_nrg) {
		function updateInfoPanel() {
			window.infoPanel.update('<div style="padding:10px 10px 10px 10px"><table border=0 style="font-size: 9pt;">'
			  + '<tr><td>Kab/Kota </td><td> :&nbsp;&nbsp;</td><td><b>' + simptk.kabkota_selected.data.Nama + ", " + simptk.propinsi_selected.data.Nama  + '</b></td></tr>'	
			  /*+ '<tr><td>PTK Penuhi Syarat </td><td> :&nbsp;&nbsp;</td><td><b>' + ptk_penuhi_syarat + ' </b> orang</td></tr>'
			  + '<tr><td>PTK Tidak Penuhi Syarat </td><td> :&nbsp;&nbsp;</td><td><b>' + ptk_tidak_penuhi_syarat + ' </b> orang</td></tr>'
			  + '<tr><td>Jumlah PTK hasil matching</td><td> :&nbsp;&nbsp;</td><td><b>' + ptk_hasil_matching + ' </b> orang</td></tr>'
			  + '<tr><td>Jumlah PTK berdasarkan NRG</td><td> :&nbsp;&nbsp;</td><td><b>' + ptk_berdasarkan_nrg + ' </b> orang</td></tr>'*/
			  + '</table></div>');
		}
	
		var cmbTahun = new simptk.Tahun();
		
		// var cmbTahun = new Ext.form.ComboBox({
			// store: store,
			// fieldLabel: 'Propinsi',
			// displayField:'state',
			// typeAhead: true,
			// width: 150,
			// mode: 'local',
			// forceSelection: true,
			// triggerAction: 'all',
			// emptyText:'Tahun Lulus...',
			// selectOnFocus:true,
		// });
		
		function onItemCheckJenis(item, checked){
			switch (item.text) {
				case '--Semua--' :
					simptk.filter_jenis_lingkup_bantuan = "0";
					break;
				case 'DAU' : 
					simptk.filter_jenis_lingkup_bantuan = "1";
					break;
				case 'DEKON' :
					simptk.filter_jenis_lingkup_bantuan = "2";
					break;
			}
		}
		
		function onItemCheckSyarat(item, checked){
			
			switch (item.text) {
				case '--Semua--' :
					simptk.filter_syarat_24_jam = "0";
					break;
				case 'Terpenuhi' : 
					simptk.filter_syarat_24_jam = "1";
					break;
				case 'Tidak Terpenuhi' :
					simptk.filter_syarat_24_jam = "2";
					break;
			}
		}
		
		simptk.RecordSetTPraDupak = [
			{ name: 'TPtkId', type: 'string'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  } ,
			{ name: 'JenisKelamin', type: 'float'  } ,
			{ name: 'IjazahTerakhirId', type: 'float'  } ,
			{ name: 'TahunIjazahTerakhir', type: 'float'  } ,
			{ name: 'GelarAkademikDepanId', type: 'float'  } ,
			{ name: 'GelarAkademikBelakangId', type: 'float'  } ,
			{ name: 'NiyNigk', type: 'string'  } ,
			{ name: 'Nuptk', type: 'string'  } ,
			{ name: 'TempatLahir', type: 'string'  } ,
			{ name: 'TglLahir', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'Nik', type: 'string'  } ,
			{ name: 'AgamaId', type: 'float'  } ,
			{ name: 'StatusPerkawinan', type: 'float'  } ,
			{ name: 'JumlahAnak', type: 'float'  } ,
			{ name: 'NamaIbuKandung', type: 'string'  } ,
			{ name: 'AlamatJalan', type: 'string'  } ,
			{ name: 'Rt', type: 'string'  } ,
			{ name: 'Rw', type: 'string'  } ,
			{ name: 'NamaDesaKelurahan', type: 'string'  } ,
			{ name: 'KodePos', type: 'string'  } ,
			{ name: 'KecamatanId', type: 'float'  } ,
			{ name: 'KabupatenKotaId', type: 'float'  } ,
			{ name: 'NoTeleponRumah', type: 'string'  } ,
			{ name: 'NoHp', type: 'string'  } ,
			{ name: 'Email', type: 'string'  } ,
			{ name: 'StatusKepegawaianId', type: 'float'  } ,
			{ name: 'TmtSekolah', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'JabatanPtkId', type: 'float'  } ,
			{ name: 'TmtJabatan', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'JabatanPtkSebelumnyaId', type: 'float'  } ,
			{ name: 'SertifikasiJabatan', type: 'float'  } ,
			{ name: 'TahunSertifikasiJabatan', type: 'float'  } ,
			{ name: 'NomorSertifikat', type: 'string'  } ,
			{ name: 'Nip', type: 'string'  } ,
			{ name: 'TmtPns', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'PangkatGolonganId', type: 'float'  } ,
			{ name: 'TmtPangkatGol', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'KodeSertifikasiBidangStudiGuruId', type: 'float'  } ,
			{ name: 'KodeProgramKeahlianLaboranId', type: 'float'  } ,
			{ name: 'SudahLisensiKepalaSekolah', type: 'float'  } ,
			{ name: 'JenjangKepengawasanId', type: 'float'  } ,
			{ name: 'PengawasBidangRumpunId', type: 'float'  } ,
			{ name: 'PengawasMapelId', type: 'float'  } ,
			{ name: 'JumlahSekolahBinaan', type: 'float'  } ,
			{ name: 'PernahMengikutiDiklatKepengawasan', type: 'float'  } ,
			{ name: 'NamaSuamiIstri', type: 'string'  } ,
			{ name: 'PekerjaanId', type: 'float'  } ,
			{ name: 'NipSuamiIstri', type: 'string'  } ,
			{ name: 'SekolahId', type: 'string'  } ,
			{ name: 'SimNuptk', type: 'string'  } ,
			{ name: 'Status', type: 'float'  } ,
			{ name: 'StatusData', type: 'float'  } ,
			{ name: 'JabatanFungsional', type: 'float'  } ,
			{ name: 'LembagaPengangkat', type: 'float'  } ,
			{ name: 'NomorSkPengangkatan', type: 'string'  } ,
			{ name: 'TmtPengangkatan', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'SumberGaji', type: 'float'  } ,
			{ name: 'NomorSkKgb', type: 'string'  } ,
			{ name: 'TmtKgb', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'Nrg', type: 'string'  } ,
			{ name: 'NamaBidangStudiSertifikasi', type: 'string'  } ,
			{ name: 'BidangStudiSertifikasiId', type: 'float'  } ,
			{ name: 'TglLulusSertifikasi', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'NamaSekolah', type: 'string'  } ,
			{ name: 'StatusSekolah', type: 'float'  } ,
			{ name: 'NamaJenisSekolah', type: 'string'  } ,
			{ name: 'JenisSekolahId', type: 'float'  } ,
			{ name: 'NamaKabupatenKota', type: 'string'  } ,
			{ name: 'KabupatenKotaSekolahId', type: 'float'  } ,
			{ name: 'NamaPropinsi', type: 'string'  } ,
			{ name: 'PropinsiSekolahId', type: 'float'  } ,
			{ name: 'NamaPangkatGolongan', type: 'string'  } ,
			{ name: 'NamaStatusKepegawaian', type: 'string'  } ,
			{ name: 'NamaTugasTambahan', type: 'string'  } ,
			{ name: 'TugasTambahanId', type: 'float'  } ,
			{ name: 'JamTugasTambahan', type: 'float'  } ,
			{ name: 'JumlahJamMengajar', type: 'float'  } ,
			{ name: 'JumlahJamMengajarSesuai', type: 'float'  } ,
			{ name: 'JumlahJamMengajarSesuaiKtsp', type: 'float'  } ,
			{ name: 'TotalJamMengajarSesuai', type: 'float'  } ,
			{ name: 'MasaKerjaTahun', type: 'string'  } ,
			{ name: 'MasaKerjaBulan', type: 'string'  } ,
			{ name: 'TglPensiun', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'SedangKuliah', type: 'string'  } ,
			{ name: 'BertugasDiWilayahTerpencil', type: 'string'  } ,
			{ name: 'LastUpdate', type: 'string'  } ,
			{ name: 'NamaRekening', type: 'string'  } ,
			{ name: 'NoRekening', type: 'string'  } ,
			{ name: 'NamaBank', type: 'string'  } ,
			{ name: 'CabangBank', type: 'string'  } ,
			{ name: 'SatuanPendidikanFormal', type: 'string'  } ,
			{ name: 'Fakultas', type: 'string'  } ,
			{ name: 'BidangStudiId', type: 'float'  } ,
			{ name: 'JenjangPendidikanId', type: 'float'  } ,
			{ name: 'TahunMasuk', type: 'float'  } ,
			{ name: 'Semester', type: 'float'  } ,
			{ name: 'StatusNuptk', type: 'float'  } ,
			{ name: 'StatusSyaratTProfesi', type: 'float'  } ,
			{ name: 'StatusSyaratTFungsional', type: 'float'  } ,
			{ name: 'StatusSyaratTKualifikasi', type: 'float'  } ,
			{ name: 'StatusSyaratTKhusus', type: 'float'  }     
		];
		
		//simptk.StoreTPraDupak = new Ext.data.Store({
		simptk.StoreTPraDupak = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'TPtkId',
				root: 'rows',
				totalProperty: 'results',
				fields: simptk.RecordSetTPraDupak        
			}),
			url: './Sinkronisasi/fetch.json',
			autoLoad: false,        
			//url: './data/TPraDupak.json',			
			//groupField: 'NamaStatusDokumen',
			sortInfo: {field: 'NamaSekolah', direction: "ASC"}
		});
		
		
		var filterSyarat = new Xond.grid.ReferenceComboBox ({
	
			/* Standard configs */
			xtype: 'referencecombobox',
			name: 'FilterMemenuhiSyarat',
			options: [[999,'Semua'],[3,'Siap Verifikasi'],[2,'Masih Edit Data'],[1,'Belum Update Data']],
			idField: 'FilterMemenuhiSyarat',
			anchor: '100%',
			emptyText: 'Pilih...',
			optionsField: 'Nama',
			listeners: {
				select: function(thiscombo, record, index) {
					//simptk.jenjang_selected = thiscombo.getValue();
					simptk.StoreTPraDupak.load({
						params: {
							kabupaten_kota_id: simptk.kabkota_selected.data.KabupatenKotaId,
							filter_nama : filterNama.getValue(),
							/*
							filter_syarat_24_jam : simptk.filter_syarat_24_jam,
							filter_jenis_lingkup_bantuan : simptk.filter_jenis_lingkup_bantuan,
							filter_memenuhi_syarat : filterSyarat.getValue(),
							filter_nama : filterNama.getValue(),
							filter_daerah_khusus: filterDaerahDarurat.getValue(),
							tahun_lulus : cmbTahun.getValue(),
							*/
							start: 0,
							limit: 30,
							terpilih: 1
						}
					});

				}
			}
		});
		
		var filterNama = new Ext.form.TextField({
			xtype: 'textfield',
			enableKeyEvents: true
		});
		
		filterNama.on('keypress', function(field, e){
			simptk.cari_text = field.getValue();
			if (e.getKey() == e.ENTER){
				simptk.StoreTPraDupak.load({
					params: {
						kabupaten_kota_id: simptk.kabkota_selected.data.KabupatenKotaId,
						filter_nama : filterNama.getValue(),
						/*filter_syarat_24_jam : simptk.filter_syarat_24_jam,
						filter_jenis_lingkup_bantuan : simptk.filter_jenis_lingkup_bantuan,
						filter_memenuhi_syarat : filterSyarat.getValue(),
						filter_nama : filterNama.getValue(),
						filter_daerah_khusus: filterDaerahDarurat.getValue(),
						tahun_lulus : cmbTahun.getValue(),
						*/
						start: 0,
						limit: 30,
						terpilih: 1
					}
				});
			}			
		});

				
		var filterDaerahDarurat = new Xond.grid.ReferenceComboBox ({
	
			/* Standard configs */
			xtype: 'referencecombobox',
			name: 'FilterDaerahKhusus',
			options: [[1, 'Semua Sekolah'],[2,'Sekolah Daerah Khusus']],
			idField: 'FilterDaerahKhusus',
			anchor: '100%',
			emptyText: 'Pilih...',
			optionsField: 'Nama',
			listeners: {
				select: function(thiscombo, record, index) {
					//simptk.jenjang_selected = thiscombo.getValue();
					simptk.StoreTPraDupak.load({
						params: {
							kabupaten_kota_id: simptk.kabkota_selected.data.KabupatenKotaId,
							filter_nama : filterNama.getValue(),
							/*
							filter_syarat_24_jam : simptk.filter_syarat_24_jam,
							filter_jenis_lingkup_bantuan : simptk.filter_jenis_lingkup_bantuan,
							filter_memenuhi_syarat : filterSyarat.getValue(),
							filter_nama : filterNama.getValue(),
							filter_daerah_khusus: filterDaerahDarurat.getValue(),
							tahun_lulus : cmbTahun.getValue(),
							*/
							start: 0,
							limit: 30,
							terpilih: 1
						}
					});

				}
			}
		});
			
		var gridValidasiGuru = new Ext.grid.GridPanel({
			flex: 4,
			title: 'Data PTK',
			store: simptk.StoreTPraDupak,
			// hideHeaders: true,
			/*
			view: new Ext.grid.GroupingView({
                //forceFit: true,
                showGroupName: false,
                hideGroupedColumn: true
            }),
			*/
			tbar: [{
				text: 'Cari Sekolah: '
			}, filterNama, '->', {
				text: 'Muat Data',
				tooltip: 'Klik untuk memuat data dari Data Pra SK',
				scope: this,
				iconCls: 'refresh',
				handler: function(){
					simptk.StoreTPraDupak.load({
						params: {
							kabupaten_kota_id: simptk.kabkota_selected.data.KabupatenKotaId,
							/*
							filter_syarat_24_jam : simptk.filter_syarat_24_jam,
							filter_jenis_lingkup_bantuan : simptk.filter_jenis_lingkup_bantuan,
							filter_memenuhi_syarat : 3,
							tahun_lulus : cmbTahun.getValue(),
							*/
							start: 0,
							limit: 30,
							terpilih: 1
						}
					});
				}
			}],
				/*
				text: 'Cek Syarat',
				tooltip: 'Klik untuk menampilkan syarat',
				scope: this,
				iconCls: 'cek',
				handler: function(){
					var rec = gridValidasiGuru.getSelectionModel().getSelected();
					if (!rec) {
						Ext.Msg.alert('Error', 'Pilih dulu salah satu');
						return;
					}
					
					var vldDs = new Ext.data.Store({
						reader: new Ext.data.JsonReader({
							id: 'No',
							root: 'rows',
							totalProperty: 'results',
							fields: [
							   {name: 'No', type: 'int'},
							   {name: 'Status', type: 'int'},
							   {name: 'Deskripsi', type: 'string'},
							   {name: 'NilaiYbs', type: 'string'},
							   {name: 'SaranPenyelesaian', type: 'string'}
							]
						}),
						url: './Sinkronisasi/fetchCekSyarat.json',
						autoLoad: false,						
						sortInfo: {field: 'No', direction: "ASC"}
					});
					
					var vldCm = new Ext.grid.ColumnModel({ 
						defaults: {
							sortable: true // columns are not sortable by default		   
						},
						columns : [{				
							header: 'No',
							width: 40,			
							align: 'center',
							tooltip: 'No',						
							sortable: true, 
							hidden: false,
							dataIndex: 'No'
						},{				
							header: 'Deskripsi Syarat',
							width: 290,
							tooltip: 'uraian',
							sortable: true, 
							dataIndex: 'Deskripsi',
							renderer: function (value, metadata, record, rowIndex, colIndex, store) {
								//metadata.attr = 'ext:qtip="' + value + '"';
								var strSolusi = (record.data.Status == 3) ?  "" : ('<br>Rekomendasi: ' + record.data.SaranPenyelesaian);
								var str = '<div style="white-space:normal !important;"><b>' + record.data.Deskripsi + '</b><br>Data: ' + record.data.NilaiYbs + strSolusi + '</div>';
								return str;
							}
						},{
							header: '',
							width: 30,
							align: 'center',
							tooltip: 'Status',							
							sortable: true, 
							renderer: function(value, metadata, record, rowIndex, colIndex, store) {
								var img = "";
								var tip = "";
								var str = "";
								img = (value == 3) ? "tick" : "error";
								tip = (value == 3) ? "Memenuhi" : "Tidak Memenuhi";
								/*
								switch (value) {
									case 1:
										img = 'exclamation';										
										str = 'Tidak Memenuhi';
										tip = 'Warning';
										break;
									case 2:
										img = 'exclamation';
										str = 'Tidak Memenuhi';
										tip = 'Warning';
										break;
									case 3:
										img = 'tick';										
										str = 'Memenuhi';
										tip = 'OK';
										break;				
									case 6:
										img = 'error';
										str = 'Tidak Memenuhi';
										tip = 'Invalid';
										break;
								}
								* /
								metadata.attr = 'ext:qtip="' + tip + '"';
								var imgStr = '<div style="background: url(/assets/icons/'+ img +'.png) no-repeat left center; padding-left:22px; height:15px;"></div>';	
								return imgStr;
							},
							dataIndex: 'Status'
						},{
							header: 'Status',
							width: 110,
							align: 'left',
							tooltip: 'Status',							
							sortable: true, 
							dataIndex: 'Status',
							renderer: function(value, metadata, record, rowIndex, colIndex, store) {
								return (value == 3) ? 'Memenuhi' :  'Tidak Memenuhi';
							}
						},{				
							header: 'Nilai YBS',
							width: 200,
							tooltip: 'uraian',
							sortable: true, 
							hidden: true,
							dataIndex: 'NilaiYbs'
						}]
					});
						
					var vldGrid = new Ext.grid.EditorGridPanel({
						ds: vldDs,
						cm: vldCm,
						flex: 3,
						//width: 300,
						anchor: '100%',
						//height: 220,
						frame: false,
						//title: 'Daftar Kasus',
						iconCls: 'icon-grid',
						//renderTo: 'browse_detil',
						stripeRows: true,
						loadMask: true,
						region: 'center',
						sm : new Ext.grid.RowSelectionModel({
							singleSelect : true
						})
					});
					
					var panelInfo = new Ext.Panel({
						flex: 1,
						// anchor: '100%',
						//height: 60,
						html: '<div style="padding:10px 10px 10px 10px;"><table border=0 style="font-size: 9pt;">' + 
							 '<tr><td>Nama PTK</td><td> :&nbsp;&nbsp;</td><td><b>' + rec.data.Nama + ' </b></td></tr>'+
							 '<tr><td>Update Terakhir</td><td> :&nbsp;&nbsp;</td><td><b>' + rec.data.TimestampLastSync + ' </b></td></tr>'+
							  '</div>'
					});
				  
					var vldWin = new Ext.Window({
						width: 510,
						title: 'Cek Persyaratan',
						height: 350,
						//layout: 'fit',
						layout: {
							type:'vbox',			
							align:'stretch'
						},
						modal: true,
						items: [
							panelInfo,
							vldGrid
							/*,
							{
								xtype: 'panel',
								id: 'summarypanel',
								width: 500,
								anchor: '100%',
								height: 100,
								frame: false,
								title: 'Rekap',
								html: '<div style="padding:10px 10px 10px 10px">Counting..</div>'
							}* /
						],
						buttons: [{
							text: 'Ok',
							iconCls: 'cek',
							handler: function(thisbutton) {
								vldWin.close();
							}
						}]
					});
					vldDs.load({
						params: {
							nuptk: rec.data.Nuptk
						}
					});
					vldWin.show();
				}
			},'-',{
				text: 'Unduh Pengecekan Syarat',
				tooltip: 'Klik untuk mengunduh pengecekan syarat',
				scope: this,
				iconCls: 'xls',
				handler: function(){
					var rec = gridValidasiGuru.getSelectionModel().getSelected();
					if (!rec) {
						Ext.Msg.alert('Error', 'Pilih dulu salah satu');
						return;
					}
				}
			},'-', filterSyarat, '-', filterNama, '-', filterDaerahDarurat ],
			*/		
			columns : [{                
                header: 'TPraDupakId',
                width: 80,
                sortable: true, 
				hidden: true,
                dataIndex: 'TPraDupakId'
            },{                
                header: 'PtkId',
                width: 80,
                sortable: true, 
				hidden: true,
                dataIndex: 'PtkId'
            },{                
                header: 'Nuptk',
                width: 120,
                sortable: true, 
                dataIndex: 'Nuptk'
            },{                
                header: 'Nrg',
                width: 80,
                sortable: true, 
                dataIndex: 'Nrg'
            },{                
                header: 'Nip',
                width: 120,
                sortable: true, 
                dataIndex: 'Nip'
            },{                
                header: 'Nama',
                width: 120,
                sortable: true, 
                dataIndex: 'Nama'
            },{                
                header: 'Tmp.Tugas',
                width: 120,
                sortable: true, 
                dataIndex: 'NamaSekolah'
            },{                
                header: 'NamaJenisSekolah',
                width: 80,
                editor: new fm.TextField({
                   allowBlank: false
                }),
                sortable: true, 
                dataIndex: 'NamaJenisSekolah'

            },{                
                header: 'NamaKabupatenKota',
                width: 80,
                editor: new fm.TextField({
                   allowBlank: false
                }),
                sortable: true, 
                dataIndex: 'NamaKabupatenKota'

            },{                
                header: 'NamaPropinsi',
                width: 80,
                editor: new fm.TextField({
                   allowBlank: false
                }),
                sortable: true, 
                dataIndex: 'NamaPropinsi'

            },{                
                header: 'Status',
                width: 70,
                sortable: true, 
                dataIndex: 'NamaStatusKepegawaian'

            },{                
                header: 'NamaBidangStudiSertifikasi',
                width: 80,
                editor: new fm.TextField({
                   allowBlank: false
                }),
                sortable: true, 
                dataIndex: 'NamaBidangStudiSertifikasi'

			},{                
                header: 'TglLulusSertifikasi',
                width: 80,
                renderer: Ext.util.Format.dateRenderer('Y/m/d'),
                editor: new fm.DateField({
                    format: 'Y/m/d',
                    minValue: '1920/01/01'
                }),
                sortable: true, 
                dataIndex: 'TglLulusSertifikasi'

            },{                
                header: 'NamaPangkatGolongan',
                width: 80,
                editor: new fm.TextField({
                   allowBlank: false
                }),
                sortable: true, 
                dataIndex: 'NamaPangkatGolongan'

            },{                
                header: 'NamaStatusKepegawaian',
                width: 80,
                editor: new fm.TextField({
                   allowBlank: false
                }),
                sortable: true, 
                dataIndex: 'NamaStatusKepegawaian'

            },{                
                header: 'NamaTugasTambahan',
                width: 80,
                editor: new fm.TextField({
                   allowBlank: false
                }),
                sortable: true, 
                dataIndex: 'NamaTugasTambahan'

            },{                
                header: 'JamTugasTambahan',
                width: 80,
                editor: new fm.TextField({
                   allowBlank: false
                }),
                sortable: true, 
                dataIndex: 'JamTugasTambahan'

            },{                
                header: 'JumlahJamMengajar',
                width: 80,
                editor: new fm.TextField({
                   allowBlank: false
                }),
                sortable: true, 
                dataIndex: 'JumlahJamMengajar'

            },{                
                header: 'JumlahJamMengajarSesuai',
                width: 80,
                editor: new fm.TextField({
                   allowBlank: false
                }),
                sortable: true, 
                dataIndex: 'JumlahJamMengajarSesuai'

            },{                
                header: 'JumlahJamMengajarSesuaiKtsp',
                width: 80,
                editor: new fm.TextField({
                   allowBlank: false
                }),
                sortable: true, 
                dataIndex: 'JumlahJamMengajarSesuaiKtsp'

            },{                
                header: 'TotalJamMengajarSesuai',
                width: 80,
                editor: new fm.TextField({
                   allowBlank: false
                }),
                sortable: true, 
                dataIndex: 'TotalJamMengajarSesuai'

            },{                
                header: 'MasaKerjaTahun',
                width: 80,
                editor: new fm.TextField({
                   allowBlank: false
                }),
                sortable: true, 
                dataIndex: 'MasaKerjaTahun'

            },{                
                header: 'MasaKerjaBulan',
                width: 80,
                editor: new fm.TextField({
                   allowBlank: false
                }),
                sortable: true, 
                dataIndex: 'MasaKerjaBulan'

            },{                
                header: 'TglPensiun',
                width: 80,
                renderer: Ext.util.Format.dateRenderer('Y/m/d'),
                editor: new fm.DateField({
                    format: 'Y/m/d',
                    minValue: '1920/01/01'
                }),
                sortable: true, 
                dataIndex: 'TglPensiun'
            }],			
			bbar: new Ext.PagingToolbar({
				pageSize	: 20,
    			store		: simptk.StoreTPraDupak,
				paramNames  : {start: 'start', limit: 'limit'},
    			displayInfo	: true,
    			displayMsg	: 'Menampilkan {0} - {1} dari {2}',
    			emptyMsg	: "Data tidak ditemukan"
			}),
			// height: 350,
			anchor: '100%'
			// width: 500
			// title: 'Array Grid',
		});
		
		filterSyarat.setValue(3);
		
		var infoPanel = new Ext.Panel({
			flex: 2,
			// anchor: '100%',
			height: 100,
			html: '<div style="padding:10px 10px 10px 10px;">'
				  + 'Mohon pilih wilayah dulu melalui tombol "Cari Wilayah"'
			      + '</div>',
			tbar: [{
				text: 'Pilih Wilayah',
				tooltip: 'Klik untuk memilih wilayah',
				scope: this,
				iconCls: 'zoom',
				handler: function(){
					var win = new simptk.CariWilayah();
					win.on('wilayahselected', function( win, propinsi_selected, kabkota_selected, kecamatan_selected ){
						//simptk.kabkota_selected = kabkota_selected;
						//simptk.propinsi_selected = propinsi_selected;
						//console.log(kabkota_selected.data.Nama);
						updateInfoPanel();
						/*
						Ext.Ajax.request({
							scope: window,
							waitMsg: 'Menyimpan...',						
							url: 'Sinkronisasi/getStatusPraSk.json',
							method: 'POST',		
							params: {
								kabupaten_kota_id: simptk.kabkota_selected.data.KabupatenKotaId								
							},
							failure:function(response,options){						   
								Ext.Msg.alert('Warning','Response : ' + response );
							},
							success: function(response,options){
								var json = Ext.util.JSON.decode(response.responseText);
								updateInfoPanel(json.ptk_penuhi_syarat, json.ptk_tidak_penuhi_syarat, json.ptk_hasil_matching, json.ptk_berdasarkan_nrg);								
							}
						});
						*/
					});
					win.show();
				}
			}]
		});
		
		window.infoPanel = infoPanel;
		
		simptk.StoreTPraDupak.on({
			'beforeload':{
				fn: function(store, options){
					Ext.apply(options.params, {
						kabupaten_kota_id: simptk.kabkota_selected.data.KabupatenKotaId,
						/*
						filter_syarat_24_jam : simptk.filter_syarat_24_jam,
						filter_jenis_lingkup_bantuan : simptk.filter_jenis_lingkup_bantuan,
						filter_memenuhi_syarat : filterSyarat.getValue(),
						filter_nama: filterNama.getValue(),
						filter_daerah_khusus: filterDaerahDarurat.getValue(),
						*/
						tahun_lulus : cmbTahun.getValue(),
						terpilih: 1
					});
				}
			}
		});
		
		var propinsiCombo = new simptk.PropinsiCombo({
			anchor: '30%',
			lastQuery: '',
			listeners: {
				select: function(thiscombo, record, index) {
					kabkotaCombo.filter(thiscombo.getValue());
				}
			}
		});
		
		var kabkotaCombo = new simptk.KabupatenKotaCombo({
			anchor: '30%',
			listeners: {
				select: function(thiscombo, record, index) {
					simptk.kabkota_selected = record;
					simptk.kabkota_selected_nama = record.data.Nama;
				}
			}
		});
		
		var form = new Ext.form.FormPanel({
			//baseCls: 'x-plain',
			frame:true,
			bodyStyle:'padding:5px 5px 0',
			//frame: true,
			labelWidth: 110,
			defaultType: 'textfield',			
			//flex: 2,		
			items: [
				propinsiCombo,
				kabkotaCombo,
			]
		});
			
		
		Ext.apply(this, {			
			items:  [
				//infoPanel,
				form,
				gridValidasiGuru
			]
		});
		
		simptk.ValidasiGuru.superclass.constructor.call(this);
	}
});