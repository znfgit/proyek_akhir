<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Verifikasi Data Guru</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<body>

<div id="psdgraphics-com-table">



<div id="psdg-header" style="background: url(/assets/images/small-unsharp-flat.png) no-repeat 5px 8px; padding-left:70px;">
<span class="psdg-bold">Info Data PTK</span><br />
Menurut data yang diupload ke Pendataan Pendidikan Dasar
Tanggal pengiriman terakhir: <?=$ptkObj->getLastUpdate('d-m-Y')?> pukul <?=$ptkObj->getLastUpdate('H:i')?>
</div>

<table width="900px">
<tr id="psdg-top">
<th class="psdg-top-cell" style="width:240px; text-align:left; padding-left: 24px;">Field</th>
<th class="psdg-top-cell" width="200px">Data</th>
<th class="psdg-top-cell" width="100px">Status</th>
<th class="psdg-top-cell" width="300px">Keterangan</th>
</tr>
<?php 
$i = 1;

foreach ($outArr as $o) {
	
	if (strpos($o["fieldName"], "status_kepegawaian_id") !== false) {
		if ($ptkObj->getStatusKepegawaianId()) {
			$o["reason"] = "Status kepegawaian terisi.";
			$o["pass"] = true;
			$o["icon"] = "/assets/icons/".$okIcon; 
		}
	}
	
	if (strpos($o["fieldName"], "nuptk") !== false) {
		if ($ptkObj->getNuptk() != $ptkObj->getNuptkLink()) {
			$o["reason"] = "Warning: Kode NUPTK salah input di dapodik.";
		}
	}
	
	if (strpos($o["fieldName"], "ijazah") !== false) {
		$ijazahObj = JenjangPendidikanPeer::retrieveByPK($o["val"]);
		$o["val"] = (is_object($ijazahObj)) ? $ijazahObj->getNama() : "-";
		$o["pass"] = (is_object($ijazahObj)) ? true : false;
	}
	
	if (strpos($o["fieldName"], "tgl") !== false) {
		$tgl = date_parse($o["val"]);		
		$o["val"] = $tgl["day"]." ".getBulan($tgl["month"])." ".$tgl["year"];		
	}
	
	if (strpos($o["fieldName"], "tmt") !== false) {
		$tgl = date_parse($o["val"]);		
		$o["val"] = $tgl["day"]." ".getBulan($tgl["month"])." ".$tgl["year"];		
	}
	
	if (strpos($o["fieldName"], "kuliah") !== false) {				
		if (!$o["pass"]) {
			$o["val"] = "Tidak sedang kuliah";			
		}		
	}
	if (strpos($o["fieldName"], "khusus") !== false) {				
		if (!$o["pass"]) {
			$o["val"] = "Tidak di wilayah khusus";			
		}
	}
	
?>
<tr id="psdg-middle">
<td class="psdg-left" width="240px"><?=$o["no"]?> - <?=humanize($o["fieldName"])?></td>
<td class="psdg-right" width="200px" style="text-align: left;">&nbsp;&nbsp;<?=$o["val"]?></td>
<td class="psdg-right" width="100px" style="text-align: left;">&nbsp;&nbsp;<img src="<?=$o["icon"]?>"/> <?=$o["status"]?></td>
<td class="psdg-right" width="300px" style="text-align: left;">&nbsp;&nbsp;<?=$o["reason"]?></td>
</tr>
<?php 
}
?>

<?php 
//$i = 1;
$passAll = true;
$strFailedNos = "";
foreach ($outArrProfesi as $o) {
	/*
	if (strpos($o["fieldName"], "tgl") !== false) {
		$tgl = date_parse($o["val"]);		
		$o["val"] = $tgl["day"]." ".getBulan($tgl["month"])." ".$tgl["year"];		
	}
	*/
	
	if (strpos($o["fieldName"], "pensiun") !== false) {		
		$o["fieldName"] = "Tahun Pensiun";
		$tgl = date_parse($o["val"]);
		$o["val"] = $tgl["year"];	
		if ($o["pass"]) {
			$o["reason"] = "Tahun pensiun belum terlewati.";
		} else {
			$o["reason"] = "Tahun pensiun sudah terlewati.";
		}
	}
	
	if (strpos($o["fieldName"], "bidang_studi") !== false) {
		//$bsObj = BidangStudiPeer::retrieveByPK($o["val"]);
		//error_reporting(E_ALL);
		
		$c = new Criteria();
		$c->add(RMappingBidangStudiPeer::KODE_LAMA, $o["val"]);
		$bsObj = RMappingBidangStudiPeer::doSelectOne($c);
				
		//$o["val"] = ($bsObj) ? $bsObj->getNama() : "-";	
		//$o["val"] = ($bsObj) ? substr($bsObj->getNamaLama(),0,20) : "-";		
	}
	if (strpos($o["fieldName"], "status_kepegawaian_id") !== false) {
		$bsObj = StatusKepegawaianPeer::retrieveByPK($o["val"]);
		$o["val"] = $bsObj ? $bsObj->getNama() : "-";		
	}
	
	$nrgOk = false;
	
	if (strpos($o["fieldName"], "kode_bidang_studi_sertifikasi") !== false) {
		/*
		if (!$o["pass"]) {
			$o["reason"] = "Kode sertifikasi tidak ditemukan di DBase NRG";
		} else {
			$nrgOk = true;
			//$o["val"] .= "<font face='Arial Narrow'>(".$ptkObj->getNamaBidangStudiSertifikasi().")</font>";
			
			$o["val"] .= " ( thn lulus : ". $ptkObj->getTglLulusSertifikasi('Y') . " ) ";
			$o["reason"] = ($ptkObj->getNamaBidangStudiSertifikasi() != "") ? "Kode valid (".$ptkObj->getNamaBidangStudiSertifikasi().")" : "Kode tidak diketahui (baca <a target='_blank' href='info_faq.php#kode-sertifikasi'>FAQ</a>)";
		}
		*/
		continue;
	}
	
	
?>
<tr id="psdg-middle">
<td class="psdg-left" width="240px"><?=$o["no"]?> - <?=humanize($o["fieldName"])?></td>
<td class="psdg-right" width="200px" style="text-align: left;">&nbsp;&nbsp;<?=$o["val"]?></td>
<td class="psdg-right" width="100px" style="text-align: left;">&nbsp;&nbsp;<img src="<?=$o["icon"]?>"/> <?=$o["status"]?></td>
<td class="psdg-right" width="300px" style="text-align: left;">&nbsp;&nbsp;<?=$o["reason"]?></td>
</tr>
<?php
	if ($o["severity"] == 3){
		$passAll = $passAll && $o["pass"];
		if ($o["pass"] == false) {
			$strFailedNos .= $o["no"].", ";
		}
	}		
}
?>
<!--tr id="psdg-bottom" >
<td class="psdg-bottom-cell" style="width:240px; text-align:left;">Rincian Jam Mengajar</td>
<td class="psdg-bottom-cell" width="200px">&nbsp;</td>
<td class="psdg-bottom-cell" width="100px">&nbsp;</td>
<td class="psdg-bottom-cell" width="300px">&nbsp;</td>
</tr-->
</table>
<table width="900px">
<tr id="psdg-top">
<th class="psdg-top-cell" style="width:240px; text-align:left; padding-left: 24px;"><b>Rincian Jam Mengajar<b></th>
<th class="psdg-top-cell" width="200px">&nbsp;</th>
<th class="psdg-top-cell" width="60px">&nbsp;</th>
<th class="psdg-top-cell" width="60px">&nbsp;</th>
<th class="psdg-top-cell" width="60px">&nbsp;</th>
<th class="psdg-top-cell" width="150px">&nbsp;</th>
<th class="psdg-top-cell" width="70px">&nbsp;</th>
<th class="psdg-top-cell" width="20px">&nbsp;</th>
</tr>
<tr id="psdg-top">
<th class="psdg-top-cell" style="width:240px; text-align:left; padding-left: 24px;">Mata Pelajaran</th>
<th class="psdg-top-cell" width="200px">Tahun/Smt</th>
<th class="psdg-top-cell" width="60px">JJM</th>
<th class="psdg-top-cell" width="60px">JJM KTSP</th>
<th class="psdg-top-cell" width="60px">JJM Linier</th>
<th class="psdg-top-cell" width="150px">Sekolah</th>
<th class="psdg-top-cell" width="70px">Normal</th>
<th class="psdg-top-cell" width="20px"></th>
</tr>
<? foreach ($outJam as $o) { ?>
<tr id="psdg-middle">
<td class="psdg-left" width="240px"><?=$o["kode_matpel_diajarkan"]."-".$o["nama_matpel_diajarkan"]?></td>
<td class="psdg-right" width="200px" style="text-align: left;">&nbsp;&nbsp;Smt <?=$o["semester"]?> Thn <?=$o["tahun_ajaran_id"]?> (Rombel: <?=$o["nama_rombel"]?>)</td>
<td class="psdg-right" width="60px" style="text-align: center;"><?=$o["jjm"]?>&nbsp;&nbsp;</td>
<td class="psdg-right" width="60px" style="text-align: center;"><?=$o["jjm_ktsp"]?>&nbsp;&nbsp;</td>
<?php
	if ($o["jjm"] > $o["jjm_ktsp"]) {
		$jjm_linier = $o["jjm_ktsp"] * $o["is_linier"];
	} else {
		$jjm_linier = $o["jjm"];
	}
	$jjmSum += $o["jjm"];
	$jjmKtspSum += $o["jjm_ktsp"];
	$jjmLinierSum += $o["jjm_linier"];
	if ($o["is_jjm_rombel_normal"] != 1) {
		$jjmLinierRombelTaknormal += $o["jjm_linier"];
	}
	$is_jjm_rombel_normal = $o["is_jjm_rombel_normal"];
	$jjm_rombel = $o["jjm_rombel"];
?>
<td class="psdg-right" width="60px" style="text-align: center;"><?=$o["jjm_linier"]?>&nbsp;&nbsp;</td>
<td class="psdg-right" width="150px" style="text-align: left;">&nbsp;&nbsp;<?=$o["nama_sekolah"]?></td>
<!--td class="psdg-right" width="70px" style="text-align: left; <?=($o["is_jjm_rombel_normal"] == 1) ? "background-color:#00FF00;": "background-color:#FF0000;color:#FFFFFF;"?> ">&nbsp;&nbsp;<?=($o["is_jjm_rombel_normal"] == 1) ? "Ya": "Tidak"?></td-->
<td class="psdg-right" width="70px" style="text-align: left; background-color:#FFFFFF;">&nbsp;&nbsp;<?=($o["is_jjm_rombel_normal"] == 1) ? "<img src='/assets/icons/accept.png'>": "<img src='/assets/icons/exclamation.png'>"?>&nbsp;<?=($o["is_jjm_rombel_normal"] == 1) ? "Ya": "Tidak"?></td>
<td class="psdg-right" width="20px" style="text-align: center; background-color:#FFFFFF;">
	<a target="_blank" href="info_rombel.php?rombel_id=<?=$o["rombongan_belajar_id"]?>&is_jjm_rombel_normal=<?=$is_jjm_rombel_normal?>&jjm_rombel=<?=$jjm_rombel?>"><img src="/assets/icons/information.png"></a>
</td>
</tr>
<? } ?>
<? 
	$jtt = 0;
	if ($ptkObj->getJamTugasTambahan() > 0) { 
		$jtt = $ptkObj->getJamTugasTambahan();
?>
<tr id="psdg-middle">
<td class="psdg-left" width="240px">Tugas Tambahan</td>
<td class="psdg-right" width="200px" style="text-align: left;">&nbsp;&nbsp;<?=$ptkObj->getNamaTugasTambahan()?></td>
<td class="psdg-right" width="60px" style="text-align: center;"><?=$ptkObj->getJamTugasTambahan()?>&nbsp;&nbsp;</td>
<td class="psdg-right" width="60px" style="text-align: center;"><?=$ptkObj->getJamTugasTambahan()?>&nbsp;&nbsp;</td>
<td class="psdg-right" width="60px" style="text-align: center;"><?=$ptkObj->getJamTugasTambahan()?>&nbsp;&nbsp;</td>
<td class="psdg-right" width="150px" style="text-align: left;">&nbsp;&nbsp;<?=$o["nama_sekolah"]?></td>
<td class="psdg-right" width="70px" style="text-align: left;">&nbsp;&nbsp;</td>
<td class="psdg-right" width="20px" style="text-align: left;">&nbsp;&nbsp;</td>
</tr>
<? } ?>
<tr id="psdg-middle">
<td class="psdg-left" width="240px"><b>Jumlah</b></td>
<td class="psdg-right" width="200px" style="text-align: left;">&nbsp;&nbsp;-</td>
<td class="psdg-right" width="60px" style="text-align: center;"><b><?=$jjmSum + $jtt?></b>&nbsp;&nbsp;</td>
<td class="psdg-right" width="60px" style="text-align: center;"><b><?=$jjmKtspSum + $jtt?></b>&nbsp;&nbsp;</td>
<td class="psdg-right" width="60px" style="text-align: center;"><b><?=$jjmLinierSum + $jtt?></b>&nbsp;&nbsp;</td>
<td class="psdg-right" width="150px" style="text-align: left;">&nbsp;&nbsp;</td>
</tr>
</table>
<?php
$nuptkIsian = $ptkObj->getNuptk();

$nuptkSqlRec = getDataBySql("select * from v_nuptk where nuptk='$nuptkIsian'");
$nuptkRec = $nuptkSqlRec[0];
//print_r($nuptkRec); die;
$nuptkNama = $nuptkRec["nama"] ? $nuptkRec["nama"] : "";
$nuptkTempatTugas = $nuptkRec["instansi_induk"];
$nuptkValid = ($ptkObj->getNuptk() == $ptkObj->getNuptkLink()) ? true : false;
$nuptkValidStr = $nuptkValid ? "Valid" : "Tidak valid";
$nuptkValidIcon = "/assets/icons/".($nuptkValid ? $okIcon : $failIcon); 

$nuptkLink = $ptkObj->getNuptkLink();
if ($nuptkNama == "") {
	$nuptkKeterangan = "Tidak ditemukan";
}
$nuptkSqlRecLink = getDataBySql("select * from v_nuptk where nuptk='$nuptkLink'");
$nuptkRecLink = $nuptkSqlRecLink[0];
//print_r($nuptkRec); die;
$nuptkNamaLink = $nuptkRecLink["nama"] ? $nuptkRecLink["nama"] : "";
$nuptkTempatTugasLink = $nuptkRecLink["instansi_induk"];


?>
<table width="900px">
<tr id="psdg-top">
<th class="psdg-top-cell" style="width:240px; text-align:left; padding-left: 24px;"><b>Status NUPTK<b></th>
<th class="psdg-top-cell" width="200px">&nbsp;</th>
<th class="psdg-top-cell" width="100px">&nbsp;</th>
<th class="psdg-top-cell" width="300px">&nbsp;</th>
</tr>
<tr id="psdg-middle">
<td class="psdg-left" width="240px">NUPTK</td>
<td class="psdg-right" width="200px" style="text-align: left;">&nbsp;&nbsp;<?=$nuptkIsian?></td>
<td class="psdg-right" width="100px" style="text-align: left;">&nbsp;&nbsp;<img src="<?=$nuptkValidIcon?>"/> <?=$nuptkValidStr?></td>
<td class="psdg-right" width="300px" style="text-align: left;">&nbsp;&nbsp;</td>
</tr>
<tr id="psdg-middle">
<td class="psdg-left" width="240px">Atas Nama</td>
<td class="psdg-right" width="200px" style="text-align: left;">&nbsp;&nbsp;<?=$nuptkNama?></td>
<td class="psdg-right" width="100px" style="text-align: left;">&nbsp;&nbsp;<img src="<?=$nuptkValidIcon?>"/> <?=$nuptkValidStr?></td>
<td class="psdg-right" width="300px" style="text-align: left;">&nbsp;&nbsp;<?=$nuptkKeterangan?></td>
</tr>
<tr id="psdg-middle">
<td class="psdg-left" width="240px">Tempat Tugas Terbit NUPTK</td>
<td class="psdg-right" width="200px" style="text-align: left;">&nbsp;&nbsp;<?=$nuptkTempatTugas?></td>
<td class="psdg-right" width="100px" style="text-align: left;">&nbsp;&nbsp;<img src="<?=$nuptkValidIcon?>"/> <?=$nuptkValidStr?></td>
<td class="psdg-right" width="300px" style="text-align: left;">&nbsp;&nbsp;<?=$nuptkKeterangan?></td>
</tr>
<tr id="psdg-middle">
<td class="psdg-left" width="240px">NUPTK Valid</td>
<td class="psdg-right" width="200px" style="text-align: left;">&nbsp;&nbsp;<?=$nuptkValidStr?></td>
<td class="psdg-right" width="100px" style="text-align: left;">&nbsp;&nbsp;<img src="<?=$nuptkValidIcon?>"/> <?=$nuptkValidStr?></td>
<td class="psdg-right" width="300px" style="text-align: left;">&nbsp;&nbsp;</td>
</tr>
<?php
if (($nuptkValid == false) && ($ptkObj->getNuptkLink())) {
?>
<tr id="psdg-middle">
<td class="psdg-left" width="240px">NUPTK Hasil Validasi</td>
<td class="psdg-right" width="200px" style="text-align: left;">&nbsp;&nbsp;<?=$nuptkLink?></td>
<td class="psdg-right" width="100px" style="text-align: left;">&nbsp;&nbsp;<img src="/assets/icons/information.png"/> Info</td>
<td class="psdg-right" width="300px" style="text-align: left;">&nbsp;&nbsp;NUPTK seharusnya menurut sistem validasi</td>
</tr>
<tr id="psdg-middle">
<td class="psdg-left" width="240px">Atas Nama Hasil Validasi</td>
<td class="psdg-right" width="200px" style="text-align: left;">&nbsp;&nbsp;<?=$nuptkNamaLink?></td>
<td class="psdg-right" width="100px" style="text-align: left;">&nbsp;&nbsp;<img src="/assets/icons/information.png"/> Info</td>
<td class="psdg-right" width="300px" style="text-align: left;">&nbsp;&nbsp;Nama menurut sistem validasi</td>
</tr>
<tr id="psdg-middle">
<td class="psdg-left" width="240px">Tempat Tugas Hasil Validasi</td>
<td class="psdg-right" width="200px" style="text-align: left;">&nbsp;&nbsp;<?=$nuptkTempatTugasLink?></td>
<td class="psdg-right" width="100px" style="text-align: left;">&nbsp;&nbsp;<img src="/assets/icons/information.png"/> Info</td>
<td class="psdg-right" width="300px" style="text-align: left;">&nbsp;&nbsp;Tempat tugas menurut sistem validasi</td>
</tr>
<?
}
?>

</table>
<?php
$nuptkIsian = $ptkObj->getNuptk();
//$sqlStr = "select * from v_nrg where nrg_nuptk='$nuptkIsian'";
$sqlStr = "select nama as nrg_nama, no_peserta_nrg as nrg_nopeserta, nrg as nrg_nrg, nama_kabupaten_kota as KabNama from t_tunjangan_profesi_new where nuptk='$nuptkIsian'";
//echo $sqlStr;die;
$vnrgSqlRec = getDataBySql($sqlStr);

if (!$vnrgSqlRec) {
	$statusKelulusanStr = "Data Kelulusan tidak ditemukan. Jika ybs telah sertifikasi pastikan bahwa NUPTK pada data kelulusan dan dapodik valid.";
} else {	
	$v = $vnrgSqlRec[0];
	$nuptkValidIcon = "/assets/icons/".$okIcon;
	$nuptkValidStr = "Valid";
}
?>
<table width="900px">
<tr id="psdg-top">
<th class="psdg-top-cell" style="width:240px; text-align:left; padding-left: 24px;"><b>Status Kelulusan Sertifikasi<b></th>
<th class="psdg-top-cell" width="200px">&nbsp;</th>
<th class="psdg-top-cell" width="100px">&nbsp;</th>
<th class="psdg-top-cell" width="300px">&nbsp;</th>
</tr>
<?php
if ($vnrgSqlRec) {
?>
<tr id="psdg-middle">
<td class="psdg-left" width="240px">Atas Nama</td>
<td class="psdg-right" width="200px" style="text-align: left;">&nbsp;&nbsp;<?=$v['nrg_nama']?></td>
<td class="psdg-right" width="100px" style="text-align: left;">&nbsp;&nbsp;<img src="<?=$nuptkValidIcon?>"/> <?=$nuptkValidStr?></td>
<td class="psdg-right" width="300px" style="text-align: left;">&nbsp;&nbsp;</td>
</tr>
<tr id="psdg-middle">
<td class="psdg-left" width="240px">Tahun Sertifikasi</td>
<td class="psdg-right" width="200px" style="text-align: left;">&nbsp;&nbsp;<?="20".substr($v['nrg_nopeserta'], 0, 2);?></td>
<td class="psdg-right" width="100px" style="text-align: left;">&nbsp;&nbsp;<img src="<?=$nuptkValidIcon?>"/> <?=$nuptkValidStr?></td>
<td class="psdg-right" width="300px" style="text-align: left;">&nbsp;&nbsp;<?=$nuptkKeterangan?></td>
</tr>
<tr id="psdg-middle">
<td class="psdg-left" width="240px">Kode Sertifikasi</td>
<td class="psdg-right" width="200px" style="text-align: left;">&nbsp;&nbsp;<?=substr($v['nrg_nopeserta'], 6, 3);?></td>
<td class="psdg-right" width="100px" style="text-align: left;">&nbsp;&nbsp;<img src="<?=$nuptkValidIcon?>"/> <?=$nuptkValidStr?></td>
<td class="psdg-right" width="300px" style="text-align: left;">&nbsp;&nbsp;</td>
</tr>
<tr id="psdg-middle">
<td class="psdg-left" width="240px">No. Peserta</td>
<td class="psdg-right" width="200px" style="text-align: left;">&nbsp;&nbsp;<?=$v['nrg_nopeserta']?></td>
<td class="psdg-right" width="100px" style="text-align: left;">&nbsp;&nbsp;<img src="<?=$nuptkValidIcon?>"/> <?=$nuptkValidStr?></td>
<td class="psdg-right" width="300px" style="text-align: left;">&nbsp;&nbsp;</td>
</tr>
<tr id="psdg-middle">
<td class="psdg-left" width="240px">NRG</td>
<td class="psdg-right" width="200px" style="text-align: left;">&nbsp;&nbsp;<?=$v['nrg_nrg']?></td>
<td class="psdg-right" width="100px" style="text-align: left;">&nbsp;&nbsp;<img src="<?=$nuptkValidIcon?>"/> <?=$nuptkValidStr?></td>
<td class="psdg-right" width="300px" style="text-align: left;">&nbsp;&nbsp;</td>
</tr>
<tr id="psdg-middle">
<td class="psdg-left" width="240px">Kab/Kota</td>
<td class="psdg-right" width="200px" style="text-align: left;">&nbsp;&nbsp;<?=$v['KabNama']?></td>
<td class="psdg-right" width="100px" style="text-align: left;">&nbsp;&nbsp;<img src="<?=$nuptkValidIcon?>"/> <?=$nuptkValidStr?></td>
<td class="psdg-right" width="300px" style="text-align: left;">&nbsp;&nbsp;</td>
</tr>
<tr id="psdg-bottom" >
<td class="psdg-bottom-cell" style="width:264px; text-align:left;">&nbsp;</td>
<td class="psdg-bottom-cell" width="200px">&nbsp;</td>
<td class="psdg-bottom-cell" width="100px">&nbsp;</td>
<td class="psdg-bottom-cell" width="300px">&nbsp;</td>
</tr>
<?php
} else {
?>
<tr id="psdg-middle">
<td class="psdg-left" width="240px" colspan="4" valign="top"><?=$statusKelulusanStr?></td>
</tr>
<?php 
}
?>
</table>
<div id="psdg-footer">
<span style="color: red; font-size: 10pt;"> 
* Mohon pastikan alamat e-mail pada Data Dikdas terisi dengan benar, karena link/tautan untuk konfirmasi data akan dikirim melalui e-mail.<br>
<?php
if ($jjmLinierRombelTaknormal > 0) {
	$total_jt = $jjmLinierSum + $jtt;
	echo "* Dari total  $total_jt jam linier terdapat $jjmLinierRombelTaknormal jam di rombel tak wajar. Jam tsb tidak akan diambil untuk tunjangan profesi. Untuk keterangan, klik icon info.";
}

if (!$passAll) {
	if ($nrgOk) {
		echo "* Poin nomor <b>$strFailedNos tidak dipenuhi</b>, ini berakibat PTK bersangkutan <b><u>tidak memenuhi syarat</u></b> untuk menerima Tunjangan Profesi.<br>";
	}
} else {
	echo "* PTK bersangkutan telah <b><u>memenuhi syarat</u></b> untuk menerima Tunjangan Profesi<br>";
}
?>
<?php
if ($nuptkValid == false) {
?>
* Jika status NUPTK tidak valid kemungkinan ada kesalahan pada data dapodik, sehingga harus diperbaiki.<br>
<?php
}
?>
* Perbaiki data guru jika diperlukan melalui Dapodik Dikdas. Data akan terupdate dalam beberapa hari.<br> 
* Jika masih ada masalah, kirim email mengenai masalah yg dimaksud beserta kode NUPTK ke <b><u>cekdataguru.dikdas@gmail.com</u></b><br>
* HARAP DIPAHAMI BAHWA MEKANISME UPDATE DATA HANYA BISA DILAKUKAN MELALUI APLIKASI DAPODIK. <br>
* KAMI <b><u>TIDAK AKAN MENJAWAB</u></b> REQUEST UPDATE DATA.<br>
</span>
</div>

</div>
<br>
<?php 
	$fromEmail = $_SESSION["fromEmail"] ? $_SESSION["fromEmail"] : $fromEmail;
	
	if ($fromEmail) {	
		?><a href="<?=$_SERVER["PHP_SELF"]?>?konfirmasi=true" style="position:relative;left:640px;" class="tombol-hijau">Konfirmasi Data</a>
		  <a href="<?=$_SERVER["PHP_SELF"]?>?logout=true" style="position:relative;left:650px;" class="tombol-merah" style="">Tutup</a>
		<?
	} else {
?>&nbsp;
<a href="<?=$_SERVER["PHP_SELF"]?>?logout=true" style="position:relative;left:810px;" class="tombol-merah" style="">Tutup</a>
<?php } ?>




</body>
</html>