<?php
error_reporting('E_NONE');
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Pengecekan Data Guru</title>
<link href="login-box.css" rel="stylesheet" type="text/css" />
<style>
h2 {
	background: url('/assets/images/small-unsharp-flat.png') no-repeat;
	text-indent: 60px;
}
</style>
</head>

<body>
<div style="padding: 10px 0 0 0px;" align="center">

<div class="login-box">
<div class="content" align="left">
<H2>Login</H2>
<span style="font-size: 15px; font-weight: bold; margin-left: 62px;">Verifikasi Data PTK</span>
<br />
<br />
<form name="login" action="<?=$_SERVER['PHP_SELF']?>" method="post">
<div class="login-box-name" style="margin-top:20px;">NUPTK:</div><div class="login-box-field" style="margin-top:20px;"><input name="nuptk" class="form-login" title="Username" value="" size="25" maxlength="16" /></div>
<div class="login-box-name">Password:</div><div class="login-box-field"><input name="password" type="password" class="form-login" title="Password" value="" size="25" maxlength="8" /></div>
<?=$_REQUEST["admin"] ? '<input name="admin" type="hidden"/>':''?>
<br />
<br />
<br />
<!-- a href="#" onClick="javascript:document.login.submit()"><img src="images/login-btn.png" width="103" height="42" style="margin-left:90px;" /></a -->
<br />
<br />
<a href="#" onClick="javascript:document.login.submit()" class="tombol-biru">Login</a>
<?php
error_reporting('E_NONE');
if ($isWrongPassword || $isWrongNuptk || $isMaintenance) { 
?>
<div style="border: solid orange 2px; width: 100%; margin: 30px 0 0 0; text-align: center; font-family: Arial; ">
	<?php 
	echo ($isMaintenance ? "Maaf system sedang maintenance. <br>Mohon tunggu bbrp saat lagi." : "");
	echo ($isWrongNuptk ? "NUPTK tidak ditemukan. Jika ada masalah, kirim e-mail dgn NUPTK ybs ke cekdataguru.dikdas@gmail.com" : "");
	echo ($isWrongPassword ? "Password (tgl lahir) salah. Jika ada masalah, kirim e-mail dgn NUPTK ybs ke cekdataguru.dikdas@gmail.com" : "");
	?>
</div>
<?php
}
?>
<div style="border: solid green 2px; width: 100%; margin: 30px 0 0 0; text-align: center; font-family: Arial; ">
	Karena banyaknya E-mail masuk dgn pertanyaan serupa, <br>
	maka kami membuat <a href="info_faq.php"><span style="font-color: white">FAQ (Frequently Asked Question)</span></a> 
</div>
</form>
</div>
</div>
</div>
<div class="login-box">
<div class="content" align="left">
<b>Keterangan</b><br>
<ul>
<li>
Masukkan NUPTK sebagai username serta tanggal lahir sebagai password dgn format YYYYMMDD. Sebagai contoh jika tgl lahir anda 2 Agustus 1986 passwordnya: 19860802
</li>
<br>
<li>
Jika muncul informasi NUPTK tidak ditemukan, ada beberapa kemungkinan. 1) Kolom NUPTK belum diisi pada data Dapodik anda; 2) Kolom NUPTK anda diisi namun salah ketik. 3) Data Dapodik utk NUPTK ybs belum ter-import ke basis data.
</li>
<br>
<li>
Jika ada masalah tertentu, sertakan NUPTK dan masalahnya melalui email ke cekdataguru.dikdas@gmail.com utk kami telusuri penyebabnya. 
HARAP DIPAHAMI BAHWA MEKANISME UPDATE DATA HANYA BISA DILAKUKAN MELALUI APLIKASI DAPODIK. KAMI TIDAK AKAN MENJAWAB REQUEST UPDATE DATA.
</li>
</ul>
</div>
</div>
</body>
</html>
