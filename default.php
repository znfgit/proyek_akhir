<?php 
error_reporting('E_NONE');
require('startup.php');

$auth = new XondAuth();
$auth->setAuthObject('Pengguna');
$auth->setUserColumn('Username');
$auth->setPasswordColumn('Password');

if (!$auth->getSession()) {
	header('Location: /login.php');	
}

$peran_id = $auth->getUser()->getPeranId();
if ($peran_id == 1) {
	//ok
} else {
	header('Location: /login.php');	
}

$config = array();
$config["custom_app_ui"] = true;
$app = new XondApplication($config);
$app->setAuth($auth);

$kabupaten_kota_id = $app->getAuth()->getUser()->getKabupatenKotaId();

if ($kabupaten_kota_id) {
	$kabKotaObj = KabupatenKotaPeer::retrieveByPk($kabupaten_kota_id);
	$propinsi_id = $kabKotaObj->getPropinsiId();
}
/*
if (empty($kabkotaId)) {
	header('Location: /login.php');
}
*/
?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="/extjs/resources/css/ext-all.css" />
	<!--link rel="stylesheet" type="text/css" href="/extjs/resources/css/xtheme-access.css"/-->
	<link rel="stylesheet" type="text/css" href="/extjs/resources/css/xtheme-gray.css" />	
	<link rel="stylesheet" type="text/css" href="/app/file-upload.css" />
	<link rel="stylesheet" type="text/css" href="/app/ProgressColumn.css" />
	<link rel="stylesheet" type="text/css" href="/app/TreeGrid.css" />
	<link rel="stylesheet" type="text/css" href="/app/TreeGridLevels.css" />
	<link rel="stylesheet" type="text/css" href="/app/default.css" />	
	<link rel="stylesheet" type="text/css" href="/assets/css/LockingGridView.css" />
	<link rel="stylesheet" type="text/css" href="/assets/css/ColumnHeaderGroup.css" />
	<link rel="stylesheet" type="text/css" href="/lib/superboxselect.css" />
	<link rel="stylesheet" type="text/css" href="/lib/superboxselect-gray-extend.css" />


	<link rel="stylesheet" type="text/css" href="application.css" />
	<script type="text/javascript" src="/extjs/adapter/ext/ext-base.js"></script>
	<script type="text/javascript" src="/extjs/ext-all-debug.js"></script>
	<script type="text/javascript" src="/extjs/examples/ux/ColumnHeaderGroup.js"></script>	
	<script type="text/javascript" src="/app/ux-all-debug.js"></script>	
	<!--script type="text/javascript" src="/app/LockingColumnHeaderGroup.js"></script-->	
	<script type="text/javascript" src="/app/Ext.tree.TreeSerializer.js"></script>	
	<script type="text/javascript" src="/app/ProgressColumn.js"></script>	
	<script type="text/javascript" src="/app/TreeGrid.js"></script>	
	<script type="text/javascript" src="/app/library.js"></script>
	<script type="text/javascript" src="/lib/SuperBoxSelect.js"></script>
	<script type="text/javascript" src="js/library.js"></script>
	<script type="text/javascript" src="js/startup.js"></script>
	<script type="text/javascript" src="js/data.js"></script>
	<script type="text/javascript" src="js/guru.js"></script>
	<script type="text/javascript" src="js/viewviewer.js"></script>
	<script type="text/javascript" src="js/components.js"></script>	
	<script type="text/javascript" src="js/rekap.js"></script>
	<script type="text/javascript" src="js/mapping.js"></script>
	<script type="text/javascript" src="js/sekolah.js"></script>
	<script type="text/javascript" src="js/cari_kabkota_prop.js"></script>
	<script type="text/javascript" src="js/mailbroadcast.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
	<script type="text/javascript">
		simptk.session_propinsi_id = '<?=$propinsi_id?>';
		simptk.session_kabupaten_kota_id = '<?=$kabupaten_kota_id?>';
		simptk.session_sekolah = '';
		simptk.session_tahun_ajaran = '2012';
	</script>
</head>
<body>
	
</body>
</html>



