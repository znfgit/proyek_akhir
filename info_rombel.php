<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Info Rombel</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="psdgraphics-com-table-small">
<?php
require_once 'startup.php';
$rombelId = $_REQUEST['rombel_id'] ? $_REQUEST['rombel_id'] : 'd4nLQfr7EeGxX4kygwZb2Q';
$rombelObj = RombonganBelajarPeer::retrieveByPk($rombelId);
$mengajars = getDataBySql("select kode_matpel_diajarkan, nama_matpel_diajarkan, nama, jjm, jjm_ktsp, tahun_ajaran_id, semester from t_mengajar where rombongan_belajar_id = '$rombelId' order by nama_matpel_diajarkan");
?>
<table width="680px">
<tr id="psdg-top">
<th class="psdg-top-cell" colspan=4 style="height:110px; width:643px; text-align:left; padding-left: 24px;">
<b><u>Daftar Guru Mengajar Rombel</u></b><br><br>
Nama Rombel: <?=$rombelObj->getNama()?><br>
Sekolah: <?=$rombelObj->getSekolah()->getNama()?><br>
Kelebihan Jam Rombel: <?=$_REQUEST['jjm_rombel']?><br>
Rombel Normal: <?=$_REQUEST['is_jjm_rombel_normal'] ? "Ya" : "Tidak"?><br>
</th>
</tr>
<tr id="psdg-top">
<th class="psdg-top-cell" style="width:240px; text-align:left; padding-left: 24px;">Mata Pelajaran</th>
<th class="psdg-top-cell" width="200px">Nama Guru</th>
<th class="psdg-top-cell" width="100px">JJM</th>
<th class="psdg-top-cell" width="100px">JJM KTSP</th>
</tr>
<? foreach ($mengajars as $m) { ?>
<tr id="psdg-middle">
<td class="psdg-left" width="240px"><?=$m["kode_matpel_diajarkan"]."-".$m["nama_matpel_diajarkan"]?></td>
<td class="psdg-right" width="200px" style="text-align: left;">&nbsp;&nbsp;<?=$m["nama"]?></td>
<td class="psdg-right" width="100px" style="text-align: center;"><?=$m["jjm"]?>&nbsp;&nbsp;</td>
<td class="psdg-right" width="100px" style="text-align: center;"><?=$m["jjm_ktsp"]?>&nbsp;&nbsp;</td>
</tr>
<?		
		$total_jjm += $m["jjm"];
		$total_jjm_ktsp += $m["jjm_ktsp"];
?>
<? } ?>
<tr id="psdg-middle">
<td class="psdg-left" width="240px"><b>Jumlah Jam Mengajar Rombel</b></td>
<td class="psdg-right" width="200px" style="text-align: left;">&nbsp;&nbsp;-</td>
<td class="psdg-right" width="100px" style="text-align: center;"><b><?=$total_jjm?></b>&nbsp;&nbsp;</td>
<td class="psdg-right" width="100px" style="text-align: center;"><b><?=$total_jjm_ktsp?></b>&nbsp;&nbsp;</td>
</tr>
</table>
<div id="psdg-footer">
<span style="color: red; font-size: 10pt;"> 
* Rombel dinyatakan tidak normal jika jumlah total jam mengajar di rombel tersebut tidak wajar.<br>
* Ketidakwajaran jumlah dapat diakibatkan mata pelajaran yang sama diajarkan oleh lebih dari satu guru.<br>
</span>
</div>
</div>
</body>
</html>