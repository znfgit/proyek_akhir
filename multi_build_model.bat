@echo off
del /Q /S /F build
del schema.xml
del serverdapodik-schema.xml
del sqlserver-schema.xml
copy build.compile.properties build.properties
copy config-dualserver\serverdapodik-schema-design.xml serverdapodik-schema.xml
copy config-dualserver\sqlserver-schema-design.xml sqlserver-schema.xml
propel-gen 
