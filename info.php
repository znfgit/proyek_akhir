<?php

	require_once 'startup.php';
	require_once 'info_lib.php';
	
	error_reporting(E_NONE);
	global $ptkObj, $validasis, $outArr, $outJam, $fromEmail, $isCorrectLogin, $isWrongPassword, $isWrongNuptk, $isMaintenance, $isPrint;
	
	$isMaintenance = false;
	
	
	/*
	$pgId = 11;
	$mk = 35;
	$pg = PangkatGolonganPeer::retrieveByPK($pgId);
	
	echo "PG = ".$pg->getNama()." | MK = ".$mk." | Gaji Pokok = ". getGajiPokok($pgId, $mk);
	die;
	*/
	
	
	
		
	$nuptk = $_REQUEST["id"];
	$fromEmail = $_REQUEST["email"]? true : false;
	
	$isCorrectLogin = false;
	$isWrongPassword = false;
	$isWrongNuptk = false;
	
	//session_start();
		
	if (isset($_REQUEST["logout"])) {
		
		//session_destroy();		
		//unset($_SESSION["nuptk"]);
		//unset($_SESSION["password"]);
		//unset($_SESSION["fromEmail"]);	
		
	}
		
	//if (isset($_REQUEST["nuptk"]) || isset($_SESSION["nuptk"])) {
	//echo $nuptkGlobal."<br>";
	//echo $isPrint."<br>";
	//echo $isAdmin."<br>";
	
	if (!isset($nuptkGlobal)) {
		$isSetNuptk = isset($_REQUEST["nuptk"]);
		$nuptk = $_REQUEST["nuptk"];
	} else {
		$isSetNuptk = true;
		$nuptk = $nuptkGlobal;
	}

	if (!isset($isPrint)) {
		$isPrint = ($_REQUEST["print"] == 1) ? true : false;
		$isMaintenance = false;
	}	
	
	if (!isset($isAdmin)) {
		$isAdmin = isset($_REQUEST["admin"]);		
	}
	
	if ($isSetNuptk || $isAdmin) {

		//$nuptk = $_REQUEST["nuptk"];
		$tglStr = $_REQUEST["password"];
		
		//$nuptk = $_REQUEST["nuptk"] ? $_REQUEST["nuptk"] : $_SESSION["nuptk"];
		//$tglStr = $_REQUEST["password"] ?  $_REQUEST["password"] : $_SESSION["password"];
		
		$tglStrWithStripes = substr($tglStr, 0, 4)."-".substr($tglStr, 4, 2)."-".substr($tglStr, 6,2);
		
		//echo $nuptk."|".$tglStr."|".$tglStrWithStripes;
		
		$c = new Criteria();
		//$c->add(TPtkPeer::NUPTK, $nuptk);
		$c->add(TPtkPeer::NUPTK_LINK, $nuptk);
		
		//$cton1 = $c->getNewCriterion(TPtkPeer::NUPTK_LINK, $nuptk);
		//$cton2 = $c->getNewCriterion(TPtkPeer::NUPTK, $nuptk);
    	//$cton1->addOr($cton2);
    	//$c->add($cton1); 
		$ptkObjNuptkOnly = "";
		
		try {
			
			$cekArr = TPtkPeer::doSelect($c);
			$lastSize = 0;
			
			if (sizeof($cekArr) > 1) {
				foreach($cekArr as $cObj) {					
					//echo getSize($cArr->toArray()).'<br>';
					$theSize = getSize($cObj->toArray());
					if ($theSize > $lastSize) {
						$ptkObjNuptkOnly = $cObj;
					}
					$lastSize = $theSize;
				}
			} else {	
				$ptkObjNuptkOnly = TPtkPeer::doSelectOne($c);
			}
			
		} catch (Exception $e) {

		}		
		
		if (!is_object($ptkObjNuptkOnly)) {
			$c = new Criteria();
			$c->add(TPtkPeer::NUPTK, $nuptk);
			
			$ptkObjNuptkOnly = TPtkPeer::doSelectOne($c);
		}
		
		$c->add(TPtkPeer::STATUS, array(NULL, 1), Criteria::IN);
		
		//$isAdmin = isset($_REQUEST["admin"]);
		
		if (!$isAdmin) {
			
			if (!$tglStr == "") {
			
				$c->add(TPtkPeer::TGL_LAHIR, $tglStrWithStripes);						
				try {
					$ptkObj = TPtkPeer::doSelectOne($c);
				} catch (Exception $e) {
					die ($e);
				}
				if (!is_object($ptkObjNuptkOnly)) {
					//$nrg
				}
				
				if (is_object($ptkObjNuptkOnly)) {
					// Berarti nuptk bener
				} else {
					$isWrongNuptk = true;
				}
				
				if (is_object($ptkObj)) {		
					$isCorrectLogin = true;	
					//$_SESSION["nuptk"] = $ptkObj->getNuptk();
					//$_SESSION["password"] = $_REQUEST["password"];
					//$_SESSION["fromEmail"] = $fromEmail;			
				}
			} else {
				$isWrongNuptk = true;
			}
		
		} else {
			
			if (is_object($ptkObjNuptkOnly)) {		
				$isCorrectLogin = true;
				$ptkObj = $ptkObjNuptkOnly;
			} else {
				if (!isPrint) {
					die("NUPTK tidak ditemukan, cari $nuptk di database.");					
				} else {
					$skipPage = true;
				}
			}
		}
	}
	
	
	if (!$skipPage) {
	
		if ($isMaintenance) {
			include "info_login.php";
			die;
		}
		
		if (!$isCorrectLogin) {
			//echo "belun login";
			if (is_object($ptkObjNuptkOnly)) {			
				$isWrongPassword = true;
			} else {
				$isWrongPassword = false;
			}					
			include "info_login.php";
			die;
		}

		
		/* Prep Module Validasi */
		require_once(dirname(__FILE__).D."modules".D."Validasi".D."Validasi.php");
		$vModule = new ValidasiModule();
		
		/* Loop RMappingValidasi utk keperluan no.1 */
		$c = new Criteria();
		$c->add(RMappingValidasiPeer::R_KEPERLUAN_ID, 1);
		$validasis = RMappingValidasiPeer::doSelect($c);
		
		/* Prepare PTK Object, in Array & fieldname form */
		$arrPtk = $ptkObj->toArray(BasePeer::TYPE_FIELDNAME);
		
		$i = 0;
		$arr["no"] = $i++;
		$fieldName = "nama";	
		$arr["fieldName"] = $fieldName;
		$arr["val"] = $arrPtk["$fieldName"];
		$arr["icon"] = "/assets/icons/information.png";
		$arr["status"] = "Info";
		$outArr[] = $arr;
		
		$arr["no"] = $i++;
		$fieldName = "nip";	
		$arr["fieldName"] = $fieldName;
		$arr["val"] = $arrPtk["$fieldName"] ? $arrPtk["$fieldName"] : "-";
		$arr["icon"] = "/assets/icons/information.png";
		$arr["status"] = "Info";
		$outArr[] = $arr;
		
		$arr["no"] = $i++;	
		$fieldName = "nama_kabupaten_kota";	
		$arr["fieldName"] = $fieldName;
		$arr["val"] = $arrPtk["$fieldName"];
		$arr["icon"] = "/assets/icons/information.png";
		$arr["status"] = "Info";
		$outArr[] = $arr;	
		
		$arr["no"] = $i++;	
		$fieldName = "nama_propinsi";	
		$arr["fieldName"] = $fieldName;	
		$arr["val"] = $arrPtk["$fieldName"];
		$arr["icon"] = "/assets/icons/information.png";
		$arr["status"] = "Info";
		$outArr[] = $arr;	
		
		foreach ($validasis as $v) {
			
			$fieldName = $v->getRValidasi()->getObyek();
			$functionName = $v->getRValidasi()->getNamaFungsi();
			
			$okIcon = "accept.png";
			$failIcon = $v->getRDerajatValidasi()->getIcon();
			$failStatus = $v->getRDerajatValidasi()->getNama();
			
			//echo "$fieldName|$functionName<br>";
			
			
			$arr["no"] = $i++;
			$arr["fieldName"] = $fieldName;
			$arr["val"] = $arrPtk["$fieldName"];
			
			if ($functionName) {
				$hasilCek = $vModule->{$functionName}($ptkObj);
				$arr["pass"] = $hasilCek["pass"];
				$arr["reason"] = $hasilCek["reason"]; 
				$arr["icon"] = "/assets/icons/".($hasilCek["pass"] ? $okIcon : $failIcon);
				$arr["status"] = $hasilCek["pass"] ? "OK" : $failStatus;			
			} else {
				$arr["icon"] = "/assets/icons/information.png";
			}
			
			$outArr[] = $arr;
			
		}
		
		
		$arr["no"] = $i++;	
		$fieldName = "gaji_pokok";	
		$arr["fieldName"] = $fieldName;	
		$arr["val"] = "Rp ". getGajiPokok($ptkObj->getPangkatGolonganId(), $ptkObj->getMasaKerjaTahun());
		$arr["icon"] = "/assets/icons/information.png";
		$arr["status"] = "Info";
		$arr["reason"] = "Info"; 
			
		$outArr[] = $arr;
		
		/* Loop RMappingValidasi utk keperluan no.1 */
		$c = new Criteria();
		$c->add(RMappingValidasiPeer::R_KEPERLUAN_ID, 3);
		$validasisProfesi = RMappingValidasiPeer::doSelect($c);
		
		$passAll = true;
		
		foreach ($validasisProfesi as $v) {
			
			$fieldName = $v->getRValidasi()->getObyek();
			$functionName = $v->getRValidasi()->getNamaFungsi();
			
			$okIcon = "accept.png";
			$failIcon = $v->getRDerajatValidasi()->getIcon();
			$failStatus = $v->getRDerajatValidasi()->getNama();
			$severity = $v->getRDerajatValidasiId();
			
			//echo "$fieldName|$functionName<br>";
			
			
			$arr["no"] = $i++;
			$arr["fieldName"] = $fieldName;
			$arr["val"] = $arrPtk["$fieldName"];
			
			if ($functionName) {
				$hasilCek = $vModule->{$functionName}($ptkObj);
				$arr["pass"] = $hasilCek["pass"];
				$arr["reason"] = $hasilCek["reason"]; 
				$arr["icon"] = "/assets/icons/".($hasilCek["pass"] ? $okIcon : $failIcon);
				$arr["status"] = $hasilCek["pass"] ? "OK" : $failStatus;
				$arr["severity"] = $severity;
			} else {
				$arr["icon"] = "/assets/icons/information.png";
			}
			
			$outArrProfesi[] = $arr;
			
		}
		
		$outJam = getDataBySql("
			select nuptk, nama_matpel_diajarkan, kode_matpel_diajarkan, tingkat, tahun_ajaran_id, semester, jjm, jjm_ktsp, is_linier, nama_sekolah, jjm_linier, rombongan_belajar_id, nama_rombel, is_jjm_rombel_normal, jjm_rombel from t_mengajar
			where nuptk = '$nuptk' order by sekolah_id
		");

		if ($isPrint) {
			include "info_tabel_print.php";		
		} else {
			include "info_tabel.php";
		}
	}
?>