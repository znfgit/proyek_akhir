<?php

include 'startup.php'; 
error_reporting(E_ALL);
function Trekap($jn, $kabKotaId){
	if($kabKotaId != 0){
		$single = true;
	}else{
		$single = false;
	}
	getTRekap($jn, $kabKotaId, $single);
}

function getTRekap($jn, $kabKotaId, $single){
	
	$c = new Criteria();
	if($kabKotaId != 0){
		$c->addAnd(KabupatenKotaPeer::KABUPATEN_KOTA_ID, $kabKotaId);
	}
	$kabKotas = KabupatenKotaPeer::doSelect($c);
	
	// $syarats = MSyaratTunjanganPeer::doSelect(new Criteria());
	// $i = 1;
	// foreach ($syarats as $s) {
		// //$s = new RSyaratTunjanganProfesi();
		// $arrRefSyarat[$i] = $s->toArray(BasePeer::TYPE_PHPNAME);
		// $i++;
	// }
	$con = Propel::getConnection(PtkPeer::DATABASE_NAME);
	$con->beginTransaction();
	$affected = 0;
	$counter = 0;
	
	foreach ($kabKotas as $kk){
//			$kk = new KabupatenKota();
		$c = new Criteria();
		$c->add(TRekapValidasiPeer::R_KEPERLUAN_ID, $jn);
		$c->add(TRekapValidasiPeer::KABUPATEN_KOTA_ID, $kk->getKabupatenKotaId());
		$rekapKk = TRekapValidasiPeer::doSelectOne($c);
		if(!is_object($rekapKk)){
			$rekapKk = new TRekapValidasi();
			$rekapKk->setRKeperluanId($jn);
			$rekapKk->setKabupatenKotaId($kk->getKabupatenKotaId());
			$rekapKk->setPropinsiId($kk->getPropinsiId());
		}
		switch($jn){
			case 3:
				// $sql = "SELECT
					// COUNT(t_ptk_id) as jumlah
					// ,(cekPensiun(tgl_pensiun)*cekSudahSertifikasi(nrg)*cekPnsGty(status_kepegawaian_id)*cekJamMengajar(total_jam_mengajar_sesuai, 24)) as validasi
				// from
					// t_ptk
				// WHERE
					// kabupaten_kota_sekolah_id = {$kk->getKabupatenKotaId()}
				// group by
					// validasi";
				break;
			case 4:
				// $sql = "SELECT
					// COUNT(t_ptk_id) as jumlah
					// ,(cekNuptk(nuptk)*cekMasaKerja(`masa_kerja_tahun`, ".$arrRefSyarat[1]["SqlCriteria"].")*cekStatusPegawai(`status_kepegawaian_id`, ".$arrRefSyarat[2]["SqlCriteria"].")*cekJamMengajar(`total_jam_mengajar_sesuai`, ".$arrRefSyarat[3]["SqlCriteria"].")*cekSertifikasi(`nrg`)) AS m_status_dokumen_id 
				// FROM 
					// t_ptk 
				// WHERE 
					// kabupaten_kota_sekolah_id = {$kk->getKabupatenKotaId()} 
					// AND status_kepegawaian_id > 3
					// AND jabatan_ptk_id < 3
				// GROUP BY m_status_dokumen_id";
				break;
			case 5:
				// $sql = "SELECT
					// COUNT(t_ptk_id) as jumlah
					// ,(cekNuptk(nuptk)*cekMasaKerja(`masa_kerja_tahun`, ".$arrRefSyarat[4]["SqlCriteria"].")*cekSedangKuliah(t_ptk_id)*cekJamMengajar(`total_jam_mengajar_sesuai`, ".$arrRefSyarat[5]["SqlCriteria"].")) AS m_status_dokumen_id 
				// FROM 
					// t_ptk 
				// WHERE 
					// kabupaten_kota_sekolah_id = {$kk->getKabupatenKotaId()} 
					// AND ijazah_terakhir_id < 9
				// GROUP BY m_status_dokumen_id";
				break;
			case 6:
				$sql = "SELECT
						COUNT(t_ptk_id) as jumlah 
					FROM 
						t_ptk 
					WHERE 
						kabupaten_kota_sekolah_id = {$kk->getKabupatenKotaId()} 
						and jabatan_ptk_id in(1,2)
					group by 
					(dbo.cekNuptk(nuptk)*dbo.cekMasaKerja(masa_kerja_tahun, -1)*dbo.cekSekolahKhusus(sekolah_id)*dbo.cekJamMengajar(total_jam_mengajar_sesuai, 24))";
			case 7:
				// $sql = "SELECT
					// COUNT(t_ptk_id) as jumlah
					// ,(cekPensiun(tgl_pensiun)*cekSudahSertifikasi(nrg)*cekDau(status_kepegawaian_id, jenis_sekolah_id)*cekJamMengajar(total_jam_mengajar_sesuai, 24)) as validasi
				// from
					// t_ptk
				// WHERE
					// kabupaten_kota_sekolah_id = {$kk->getKabupatenKotaId()}
				// group by
					// validasi";
				break;
			case 8:
				// $sql = "SELECT
					// COUNT(t_ptk_id) as jumlah
					// ,(cekPensiun(tgl_pensiun)*cekSudahSertifikasi(nrg)*cekDekon(status_kepegawaian_id, jenis_sekolah_id)*cekJamMengajar(total_jam_mengajar_sesuai, 24)) as validasi
				// from
					// t_ptk
				// WHERE
					// kabupaten_kota_sekolah_id = {$kk->getKabupatenKotaId()}
				// group by
					// validasi";
				break;
		}
		$data = getDataBySql($sql);
		$count = count($data);
		$sql = "SELECT
				COUNT(t_ptk_id)
				from
				t_ptk
				WHERE
				kabupaten_kota_sekolah_id = {$kk->getKabupatenKotaId()}";
		if($count == 2){
			$jumlah =  getValueBySql($sql) - ($data[1]["jumlah"]+$data[0]["jumlah"]);
			$rekapKk->setJumlahIncomplete($jumlah);
			$rekapKk->setJumlahPass($data[1]["jumlah"]);
			$rekapKk->setJumlahUnqualified($data[0]["jumlah"]);
			echo $kk->getNama()." Incomplete : {$jumlah} Pass : ".$data[1]["jumlah"].", Unqualified : ".$data[0]["jumlah"]."\n";
		}else if($count == 3){
			$jumlah =  getValueBySql($sql) - ($data[2]["jumlah"]+$data[1]["jumlah"]+$data[0]["jumlah"]);
			$rekapKk->setJumlahIncomplete($jumlah);
			$rekapKk->setJumlahPass($data[2]["jumlah"]);
			$rekapKk->setJumlahUnqualified($data[1]["jumlah"]);
			echo $kk->getNama()." Incomplete : {$jumlah} Pass : ".$data[2]["jumlah"].", Unqualified : ".$data[1]["jumlah"]."\n";
		}
		if($rekapKk->save()){
			$affected++;
		}
		$counter++;
		if(($counter % 100) == 0){
			$con->commit();
		}
	}
	$con->commit();
}

if ($argv) {
	
	$jn = $argv[1];
	$kabKotaId = $argv[2];

} else {

	$jn = ($_REQUEST["jn"]) ? $_REQUEST["jn"] : 1;
	$kabKotaId = ($_REQUEST["kabkota"]) ? $_REQUEST["kabkota"] : 0;
			
}

Trekap($jn, $kabKotaId);

?>