<?php
//error_reporting(E_ALL);

define ('STATUS_SYARAT_KOSONG', 1);
define ('STATUS_SYARAT_WARNING', 2);	
define ('STATUS_SYARAT_OK', 3);
define ('STATUS_SYARAT_INVALID', 6);	

define ('SYARAT_MEMENUHI', 1);
define ('SYARAT_TIDAK_MEMENUHI', 2);

/*
function zero_filter($val) {
	$filtered = ($val === 0);
	return !$filtered;
}
*/

function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}


class SinkronisasiModule extends XondModule {
	
	function SinkronisasiModule() {
		parent::__construct();
		$this->registerUi('sinkronisasi');
		$this->setMinifyUi(false);
		$this->setTitle('Sinkronisasi');
	}
	
	function execGetRekap() {
		
		//$ptks = ne
		//echo "test";
		/*
		$kabKotaId = $_REQUEST["kabupaten_kota_id"] ? $_REQUEST["kabupaten_kota_id"] : 144;	
		$c->add(TPtkPeer::KABUPATEN_KOTA_SEKOLAH_ID);
		$ptks = TPtkPeer::doSelect($c);		
		$this->write(tableJson(getArray($ptks), sizeof($ptks), TPtkPeer::getFieldNames(BasePeer::TYPE_PHPNAME)));
		*/
		//$kabKotaId = $_REQUEST["kabupaten_kota_id"] ? $_REQUEST["kabupaten_kota_id"] : 144;
		$pilihtunjangan = $_REQUEST['pilihtunjangan'] ? $_REQUEST['pilihtunjangan'] : 3;
		
		$c = new Criteria();
		$c->addAscendingOrderByColumn(TRekapValidasiPeer::KABUPATEN_KOTA_ID);
		$c->add(TRekapValidasiPeer::R_KEPERLUAN_ID, $pilihtunjangan);
		
		/* Nasional */
		$arrOut = array(); 
		$arrTemp = array();
		$incompleteTemp = 0;
		$passTemp = 0;
		$unqualifiedTemp = 0;
		
		$incompleteTempNasional = 0;
		$passTempNasional = 0;
		$unqualifiedTempNasional = 0;
		
		$rvs = TRekapValidasiPeer::doSelectJoinAll($c);
		array_push($rvs, array());
		$i = 0;
		foreach ($rvs as $rv) {
			//$rv = new TRekapValidasi();			
			$arr = (is_object($rv)) ? $rv->toArray(BasePeer::TYPE_PHPNAME) : array();
			$propinsiId = (is_object($rv)) ?  $rv->getPropinsiId() : NULL;
			
			if ($tmpPropinsiId != $propinsiId) {
				if ($i != 0) {
					$kode = addZeroes($propinsiIdTemp, 2)."000";
					
					$arrProp["RKeperluanId"] = 3;
					$arrProp["_id"] = $kode;
					$arrProp["_parent"] = "00001";
					$arrProp["_is_leaf"] = false; //prop
					$arrProp["Level"] = 2;
					$arrProp["KabupatenKotaId"] = NULL;
					$arrProp["PropinsiId"] = $propinsiIdTemp;
					$arrProp["Nama"] = $namaPropinsiTemp;
					$arrProp["JumlahIncomplete"] = $incompleteTemp;
					$arrProp["JumlahPass"] = $passTemp;
					$arrProp["JumlahUnqualified"] = $unqualifiedTemp;
					$arrPropDanKab[] = $arrProp;
					$arrPropDanKab = array_merge($arrPropDanKab, $arrTemp);					
					$arrOut = array_merge($arrOut, $arrPropDanKab);
					
					$arrTemp = array();
					$arrPropDanKab = array();
					
					$incompleteTemp = 0;
					$passTemp = 0;
					$unqualifiedTemp = 0;					
				}
			} 
			
			if (!is_object($rv)) {
				continue;
			}
			
			$kode = addZeroes($arr["PropinsiId"], 2).addZeroes($arr["KabupatenKotaId"],3);
			$kodeProp = addZeroes($arr["PropinsiId"], 2)."000";			
			$arr["Level"] = 3;
			$arr["Nama"] = $rv->getKabupatenKota()->getNama();
			$arr["_id"] = $kode;
			$arr["_parent"] = $kodeProp;
			$arr["_is_leaf"] = true;
			
			$arrTemp[] = $arr;
			
			$propinsiIdTemp = $rv->getPropinsiId();
			$namaPropinsiTemp = $rv->getPropinsi()->getNama();
			$incompleteTemp += $rv->getJumlahIncomplete();
			$passTemp += $rv->getJumlahPass();
			$unqualifiedTemp += $rv->getJumlahUnqualified();
			
			$incompleteTempNasional += $rv->getJumlahIncomplete();
			$passTempNasional += $rv->getJumlahPass();
			$unqualifiedTempNasional += $rv->getJumlahUnqualified();
			
			$tmpPropinsiId = $rv->getPropinsiId();
			$i++;			
		}
		
		$arrNasional["RKeperluanId"] = 3;
		$arrNasional["_id"] = "00001";
		$arrNasional["_parent"] = 0;
		$arrNasional["_is_leaf"] = false; //nasional
		$arrNasional["Level"] = 1;
		$arrNasional["KabupatenKotaId"] = NULL;
		$arrNasional["PropinsiId"] = NULL;
		$arrNasional["Nama"] = "Nasional";
		$arrNasional["JumlahIncomplete"] = $incompleteTempNasional;
		$arrNasional["JumlahPass"] = $passTempNasional;
		$arrNasional["JumlahUnqualified"] = $unqualifiedTempNasional;
		$nasional[] = $arrNasional;
		//$arrOut[] = $arrNasional;	
		
		$arrExit = array_merge($nasional, $arrOut);		
		foreach ($arrOut as $a){
			$kode = addZeroes($a["PropinsiId"], 2).addZeroes($a["KabupatenKotaId"],3);
			$arrExit[$kode] = $a;			 
		}
		//print_r($arrExit);
		
		$props = PropinsiPeer::doSelect(new Criteria());
		
		foreach ($props as $p) {
			//$p = new Propinsi();
			$kode = addZeroes($p->getPrimaryKey(), 2)."000";
			if (!$arrExit[$kode]) {
				$arrProp["RKeperluanId"] = 3;
				$arrProp["_id"] = $kode;
				$arrProp["_parent"] = "00001";
				$arrProp["_is_leaf"] = false; //prop
				$arrProp["Level"] = 2;
				$arrProp["KabupatenKotaId"] = NULL;
				$arrProp["PropinsiId"] = $p->getPrimaryKey();
				$arrProp["Nama"] = "Provinsi ". $p->getNama();
				$arrProp["JumlahIncomplete"] = 0;
				$arrProp["JumlahPass"] = 0;
				$arrProp["JumlahUnqualified"] = 0;
				$arrExit[$kode] = $arrProp;
			}
			$kks = $p->getKabupatenKotas();
			foreach ($kks as $kk) {
				//$kk = new KabupatenKota();
				$kode = addZeroes($p->getPrimaryKey(), 2).addZeroes($kk->getPrimaryKey(),3);
				$kodeProp = addZeroes($p->getPrimaryKey(), 2)."000";
				if (!$arrExit[$kode]) {
					$arrKabkota["RKeperluanId"] = 3;
					$arrKabkota["_id"] = $kode;
					$arrKabkota["_parent"] = $kodeProp;
					$arrKabkota["_is_leaf"] = true;
					
					$arrKabkota["Level"] = 3;
					$arrKabkota["KabupatenKotaId"] = $kk->getPrimaryKey();
					$arrKabkota["PropinsiId"] = $p->getPrimaryKey();
					$arrKabkota["Nama"] = $kk->getNama();
					$arrKabkota["JumlahIncomplete"] = 0;
					$arrKabkota["JumlahPass"] = 0;
					$arrKabkota["JumlahUnqualified"] = 0;
					$arrExit[$kode] = $arrKabkota;					
				}				
			}
		}
		
		ksort($arrExit);
		//print_r($arrExit);
		
		foreach ($arrExit as $a) {
			$arrDone[] = $a;
		}
		
		//print_r($arrDone);
    	//print (tableJson($arrDone, sizeof($arrDone), JenisTransaksiPeer::getFieldNames(BasePeer::TYPE_PHPNAME)));
		$this->write(tableJson($arrDone, sizeof($arrDone), array("RKeperluanId", "_id", "_parent", "Level", "KabupatenKotaId", "PropinsiId", "Nama", "JumlahIncomplete", "JumlahPass", "JumlahUnqualified")));		
	
		
	}
	
	function execGetProgressSinkronisasi() {		
		
		if ($_REQUEST["level"] == 2) {
			$propId = $_REQUEST["id"];			
			$filename = "konversi-propinsi-$propId.txt";			
		} else if ($_REQUEST["level"] == 3) {			
			$kabKotaId = $_REQUEST["id"];		
			$filename = "konversi-kabkota-$kabKotaId.txt";
		}
		$start = $_REQUEST["start"];
		
		$path = ROOT;
		$filepath = $path.D.'tmp'.D.$filename;
		//die ($filepath);
		
		if (file_exists($filepath)) {
			$arrKonversi = unserialize(file_get_contents($filepath));
			//$arrKonversi = array("kabupaten_kota_id" => $kabKotaId, "progress" => $progress, "total" => $total);			
			$this->write("{ 'success': true, 'progress': {$arrKonversi["progress"]}, 'total': {$arrKonversi["total"]}, 'start': false }");
		} else {			
			if ($start == 1) {
				$this->write("{ 'success': true, 'start': 1, 'progress': 0 }");
			} else {
				$this->write("{ 'success': false }");
			}
		}	
	}
	
	function execFetch() {
		
		$kabKotaId = $_REQUEST["kabupaten_kota_id"] ? $_REQUEST["kabupaten_kota_id"] : 146;
		$start = $_REQUEST["start"] ? $_REQUEST["start"] : 0;
		$limit = $_REQUEST["limit"] ? $_REQUEST["limit"] : 30;
		$filter_nama = "%".str_replace(" ", "%", $_REQUEST["filter_nama"])."%"; 
		
		$c = new Criteria();
		$c->add(TPtkPeer::KABUPATEN_KOTA_SEKOLAH_ID, $kabKotaId);
		$c->addAscendingOrderByColumn(TPtkPeer::NAMA_SEKOLAH);
		
		if ($_REQUEST["filter_nama"]) 
			$c->add(TPtkPeer::NAMA_SEKOLAH, $filter_nama, Criteria::LIKE);
		$count = TPtkPeer::doCount($c);
		
		$c->setLimit($limit);
		$c->setOffset($start);
		
		$praDupaks = TPtkPeer::doSelect($c);
		$this->write(tableJson(getArray($praDupaks), $count, TPtkPeer::getFieldNames(BasePeer::TYPE_PHPNAME)));		
	}
	
	function execRefreshJjm() {
		try {
			
			$execute = "execute dbo.syncStepByStepRefreshJam 1
					execute dbo.syncStepByStepRefreshJam 2
					execute dbo.syncStepByStepRefreshJam 3
					execute dbo.syncStepByStepRefreshJam 4
					execute dbo.syncStepByStepRefreshJam 5
					execute dbo.syncStepByStepRefreshJam 6
					execute dbo.syncStepByStepRefreshJam 7
					execute dbo.syncStepByStepRefreshJam 8
					execute dbo.syncStepByStepRefreshJam 9
					execute dbo.syncStepByStepRefreshJam 10
					execute dbo.syncStepByStepRefreshJam 11
					execute dbo.syncStepByStepRefreshJam 12
					execute dbo.syncStepByStepRefreshJam 13
					execute dbo.syncStepByStepRefreshJam 14
					execute dbo.syncStepByStepRefreshJam 15
					execute dbo.syncStepByStepRefreshJam 16
					execute dbo.syncStepByStepRefreshJam 17
					execute dbo.syncStepByStepRefreshJam 18
					execute dbo.syncStepByStepRefreshJam 19
					execute dbo.syncStepByStepRefreshJam 20
					execute dbo.syncStepByStepRefreshJam 21
					execute dbo.syncStepByStepRefreshJam 22
					execute dbo.syncStepByStepRefreshJam 23
					execute dbo.syncStepByStepRefreshJam 24
					execute dbo.syncStepByStepRefreshJam 25
					execute dbo.syncStepByStepRefreshJam 26
					execute dbo.syncStepByStepRefreshJam 27
					execute dbo.syncStepByStepRefreshJam 28
					execute dbo.syncStepByStepRefreshJam 29
					execute dbo.syncStepByStepRefreshJam 30
					execute dbo.syncStepByStepRefreshJam 31
					execute dbo.syncStepByStepRefreshJam 32
					execute dbo.syncStepByStepRefreshJam 33
					";
			$execute = "execute dbo.syncStepByStepRefreshJam 1";
			getDataBySql($execute);
			
			$this->write("{ 'success': 'true', 'message': 'Berhasil mengupdate' }");
		} catch (Exception $e) {
			$this->write("{ 'success': 'true', 'message': 'Gagal: ".$e->getMessage()."' }") ;
		}
	}
}
?>