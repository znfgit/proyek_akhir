<?php
//error_reporting(E_ALL);

class MailModule extends XondModule {
	
	function MailModule() {
		parent::__construct();
		//$this->registerUi('sinkronisasi');
		//$this->setMinifyUi(false);
		//$this->setTitle('Mail');
	}
	
	function execSend() {
		
		$swift_lib = ROOT.D.'swift'.D.'swift_required.php';
		require_once($swift_lib);
		
		//print_r($_REQUEST);
		$from = $_REQUEST["From"];
		$subject = $_REQUEST["Subject"];
		$body = $_REQUEST["Body"];
		$toArr = $_REQUEST["To"];
		$cc = $_REQUEST["Cc"];
		
		$transport = Swift_SmtpTransport::newInstance('10.20.10.212', 25);
		$mailer = Swift_Mailer::newInstance($transport);
		$message = Swift_Message::newInstance()
		
		// Give the message a subject
		->setSubject($subject)
			
		// Set the From address with an associative array
		//->setFrom(array('dit.p2tk.dikdas@cni.net.id' => 'Dit.P2TK Dikdas'))
		->setFrom($from)
		
		// Set the To addresses with an associative array
		//->setTo(array('donny.fauzan@gmail.com' => 'Donny Fauzan'))
		->addTo($cc)

		// Format it
		->setContentType("text/html")
		
		// Give it a body
		->setBody($body, 'text/html')
		
		// Add alternative parts with addPart()
		->addPart(strip_tags($body), 'text/plain');
		
		
		/*
		$message->getHeaders();
		$headers->addParameterizedHeader(
			array (
				"MIME-Version:" => "1.0"
			)
		);
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type: text/plain; charset=iso-8859-1\n";
		$headers .= "Message-ID: <".time()." TheSystem@".$_SERVER['SERVER_NAME'].">\r\n";
		$headers .= "X-Mailer: PHP v".phpversion()."\r\n";
		/*
		foreach ($toArr as $to) {
			
			switch ($to) {
				case 10:
					// Set the To addresses with an associative array
					$message->addTo(array('donny.fauzan@gmail.com' => 'Donny Fauzan'));
					break; 
			}
			
			// And optionally an alternative body
			//->addPart('<q>Here is the message itself</q>', 'text/html')
			
			// Optionally add any attachments
			//->attach(Swift_Attachment::fromPath('my-document.pdf')) 	  ;
		
		}
		*/
		
		$mailer->send($message);
		
		
		if (!$mailer->send($message, $failures)) {
			//echo "Failures:";
			//print_r($failures); die;
			
			$this->write("{'success': 'false', 'message': Gagal mengirim '".implode(",", $failures)."'}");
		} else {
			//echo "<br>Email berhasil dikirim";
			$this->write("{'success': 'true', 'message': 'Berhasil terkirim'}");
		}
	}

}
?>