<?php

class SekolahModule extends XondModule {
	
	function SekolahModule() {
		parent::__construct();
		$this->registerUi('sinkronisasi');
		$this->setMinifyUi(false);
		$this->setTitle('Sinkronisasi');
	}
	
	function execFetchSekolah() {
		$c = new Criteria();
		
	
		if ($_REQUEST["kabkota"]) {
			$c->add(SekolahPeer::KABUPATEN_KOTA_ID, $_REQUEST["kabkota"]);
		}
		if ($_REQUEST["jenissekolah"]) {
			$c->add(SekolahPeer::JENIS_SEKOLAH_ID, $_REQUEST["jenissekolah"]);
		}		
		if ($_REQUEST["sekolah_id"]) {
			$queryStr = str_replace(" ","%",$_REQUEST["sekolah_id"]);			
			$c->add(SekolahPeer::SEKOLAH_ID, $queryStr);
		}
		if ($_REQUEST["statussekolah"]) {			
			$c->add(SekolahPeer::STATUS_SEKOLAH, $_REQUEST["statussekolah"]);
		}
		if ($_REQUEST["namasekolah"]) {
			$namaSekolah = $_REQUEST["namasekolah"];			
			$namaSekolah = "%".str_replace(" ", "%", $namaSekolah)."%";
			$c->add(SekolahPeer::NAMA, $namaSekolah, Criteria::LIKE);
		}
		
		$numrows = SekolahPeer::doCount($c);
		
		if (isset($_REQUEST['limit']) && isset($_REQUEST['start'])) {
			$limit = $_REQUEST['limit'];
			$start = $_REQUEST['start'];			
		} else {
			$limit = 20;
			$start = 0;
		}	
			
		$c->setLimit($limit);
		$c->setOffset($start);
		$sekolahs = SekolahPeer::doSelect($c);
		foreach($sekolahs as $s) {
			$arr = $s->toArray();
			$arr["NamaKecamatan"] = ($s->getKecamatan()) ? $s->getKecamatan()->getNama() : "-";
			$arr["NamaKabupatenKota"] = $s->getKabupatenKota()->getNama();			
			$arr["NamaPropinsi"] = $s->getKabupatenKota()->getPropinsi()->getNama();
			$outArr[] = $arr;
			
		}
		$this->write(tableJson($outArr, $numrows, SekolahPeer::getFieldNames(BasePeer::TYPE_PHPNAME)));
		
	}
	
	function execFetchTPtk(){
		$c = new Criteria();
		if($_REQUEST["sekolah_id"]){
			$c->add(TPtkPeer::SEKOLAH_ID, $_REQUEST["sekolah_id"]);
		}
		$numrows = TPtkPeer::doCount($c);
		if (isset($_REQUEST['limit']) && isset($_REQUEST['start'])) {
			$limit = $_REQUEST['limit'];
			$start = $_REQUEST['start'];			
		} else {
			$limit = 20;
			$start = 0;
		}
		$c->setLimit($limit);
		$c->setOffset($start);
		$ptks = TPtkPeer::doSelect($c);
		foreach ($ptks as $p){
//			$p = new TPtk();
			$arr = $p->toArray();
			$arr["TglLahir"] = $p->getTglLahir("Y/m/d");
			$arr["TmtSekolah"] = $p->getTmtSekolah("Y/m/d");
			$arr["TmtJabatan"] = $p->getTmtJabatan("Y/m/d");
			$arr["TmtPns"] = $p->getTmtPns("Y/m/d");
			$arr["TmtPangkatGol"] = $p->getTmtPangkatGol("Y/m/d");
			$arr["TmtPengangkatan"] = $p->getTmtPengangkatan("Y/m/d");
			$arr["TmtKgb"] = $p->getTmtKgb("Y/m/d");
			$arr["TglLulusSertifikasi"] = $p->getTglLulusSertifikasi("Y/m/d");
			$arr["TglPensiun"] = $p->getTglPensiun("Y/m/d");
			
			$outArr[] = $arr;
		}
//		print_r($outArr);die;
		$this->write(tableJson($outArr, $numrows, TPtkPeer::getFieldNames(BasePeer::TYPE_PHPNAME)));
	}
}

?>