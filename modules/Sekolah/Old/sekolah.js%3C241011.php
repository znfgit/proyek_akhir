pendataan.SekolahModule = Ext.extend(Xond.Module, {
	
	name: 'SekolahId',
	title: 'Sekolah',
	panel: null,
	mainContainer: null,
	//parentMenu: 1,
	
	init : function(){		
		this.addEvents({
			'create' : true
		});
				
	},
	
	create : function(src){
		
		
		///////////// GRID SEKOLAH ///////////////
		
		pendataan.RecordSetBentukPendidikan = [
			{ name: 'BentukPendidikanId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  } ,
			{ name: 'TingkatPendidikanId', type: 'float'  } ,
			{ name: 'JalurPendidikanId', type: 'float'  } ,
			{ name: 'Active', type: 'float'  }	 
		];
		
		pendataan.RecordObjBentukPendidikan = Ext.data.Record.create(pendataan.RecordSetBentukPendidikan);
		
		pendataan.LookupStoreBentukPendidikan = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'BentukPendidikanId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetBentukPendidikan		
			}),
			//url: './BentukPendidikan/fetch.json',
			autoLoad: true,		
			url: './data/BentukPendidikan.json',
			
			sortInfo: {field: 'BentukPendidikanId', direction: "ASC"}
		});
		
		pendataan.RecordSetKategoriWilayah = [
			{ name: 'KategoriWilayahId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjKategoriWilayah = Ext.data.Record.create(pendataan.RecordSetKategoriWilayah);
		
		pendataan.LookupStoreKategoriWilayah = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'KategoriWilayahId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetKategoriWilayah		
			}),
			//url: './KategoriWilayah/fetch.json',
			autoLoad: true,		
			url: './data/KategoriWilayah.json',
			
			sortInfo: {field: 'KategoriWilayahId', direction: "ASC"}
		});
		
		pendataan.RecordSetAksesInternet = [
			{ name: 'AksesInternetId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjAksesInternet = Ext.data.Record.create(pendataan.RecordSetAksesInternet);
		
		pendataan.LookupStoreAksesInternet = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'AksesInternetId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetAksesInternet		
			}),
			//url: './AksesInternet/fetch.json',
			autoLoad: true,		
			url: './data/AksesInternet.json',
			
			sortInfo: {field: 'AksesInternetId', direction: "ASC"}
		});
		
		pendataan.RecordSetIsp = [
			{ name: 'IspId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjIsp = Ext.data.Record.create(pendataan.RecordSetIsp);
		
		pendataan.LookupStoreIsp = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'IspId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetIsp		
			}),
			//url: './Isp/fetch.json',
			autoLoad: true,		
			url: './data/Isp.json',
			
			sortInfo: {field: 'IspId', direction: "ASC"}
		});
		
		pendataan.RecordSetStatusKepemilikan = [
			{ name: 'StatusKepemilikanId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjStatusKepemilikan = Ext.data.Record.create(pendataan.RecordSetStatusKepemilikan);
		
		pendataan.LookupStoreStatusKepemilikan = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'StatusKepemilikanId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetStatusKepemilikan		
			}),
			//url: './StatusKepemilikan/fetch.json',
			autoLoad: true,		
			url: './data/StatusKepemilikan.json',
			
			sortInfo: {field: 'StatusKepemilikanId', direction: "ASC"}
		});
		
		pendataan.RecordSetAkreditasi = [
			{ name: 'AkreditasiId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjAkreditasi = Ext.data.Record.create(pendataan.RecordSetAkreditasi);
		
		pendataan.LookupStoreAkreditasi = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'AkreditasiId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetAkreditasi		
			}),
			//url: './Akreditasi/fetch.json',
			autoLoad: true,		
			url: './data/Akreditasi.json',
			
			sortInfo: {field: 'AkreditasiId', direction: "ASC"}
		});
		
		pendataan.RecordSetStatusMutu = [
			{ name: 'StatusMutuId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjStatusMutu = Ext.data.Record.create(pendataan.RecordSetStatusMutu);
		
		pendataan.LookupStoreStatusMutu = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'StatusMutuId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetStatusMutu		
			}),
			//url: './StatusMutu/fetch.json',
			autoLoad: true,		
			url: './data/StatusMutu.json',
			
			sortInfo: {field: 'StatusMutuId', direction: "ASC"}
		});
		
		pendataan.RecordSetSertifikasiIso = [
			{ name: 'SertifikasiIsoId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjSertifikasiIso = Ext.data.Record.create(pendataan.RecordSetSertifikasiIso);
		
		pendataan.LookupStoreSertifikasiIso = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'SertifikasiIsoId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetSertifikasiIso		
			}),
			//url: './SertifikasiIso/fetch.json',
			autoLoad: true,		
			url: './data/SertifikasiIso.json',
			
			sortInfo: {field: 'SertifikasiIsoId', direction: "ASC"}
		});
		
		pendataan.RecordSetWaktuPenyelenggaraan = [
			{ name: 'WaktuPenyelenggaraanId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjWaktuPenyelenggaraan = Ext.data.Record.create(pendataan.RecordSetWaktuPenyelenggaraan);
		
		pendataan.LookupStoreWaktuPenyelenggaraan = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'WaktuPenyelenggaraanId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetWaktuPenyelenggaraan		
			}),
			//url: './WaktuPenyelenggaraan/fetch.json',
			autoLoad: true,		
			url: './data/WaktuPenyelenggaraan.json',
			
			sortInfo: {field: 'WaktuPenyelenggaraanId', direction: "ASC"}
		});
		
		pendataan.RecordSetGugusSekolah = [
			{ name: 'GugusSekolahId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjGugusSekolah = Ext.data.Record.create(pendataan.RecordSetGugusSekolah);
		
		pendataan.LookupStoreGugusSekolah = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'GugusSekolahId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetGugusSekolah		
			}),
			//url: './GugusSekolah/fetch.json',
			autoLoad: true,		
			url: './data/GugusSekolah.json',
			
			sortInfo: {field: 'GugusSekolahId', direction: "ASC"}
		});
		
		pendataan.RecordSetKategoriSekolah = [
			{ name: 'KategoriSekolahId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjKategoriSekolah = Ext.data.Record.create(pendataan.RecordSetKategoriSekolah);
		
		pendataan.LookupStoreKategoriSekolah = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'KategoriSekolahId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetKategoriSekolah		
			}),
			//url: './KategoriSekolah/fetch.json',
			autoLoad: true,		
			url: './data/KategoriSekolah.json',
			
			sortInfo: {field: 'KategoriSekolahId', direction: "ASC"}
		});
		
		pendataan.RecordSetSumberListrik = [
			{ name: 'SumberListrikId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjSumberListrik = Ext.data.Record.create(pendataan.RecordSetSumberListrik);
		
		pendataan.LookupStoreSumberListrik = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'SumberListrikId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetSumberListrik		
			}),
			//url: './SumberListrik/fetch.json',
			autoLoad: true,		
			url: './data/SumberListrik.json',
			
			sortInfo: {field: 'SumberListrikId', direction: "ASC"}
		});
		
		pendataan.RecordSetDayaListrik = [
			{ name: 'DayaListrikId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjDayaListrik = Ext.data.Record.create(pendataan.RecordSetDayaListrik);
		
		pendataan.LookupStoreDayaListrik = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'DayaListrikId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetDayaListrik		
			}),
			//url: './DayaListrik/fetch.json',
			autoLoad: true,		
			url: './data/DayaListrik.json',
			
			sortInfo: {field: 'DayaListrikId', direction: "ASC"}
		});
		
		pendataan.RecordSetYayasan = [
			{ name: 'YayasanId', type: 'float'  } ,
			{ name: 'Nama', type: 'string'  } ,
			{ name: 'NamaPimpinanYayasan', type: 'string'  } ,
			{ name: 'AlamatJalanYayasan', type: 'string'  } ,
			{ name: 'NamaDesaKelurahan', type: 'string'  } ,
			{ name: 'KecamatanId', type: 'float'  } ,
			{ name: 'KabupatenKotaId', type: 'float'  } ,
			{ name: 'NoPendirianYayasan', type: 'string'  } ,
			{ name: 'TanggalPendirianYayasan', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'KelompokYayasanId', type: 'float'  }	 
		];
		
		pendataan.RecordObjYayasan = Ext.data.Record.create(pendataan.RecordSetYayasan);
		
		pendataan.LookupStoreYayasan = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'YayasanId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetYayasan		
			}),
			//url: './Yayasan/fetch.json',
			autoLoad: true,		
			url: './data/Yayasan.json',
			
			sortInfo: {field: 'YayasanId', direction: "ASC"}
		});

				
		
		pendataan.RecordSetKabupatenKota = [
			{ name: 'KabupatenKotaId', type: 'float'  } ,
			{ name: 'PropinsiId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  } ,
			{ name: 'Type', type: 'float'  } ,
			{ name: 'Status', type: 'float'  } ,
			{ name: 'Keterangan', type: 'string'  }	 
		];
		
		pendataan.RecordObjKabupatenKota = Ext.data.Record.create(pendataan.RecordSetKabupatenKota);
		
		pendataan.StoreKabupatenKota = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'KabupatenKotaId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetKabupatenKota		
			}),
			//url: './KabupatenKota/fetch.json',
			autoLoad: true,		
			url: './data/KabupatenKota.json',
			
			sortInfo: {field: 'KabupatenKotaId', direction: "ASC"}
		});
		
		pendataan.RecordSetPropinsi = [
			{ name: 'PropinsiId', type: 'float'  } ,
			{ name: 'NegaraId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  } ,
			{ name: 'Status', type: 'float'  }	 
		];
		
		pendataan.RecordObjPropinsi = Ext.data.Record.create(pendataan.RecordSetPropinsi);
		
		pendataan.LookupStorePropinsi = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'PropinsiId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetPropinsi		
			}),
			//url: './Propinsi/fetch.json',
			autoLoad: true,		
			url: './data/Propinsi.json',
			
			sortInfo: {field: 'PropinsiId', direction: "ASC"}
		});
		
		pendataan.RecordSetSekolah = [
			{ name: 'SekolahId', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  } ,
			{ name: 'Nss', type: 'string'  } ,
			{ name: 'Npsn', type: 'string'  } ,
			{ name: 'JenisSekolahId', type: 'float'  } ,
			{ name: 'AlamatJalan', type: 'string'  } ,
			{ name: 'Rt', type: 'string'  } ,
			{ name: 'Rw', type: 'string'  } ,
			{ name: 'NamaDusun', type: 'string'  } ,
			{ name: 'NamaDesaKelurahan', type: 'string'  } ,
			{ name: 'KecamatanId', type: 'float'  } ,
			{ name: 'KabupatenKotaId', type: 'float'  } ,
			{ name: 'KodePos', type: 'string'  } ,
			{ name: 'KategoriWilayahId', type: 'float'  } ,
			{ name: 'Lintang', type: 'string'  } ,
			{ name: 'Bujur', type: 'string'  } ,
			{ name: 'NomorTelepon', type: 'string'  } ,
			{ name: 'NomorFax', type: 'string'  } ,
			{ name: 'AksesInternetId', type: 'float'  } ,
			{ name: 'IspId', type: 'float'  } ,
			{ name: 'Email', type: 'string'  } ,
			{ name: 'Website', type: 'string'  } ,
			{ name: 'StatusSekolah', type: 'float'  } ,
			{ name: 'StatusKepemilikanId', type: 'float'  } ,
			{ name: 'SkPendirianSekolah', type: 'string'  } ,
			{ name: 'TanggalSkPendirian', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'SkIzinOperasional', type: 'string'  } ,
			{ name: 'TanggalSkIzinOperasional', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'AkreditasiId', type: 'float'  } ,
			{ name: 'NoSkAkreditasi', type: 'string'  } ,
			{ name: 'TanggalSkAkreditasi', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'StatusMutuId', type: 'float'  } ,
			{ name: 'SertifikasiIsoId', type: 'float'  } ,
			{ name: 'WaktuPenyelenggaraanId', type: 'float'  } ,
			{ name: 'GugusSekolahId', type: 'float'  } ,
			{ name: 'KategoriSekolahId', type: 'float'  } ,
			{ name: 'NoRekening', type: 'string'  } ,
			{ name: 'NamaBank', type: 'string'  } ,
			{ name: 'RekeningAtasNama', type: 'string'  } ,
			{ name: 'Mbs', type: 'float'  } ,
			{ name: 'SumberListrikId', type: 'float'  } ,
			{ name: 'DayaListrik', type: 'float'  } ,
			{ name: 'YayasanId', type: 'float'  }	 
		];
		
		pendataan.RecordObjSekolah = Ext.data.Record.create(pendataan.RecordSetSekolah);
		
		pendataan.StoreSekolah = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'SekolahId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetSekolah		
			}),
			url: './Sekolah/fetchSekolah.json',
			autoLoad: false,		
			//url: './data/Sekolah.json',
			
			sortInfo: {field: 'SekolahId', direction: "ASC"}
		});				
		
		
		pendataan.StoreSekolah.on('beforeload', function(store, options) {
			//if (options.params.query == "") {							
			options.params.kabkota = kabkotaCombo.getValue();
			options.params.jenissekolah = jenisSekolahCombo.getValue();
			//}
			return true;
		});
		
		
		pendataan.RecordSetSearchSekolah = [
			{ name: 'SekolahId', type: 'float'  } ,
			{ name: 'Nama', type: 'string'  } 
		];
		   
		pendataan.RecordObjSearchSekolah = Ext.data.Record.create(pendataan.RecordSetSearchSekolah);
		
		pendataan.StoreSearchSekolah = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'SekolahId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetSekolah		
			}),
			//url: './Sekolah/fetchSekolah.json',
			autoLoad: false,		
			url: './data/Sekolah.json',			
			sortInfo: {field: 'SekolahId', direction: "ASC"}
		});	
		
		var jenisSekolahData = [
			['5', 'SD'],
			['6', 'SMP'],
			['7', 'SDLB'],
			['8', 'SMPLB'],
			['14', 'SLB']
		]
		
		var jenisSekolahStore = new Ext.data.ArrayStore({
			fields: ['JenisSekolah', 'Nama'],
			data : jenisSekolahData // from states.js
		});
	
		var jenisSekolahCombo = new Ext.form.ComboBox({
			store: jenisSekolahStore,
			displayField: 'Nama',
			valueField: 'JenisSekolah',
			typeAhead: true,
			mode: 'local',
			triggerAction: 'all',
			emptyText: 'Pilih Sekolah...',
			selectOnFocus: true
			 
		});
		
		var propinsiCombo = new Ext.form.ComboBox({
			store: pendataan.LookupStorePropinsi,
			displayField: 'Nama',
			valueField: 'PropinsiId',
			typeAhead: true,
			mode: 'local',
			triggerAction: 'all',
			emptyText: 'Pilih Propinsi...',
			selectOnFocus: true,
			listeners : {
				select: function(thiscombo, record, index) {
					//Ext.Msg.alert('Info ', thiscombo.getValue());
					kabkotaCombo.clearValue();
					pendataan.StoreKabupatenKota.reload({
						params: {
							query: thiscombo.getValue(), 
							query_column: 'propinsi_id', 
							query_type: 'EQUAL'
						}
					})
				}
			}	
		});
		
		/*
		var searchSekolahTextField = new Ext.form.ComboBox({
			store: pendataan.StoreSearchSekolah,
			displayField: 'Nama',
			valueField: 'SekolahId',
			typeAhead: true,
			mode: 'remote',
			hideTrigger: true,
			width: 150, 
			triggerAction: 'all',
			emptyText: 'Cari sekolah...',
			selectOnFocus: true,
			listeners : {
				select: function(thiscombo, record, index) {
					//Ext.Msg.alert('Info ', thiscombo.getValue());
					pendataan.StoreSekolah.reload({
						params: {
							sekolah_id: thiscombo.getValue() 
						}
					})
				}
			}	
		});
		*/
		
		
		var kabkotaCombo = new Ext.form.ComboBox({
			store: pendataan.StoreKabupatenKota,
			displayField: 'Nama',
			valueField: 'KabupatenKotaId',
			typeAhead: true,
			mode: 'local',
			triggerAction: 'all',
			emptyText: 'Pilih Kabupaten Kota...',
			selectOnFocus: true,
			listeners : {
				select: function(thiscombo, record, index) {
					//Ext.Msg.alert('Info ', thiscombo.getValue());
					pendataan.StoreSekolah.reload({
						params: {						
							kabkota: thiscombo.getValue(), 
							jenissekolah: jenisSekolahCombo.getValue(),
							start: 0, 
							limit: 20							
						}
					});
				}
			}
		});
		
		var searchSekolahTextField = new Ext.form.TextField({
			emptyText: 'Cari nama sekolah..',
			enableKeyEvents: true,
			listeners: {
				keypress : function(field, e) {					
					if ((e.getKey() == Ext.EventObject.ENTER) || (e.getKey() == Ext.EventObject.TAB)) {

						pendataan.StoreSekolah.reload({
							params: {						
								kabkota: kabkotaCombo.getValue(), 
								jenissekolah: jenisSekolahCombo.getValue(),
								namasekolah: searchSekolahTextField.getValue(),
								start: 0, 
								limit: 20							
							}
						});
					}
				}
			}
		});
		
		var propertyStore = new Ext.data.JsonStore({
			autoLoad: true,  //autoload the data
			url: '/Sekolah/fetchDetilSekolah.json',
			root: 'rows',
			fields: ["sekolah_id","nama","sertifikasi_iso","alamat_jalan","rt","rw","nama_dusun","klasifikasi_geografis","kategori_wilayah","desa_kelurahan_id","kabupaten_kota_id","kode_pos","lintang","bujur","nomor_telepon","nomor_fax","akses_internet","isp","email","website","jarak_sekolah_terdekat","tahun_sekolah_dibuka","status_sekolah","jenis_sekolah","nss","nama_pimpinan","sk_pendirian_sekolah","tanggal_sk_pendirian","status_kepemilikan","sk_status_sekolah_terakhir","tanggal_status_sekolah","status_mutu","sertifikasi_iso","waktu_penyelenggaraan","gugus_sekolah","kategori_sekolah","no_rekening","nama_bank","rekening_atas_nama","mbs","menyelenggarakan_sbi"],
			listeners: {
				load: function(store, records, options){
						// get the property grid component
						//Ext.Msg.alert('Info ', 'Test !');
						//var propGrid = Ext.getCmp('propGrid');
						var propGrid = propertyGrid;
						// make sure the property grid exists
						if (propGrid) {
							// populate the property grid with store data
							propGrid.setSource(store.getAt(0).data);
						}
				}
			}
			
		});
		
		 Ext.grid.PropertyGrid.override({
			initComponent : function(){
				this.customRenderers = this.customRenderers || {};
				this.customEditors = this.customEditors || {};
				this.lastEditRow = null;
				var store = new Ext.grid.PropertyStore(this);
				this.propStore = store;
				var cm = new Ext.grid.PropertyColumnModel(this, store);
				// Need remove (comment out) the following line to avoid predefined sorting
				//store.store.sort('name', 'ASC');
				this.addEvents(
					'beforepropertychange',
					'propertychange'
				);
				this.cm = cm;
				this.ds = store.store;
				Ext.grid.PropertyGrid.superclass.initComponent.call(this);
 
				this.mon(this.selModel, 'beforecellselect', function(sm, rowIndex, colIndex){
					if(colIndex === 0){
						this.startEditing.defer(200, this, [rowIndex, 1]);
						return false;
					}
				}, this);
			}
		});
		
		
		var gridSekolah = new Ext.grid.GridPanel({
			sm: new Ext.grid.RowSelectionModel({
				listeners: {
					rowselect: function( thisSm, index, record ) {
						//propertyStore.reload({params:{id:record.data.SekolahId}});					
						
						propertyGrid.setSource(record.data);
						
						pendataan.StorePrasarana.reload({params:{
							query: record.data.SekolahId, 
							query_column: 'sekolah_id', 
							query_type: 'EQUAL'							
						}});
						pendataan.StorePesertaDidik.reload({params:{start:0,limit:20}});
						pendataan.StorePtk.reload({params:{start:0,limit:20}});																			
					} 
				}
			}),
			ds: pendataan.StoreSekolah,
			tbar: [{
				text: 'Filter&nbsp;:&nbsp;'
				//xtype: 'label'
			},
				jenisSekolahCombo,
			{
				text: '&nbsp;'
				//xtype: 'label
			},
				propinsiCombo,
			{
				text: '&nbsp;'
				//xtype: 'label
			},
				kabkotaCombo,
			{
				text: '&nbsp;'
				//xtype: 'label
			},
				searchSekolahTextField
			],
			bbar : new Ext.PagingToolbar({
				pageSize: 20,
				store: pendataan.StoreSekolah,
				paramNames: {start: 'start', limit: 'limit', kabkota: kabkotaCombo.getValue(), jenissekolah: jenisSekolahCombo.getValue(), namasekolah: searchSekolahTextField.getValue()},
				displayInfo: true, 
				displayMsg: 'Menampilkan sekolah {0} - {1} dari {2}',
				emptyMsg: "Data tidak ditemukan"
			}),
			
			columns : [{				
				header: 'Sekolah ID',
				width: 150,
				renderer: Xond.format.idMoney,
				align: 'center',
				sortable: true, 
				dataIndex: 'SekolahId'

			},{				
				header: 'Nama',
				width: 270,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'Nama'

			},{				
				header: 'NSS',
				width: 120,
				align: 'center',
				sortable: true, 
				dataIndex: 'Nss'

			},{				
				header: 'NPSN',
				width: 100,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'Npsn'

			},{				
				header: 'Jenis Sekolah',
				width: 80,
				editor: new Ext.form.ComboBox({ 
					store: pendataan.LookupStoreBentukPendidikan,
					displayField: 'Nama',
					valueField: 'BentukPendidikanId',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Pilih...',
					selectOnFocus:true
				}),
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStoreBentukPendidikan.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'JenisSekolahId'

			},{				
				header: 'Status Sekolah',
				width: 100,
				renderer:
					function(v,params,record) {
						return (v==1) ? "Negeri" : "Swasta";
					},
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'StatusSekolah'

			},{				
				header: 'Alamat Jalan',
				width: 150,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'AlamatJalan'

			},{				
				header: 'RT',
				width: 40,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'Rt'

			},{				
				header: 'RW',
				width: 40,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'Rw'

			},{				
				header: 'Nama Dusun',
				width: 100,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'NamaDusun'

			},{				
				header: 'Nama Desa/Kelurahan',
				width: 100,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'NamaDesaKelurahan'

			},{				
				header: 'Kecamatan',
				width: 100,
				editor: new Ext.form.ComboBox({ 
					store: pendataan.LookupStoreKecamatan,
					displayField: 'Nama',
					valueField: 'KecamatanId',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Pilih...',
					selectOnFocus:true
				}),
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStoreKecamatan.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'KecamatanId'

			},{				
				header: 'Kabupaten Kota',
				width: 160,
				editor: new Ext.form.ComboBox({ 
					store: pendataan.LookupStoreKabupatenKota,
					displayField: 'Nama',
					valueField: 'KabupatenKotaId',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Pilih...',
					selectOnFocus:true
				}),
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStoreKabupatenKota.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'KabupatenKotaId'

			},{				
				header: 'Kode Pos',
				width: 100,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'KodePos'

			},{				
				header: 'Kategori Wilayah',
				width: 100,
				editor: new Ext.form.ComboBox({ 
					store: pendataan.LookupStoreKategoriWilayah,
					displayField: 'Nama',
					valueField: 'KategoriWilayahId',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Pilih...',
					selectOnFocus:true
				}),
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStoreKategoriWilayah.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'KategoriWilayahId'

			},{				
				header: 'Lintang',
				width: 100,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'Lintang'

			},{				
				header: 'Bujur',
				width: 100,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'Bujur'

			},{				
				header: 'Nomor Telepon',
				width: 120,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'NomorTelepon'

			},{				
				header: 'Nomor Fax',
				width: 100,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'NomorFax'

			},{				
				header: 'Akses internet',
				width: 100,
				editor: new Ext.form.ComboBox({ 
					store: pendataan.LookupStoreAksesInternet,
					displayField: 'Nama',
					valueField: 'AksesInternetId',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Pilih...',
					selectOnFocus:true
				}),
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStoreAksesInternet.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'AksesInternetId'

			},{				
				header: 'ISP',
				width: 100,
				editor: new Ext.form.ComboBox({ 
					store: pendataan.LookupStoreIsp,
					displayField: 'Nama',
					valueField: 'IspId',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Pilih...',
					selectOnFocus:true
				}),
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStoreIsp.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'IspId'

			},{				
				header: 'Email',
				width: 100,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'Email'

			},{				
				header: 'Website',
				width: 120,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'Website'

			},{				
				header: 'Status Kepemilikan',
				width: 100,
				editor: new Ext.form.ComboBox({ 
					store: pendataan.LookupStoreStatusKepemilikan,
					displayField: 'Nama',
					valueField: 'StatusKepemilikanId',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Pilih...',
					selectOnFocus:true
				}),
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStoreStatusKepemilikan.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'StatusKepemilikanId'

			},{				
				header: 'SK Pendirian Sekolah',
				width: 100,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'SkPendirianSekolah'

			},{				
				header: 'Tanggal SK Pendirian',
				width: 150,
				renderer: Ext.util.Format.dateRenderer('Y/m/d'),
				editor: new fm.DateField({
					format: 'Y/m/d',
					minValue: '1920/01/01'
				}),
				sortable: true, 
				dataIndex: 'TanggalSkPendirian'

			},{				
				header: 'SK Izin Op',
				width: 40,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'SkIzinOperasional'

			},{				
				header: 'Tgl.SK Izin Op',
				width: 40,
				renderer: Ext.util.Format.dateRenderer('Y/m/d'),
				editor: new fm.DateField({
					format: 'Y/m/d',
					minValue: '1920/01/01'
				}),
				sortable: true, 
				dataIndex: 'TanggalSkIzinOperasional'

			},{				
				header: 'Akreditasi',
				width: 100,
				editor: new Ext.form.ComboBox({ 
					store: pendataan.LookupStoreAkreditasi,
					displayField: 'Nama',
					valueField: 'AkreditasiId',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Pilih...',
					selectOnFocus:true
				}),
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStoreAkreditasi.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'AkreditasiId'

			},{				
				header: 'No.SK Akreditasi',
				width: 100,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'NoSkAkreditasi'

			},{				
				header: 'Tgl.SK Akreditasi',
				width: 100,
				renderer: Ext.util.Format.dateRenderer('Y/m/d'),
				editor: new fm.DateField({
					format: 'Y/m/d',
					minValue: '1920/01/01'
				}),
				sortable: true, 
				dataIndex: 'TanggalSkAkreditasi'

			},{				
				header: 'Status Mutu',
				width: 100,
				editor: new Ext.form.ComboBox({ 
					store: pendataan.LookupStoreStatusMutu,
					displayField: 'Nama',
					valueField: 'StatusMutuId',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Pilih...',
					selectOnFocus:true
				}),
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStoreStatusMutu.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'StatusMutuId'

			},{				
				header: 'Sert.ISO',
				width: 100,
				editor: new Ext.form.ComboBox({ 
					store: pendataan.LookupStoreSertifikasiIso,
					displayField: 'Nama',
					valueField: 'SertifikasiIsoId',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Pilih...',
					selectOnFocus:true
				}),
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStoreSertifikasiIso.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'SertifikasiIsoId'

			},{				
				header: 'Wkt.Pnylnggaraan',
				width: 100,
				editor: new Ext.form.ComboBox({ 
					store: pendataan.LookupStoreWaktuPenyelenggaraan,
					displayField: 'Nama',
					valueField: 'WaktuPenyelenggaraanId',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Pilih...',
					selectOnFocus:true
				}),
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStoreWaktuPenyelenggaraan.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'WaktuPenyelenggaraanId'

			},{				
				header: 'Gugus Sekolah',
				width: 100,
				editor: new Ext.form.ComboBox({ 
					store: pendataan.LookupStoreGugusSekolah,
					displayField: 'Nama',
					valueField: 'GugusSekolahId',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Pilih...',
					selectOnFocus:true
				}),
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStoreGugusSekolah.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'GugusSekolahId'

			},{				
				header: 'Kategori Sekolah',
				width: 100,
				editor: new Ext.form.ComboBox({ 
					store: pendataan.LookupStoreKategoriSekolah,
					displayField: 'Nama',
					valueField: 'KategoriSekolahId',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Pilih...',
					selectOnFocus:true
				}),
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStoreKategoriSekolah.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'KategoriSekolahId'

			},{				
				header: 'No.Rek',
				width: 100,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'NoRekening'

			},{				
				header: 'Nama Bank',
				width: 100,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'NamaBank'

			},{				
				header: 'Rekening Atas Nama',
				width: 80,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'RekeningAtasNama'

			},{				
				header: 'MBS',
				width: 100,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'Mbs'

			},{				
				header: 'Sumber Listrik',
				width: 100,
				editor: new Ext.form.ComboBox({ 
					store: pendataan.LookupStoreSumberListrik,
					displayField: 'Nama',
					valueField: 'SumberListrikId',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Pilih...',
					selectOnFocus:true
				}),
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStoreSumberListrik.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'SumberListrikId'

			},{				
				header: 'Daya Listrik',
				width: 100,
				editor: new Ext.form.ComboBox({ 
					store: pendataan.LookupStoreDayaListrik,
					displayField: 'Nama',
					valueField: 'DayaListrikId',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Pilih...',
					selectOnFocus:true
				}),
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStoreDayaListrik.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'DayaListrik'

			},{				
				header: 'Yayasan',
				width: 100,				
				align: 'right',
				editor: new Ext.form.ComboBox({ 
					store: pendataan.LookupStoreYayasan,
					displayField: 'Nama',
					valueField: 'YayasanId',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Pilih...',
					selectOnFocus:true
				}),
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStoreYayasan.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'YayasanId'

			}]
			/*
			columns : [{				
				header: 'SekolahId',
				width: 80,
				sortable: true, 
				dataIndex: 'SekolahId'
			},{				
				header: 'Nss',
				width: 120,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'Nss'

			},{				
				header: 'Nama',
				width: 300,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'Nama'

			},{
				header: 'StatusSekolah',
				width: 80,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'StatusSekolah'
			}]*/
			
			,			
			loadMask: true,
			stripeRows: true
		});
		
		
		///////////////GRID PRASARANA/////////////////
		
		var propertyGrid = new Ext.grid.PropertyGrid({
			title: 'Data Sekolah',
			id: 'propGrid',
			listeners:  {
				render: function(grid)
				{
					grid.getColumnModel().setColumnWidth(0, 50);
				}
			},
			source: {}
		});
		
		pendataan.RecordSetPrasarana = [
			{ name: 'PrasaranaId', type: 'float'  } ,
			{ name: 'Kode', type: 'float'  } ,
			{ name: 'Nama', type: 'string'  } ,
			{ name: 'JenisPrasaranaId', type: 'string'  } ,
			{ name: 'Panjang', type: 'string'  } ,
			{ name: 'Lebar', type: 'string'  } ,
			{ name: 'KondisiAtap', type: 'string'  } ,
			{ name: 'KondisiDinding', type: 'string'  } ,
			{ name: 'KondisiKusen', type: 'string'  } ,
			{ name: 'KondisiLantai', type: 'string'  } ,
			{ name: 'KondisiPondasi', type: 'string'  } ,
			{ name: 'UrlFoto', type: 'string'  } ,
			{ name: 'SekolahId', type: 'float'  }	 
		];
		
		pendataan.RecordObjPrasarana = Ext.data.Record.create(pendataan.RecordSetPrasarana);
		
		pendataan.StorePrasarana = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'PrasaranaId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetPrasarana		
			}),
			//url: './Prasarana/fetch.json',
			autoLoad: false,		
			url: './data/Prasarana.json',
			sortInfo: {field: 'PrasaranaId', direction: "ASC"}
		});		
		
		pendataan.RecordSetSarana = [
			{ name: 'SaranaId', type: 'float'  } ,
			{ name: 'Nama', type: 'string'  } ,
			{ name: 'JenisSarana', type: 'float'  } ,
			{ name: 'Jumlah', type: 'float'  } ,
			{ name: 'PrasaranaId', type: 'float'  }	 
		];
		
		pendataan.RecordObjSarana = Ext.data.Record.create(pendataan.RecordSetSarana);
		
		pendataan.StoreSarana = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'SaranaId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetSarana		
			}),
			//url: './Sarana/fetch.json',
			autoLoad: false,		
			url: './data/Sarana.json',			
			sortInfo: {field: 'SaranaId', direction: "ASC"}
		});
		
		var view = new Ext.DataView({
			itemSelector: 'div.thumb-wrap',
			style:'overflow:auto',
			multiSelect: true,
			//plugins: new Ext.DataView.DragSelector({dragSafe:true}),
			store: pendataan.StorePrasarana,
			region: 'west',
			width: 340,
			listeners: {
				dblclick: function(view,index,node,e) {
					rec = pendataan.StorePrasarana.getAt(index);
					imageWindow = new ImageViewer({
						title: rec.data.Nama,
						src: "/images/" + rec.data.UrlFoto,
						hideAction: 'close'
					}).show();
				},
				click: function (view,index,node,e) {
					rec = pendataan.StorePrasarana.getAt(index);
					keteranganPrasaranaTemplate.overwrite(keteranganPrasaranaPanel.body, rec.data);					
					pendataan.StoreSarana.reload({params:{
						query: rec.data.PrasaranaId, 
						query_column: 'prasarana_id', 
						query_type: 'EQUAL'							
					}});
				}				
			},
			tpl: new Ext.XTemplate(
				'<tpl for=".">',
				'<div class="thumb-wrap" id="{name}">',
				'<div class="thumb"><img src="/images/{UrlFoto}" class="thumb-img"></div>',
				'<span>{Nama}</span></div>',
				'</tpl>'
			)
		});
	 	
	 	var keteranganPrasaranaTemplate = new Ext.Template([
			'<h2>{Nama}</h2><br>',
			'Luas: {Panjang}m x {Lebar}m<br/>',
			'Kondisi: Atap {KondisiAtap}%, Dinding {KondisiDinding}%, Kusen {KondisiKusen}%, Lantai {KondisiPintu}%<br/>'
	 	]);
	 	
	 	var keteranganPrasaranaPanel = new Ext.Panel({
			xtype: 'panel',
			region: 'north',
			height: 100,	
			bodyStyle: 'padding:15px',		
			tpl: keteranganPrasaranaTemplate
		});

		Ext.apply(keteranganPrasaranaPanel, {
			bodyStyle: {
				background: '#ffffff',
				padding: '7px'
			},
			html: 'Silakan pilih gambar prasarana untuk menampilkan informasi'
		});

		var gridSarana = new Ext.grid.EditorGridPanel({
			sm: new Ext.grid.RowSelectionModel(),
			ds: pendataan.StoreSarana,
			region: 'center',
			title: 'Daftar Sarana',
			columns : [{				
				header: 'SaranaId',
				width: 80,
				sortable: true, 
				dataIndex: 'SaranaId'
			},{				
				header: 'Nama',
				width: 150,
				sortable: true, 
				dataIndex: 'Nama'
			},{				
				header: 'JenisSarana',
				width: 100,
				sortable: true, 
				dataIndex: 'JenisSarana'
			},{				
				header: 'Jumlah',
				width: 80,
				sortable: true, 
				dataIndex: 'Jumlah'
			 }]
		});
				
		var fasilitasViewPanel = new Ext.Panel({
			id:'images',
			title:'Daftar Sarpras',			
			margins: '5 5 5 0',
			layout: 'border',
			//layoutConfig: {columns:2},
			defaults: {
				collapsible: false,
				split: true,
				bodyStyle: 'padding:15px'
			}, 
			items: [
				view,
				{
					xtype: 'panel',
					layout: 'border',
					region: 'center',
					//plain: true,
					border: false,					
					//html:'Kanan'				
					defaults: {
						collapsible: false,
						split: true						
					}, 
					items: [
						keteranganPrasaranaPanel,
						gridSarana
					]
				}
			]
		});
		
		
		////////////// GRID SISWA ///////////////////
		
		pendataan.RecordSetPesertaDidik = [
			{ name: 'PesertaDidikId', type: 'float'  } ,
			{ name: 'Nama', type: 'string'  } ,
			{ name: 'JenisKelamin', type: 'string'  } ,
			{ name: 'Nisn', type: 'string'  } ,
			{ name: 'Nik', type: 'string'  } ,
			{ name: 'TempatLahir', type: 'string'  } ,
			{ name: 'TanggalLahir', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'AgamaId', type: 'float'  } ,
			{ name: 'NamaAyah', type: 'string'  } ,
			{ name: 'TahunLahirAyah', type: 'float'  } ,
			{ name: 'PekerjaanAyahId', type: 'float'  } ,
			{ name: 'PendidikanAyahId', type: 'float'  } ,
			{ name: 'PenghasilanBulananAyah', type: 'float'  } ,
			{ name: 'NamaIbu', type: 'string'  } ,
			{ name: 'TahunLahirIbu', type: 'float'  } ,
			{ name: 'PekerjaanIbuId', type: 'float'  } ,
			{ name: 'PendidikanIbuId', type: 'float'  } ,
			{ name: 'PenghasilanBulananIbu', type: 'float'  } ,
			{ name: 'NamaWali', type: 'string'  } ,
			{ name: 'TahunLahirWali', type: 'float'  } ,
			{ name: 'PekerjaanWaliId', type: 'float'  } ,
			{ name: 'PendidikanWaliId', type: 'float'  } ,
			{ name: 'PenghasilanBulananWali', type: 'float'  } ,
			{ name: 'JenisTinggalId', type: 'float'  } ,
			{ name: 'AlamatRumah', type: 'string'  } ,
			{ name: 'Rt', type: 'string'  } ,
			{ name: 'Rw', type: 'string'  } ,
			{ name: 'NamaDesaKelurahan', type: 'string'  } ,
			{ name: 'KecamatanId', type: 'float'  } ,
			{ name: 'KabupatenKotaId', type: 'float'  } ,
			{ name: 'KodePos', type: 'string'  } ,
			{ name: 'TinggiBadan', type: 'string'  } ,
			{ name: 'BeratBadan', type: 'string'  } ,
			{ name: 'BerkebutuhanKhususId', type: 'float'  } ,
			{ name: 'NomorTeleponRumah', type: 'string'  } ,
			{ name: 'NomorTeleponSeluler', type: 'string'  } ,
			{ name: 'JarakRumahKeSekolah', type: 'float'  } ,
			{ name: 'JarakRumahKeSekolahKm', type: 'float'  } ,
			{ name: 'AlatTransportasiId', type: 'float'  } ,
			{ name: 'Email', type: 'string'  } ,
			{ name: 'SekolahId', type: 'float'  }	 
		];
		
		pendataan.RecordObjPesertaDidik = Ext.data.Record.create(pendataan.RecordSetPesertaDidik);
		
		pendataan.StorePesertaDidik = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'PesertaDidikId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetPesertaDidik		
			}),
			//url: './PesertaDidik/fetch.json',
			autoLoad: false,	   
			url: './data/PesertaDidik.json',
			listeners: {
				beforeload: function(store, options) {
					rec = gridSekolah.getSelectionModel().getSelected();
					options.params.query = rec.data.SekolahId;
					options.params.query_column = 'sekolah_id';
					options.params.query_type= 'EQUAL';
					return true;
				}
			},
			sortInfo: {field: 'PesertaDidikId', direction: "ASC"}
		});
		
		pendataan.RecordSetPekerjaan = [
			{ name: 'PekerjaanId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjPekerjaan = Ext.data.Record.create(pendataan.RecordSetPekerjaan);
		
		pendataan.LookupStorePekerjaan = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'PekerjaanId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetPekerjaan		
			}),
			//url: './Pekerjaan/fetch.json',
			autoLoad: true,	   
			url: './data/Pekerjaan.json',
			
			sortInfo: {field: 'PekerjaanId', direction: "ASC"}
		});
		
		pendataan.RecordSetJenjangPendidikan = [
			{ name: 'JenjangPendidikanId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjJenjangPendidikan = Ext.data.Record.create(pendataan.RecordSetJenjangPendidikan);
		
		pendataan.LookupStoreJenjangPendidikan = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'JenjangPendidikanId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetJenjangPendidikan		
			}),
			//url: './JenjangPendidikan/fetch.json',
			autoLoad: true,	   
			url: './data/JenjangPendidikan.json',
			
			sortInfo: {field: 'JenjangPendidikanId', direction: "ASC"}
		});
		
		pendataan.RecordSetPenghasilanOrangtuaWali = [
			{ name: 'PenghasilanOrangtuaWaliId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjPenghasilanOrangtuaWali = Ext.data.Record.create(pendataan.RecordSetPenghasilanOrangtuaWali);
		
		pendataan.LookupStorePenghasilanOrangtuaWali = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'PenghasilanOrangtuaWaliId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetPenghasilanOrangtuaWali		
			}),
			//url: './PenghasilanOrangtuaWali/fetch.json',
			autoLoad: true,	   
			url: './data/PenghasilanOrangtuaWali.json',
			
			sortInfo: {field: 'PenghasilanOrangtuaWaliId', direction: "ASC"}
		});
		
		pendataan.RecordSetPekerjaan = [
			{ name: 'PekerjaanId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjPekerjaan = Ext.data.Record.create(pendataan.RecordSetPekerjaan);
		
		pendataan.LookupStorePekerjaan = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'PekerjaanId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetPekerjaan		
			}),
			//url: './Pekerjaan/fetch.json',
			autoLoad: true,	   
			url: './data/Pekerjaan.json',
			
			sortInfo: {field: 'PekerjaanId', direction: "ASC"}
		});
		
		pendataan.RecordSetJenjangPendidikan = [
			{ name: 'JenjangPendidikanId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjJenjangPendidikan = Ext.data.Record.create(pendataan.RecordSetJenjangPendidikan);
		
		pendataan.LookupStoreJenjangPendidikan = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'JenjangPendidikanId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetJenjangPendidikan		
			}),
			//url: './JenjangPendidikan/fetch.json',
			autoLoad: true,	   
			url: './data/JenjangPendidikan.json',
			
			sortInfo: {field: 'JenjangPendidikanId', direction: "ASC"}
		});
		
		pendataan.RecordSetPenghasilanOrangtuaWali = [
			{ name: 'PenghasilanOrangtuaWaliId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjPenghasilanOrangtuaWali = Ext.data.Record.create(pendataan.RecordSetPenghasilanOrangtuaWali);
		
		pendataan.LookupStorePenghasilanOrangtuaWali = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'PenghasilanOrangtuaWaliId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetPenghasilanOrangtuaWali		
			}),
			//url: './PenghasilanOrangtuaWali/fetch.json',
			autoLoad: true,	   
			url: './data/PenghasilanOrangtuaWali.json',
			
			sortInfo: {field: 'PenghasilanOrangtuaWaliId', direction: "ASC"}
		});
		
		pendataan.RecordSetPekerjaan = [
			{ name: 'PekerjaanId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjPekerjaan = Ext.data.Record.create(pendataan.RecordSetPekerjaan);
		
		pendataan.LookupStorePekerjaan = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'PekerjaanId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetPekerjaan		
			}),
			//url: './Pekerjaan/fetch.json',
			autoLoad: true,	   
			url: './data/Pekerjaan.json',
			
			sortInfo: {field: 'PekerjaanId', direction: "ASC"}
		});
		
		pendataan.RecordSetJenjangPendidikan = [
			{ name: 'JenjangPendidikanId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjJenjangPendidikan = Ext.data.Record.create(pendataan.RecordSetJenjangPendidikan);
		
		pendataan.LookupStoreJenjangPendidikan = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'JenjangPendidikanId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetJenjangPendidikan		
			}),
			//url: './JenjangPendidikan/fetch.json',
			autoLoad: true,	   
			url: './data/JenjangPendidikan.json',
			
			sortInfo: {field: 'JenjangPendidikanId', direction: "ASC"}
		});
		
		pendataan.RecordSetPenghasilanOrangtuaWali = [
			{ name: 'PenghasilanOrangtuaWaliId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjPenghasilanOrangtuaWali = Ext.data.Record.create(pendataan.RecordSetPenghasilanOrangtuaWali);
		
		pendataan.LookupStorePenghasilanOrangtuaWali = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'PenghasilanOrangtuaWaliId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetPenghasilanOrangtuaWali		
			}),
			//url: './PenghasilanOrangtuaWali/fetch.json',
			autoLoad: true,	   
			url: './data/PenghasilanOrangtuaWali.json',
			
			sortInfo: {field: 'PenghasilanOrangtuaWaliId', direction: "ASC"}
		});
		
		pendataan.RecordSetJenisTinggal = [
			{ name: 'JenisTinggalId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjJenisTinggal = Ext.data.Record.create(pendataan.RecordSetJenisTinggal);
		
		pendataan.LookupStoreJenisTinggal = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'JenisTinggalId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetJenisTinggal		
			}),
			//url: './JenisTinggal/fetch.json',
			autoLoad: true,	   
			url: './data/JenisTinggal.json',
			
			sortInfo: {field: 'JenisTinggalId', direction: "ASC"}
		});
	
		pendataan.RecordSetBerkebutuhanKhusus = [
			{ name: 'BerkebutuhanKhususId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjBerkebutuhanKhusus = Ext.data.Record.create(pendataan.RecordSetBerkebutuhanKhusus);
		
		pendataan.LookupStoreBerkebutuhanKhusus = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'BerkebutuhanKhususId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetBerkebutuhanKhusus		
			}),
			//url: './BerkebutuhanKhusus/fetch.json',
			autoLoad: true,	   
			url: './data/BerkebutuhanKhusus.json',
			
			sortInfo: {field: 'BerkebutuhanKhususId', direction: "ASC"}
		});
		
		pendataan.RecordSetAlatTransportasi = [
			{ name: 'AlatTransportasiId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjAlatTransportasi = Ext.data.Record.create(pendataan.RecordSetAlatTransportasi);
		
		pendataan.LookupStoreAlatTransportasi = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'AlatTransportasiId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetAlatTransportasi		
			}),
			//url: './AlatTransportasi/fetch.json',
			autoLoad: true,	   
			url: './data/AlatTransportasi.json',
			
			sortInfo: {field: 'AlatTransportasiId', direction: "ASC"}
		});
	
		var siswaGrid = new Ext.grid.GridPanel({
			title: 'Daftar Siswa',
			tbar: [{
				text: 'Riwayat Beasiswa',
				iconCls: 'money_add',
				tooltip: 'Tampilkan daftar riwayat beasiswa siswa ini',
				handler: function() {
					var selected = siswaGrid.selModel.getSelected();				
					if (!(selected)) {
						Ext.Msg.alert('Error', 'Mohon pilih salah satu baris data..');
						return false;
					} else {
						var data = selected.data;
					}					
					
					pendataan.RecordSetPerolehanBeasiswa = [
						{ name: 'PerolehanBeasiswaId', type: 'float'  } ,
						{ name: 'PesertaDidikId', type: 'float'  } ,
						{ name: 'TahunAjaranIdMulai', type: 'string'  } ,
						{ name: 'TahunAjaranIdSelesai', type: 'string'  } ,
						{ name: 'SumberBeasiswaId', type: 'string'  }	 
					];
					
					pendataan.RecordObjPerolehanBeasiswa = Ext.data.Record.create(pendataan.RecordSetPerolehanBeasiswa);
					
					var riwayatBeasiswaStore = new Ext.data.Store({
						reader: new Ext.data.JsonReader({
							id: 'PerolehanBeasiswaId',
							root: 'rows',
							totalProperty: 'results',
							fields: pendataan.RecordSetPerolehanBeasiswa		
						}),
						url: './Sekolah/fetchPerolehanBeasiswa.json',
						autoLoad: true,		
						//url: './data/PerolehanBeasiswa.json',
						baseParams: {
							query: data.PesertaDidikId, 
							query_column: 'peserta_didik_id', 
							query_type: 'EQUAL'
						},						
						sortInfo: {field: 'PerolehanBeasiswaId', direction: "ASC"}
					});

					
					var summary = new Ext.grid.HybridSummary();
					
					var riwayatBeasiswaWin = new Ext.Window({
						title: 'Daftar Beasiswa Untuk : ' + data.NamaLengkap,
						width: 720,
						height: 360,
						//minWidth: 300,
						//minHeight: 200,
						layout: 'anchor',
						plain: true,
						bodyStyle: 'padding:5px;',
						buttonAlign: 'center',
						closable: true,
						items: [{
							ds: riwayatBeasiswaStore,
							id: 'riwayatbeasiswa',
							frame: false,
							plugins: summary,
							iconCls: 'icon-grid',
							stripeRows: true,
							loadMask: true,
							columns : [{				
								header: 'PerolehanBeasiswaId',
								width: 80,
								sortable: true, 
								dataIndex: 'PerolehanBeasiswaId'
				
							},{				
								header: 'PesertaDidikId',
								width: 80,
								sortable: true, 
								dataIndex: 'PesertaDidikId'				
							},{				
								header: 'TahunAjaranIdMulai',
								width: 80,
								sortable: true, 
								dataIndex: 'TahunAjaranIdMulai'
							},{				
								header: 'TahunAjaranIdMulai',
								width: 80,
								sortable: true, 
								dataIndex: 'TahunAjaranIdMulai'
							},{				
								header: 'SumberBeasiswaId',
								width: 200,
							}],
							sm : new Ext.grid.RowSelectionModel({
								singleSelect : true
							}),
							viewConfig: {
								forceFit: false,
							},
							anchor: "100%, 100%",							
							xtype: 'grid',
						}],
						buttons: [{
							text: 'Tutup',
							iconCls: 'logout',
							handler: function(thisbutton) {
								riwayatBeasiswaWin.close();
							}
						}]
					});					
					riwayatBeasiswaWin.show();
				}
			},'-',{
				text: 'Riwayat Pendidikan',
				iconCls: 'report',
				handler: function() {
				
				}
			}],
			sm: new Ext.grid.RowSelectionModel({
				listeners: {
					rowselect: function( thisSm, index, record ) {
						propertyStore.reload({params:{id:record.data.SekolahId}});
					} 
				}
			}),
			bbar : new Ext.PagingToolbar({
				pageSize: 20,
				store: pendataan.StorePesertaDidik,
				paramNames: {start: 'start', limit: 'limit'},
				displayInfo: true, 
				displayMsg: 'Menampilkan sekolah {0} - {1} dari {2}',
				emptyMsg: "Data tidak ditemukan"
			}),
			ds: pendataan.StorePesertaDidik,
			columns : [{				
				header: 'ID',
				width: 120,
				align: 'right',
				sortable: true, 
				dataIndex: 'PesertaDidikId'
			},{				
				header: 'Nama',
				width: 140,
				sortable: true, 
				dataIndex: 'Nama'
			},{				
				header: 'JK',
				width: 60,
				sortable: true, 
				dataIndex: 'JenisKelamin'
			},{				
				header: 'NISN',
				width: 80,
				sortable: true, 
				dataIndex: 'Nisn'

			},{				
				header: 'NIK',
				width: 80,
				sortable: true, 
				dataIndex: 'Nik'
			},{				
				header: 'Tmp.Lahir',
				width: 80,
				sortable: true, 
				dataIndex: 'Tmp.Lahir'
			},{				
				header: 'Tgl.Lahir',
				width: 80,
				renderer: Ext.util.Format.dateRenderer('d/m/Y'),
				sortable: true, 
				dataIndex: 'Tgl.Lahir'
			},{				
				header: 'Agama',
				width: 100,
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStoreAgama.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'AgamaId'
			},{				
				header: 'Nama Ayah',
				width: 100,
				sortable: true, 
				dataIndex: 'NamaAyah'
			},{				
				header: 'Thn.Lhr.Ayah',
				width: 100,
				sortable: true, 
				dataIndex: 'TahunLahirAyah'
			},{				
				header: 'Pek.Ayah',
				width: 120,
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStorePekerjaan.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'PekerjaanAyahId'
			},{				
				header: 'Pend.Ayah',
				width: 100,
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStoreJenjangPendidikan.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'PendidikanAyahId'
			},{				
				header: 'Pghsln Blnan Ayah',
				width: 180,
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStorePenghasilanOrangtuaWali.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'PenghasilanBulananAyah'
			},{				
				header: 'Nama Ibu',
				width: 100,
				sortable: true, 
				dataIndex: 'NamaIbu'

			},{				
				header: 'Thn.Lhr.Ibu',
				width: 100,
				sortable: true, 
				dataIndex: 'TahunLahirIbu'

			},{				
				header: 'Pek.Ibu',
				width: 120,
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStorePekerjaan.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'PekerjaanIbuId'

			},{				
				header: 'Pend.Ibu',
				width: 100,
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStoreJenjangPendidikan.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'PendidikanIbuId'
			},{				
				header: 'Pghsln.Blnan.Ibu',
				width: 180,
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStorePenghasilanOrangtuaWali.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'PenghasilanBulananIbu'
			},{				
				header: 'Nama Wali',
				width: 100,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'NamaWali'

			},{				
				header: 'Thn.Lhr.Wali',
				width: 100,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'TahunLahirWali'

			},{				
				header: 'Pek.Wali',
				width: 120,
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStorePekerjaan.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'PekerjaanWaliId'

			},{				
				header: 'Pend.Wali',
				width: 100,
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStoreJenjangPendidikan.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'PendidikanWaliId'

			},{				
				header: 'Pghsln.Blnan.Wali',
				width: 120,
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStorePenghasilanOrangtuaWali.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'PenghasilanBulananWali'

			},{				
				header: 'Jns.Tinggal',
				width: 100,
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStoreJenisTinggal.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'JenisTinggalId'

			},{				
				header: 'Alamat Rumah',
				width: 150,
				sortable: true, 
				dataIndex: 'AlamatRumah'
			},{				
				header: 'RT',
				width: 40,
				sortable: true, 
				dataIndex: 'Rt'

			},{				
				header: 'RW',
				width: 40,
				sortable: true, 
				dataIndex: 'Rw'

			},{				
				header: 'Nama Desa/Kelurahan',
				width: 100,
				sortable: true, 
				dataIndex: 'NamaDesaKelurahan'

			},{				
				header: 'Kecamatan',
				width: 100,
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStoreKecamatan.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'KecamatanId'

			},{				
				header: 'Kab/Kota',
				width: 100,
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStoreKabupatenKota.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'KabupatenKotaId'

			},{				
				header: 'Kode Pos',
				width: 80,
				sortable: true, 
				dataIndex: 'KodePos'

			},{				
				header: 'Tinggi',
				width: 60,
				sortable: true, 
				dataIndex: 'TinggiBadan'

			},{				
				header: 'Berat',
				width: 60,
				sortable: true, 
				dataIndex: 'BeratBadan'

			},{				
				header: 'Brkbuthn.Khusus',
				width: 150,
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStoreBerkebutuhanKhusus.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'BerkebutuhanKhususId'

			},{				
				header: 'No.Tlp.Rumah',
				width: 80,
				sortable: true, 
				dataIndex: 'NomorTeleponRumah'

			},{				
				header: 'No.HP',
				width: 80,
				sortable: true, 
				dataIndex: 'NomorTeleponSeluler'

			},{				
				header: 'Jarak Rmh-Sklh',
				width: 80,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'JarakRumahKeSekolah'

			},{				
				header: 'Jarak(KM)',
				width: 80,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'JarakRumahKeSekolahKm'

			},{				
				header: 'Alat Trnsprt',
				width: 100,
				editor: new Ext.form.ComboBox({ 
					store: pendataan.LookupStoreAlatTransportasi,
					displayField: 'Nama',
					valueField: 'AlatTransportasiId',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Pilih...',
					selectOnFocus:true
				}),
				renderer:
					function(v,params,record) {
						record = pendataan.LookupStoreAlatTransportasi.getById(v);
						if(record) {
							return record.get('Nama');
						} else {
							return v;
						}						
					},
				sortable: true, 
				dataIndex: 'AlatTransportasiId'

			},{				
				header: 'Email',
				width: 100,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'Email'
			}]  
		});
		
		////////////////GRID PTK/////////////////
		
		pendataan.RecordSetPtk = [
			{ name: 'PtkId', type: 'string'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  } ,
			{ name: 'JenisKelamin', type: 'string'  } ,
			{ name: 'IjazahTerakhirId', type: 'float'  } ,
			{ name: 'TahunIjazahTerakhir', type: 'float'  } ,
			{ name: 'GelarAkademikDepanId', type: 'float'  } ,
			{ name: 'GelarAkademikBelakangId', type: 'float'  } ,
			{ name: 'NiyNigk', type: 'string'  } ,
			{ name: 'Nuptk', type: 'string'  } ,
			{ name: 'TempatLahir', type: 'string'  } ,
			{ name: 'TglLahir', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'Nik', type: 'string'  } ,
			{ name: 'AgamaId', type: 'float'  } ,
			{ name: 'StatusPerkawinan', type: 'float'  } ,
			{ name: 'JumlahAnak', type: 'float'  } ,
			{ name: 'NamaIbuKandung', type: 'string'  } ,
			{ name: 'AlamatJalan', type: 'string'  } ,
			{ name: 'Rt', type: 'string'  } ,
			{ name: 'Rw', type: 'string'  } ,
			{ name: 'NamaDesaKelurahan', type: 'string'  } ,
			{ name: 'KodePos', type: 'string'  } ,
			{ name: 'KecamatanId', type: 'float'  } ,
			{ name: 'KabupatenKotaId', type: 'float'  } ,
			{ name: 'NoTeleponRumah', type: 'string'  } ,
			{ name: 'NoHp', type: 'string'  } ,
			{ name: 'Email', type: 'string'  } ,
			{ name: 'StatusKepegawaianId', type: 'float'  } ,
			{ name: 'TmtSekolah', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'JabatanPtkId', type: 'float'  } ,
			{ name: 'TmtJabatan', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'JabatanPtkSebelumnyaId', type: 'float'  } ,
			{ name: 'SertifikasiJabatan', type: 'float'  } ,
			{ name: 'TahunSertifikasiJabatan', type: 'float'  } ,
			{ name: 'NomorSertifikat', type: 'string'  } ,
			{ name: 'Nip', type: 'string'  } ,
			{ name: 'TmtPns', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'PangkatGolonganId', type: 'float'  } ,
			{ name: 'TmtPangkatGol', type: 'date' , dateFormat: 'Y/m/d' } ,
			{ name: 'KodeSertifikasiBidangStudiGuruId', type: 'float'  } ,
			{ name: 'KodeProgramKeahlianLaboranId', type: 'float'  } ,
			{ name: 'SudahLisensiKepalaSekolah', type: 'float'  } ,
			{ name: 'JenjangKepengawasanId', type: 'float'  } ,
			{ name: 'PengawasBidangRumpunId', type: 'float'  } ,
			{ name: 'PengawasMapelId', type: 'float'  } ,
			{ name: 'JumlahSekolahBinaan', type: 'float'  } ,
			{ name: 'PernahMengikutiDiklatKepengawasan', type: 'float'  } ,
			{ name: 'NamaSuamiIstri', type: 'string'  } ,
			{ name: 'Pekerjaan', type: 'float'  } ,
			{ name: 'NipSuamiIstri', type: 'string'  } ,
			{ name: 'SekolahId', type: 'float'  }	 
		];
		
		pendataan.RecordObjPtk = Ext.data.Record.create(pendataan.RecordSetPtk);
		
		pendataan.StorePtk = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'PtkId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetPtk		
			}),
			//url: './Ptk/fetch.json',
			autoLoad: false,		
			url: './data/Ptk.json',
			listeners: {
				beforeload: function(store, options) {
					rec = gridSekolah.getSelectionModel().getSelected();
					options.params.query = rec.data.SekolahId;
					options.params.query_column = 'sekolah_id';
					options.params.query_type= 'EQUAL';
					return true;
				} 
			},			
			sortInfo: {field: 'PtkId', direction: "ASC"}
		});
		
		pendataan.RecordSetJenjangPendidikan = [
			{ name: 'JenjangPendidikanId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjJenjangPendidikan = Ext.data.Record.create(pendataan.RecordSetJenjangPendidikan);
		
		pendataan.LookupStoreJenjangPendidikan = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'JenjangPendidikanId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetJenjangPendidikan		
			}),
			//url: './JenjangPendidikan/fetch.json',
			autoLoad: true,
			url: './data/JenjangPendidikan.json',
			
			sortInfo: {field: 'JenjangPendidikanId', direction: "ASC"}
		});
		
		pendataan.RecordSetGelarAkademik = [
			{ name: 'GelarAkademikId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  } ,
			{ name: 'Bidang', type: 'string'  }	 
		];
		
		pendataan.RecordObjGelarAkademik = Ext.data.Record.create(pendataan.RecordSetGelarAkademik);
		
		pendataan.LookupStoreGelarAkademik = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'GelarAkademikId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetGelarAkademik		
			}),
			//url: './GelarAkademik/fetch.json',
			autoLoad: true,		
			url: './data/GelarAkademik.json',
			
			sortInfo: {field: 'GelarAkademikId', direction: "ASC"}
		});
		
		pendataan.RecordSetGelarAkademik = [
			{ name: 'GelarAkademikId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  } ,
			{ name: 'Bidang', type: 'string'  }	 
		];
		
		pendataan.RecordObjGelarAkademik = Ext.data.Record.create(pendataan.RecordSetGelarAkademik);
		
		pendataan.LookupStoreGelarAkademik = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'GelarAkademikId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetGelarAkademik		
			}),
			//url: './GelarAkademik/fetch.json',
			autoLoad: true,		
			url: './data/GelarAkademik.json',
			
			sortInfo: {field: 'GelarAkademikId', direction: "ASC"}
		});
		
		pendataan.RecordSetAgama = [
			{ name: 'AgamaId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjAgama = Ext.data.Record.create(pendataan.RecordSetAgama);
		
		pendataan.LookupStoreAgama = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'AgamaId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetAgama		
			}),
			//url: './Agama/fetch.json',
			autoLoad: true,		
			url: './data/Agama.json',
			
			sortInfo: {field: 'AgamaId', direction: "ASC"}
		});
		
		pendataan.RecordSetKecamatan = [
			{ name: 'KecamatanId', type: 'float'  } ,
			{ name: 'KabupatenKotaId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  } ,
			{ name: 'Status', type: 'float'  } ,
			{ name: 'Keterangan', type: 'string'  }	 
		];
		
		pendataan.RecordObjKecamatan = Ext.data.Record.create(pendataan.RecordSetKecamatan);
		
		pendataan.LookupStoreKecamatan = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'KecamatanId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetKecamatan		
			}),
			//url: './Kecamatan/fetch.json',
			autoLoad: true,		
			url: './data/Kecamatan.json',
			
			sortInfo: {field: 'KecamatanId', direction: "ASC"}
		});
		
		pendataan.RecordSetKabupatenKota = [
			{ name: 'KabupatenKotaId', type: 'float'  } ,
			{ name: 'PropinsiId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  } ,
			{ name: 'Type', type: 'float'  } ,
			{ name: 'Status', type: 'float'  } ,
			{ name: 'LuasWilayah', type: 'string'  } ,
			{ name: 'JumlahPenduduk', type: 'string'  } ,
			{ name: 'Keterangan', type: 'string'  }	 
		];
		
		pendataan.RecordObjKabupatenKota = Ext.data.Record.create(pendataan.RecordSetKabupatenKota);
		
		pendataan.LookupStoreKabupatenKota = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'KabupatenKotaId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetKabupatenKota		
			}),
			//url: './KabupatenKota/fetch.json',
			autoLoad: true,		
			url: './data/KabupatenKota.json',
			
			sortInfo: {field: 'KabupatenKotaId', direction: "ASC"}
		});
		
		pendataan.RecordSetStatusKepegawaian = [
			{ name: 'StatusKepegawaianId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjStatusKepegawaian = Ext.data.Record.create(pendataan.RecordSetStatusKepegawaian);
		
		pendataan.LookupStoreStatusKepegawaian = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'StatusKepegawaianId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetStatusKepegawaian		
			}),
			//url: './StatusKepegawaian/fetch.json',
			autoLoad: true,		
			url: './data/StatusKepegawaian.json',
			
			sortInfo: {field: 'StatusKepegawaianId', direction: "ASC"}
		});
		
		pendataan.RecordSetJabatanPtk = [
			{ name: 'JabatanPtkId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjJabatanPtk = Ext.data.Record.create(pendataan.RecordSetJabatanPtk);
		
		pendataan.LookupStoreJabatanPtk = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'JabatanPtkId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetJabatanPtk		
			}),
			//url: './JabatanPtk/fetch.json',
			autoLoad: true,		
			url: './data/JabatanPtk.json',
			
			sortInfo: {field: 'JabatanPtkId', direction: "ASC"}
		});
		
		pendataan.RecordSetJabatanPtk = [
			{ name: 'JabatanPtkId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjJabatanPtk = Ext.data.Record.create(pendataan.RecordSetJabatanPtk);
		
		pendataan.LookupStoreJabatanPtk = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'JabatanPtkId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetJabatanPtk		
			}),
			//url: './JabatanPtk/fetch.json',
			autoLoad: true,		
			url: './data/JabatanPtk.json',
			
			sortInfo: {field: 'JabatanPtkId', direction: "ASC"}
		});
		
		pendataan.RecordSetPangkatGolongan = [
			{ name: 'PangkatGolonganId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjPangkatGolongan = Ext.data.Record.create(pendataan.RecordSetPangkatGolongan);
		
		pendataan.LookupStorePangkatGolongan = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'PangkatGolonganId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetPangkatGolongan		
			}),
			//url: './PangkatGolongan/fetch.json',
			autoLoad: true,		
			url: './data/PangkatGolongan.json',
			
			sortInfo: {field: 'PangkatGolonganId', direction: "ASC"}
		});
		
		pendataan.RecordSetBidangStudi = [
			{ name: 'BidangStudiId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  } ,
			{ name: 'Tingkat', type: 'string'  }	 
		];
		
		pendataan.RecordObjBidangStudi = Ext.data.Record.create(pendataan.RecordSetBidangStudi);
		
		pendataan.LookupStoreBidangStudi = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'BidangStudiId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetBidangStudi		
			}),
			//url: './BidangStudi/fetch.json',
			autoLoad: true,		
			url: './data/BidangStudi.json',
			
			sortInfo: {field: 'BidangStudiId', direction: "ASC"}
		});
		
		pendataan.RecordSetKeahlianLaboratorium = [
			{ name: 'KeahlianLaboratoriumId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjKeahlianLaboratorium = Ext.data.Record.create(pendataan.RecordSetKeahlianLaboratorium);
		
		pendataan.LookupStoreKeahlianLaboratorium = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'KeahlianLaboratoriumId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetKeahlianLaboratorium		
			}),
			//url: './KeahlianLaboratorium/fetch.json',
			autoLoad: true,		
			url: './data/KeahlianLaboratorium.json',
			
			sortInfo: {field: 'KeahlianLaboratoriumId', direction: "ASC"}
		});
		
		pendataan.RecordSetJenjangKepengawasan = [
			{ name: 'JenjangKepengawasanId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjJenjangKepengawasan = Ext.data.Record.create(pendataan.RecordSetJenjangKepengawasan);
		
		pendataan.LookupStoreJenjangKepengawasan = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'JenjangKepengawasanId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetJenjangKepengawasan		
			}),
			//url: './JenjangKepengawasan/fetch.json',
			autoLoad: true,		
			url: './data/JenjangKepengawasan.json',
			
			sortInfo: {field: 'JenjangKepengawasanId', direction: "ASC"}
		});
		
		pendataan.RecordSetBidangStudi = [
			{ name: 'BidangStudiId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  } ,
			{ name: 'Tingkat', type: 'string'  }	 
		];
		
		pendataan.RecordObjBidangStudi = Ext.data.Record.create(pendataan.RecordSetBidangStudi);
		
		pendataan.LookupStoreBidangStudi = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'BidangStudiId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetBidangStudi		
			}),
			//url: './BidangStudi/fetch.json',
			autoLoad: true,		
			url: './data/BidangStudi.json',
			
			sortInfo: {field: 'BidangStudiId', direction: "ASC"}
		});
		
		pendataan.RecordSetMataPelajaran = [
			{ name: 'MataPelajaranId', type: 'float'  } ,
			{ name: 'Kode', type: 'string'  } ,
			{ name: 'Nama', type: 'string'  }	 
		];
		
		pendataan.RecordObjMataPelajaran = Ext.data.Record.create(pendataan.RecordSetMataPelajaran);
		
		pendataan.LookupStoreMataPelajaran = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'MataPelajaranId',
				root: 'rows',
				totalProperty: 'results',
				fields: pendataan.RecordSetMataPelajaran		
			}),
			//url: './MataPelajaran/fetch.json',
			autoLoad: true,		
			url: './data/MataPelajaran.json',
			
			sortInfo: {field: 'MataPelajaranId', direction: "ASC"}
		});
		
		var ptkGrid = new Ext.grid.GridPanel({
			title: 'Daftar PTK',
			store: pendataan.StorePtk,
			loadMask: true,
			tbar: [{
				text: 'Tampilkan Detil',
				iconCls: 'report',
				tooltip: 'Tampilkan data detil PTK ini',
				handler: function() {
					
				}
			}],
			bbar : new Ext.PagingToolbar({
				pageSize: 20,
				store: pendataan.StorePtk,
				paramNames: {start: 'start', limit: 'limit'},
				displayInfo: true, 
				displayMsg: 'Menampilkan sekolah {0} - {1} dari {2}',
				emptyMsg: "Data tidak ditemukan"
			}),
			columns : [{				
				header: 'PTK ID',
				width: 120,
				align: 'center',
				sortable: true, 
				dataIndex: 'PtkId'
			},{				
				header: 'NUPTK',
				width: 130,
				align: 'center',				
				sortable: true, 
				dataIndex: 'Nuptk'
			},{				
				header: 'NIY/NIGK',
				width: 100,
				align: 'center',
				sortable: true, 
				dataIndex: 'NiyNigk'
			},{				
				header: 'Nama',
				width: 200,
				sortable: true, 
				dataIndex: 'Nama'
			},{				
				header: 'JK',
				width: 80,
				renderer: function(v) {
					return (v==1) ? "Laki-laki" : "Perempuan";
				},
				sortable: true, 
				dataIndex: 'JenisKelamin'

			},{				
				header: 'Ijazah',
				width: 80,
				renderer:
					function(v,params,record) {
					record = pendataan.LookupStoreJenjangPendidikan.getById(v);
					if(record) {
						return record.get('Nama');
					} else {
						return v;
					}						
					},
				sortable: true, 
				dataIndex: 'IjazahTerakhirId'

			},{				
				header: 'Thn.Ijzh.Trkhr',
				width: 60,
				sortable: true, 
				dataIndex: 'TahunIjazahTerakhir'

			},{				
				header: 'Gelar Dpn',
				width: 140,
				renderer:
					function(v,params,record) {
					record = pendataan.LookupStoreGelarAkademik.getById(v);
					if(record) {
						return record.get('Nama');
					} else {
						return v;
					}						
					},
				sortable: true, 
				dataIndex: 'GelarAkademikDepanId'

			},{				
				header: 'Gelar Blk',
				width: 140,
				renderer:
					function(v,params,record) {
					record = pendataan.LookupStoreGelarAkademik.getById(v);
					if(record) {
						return record.get('Nama');
					} else {
						return v;
					}						
					},
				sortable: true, 
				dataIndex: 'GelarAkademikBelakangId'
			},{				
				header: 'Tmp.Lhr',
				width: 100,
				sortable: true, 
				dataIndex: 'TempatLahir'

			},{				
				header: 'Tgl.Lahir',
				width: 100,
				renderer: Ext.util.Format.dateRenderer('d/m/Y'),
				editor: new fm.DateField({
					format: 'd/m/Y',
					minValue: '1920/01/01'
				}),
				sortable: true, 
				dataIndex: 'TglLahir'

			},{				
				header: 'NIK',
				width: 90,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'Nik'

			},{				
				header: 'Agama ID',
				width: 100,
				editor: new Ext.form.ComboBox({ 
					store: pendataan.LookupStoreAgama,
					displayField: 'Nama',
					valueField: 'AgamaId',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Pilih...',
					selectOnFocus:true
				}),
				renderer:
					function(v,params,record) {
					record = pendataan.LookupStoreAgama.getById(v);
					if(record) {
						return record.get('Nama');
					} else {
						return v;
					}						
					},
				sortable: true, 
				dataIndex: 'AgamaId'

			},{				
				header: 'Status Kawin',
				width: 100,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'StatusPerkawinan'

			},{				
				header: 'Jml.Anak',
				width: 80,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'JumlahAnak'

			},{				
				header: 'Nama Ibu Kandung',
				width: 120,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'NamaIbuKandung'

			},{				
				header: 'Alamat Jalan',
				width: 180,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'AlamatJalan'

			},{				
				header: 'RT',
				width: 40,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'Rt'

			},{				
				header: 'RW',
				width: 40,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'Rw'

			},{				
				header: 'Nama Desa/Kelurahan',
				width: 100,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'NamaDesaKelurahan'

			},{				
				header: 'Kode Pos',
				width: 80,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'KodePos'

			},{				
				header: 'Kecamatan',
				width: 120,
				editor: new Ext.form.ComboBox({ 
					store: pendataan.LookupStoreKecamatan,
					displayField: 'Nama',
					valueField: 'KecamatanId',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Pilih...',
					selectOnFocus:true
				}),
				renderer:
					function(v,params,record) {
					record = pendataan.LookupStoreKecamatan.getById(v);
					if(record) {
						return record.get('Nama');
					} else {
						return v;
					}						
					},
				sortable: true, 
				dataIndex: 'KecamatanId'

			},{				
				header: 'Kab/Kota',
				width: 120,
				editor: new Ext.form.ComboBox({ 
					store: pendataan.LookupStoreKabupatenKota,
					displayField: 'Nama',
					valueField: 'KabupatenKotaId',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Pilih...',
					selectOnFocus:true
				}),
				renderer:
					function(v,params,record) {
					record = pendataan.LookupStoreKabupatenKota.getById(v);
					if(record) {
						return record.get('Nama');
					} else {
						return v;
					}						
					},
				sortable: true, 
				dataIndex: 'KabupatenKotaId'

			},{				
				header: 'No.Tlp.Rmh',
				width: 100,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'NoTeleponRumah'

			},{				
				header: 'No.HP',
				width: 100,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'NoHp'

			},{				
				header: 'Email',
				width: 100,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'Email'

			},{				
				header: 'Status.Pgw',
				width: 80,
				editor: new Ext.form.ComboBox({ 
					store: pendataan.LookupStoreStatusKepegawaian,
					displayField: 'Nama',
					valueField: 'StatusKepegawaianId',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Pilih...',
					selectOnFocus:true
				}),
				renderer:
					function(v,params,record) {
					record = pendataan.LookupStoreStatusKepegawaian.getById(v);
					if(record) {
						return record.get('Nama');
					} else {
						return v;
					}						
					},
				sortable: true, 
				dataIndex: 'StatusKepegawaianId'

			},{				
				header: 'TMT Sekolah',
				width: 100,
				renderer: Ext.util.Format.dateRenderer('d/m/Y'),
				editor: new fm.DateField({
					format: 'd/m/Y',
					minValue: '1920/01/01'
				}),
				sortable: true, 
				dataIndex: 'TmtSekolah'

			},{				
				header: 'Jabatan',
				width: 100,
				editor: new Ext.form.ComboBox({ 
					store: pendataan.LookupStoreJabatanPtk,
					displayField: 'Nama',
					valueField: 'JabatanPtkId',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Pilih...',
					selectOnFocus:true
				}),
				renderer:
					function(v,params,record) {
					record = pendataan.LookupStoreJabatanPtk.getById(v);
					if(record) {
						return record.get('Nama');
					} else {
						return v;
					}						
					},
				sortable: true, 
				dataIndex: 'JabatanPtkId'

			},{				
				header: 'TMT Jabatan',
				width: 100,
				renderer: Ext.util.Format.dateRenderer('d/m/Y'),
				editor: new fm.DateField({
					format: 'd/m/Y',
					minValue: '1920/01/01'
				}),
				sortable: true, 
				dataIndex: 'TmtJabatan'

			},{				
				header: 'Jabatan Sblm',
				width: 100,
				editor: new Ext.form.ComboBox({ 
					store: pendataan.LookupStoreJabatanPtk,
					displayField: 'Nama',
					valueField: 'JabatanPtkId',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Pilih...',
					selectOnFocus:true
				}),
				renderer:
					function(v,params,record) {
					record = pendataan.LookupStoreJabatanPtk.getById(v);
					if(record) {
						return record.get('Nama');
					} else {
						return v;
					}						
					},
				sortable: true, 
				dataIndex: 'JabatanPtkSebelumnyaId'

			},{				
				header: 'Sertfksi Jbtn',
				width: 100,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'SertifikasiJabatan'

			},{				
				header: 'Thn Sertfksi',
				width: 80,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'TahunSertifikasiJabatan'

			},{				
				header: 'Nomor Sertifikat',
				width: 150,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'NomorSertifikat'

			},{				
				header: 'NIP',
				width: 80,
				editor: new fm.TextField({
				   allowBlank: false
				}),
				sortable: true, 
				dataIndex: 'Nip'

			},{				
				header: 'TMT PNS',
				width: 80,
				renderer: Ext.util.Format.dateRenderer('d/m/Y'),
				editor: new fm.DateField({
					format: 'd/m/Y',
					minValue: '1920/01/01'
				}),
				sortable: true, 
				dataIndex: 'TmtPns'

			},{				
				header: 'Pangkat/Golongan ID',
				width: 80,
				editor: new Ext.form.ComboBox({ 
					store: pendataan.LookupStorePangkatGolongan,
					displayField: 'Nama',
					valueField: 'PangkatGolonganId',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Pilih...',
					selectOnFocus:true
				}),
				renderer:
					function(v,params,record) {
					record = pendataan.LookupStorePangkatGolongan.getById(v);
					if(record) {
						return record.get('Nama');
					} else {
						return v;
					}						
					},
				sortable: true, 
				dataIndex: 'PangkatGolonganId'

			},{				
				header: 'TMT Pangkat/Gol',
				width: 80,
				renderer: Ext.util.Format.dateRenderer('d/m/Y'),
				editor: new fm.DateField({
					format: 'd/m/Y',
					minValue: '1920/01/01'
				}),
				sortable: true, 
				dataIndex: 'TmtPangkatGol'

			},{				
				header: 'Kode Srtfks Bid.Studi',
				width: 120,
				editor: new Ext.form.ComboBox({ 
					store: pendataan.LookupStoreBidangStudi,
					displayField: 'Nama',
					valueField: 'BidangStudiId',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Pilih...',
					selectOnFocus:true
				}),
				renderer:
					function(v,params,record) {
					record = pendataan.LookupStoreBidangStudi.getById(v);
					if(record) {
						return record.get('Nama');
					} else {
						return v;
					}						
					},
				sortable: true, 
				dataIndex: 'KodeSertifikasiBidangStudiGuruId'

			},{				
				header: 'Kode Prog.Keahlian Laboran',
				width: 120,
				editor: new Ext.form.ComboBox({ 
					store: pendataan.LookupStoreKeahlianLaboratorium,
					displayField: 'Nama',
					valueField: 'KeahlianLaboratoriumId',
					typeAhead: true,
					mode: 'local',
					triggerAction: 'all',
					emptyText:'Pilih...',
					selectOnFocus:true
				}),
				renderer:
					function(v,params,record) {
					record = pendataan.LookupStoreKeahlianLaboratorium.getById(v);
					if(record) {
						return record.get('Nama');
					} else {
						return v;
					}						
					},
				sortable: true, 
				dataIndex: 'KodeProgramKeahlianLaboranId'

			},{				
				header: 'Lisensi Kepsek',
				width: 80,
				sortable: true, 
				dataIndex: 'SudahLisensiKepalaSekolah'

			},{				
				header: 'Jenjang Kpgwsn',
				width: 100,
				renderer:
					function(v,params,record) {
					record = pendataan.LookupStoreJenjangKepengawasan.getById(v);
					if(record) {
						return record.get('Nama');
					} else {
						return v;
					}						
					},
				sortable: true, 
				dataIndex: 'JenjangKepengawasanId'

			},{				
				header: 'Pgws Bid/Rumpun',
				width: 100,
				renderer:
					function(v,params,record) {
					record = pendataan.LookupStoreBidangStudi.getById(v);
					if(record) {
						return record.get('Nama');
					} else {
						return v;
					}						
					},
				sortable: true, 
				dataIndex: 'PengawasBidangRumpunId'

			},{				
				header: 'Pgws Mapel',
				width: 100,
				renderer:
					function(v,params,record) {
					record = pendataan.LookupStoreMataPelajaran.getById(v);
					if(record) {
						return record.get('Nama');
					} else {
						return v;
					}						
					},
				sortable: true, 
				dataIndex: 'PengawasMapelId'

			},{				
				header: 'Jml.Sek.Binaan',
				width: 80,
				sortable: true, 
				dataIndex: 'JumlahSekolahBinaan'

			},{				
				header: 'Pernah Ikut Diklat Kpgwsn',
				width: 120,
				sortable: true, 
				dataIndex: 'PernahMengikutiDiklatKepengawasan'
			},{				
				header: 'Nama Suami/Istri',
				width: 120,
				sortable: true, 
				dataIndex: 'NamaSuamiIstri'

			},{				
				header: 'Pekerjaan',
				width: 100,
				sortable: true, 
				dataIndex: 'Pekerjaan'

			},{				
				header: 'NIP Suami/Istri',
				width: 100,
				sortable: true, 
				dataIndex: 'NipSuamiIstri'
			}]
		});
				
		var detailPanel = new Ext.TabPanel({
			activeTab: 0,
			width:600,
			height:250,
			//plain:true,
			frame:true,			
			defaults:{autoScroll: true},
			items:[
				propertyGrid					
				,
				fasilitasViewPanel
				,
				siswaGrid
				,
				ptkGrid
			]
		});
				
		
		this.panel = new Xond.layout.UL ({
			id: 'sekolah-tab',
			title: 'Data Sekolah',
			upperHeight: 320,
			upperPanel: gridSekolah,
			lowerPanel: detailPanel
		});			
		
		return this.panel;			
				
	}	

});
