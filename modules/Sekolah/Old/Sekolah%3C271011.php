<?php

class SekolahModule extends XondModule {
	
	var $component;
	
	function SekolahModule() {
		$this->setName(get_class($this));
		$this->setMinifyUi(false);
		$this->headerContent = array();		
		$this->registerUi('sekolah');
		//$this->registerStyle('RincianSpp');
		$this->setTitle('Sekolah');
		//$this->setParams();
	}
	
	function execGetStores() {
		//$this->write(XondComponent::getStoresForGrid("PerolehanBeasiswa"));
		//var gridRincian = new Ext.grid.EditorGridPanel({
		//$this->write(XondComponent::getGrid("PerolehanBeasiswa"));
		//$this->write(XondComponent::getStoresForGrid("PesertaDidikTerdaftar"));
		//$this->write(XondComponent::getGrid("PesertaDidikTerdaftar"));
		/*
		$params['colNames'] = array("Sekolah ID", "Nama", "NSS", "NPSN", "Jenis Sekolah", "Alamat Jalan", "RT", "RW", "Nama Dusun", "Nama Desa/Kelurahan", "Kecamatan", "Kabupaten Kota", "Kode Pos", "Kategori Wilayah", "Lintang", "Bujur", "Nomor Telepon", "Nomor Fax", "Akses internet", "ISP", "Email", "Website", "Status Sekolah", "Status Kepemilikan", "SK Pendirian Sekolah", "Tanggal SK Pendirian", "SK Izin Op", "Tgl.SK Izin Op", "Akreditasi", "No.SK Akreditasi", "Tgl.SK Akreditasi", "Status Mutu", "Sert.ISO", "Wkt.Pnylnggaraan", "Gugus Sekolah", "Kategori Sekolah", "No.Rek", "Nama Bank", "Rekening Atas Nama", "MBS", "Sumber Listrik", "Daya Listrik", "Yayasan" );
		$params['colWidths'] = array(		40, 	140,   120,	   100,	  		80,	   			150,		80,  80,		100,			100,				100,		160,			100,		100,				100,		100,		120,			100,			100,		100,	100,	120,		100,			100,				100,			150,	40,		40,			100,		100,		100,			100,				100,				100,				100,		100,			100,				100,			80,				100, 		100, 		100, 		     100, 		   100, 		  100, 		    100);
		$this->write(XondComponent::getStoresForGrid("Sekolah", $params));
		$this->write(XondComponent::getGrid("Sekolah", $params));
		*/
		
		/*
		$params['colNames'] =  array("PTK ID", "Kode", "Nama", "JK", "Ijazah", "Thn.Ijzh.Trkhr", "Gelar Dpn", "Gelar Blk", "NIY/NIGK", "NUPTK", "Tmp.Lhr", "Tgl.Lahir", "NIK", "Agama ID", "Status Kawin", "Jml.Anak", "Nama Ibu Kandung", "Alamat Jalan", "RT", "RW", "Nama Desa/Kelurahan", "Kode Pos", "Kecamatan", "Kab/Kota", "No.Tlp.Rmh", "No.HP", "Email", "Status.Pgw", "TMT Sekolah", "Jabatan", "TMT Jabatan", "Jabatan Sblm", "Sertfksi Jbtn", "Thn Sertfksi", "Nomor Sertifikat", "NIP", "TMT PNS", "Pangkat/Golongan ID", "TMT Pangkat/Gol", "Kode Srtfks Bid.Studi", "Kode Prog.Keahlian Laboran", "Lisensi Kepsek", "Jenjang Kpgwsn", "Pgws Bid/Rumpun", "Pgws Mapel", "Jml.Sek.Binaan", "Pernah Ikut Diklat Kpgwsn", "Nama Suami/Istri", "Pekerjaan", "NIP Suami/Istri");
		$params['colWidths'] = array(	40, 	40,	  	140,   	60,	   80,	  	60,	   				140,			140,		  100,		100,		100,		100,	90,		100,			100,			80,		120,				180,			40,	  40,			100,			80,				120,		120,		100,		100,	100,		80,			100,			100,		100,		100,			100,					80,				150,			80,			80,				80,			80,					120, 						120,						80 ,				100,			100,			100,			80,						120,					120,			100,			100);
		$params['enumPairFields'] = array("JenisKelamin" => array( "L"=>"Laki-laki", "P"=>"Perempuan"), "StatusPerkawinan" => array("1" => "Kawin" , "2" => "Belum Kawin", "3" => "Duda/Janda"), "SertifikasiJabatan" => array("1"=>"Belum", "2"=>"Sedang Proses", "3" => "Sudah" ), "SudahLisensiKepalaSekolah" => array( "1"=>"Sudah", "2"=> "Belum") );
		$this->write(XondComponent::getStoresForGrid("Ptk", $params));
		$this->write(XondComponent::getGrid("Ptk", $params));
		*/
		
		/*
		$params['colNames'] =  array("Peserta Didik ID", "Nama", "JK", "NISN", "NIK", "Tmp.Lahir", "Tgl.Lahir", "Agama", "Nama Ayah", "Thn.Lhr.Ayah", "Pek.Ayah", "Pend.Ayah", "Pghsln Blnan Ayah", "Nama Ibu", "Thn.Lhr.Ibu", "Pek.Ibu", "Pend.Ibu", "Pghsln.Blnan.Ibu", "Nama Wali", "Thn.Lhr.Wali", "Pek.Wali", "Pend.Wali", "Pghsln.Blnan.Wali", "Jns.Tinggal", "Alamat Rumah", "RT", "RW", "Nama Desa/Kelurahan", "Kecamatan", "Kab/Kota", "Kode Pos", "Tinggi", "Berat", "Brkbuthn.Khusus", "No.Tlp.Rumah", "No.HP", "Jarak Rmh-Sklh", "Jarak(KM)", "Alat Trnsprt", "Email");
		$params['colWidths'] = array(		40, 		  140,   60,	   80,	  80,	   80,			80,		  100,		100,			100,		120,			100,		180,				100,			100,		120,	100,			180,			100,			100,			120,		100,			120,				100,			150,	40,		40,			100,				100,		100,		80,			60,		60,				150,			80,			80,				80,			80,				100, 		100);
		$params['enumPairFields'] = array("JenisKelamin" => array("L"=>"Laki-laki", "P"=>"Perempuan"), "JarakRumahKeSekolah" => array("1" => "<=1km" , "2" => ">1km"));
		$this->write(XondComponent::getStoresForGrid("PesertaDidik", $params));
		$this->write(XondComponent::getGrid("PesertaDidik", $params));	
		*/
		$this->write(XondComponentSqlite::getStore("Pengguna"));
		
	}
	
	function execFetchPesertaDidik() {
		
		$c = new Criteria();
		$c->add(PesertaDidikPeer::SEKOLAH_ID, $_REQUEST["sekolah_id"]);
		
		$numrows = PesertaDidikPeer::doCount($c);
		
		if (isset($_REQUEST['limit']) && isset($_REQUEST['start'])) {
			$limit = $_REQUEST['limit'];
			$start = $_REQUEST['start'];			
		} else {
			$limit = 20;
			$start = 0;
		}
 		
		$c->setLimit($limit);
		$c->setOffset($start);
		
		$siswas = PesertaDidikPeer::doSelect($c);
		
		$this->write(tableJson(getArray($siswas), $numrows, PesertaDidikPeer::getFieldNames(BasePeer::TYPE_PHPNAME)));
		
	}
	
	function execFetchSekolah() {
		
		$c = new Criteria();		
		if ($_REQUEST["kabkota"]) {
			$c->add(SekolahPeer::KABUPATEN_KOTA_ID, $_REQUEST["kabkota"]);
		}
		if ($_REQUEST["jenissekolah"]) {
			$c->add(SekolahPeer::JENIS_SEKOLAH_ID, $_REQUEST["jenissekolah"]);
		}		
		if ($_REQUEST["sekolah_id"]) {
			$queryStr = str_replace(" ","%",$_REQUEST["sekolah_id"]);			
			$c->add(SekolahPeer::SEKOLAH_ID, $queryStr);
		}
		if ($_REQUEST["namasekolah"]) {
			$namaSekolah = $_REQUEST["namasekolah"];			
			$namaSekolah = "%".str_replace(" ", "%", $namaSekolah)."%";
			$c->add(SekolahPeer::NAMA, $namaSekolah, Criteria::LIKE);
		}
		
		$numrows = SekolahPeer::doCount($c);
		
		if (isset($_REQUEST['limit']) && isset($_REQUEST['start'])) {
			$limit = $_REQUEST['limit'];
			$start = $_REQUEST['start'];			
		} else {
			$limit = 20;
			$start = 0;
		}	
			
		$c->setLimit($limit);
		$c->setOffset($start);
		
		$sekolahs = SekolahPeer::doSelect($c);
		
		$this->write(tableJson(getArray($sekolahs), $numrows, SekolahPeer::getFieldNames(BasePeer::TYPE_PHPNAME)));
		
	}
	
	function execFetchDetilSekolah() {
		
		//Enums//
		$klasifikasi_geografis = array("Terpencil","Daerah Sulit","Pedesaan","Perkotaan");
		$kategori_wilayah =  array("Daerah Terpencil","Daerah Perbatasan Negara","Daerah Transmigrasi","Bukan semua");
		$akses_internet = array("Tidak ada","Ada tapi terputus");
		$isp = array("Jardiknas","Telkom","Lainnya");
		$jenis_sekolah = array("SD","SMP","SDLB","SMPLB","SLB");
		$status_kepemilikan = array("Pemerintah Pusat","Pemerintah Daerah","Yayasan","Lainnya");
		$sertifikasi_iso = array("9001:2000","9001:2008","Proses Sertifikasi","Belum Bersertifikat");		
		$waktu_penyelenggaraan = array("Pagi","siang","kombinasi");
		$gugus_sekolah = array("Inti","Imbas","Belum Ikut");
		$kategori_sekolah = array("Rujukan","SD-SMP satu atap","SD biasa");
		$mbs = array("Ya","Tidak");
		$menyelenggarakan_sbi = array("Ya","Tidak");
		
		//$sekolah = SekolahPeer::retrieveByPK($_REQUEST["sekolah_id"]);
		$sql = "select sekolah_id, nama, sertifikasi_iso, alamat_jalan,	rt,	rw,	nama_dusun,	klasifikasi_geografis, kategori_wilayah, 
				desa_kelurahan_id, kabupaten_kota_id, kode_pos, lintang, bujur, nomor_telepon, nomor_fax, akses_internet, isp, email, 
				website, jarak_sekolah_terdekat, tahun_sekolah_dibuka, status_sekolah, jenis_sekolah, nss, nama_pimpinan, sk_pendirian_sekolah, 
				tanggal_sk_pendirian, status_kepemilikan, sk_status_sekolah_terakhir, tanggal_status_sekolah, status_mutu, sertifikasi_iso, 
				waktu_penyelenggaraan, gugus_sekolah, kategori_sekolah, no_rekening, nama_bank, rekening_atas_nama, mbs, menyelenggarakan_sbi 
				from sekolah where sekolah_id = {$_REQUEST["id"]}";
		
		$sekolah = getDataBySql($sql);		
		//echo $sql;
		foreach ($sekolah as $s) {
			$out = $s;
			$out["klasifikasi_geografis"] =  $klasifikasi_geografis[$s["klasifikasi_geografis"]];
			$out["kategori_wilayah"] = $kategori_wilayah[$s["kategori_wilayah"]];
			$out["akses_internet"] = $akses_internet[$s["akses_internet"]];
			$out["isp"] = $isp[$s["isp"]];
			$out["jenis_sekolah"] = $jenis_sekolah[$s["jenis_sekolah"]];
			$out["status_kepemilikan"] = $status_kepemilikan[$s["status_kepemilikan"]];
			$out["sertifikasi_iso"] = $sertifikasi_iso[$s["sertifikasi_iso"]];		
			$out["waktu_penyelenggaraan"] = $waktu_penyelenggaraan [$s["waktu_penyelenggaraan"]];
			$out["gugus_sekolah"] = $gugus_sekolah [$s["gugus_sekolah"]];
			$out["kategori_sekolah"] = $kategori_sekolah [$s["kategori_sekolah"]];
			$out["mbs"] = $mbs [$s["mbs"]];
			$out["menyelenggarakan_sbi"] = $menyelenggarakan_sbi [$s["menyelenggarakan_sbi"]];			
			$arrOut[] = $out;
		}
		$this->write(tableJson($arrOut, 1, array("sekolah_id","nama","sertifikasi_iso","alamat_jalan","rt","rw","nama_dusun","klasifikasi_geografis","kategori_wilayah","desa_kelurahan_id","kabupaten_kota_id","kode_pos","lintang","bujur","nomor_telepon","nomor_fax","akses_internet","isp","email","website","jarak_sekolah_terdekat","tahun_sekolah_dibuka","status_sekolah","jenis_sekolah","nss","nama_pimpinan","sk_pendirian_sekolah","tanggal_sk_pendirian","status_kepemilikan","sk_status_sekolah_terakhir","tanggal_status_sekolah","status_mutu","sertifikasi_iso","waktu_penyelenggaraan","gugus_sekolah","kategori_sekolah","no_rekening","nama_bank","rekening_atas_nama","mbs","menyelenggarakan_sbi")));
			 
	}
	
	function execFetchPerolehanBeasiswa() {
		
		$c = new Criteria();
		$c->add(PerolehanBeasiswaPeer::PESERTA_DIDIK_ID, $_REQUEST["query"]);
		$perolehan = PerolehanBeasiswaPeer::doSelect($c);
		$perolehans = getArray($perolehan);
		$arrOut = array();
		
		foreach ($perolehans as $p) {
			//print_r($p); die;
			$sumberBeasiswa = SumberBeasiswaPeer::retrieveByPK($p["SumberBeasiswaId"]);
			$p["SumberBeasiswaId"] = $sumberBeasiswa->getNamaBeasiswa();  		
			$tahunMulai = TahunAjaranPeer::retrieveByPK($p["TahunAjaranIdMulai"]);
			$p["TahunAjaranIdMulai"] = $tahunMulai->getKodeTahunAjaran();
			//$tahunSelesai = TahunAjaranPeer::retrieveByPK($p["TahunAjaranIdSelesai"]);
			//$p["TahunAjaranIdSelesai"] = //$tahunSelesai->getKodeTahunAjaran();			
			$arrOut[] = $p;
		}
		
		$this->write(tableJson($arrOut, sizeof($arrOut), SumberBeasiswaPeer::getFieldNames(BasePeer::TYPE_PHPNAME)));
	}
	
	function execSave(){
		// error_reporting(E_ALL);
		$user = $this->getAuth()->getUser();
		if (!$user){
			$this->write("{success : 'false'}");
		}else {
			try {
				if (empty($_REQUEST['nama']) || empty($_REQUEST['jenis_sekolah'])){
					exit();
				}
				$n = new Sekolah();
				$sekolahId = shortenUuid(generateUuid());
				$kodeRegistrasi = getTimeBasedRandom();
				//echo $sekolahId."<br>".$kodeRegistrasi;
				
				$n->setSekolahId($sekolahId);
				$n->setKodeRegistrasi($kodeRegistrasi);
				$n->setNama(strtoupper(trim($_REQUEST['nama'])));
				$n->setNss($_REQUEST['nss']);
				$n->setNpsn($_REQUEST['npsn']);
				$n->setJenisSekolahId($_REQUEST['jenis_sekolah']);
				$n->setStatusSekolah($_REQUEST['status_sekolah']);
				$n->setAlamatJalan(trim(ucwords(strtolower($_REQUEST['alamat']))));
				$n->setKabupatenKotaId($user->getKabupatenKotaId());
				
				//print_r($n);
				if ($n->save()){
					$this->write("{success : 'true'}");
				} else {
					$this->write("{success : 'false'}");
				}
			} catch (Exception $exc){
				$this->write("{success : 'false'}");
			}
		}
	}
}

?>
