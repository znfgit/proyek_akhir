<?
	//error_reporting(E_ERROR);
	
	$this->user = $this->getAuth()->getUser();
	$is_admin = ($this->user->getPeranId() == 1) ? TRUE : FALSE;
	
	list($kabkota, $jenissekolah, $statussekolah) = explode("|", $_REQUEST["id"]);
	
	$kabupatenKotaId = ($is_admin) ? $kabkota : $this->user->getKabupatenKotaId();
	$sekolah = SekolahPeer::retrieveByPK($this->user->getSekolahId());
	$kabupatenKota = KabupatenKotaPeer::retrieveByPk($kabupatenKotaId);
	$kabupatenNama = ($this->user->getPeranId() == 10) ? $sekolah->getNama() : $kabupatenKota->getNama();
	
	$c = new Criteria();
	
	if ($is_admin) {
		$c->add(SekolahPeer::KABUPATEN_KOTA_ID, $kabkota);
	} else {
		if ($this->getAuth()->getUser()->getPeranId() == 10) {
			$c->add(SekolahPeer::SEKOLAH_ID, $this->getAuth()->getUser()->getSekolahId());
		} else {
			$c->add(SekolahPeer::KABUPATEN_KOTA_ID, $this->user->getKabupatenKotaId());	
		}
	}
	if ($jenissekolah) {
		$c->add(SekolahPeer::JENIS_SEKOLAH_ID, $jenissekolah);
	}
	if ($statussekolah) {			
		$c->add(SekolahPeer::STATUS_SEKOLAH, $statussekolah);
	}
	
	$sekolahCount = SekolahPeer::doCount($c);
	$sekolahs = SekolahPeer::doSelect($c);
		
	$date = getdate();
	$this->tanggal = $date['mday']." ".getbulan($date['mon'])." ".$date['year'];
?>
<?php 
	echo '<?xml version="1.0"?>';
	echo '<?mso-application progid="Excel.Sheet"?>';
?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author>Abah</Author>
  <LastAuthor>Abah</LastAuthor>
  <Created>2011-12-19T07:23:23Z</Created>
  <LastSaved>2011-12-19T08:22:11Z</LastSaved>
  <Version>12.00</Version>
 </DocumentProperties>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>7995</WindowHeight>
  <WindowWidth>20115</WindowWidth>
  <WindowTopX>240</WindowTopX>
  <WindowTopY>120</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:CharSet="1" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s54" ss:Name="Hyperlink">
   <Font ss:FontName="Calibri" x:CharSet="1" x:Family="Swiss" ss:Size="11"
    ss:Color="#0000FF" ss:Underline="Single"/>
  </Style>
  <Style ss:ID="m60617600">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#8DB4E3" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="m60617720">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#8DB4E3" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s64">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
  </Style>
  <Style ss:ID="s65">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
  </Style>
  <Style ss:ID="s66" ss:Parent="s54">
   <Alignment ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Protection/>
  </Style>
  <Style ss:ID="s67">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s68">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s69">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
  </Style>
  <Style ss:ID="s70">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#8DB4E3" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s71">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Interior ss:Color="#D8D8D8" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s72" ss:Parent="s54">
   <Alignment ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Interior ss:Color="#D8D8D8" ss:Pattern="Solid"/>
   <Protection/>
  </Style>
  <Style ss:ID="s73">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Interior ss:Color="#D8D8D8" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s74">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#8DB4E3" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s89">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="14" ss:Color="#000000"
    ss:Bold="1"/>
  </Style>
  <Style ss:ID="s90">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>
  </Style>
 </Styles>
 <Worksheet ss:Name="Laporan 1">
  <Table ss:ExpandedColumnCount="14" ss:ExpandedRowCount="<?=$sekolahCount+20;?>" x:FullColumns="1"
   x:FullRows="1" ss:DefaultColumnWidth="41.25" ss:DefaultRowHeight="15">
   <Column ss:StyleID="s64" ss:AutoFitWidth="0" ss:Width="40.5"/>
   <Column ss:StyleID="s64" ss:AutoFitWidth="0" ss:Width="72"/>
   <Column ss:AutoFitWidth="0" ss:Width="159.75"/>
   <Column ss:StyleID="s69" ss:Width="508.5"/>
   <Column ss:StyleID="s64" ss:AutoFitWidth="0" ss:Width="48.75"/>
   <Column ss:StyleID="s64" ss:Width="45"/>
   <Column ss:StyleID="s64" ss:AutoFitWidth="0" ss:Span="4"/>
   <Column ss:Index="12" ss:StyleID="s64" ss:Width="98.25"/>
   <Column ss:StyleID="s64" ss:Width="81"/>
   <Column ss:StyleID="s64" ss:Width="93.75"/>
   <Row ss:Height="18.75">
    <Cell ss:MergeAcross="13" ss:StyleID="s89"><Data ss:Type="String">Rekap Data <?=$kabupatenNama;?></Data></Cell>
   </Row>
   <Row>
    <Cell ss:MergeAcross="13" ss:StyleID="s90"><Data ss:Type="String">per tanggal <?=$this->tanggal;?></Data></Cell>
   </Row>
   <Row ss:Index="4" ss:Height="15.75" ss:StyleID="s65">
    <Cell ss:MergeDown="1" ss:StyleID="s70"><Data ss:Type="String">No</Data></Cell>
    <Cell ss:MergeDown="1" ss:StyleID="s70"><Data ss:Type="String">NPSN</Data></Cell>
    <Cell ss:MergeDown="1" ss:StyleID="s70"><Data ss:Type="String">Nama</Data></Cell>
    <Cell ss:MergeDown="1" ss:StyleID="m60617720"><Data ss:Type="String">Alamat</Data></Cell>
    <Cell ss:MergeDown="1" ss:StyleID="s70"><Data ss:Type="String">Status</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s70"><Data ss:Type="String">Jumlah Siswa</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s70"><Data ss:Type="String">Jumlah PTK</Data></Cell>
    <Cell ss:MergeDown="1" ss:StyleID="s70"><Data ss:Type="String">Jumlah Prasarana</Data></Cell>
    <Cell ss:MergeDown="1" ss:StyleID="s70"><Data ss:Type="String">Jumlah Sarana</Data></Cell>
    <Cell ss:MergeDown="1" ss:StyleID="s70"><Data ss:Type="String">Jumlah Operator</Data></Cell>
   </Row>
   <Row ss:Height="15.75" ss:StyleID="s65">
    <Cell ss:Index="6" ss:StyleID="s70"><Data ss:Type="String">L</Data></Cell>
    <Cell ss:StyleID="s70"><Data ss:Type="String">P</Data></Cell>
    <Cell ss:StyleID="s70"><Data ss:Type="String">T</Data></Cell>
    <Cell ss:StyleID="s70"><Data ss:Type="String">L</Data></Cell>
    <Cell ss:StyleID="s70"><Data ss:Type="String">P</Data></Cell>
    <Cell ss:StyleID="s70"><Data ss:Type="String">T</Data></Cell>
   </Row>
   <?php 
   		$no = 1;
   		foreach ($sekolahs as $s) {
   			
			// Menghitung Jumlah Peserta Didik
			$sqlPesertaDidikL = "SELECT COUNT(peserta_didik_id) FROM peserta_didik WHERE jenis_kelamin=1 AND sekolah_id='".$s->getSekolahId()."'";
			$sqlPesertaDidikP = "SELECT COUNT(peserta_didik_id) FROM peserta_didik WHERE jenis_kelamin=2 AND sekolah_id='".$s->getSekolahId()."'";
			$countPesertaDidikL = getValueBySql($sqlPesertaDidikL);
			$countPesertaDidikP = getValueBySql($sqlPesertaDidikP);
			$totalPesertaDidik = $countPesertaDidikL + $countPesertaDidikP;
			
   			//echo "<br><br> Peserta : ".$countPesertaDidikL;die();
			
			//Menghitung Jumlah Ptk
			$sqlPtkL = "SELECT COUNT(ptk_id) FROM ptk WHERE jenis_kelamin=1 AND sekolah_id='".$s->getSekolahId()."'";
			$sqlPtkP = "SELECT COUNT(ptk_id) FROM ptk WHERE jenis_kelamin=2 AND sekolah_id='".$s->getSekolahId()."'";
			$countPtkL = getValueBySql($sqlPtkL);
			$countPtkP = getValueBySql($sqlPtkP);
			$totalPtk = $countPtkL + $countPtkP;
			
			//Menghitung Prasarana
			$sqlPrasarana = "SELECT COUNT(prasarana_id) FROM prasarana WHERE sekolah_id='".$s->getSekolahId()."'";
			$countPrasarana = getValueBySql($sqlPrasarana);
			
			//Menghitung Sarana
			$sqlSarana = "SELECT COUNT(sarana_id) FROM sarana a1, prasarana a2 WHERE a1.prasarana_id=a2.prasarana_id AND a2.sekolah_id='".$s->getSekolahId()."'";
			$countSarana = getValueBySql($sqlSarana);
			
			//Menghitung Operator
			$sqlPengguna = "SELECT COUNT(pengguna_id) FROM pengguna WHERE peran_id=10 AND sekolah_id='".$s->getSekolahId()."'";
			$countPengguna = getValueBySql($sqlPengguna);
			
   			if ($no % 2){
				$styleA = "67";
				$styleB = "66";
				$styleC = "68";
			} else {
				$styleA = "71";
				$styleB = "72";
				$styleC = "73";
			}
			
			$npsn = is_numeric($s->getNpsn()) ? $s->getNpsn() : "";
			
			$alamatJalan = $s->getAlamatJalan() ? $s->getAlamatJalan() : "-";
			$noTelp = $s->getNomorTelepon() ? $s->getNomorTelepon() : "-";
			$kodePos = $s->getKodePos() ? $s->getKodePos() : "-";
			$email = $s->getEmail() ? $s->getEmail() : "-";
			$alamat = $alamatJalan." Telp.".$noTelp." Kode Pos ".$kodePos." Email:".$email;
			
			$statusSekolah = ($s->getStatusSekolah() == 1) ? "Negeri" : "Swasta"; 
			
			/*
			 * s67 => $styleA
			 * s66 => $styleB
			 * s68 => $styleC 
			 */
   ?>
   
   <Row>
    <Cell ss:StyleID="s<?=$styleA;?>"><Data ss:Type="Number"><?=$no;?></Data></Cell>
    <Cell ss:StyleID="s<?=$styleA;?>"><Data ss:Type="String"><?=$npsn;?></Data></Cell>
    <Cell ss:StyleID="s<?=$styleC;?>"><Data ss:Type="String"><?=strtoupper($s->getNama());?></Data></Cell>
    <Cell ss:StyleID="s<?=$styleC;?>"><Data ss:Type="String"><?=$alamat;?></Data></Cell>
    <Cell ss:StyleID="s<?=$styleA;?>"><Data ss:Type="String"><?=$statusSekolah;?></Data></Cell>
    <Cell ss:StyleID="s<?=$styleA;?>"><Data ss:Type="Number"><?=$countPesertaDidikL;?></Data></Cell>
    <Cell ss:StyleID="s<?=$styleA;?>"><Data ss:Type="Number"><?=$countPesertaDidikP;?></Data></Cell>
    <Cell ss:StyleID="s<?=$styleA;?>"><Data ss:Type="Number"><?=$totalPesertaDidik;?></Data></Cell>
    <Cell ss:StyleID="s<?=$styleA;?>"><Data ss:Type="Number"><?=$countPtkL;?></Data></Cell>
    <Cell ss:StyleID="s<?=$styleA;?>"><Data ss:Type="Number"><?=$countPtkP;?></Data></Cell>
    <Cell ss:StyleID="s<?=$styleA;?>"><Data ss:Type="Number"><?=$totalPtk;?></Data></Cell>
    <Cell ss:StyleID="s<?=$styleA;?>"><Data ss:Type="Number"><?=$countPrasarana;?></Data></Cell>
    <Cell ss:StyleID="s<?=$styleA;?>"><Data ss:Type="Number"><?=$countSarana;?></Data></Cell>
    <Cell ss:StyleID="s<?=$styleA;?>"><Data ss:Type="Number"><?=$countPengguna;?></Data></Cell>
   </Row>
   
   <?php 
   		$no++;
   		}
   ?>

   <Row ss:Height="15.75">
    <Cell ss:MergeAcross="4" ss:StyleID="m60617600"><Data ss:Type="String">Total</Data></Cell>
    <Cell ss:StyleID="s74" ss:Formula="=SUM(R[-<?=$sekolahCount?>]C:R[-1]C)"><Data ss:Type="Number"></Data></Cell>
    <Cell ss:StyleID="s74" ss:Formula="=SUM(R[-<?=$sekolahCount?>]C:R[-1]C)"><Data ss:Type="Number"></Data></Cell>
    <Cell ss:StyleID="s74" ss:Formula="=SUM(R[-<?=$sekolahCount?>]C:R[-1]C)"><Data ss:Type="Number"></Data></Cell>
    <Cell ss:StyleID="s74" ss:Formula="=SUM(R[-<?=$sekolahCount?>]C:R[-1]C)"><Data ss:Type="Number"></Data></Cell>
    <Cell ss:StyleID="s74" ss:Formula="=SUM(R[-<?=$sekolahCount?>]C:R[-1]C)"><Data ss:Type="Number"></Data></Cell>
    <Cell ss:StyleID="s74" ss:Formula="=SUM(R[-<?=$sekolahCount?>]C:R[-1]C)"><Data ss:Type="Number"></Data></Cell>
    <Cell ss:StyleID="s74" ss:Formula="=SUM(R[-<?=$sekolahCount?>]C:R[-1]C)"><Data ss:Type="Number"></Data></Cell>
    <Cell ss:StyleID="s74" ss:Formula="=SUM(R[-<?=$sekolahCount?>]C:R[-1]C)"><Data ss:Type="Number"></Data></Cell>
    <Cell ss:StyleID="s74" ss:Formula="=SUM(R[-<?=$sekolahCount?>]C:R[-1]C)"><Data ss:Type="Number"></Data></Cell>
   </Row>
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
   </PageSetup>
   <Print>
    <ValidPrinterInfo/>
    <PaperSizeIndex>9</PaperSizeIndex>
    <HorizontalResolution>600</HorizontalResolution>
    <VerticalResolution>600</VerticalResolution>
   </Print>
   <Zoom>85</Zoom>
   <Selected/>
   <Panes>
    <Pane>
     <Number>3</Number>
     <RangeSelection>R1C1:R1C14</RangeSelection>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>
