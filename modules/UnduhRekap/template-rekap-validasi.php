<?
	// print_r($_REQUEST);
	$pilihtunjangan = $_REQUEST['id'] ? $_REQUEST['id'] : 3; 
	
	$c = new Criteria();
	$c->add(TRekapValidasiPeer::R_KEPERLUAN_ID, $pilihtunjangan);
	$c->addAscendingOrderByColumn(TRekapValidasiPeer::PROPINSI_ID);
	$c->addAscendingOrderByColumn(TRekapValidasiPeer::KABUPATEN_KOTA_ID);
	
	$trekaps = TRekapValidasiPeer::doSelect($c);
	$count = TRekapValidasiPeer::doCount($c);
	$expandedRowCount = $count + 100;

	$date = getdate();
	$this->tanggal = $date['mday']." ".getbulan($date['mon'])." ".$date['year'];
	
	switch ($_REQUEST['id']) {
		case 3:
			$judul = "Tunjangan Profesi";
			break;
		case 4:
			$judul = "Tunjangan Fungsional";
			break;
		case 5:
			$judul = "Tunjangan Kualifikasi";
			break;
		case 6:
			$judul = "Tunjangan Daerah Khusus";
			break;
	}
	
	echo '<?xml version="1.0"?>';
	echo '<?mso-application progid="Excel.Sheet"?>';
?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author>ACER</Author>
  <LastAuthor>Abah</LastAuthor>
  <Created>2012-08-08T05:18:46Z</Created>
  <LastSaved>2012-11-18T19:00:17Z</LastSaved>
  <Version>12.00</Version>
 </DocumentProperties>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>8190</WindowHeight>
  <WindowWidth>20115</WindowWidth>
  <WindowTopX>240</WindowTopX>
  <WindowTopY>60</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:CharSet="1" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="m73410752">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#DBEEF3" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="m73410772">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#DBEEF3" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s62">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s63">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s64">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s65">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="14" ss:Color="#000000"
    ss:Bold="1"/>
  </Style>
  <Style ss:ID="s66">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#DBEEF3" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="#,##0"/>
  </Style>
  <Style ss:ID="s72">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#E5E0EC" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s73">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#E5E0EC" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="#,##0"/>
  </Style>
  <Style ss:ID="s74">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s75">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s76">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>
   <NumberFormat ss:Format="#,##0"/>
  </Style>
  <Style ss:ID="s79">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#DBEEF3" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s80">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>
  </Style>
 </Styles>
 <Worksheet ss:Name="Rekap Validasi">
  <Table ss:ExpandedColumnCount="7" ss:ExpandedRowCount="<?=$expandedRowCount?>" x:FullColumns="1"
   x:FullRows="1" ss:StyleID="s62" ss:DefaultRowHeight="15">
   <Column ss:StyleID="s62" ss:AutoFitWidth="0" ss:Width="6.75"/>
   <Column ss:StyleID="s63" ss:AutoFitWidth="0" ss:Width="32.25"/>
   <Column ss:StyleID="s63" ss:AutoFitWidth="0" ss:Width="150"/>
   <Column ss:StyleID="s62" ss:AutoFitWidth="0" ss:Width="99.75" ss:Span="2"/>
   <Column ss:Index="7" ss:StyleID="s63" ss:AutoFitWidth="0" ss:Width="105.75"/>
   <Row ss:AutoFitHeight="0" ss:Height="15.75"/>
   <Row ss:AutoFitHeight="0">
    <Cell ss:Index="2" ss:StyleID="s64"><Data ss:Type="String">Direktorat P2TK Ditjen Dikdas</Data></Cell>
    <Cell ss:Index="5" ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:Index="2" ss:StyleID="s65"><Data ss:Type="String">Rekap Validasi Data Guru Nasional <?=$judul?></Data></Cell>
    <Cell ss:Index="5" ss:StyleID="s65"/>
    <Cell ss:StyleID="s65"/>
    <Cell ss:StyleID="s65"/>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:Index="2"><Data ss:Type="String">per tanggal <?=$this->tanggal;?></Data></Cell>
    <Cell ss:Index="4" ss:StyleID="s63"/>
    <Cell ss:StyleID="s63"/>
    <Cell ss:StyleID="s63"/>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:Index="2"></Cell>
    <Cell ss:Index="4" ss:StyleID="s63"/>
    <Cell ss:StyleID="s63"/>
    <Cell ss:StyleID="s63"/>
   </Row>
   <Row ss:Index="6" ss:AutoFitHeight="0" ss:Height="30.75">
    <Cell ss:Index="2" ss:MergeAcross="1" ss:StyleID="m73410752"><Data
      ss:Type="String">Wilayah</Data></Cell>
    <Cell ss:StyleID="s66"><Data ss:Type="String">Belum Update</Data></Cell>
    <Cell ss:StyleID="s66"><Data ss:Type="String">Memenuhi Syarat</Data></Cell>
    <Cell ss:StyleID="s66"><Data ss:Type="String">Tidak Memenuhi</Data></Cell>
    <Cell ss:StyleID="s66"><Data ss:Type="String">Total</Data></Cell>
   </Row>
   <?  	
   
   	$arr = array();
	foreach ($trekaps as $t) {
		$arr[$t->getPropinsiId()] += 1;
	}
	
	$noProp = 0;
	$propId = 0;
	$tot1 = 0;
	$tot2 = 0;
	$tot3 = 0;
	$tot4 = 0;
	$i = 0;
	foreach ($trekaps as $t) {
		
		if ($propId != $t->getPropinsiId()) {
			// header propinsi
			$noProp++;
			$noKab = 1;
			$jmlKabkota = $arr[$t->getPropinsiId()];
			if ($jmlKabkota < 1)
				continue;
			?>
			<Row ss:AutoFitHeight="0">
			    <Cell ss:Index="2" ss:StyleID="s72"><Data ss:Type="String"><?=$noProp?>. <?=$t->getPropinsi()->getNama()?></Data></Cell>
			    <Cell ss:StyleID="s72"/>
			    <Cell ss:StyleID="s73" ss:Formula="=SUM(R[1]C:R[<?=$jmlKabkota?>]C)"><Data ss:Type="Number"></Data></Cell>
			    <Cell ss:StyleID="s73" ss:Formula="=SUM(R[1]C:R[<?=$jmlKabkota?>]C)"><Data ss:Type="Number"></Data></Cell>
			    <Cell ss:StyleID="s73" ss:Formula="=SUM(R[1]C:R[<?=$jmlKabkota?>]C)"><Data ss:Type="Number"></Data></Cell>
			    <Cell ss:StyleID="s73" ss:Formula="=SUM(RC[-3]:RC[-1])"><Data ss:Type="Number"></Data></Cell>
		   	</Row>
		   	<Row ss:AutoFitHeight="0">
			    <Cell ss:Index="2" ss:StyleID="s74"><Data ss:Type="String"><?=$noProp?>.<?=$noKab?></Data></Cell>
			    <Cell ss:StyleID="s75"><Data ss:Type="String"><?=$t->getKabupatenKota()->getNama()?></Data></Cell>
			    <Cell ss:StyleID="s76"><Data ss:Type="Number"><?=$t->getJumlahIncomplete()?></Data></Cell>
			    <Cell ss:StyleID="s76"><Data ss:Type="Number"><?=$t->getJumlahPass()?></Data></Cell>
			    <Cell ss:StyleID="s76"><Data ss:Type="Number"><?=$t->getJumlahUnqualified()?></Data></Cell>
			    <Cell ss:StyleID="s76" ss:Formula="=SUM(RC[-3]:RC[-1])"><Data ss:Type="Number"></Data></Cell>
	   		</Row>	
			<?
			
		} else {
			// header kabkota
			$noKab++;
			?>
			<Row ss:AutoFitHeight="0">
			    <Cell ss:Index="2" ss:StyleID="s74"><Data ss:Type="String"><?=$noProp?>.<?=$noKab?></Data></Cell>
			    <Cell ss:StyleID="s75"><Data ss:Type="String"><?=$t->getKabupatenKota()->getNama()?></Data></Cell>
			    <Cell ss:StyleID="s76"><Data ss:Type="Number"><?=$t->getJumlahIncomplete()?></Data></Cell>
			    <Cell ss:StyleID="s76"><Data ss:Type="Number"><?=$t->getJumlahPass()?></Data></Cell>
			    <Cell ss:StyleID="s76"><Data ss:Type="Number"><?=$t->getJumlahUnqualified()?></Data></Cell>
			    <Cell ss:StyleID="s76" ss:Formula="=SUM(RC[-3]:RC[-1])"><Data ss:Type="Number"></Data></Cell>
	   		</Row>	
	   		
			<?	
		}
		
		$tot1 += $t->getJumlahIncomplete();
   		$tot2 += $t->getJumlahPass();
   		$tot3 += $t->getJumlahUnqualified();
   		$tot4 += ($t->getJumlahIncomplete() + $t->getJumlahPass() + $t->getJumlahUnqualified());
		
		$propId = $t->getPropinsiId();
	}
   
   ?>
   
   <Row ss:AutoFitHeight="0" ss:Height="30">
    <Cell ss:Index="2" ss:MergeAcross="1" ss:StyleID="m73410772"><Data
      ss:Type="String">Nasional</Data></Cell>
    <Cell ss:StyleID="s66" ><Data ss:Type="Number"><?=$tot1?></Data></Cell>
    <Cell ss:StyleID="s66" ><Data ss:Type="Number"><?=$tot2?></Data></Cell>
    <Cell ss:StyleID="s66" ><Data ss:Type="Number"><?=$tot3?></Data></Cell>
    <Cell ss:StyleID="s66" ><Data ss:Type="Number"><?=$tot4?></Data></Cell>
   </Row>
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
   </PageSetup>
   <Unsynced/>
   <Print>
    <ValidPrinterInfo/>
    <PaperSizeIndex>9</PaperSizeIndex>
    <HorizontalResolution>-3</HorizontalResolution>
    <VerticalResolution>-3</VerticalResolution>
   </Print>
   <Selected/>
   <Panes>
    <Pane>
     <Number>3</Number>
     <ActiveRow>2</ActiveRow>
     <ActiveCol>2</ActiveCol>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>
