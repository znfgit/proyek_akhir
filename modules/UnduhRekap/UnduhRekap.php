<?php
class UnduhRekapModule extends XondModule {
	
	var $component;	
	
	function UnduhRekapModule() {
		parent::__construct();		
	}
	
	function execUnduhRekapNasional() {
		
		switch ($_REQUEST['id']) {
			case 3:
				$nama = "tunjangan-profesi";
				$judul = "Tunjangan Profesi";
				break;
			case 4:
				$nama = "tunjangan-fungsional";
				$judul = "Tunjangan Fungsional";
				break;
			case 5:
				$nama = "tunjangan-kualifikasi";
				$judul = "Tunjangan Kualifikasi";
				break;
			case 6:
				$nama = "tunjangan-daerah-khusus";
				$judul = "Tunjangan Daerah Khusus";
				break;
			case 7:
				$nama = "tunjangan-profesi-DAU";
				$judul = "Tunjangan Profesi DAU";
				break;
			case 8:
				$nama = "tunjangan-profesi-DEKON";
				$judul = "Tunjangan Profesi DEKON";
				break;
			default:
				$nama = "tunjangan-profesi";
				$judul = "Tunjangan Profesi";
				break;
		}
		
		
		$this->writeHttpHeader('Content-type: application/x-excel');
		$date = date("Ymd-Hi");
		$this->writeHttpHeader('Content-Disposition: attachment; filename="rekap-validasi-'.$nama.'-'.$date.'.xls"');
		$this->writeUsingTemplate(dirname(__FILE__).D.'template-rekap-validasi.php');
		
	}
	
	function execUnduhLaporanKoreksiData() {
		
		switch ($_REQUEST['id']) {
			case 3:
				$nama = "tunjangan-profesi";
				$judul = "Tunjangan Profesi";
				break;
			case 4:
				$nama = "tunjangan-fungsional";
				$judul = "Tunjangan Fungsional";
				break;
			case 5:
				$nama = "tunjangan-kualifikasi";
				$judul = "Tunjangan Kualifikasi";
				break;
			case 6:
				$nama = "tunjangan-daerah-khusus";
				$judul = "Tunjangan Daerah Khusus";
				break;
			case 7:
				$nama = "tunjangan-profesi-DAU";
				$judul = "Tunjangan Profesi DAU";
				break;
			case 8:
				$nama = "tunjangan-profesi-DEKON";
				$judul = "Tunjangan Profesi DEKON";
				break;
			default:
				$nama = "tunjangan-profesi";
				$judul = "Tunjangan Profesi";
				break;
		}
		
		
		$this->writeHttpHeader('Content-type: application/x-excel');
		$date = date("Ymd-Hi");
		$this->writeHttpHeader('Content-Disposition: attachment; filename="laporan-koreksi-data-'.$nama.'-'.$date.'.xls"');
		$this->writeUsingTemplate(dirname(__FILE__).D.'template-koreksi-data.php');
		
	}
	
	function execExcekRekapValidasi() {
		$pilihtunjangan = $_REQUEST['pilihtunjangan'];
		
		$c = new Criteria();
		if ($pilihtunjangan) {
			$c->add(TRekapValidasiPeer::R_KEPERLUAN_ID, $pilihtunjangan);
		}
		$trekaps = TRekapValidasiPeer::doSelect($c);
		
		
		$filepath = dirname(__FILE__).D.'templates'.D.'Template-Rekap.xlsx';
		$reader = PHPExcel_IOFactory::createReader('Excel2007');
		
		$excelFile = new PHPExcel();
		$execFile = $reader->load($filepath);
		
		$baris = $execFile->getSheetByName("Rekap Validasi");
		$baris->getCell("C4")->setValue("per tanggal ".date("Y-m-d H:i:s"));
		
		$isi = 7;
		$styleArray = array(
	       'borders' => array(
	             'outline' => array(
	                    'style' => PHPExcel_Style_Border::BORDER_THIN,
	                    'color' => array('rgb' => '000000'),
	             ),
	       ),
		);
		
		$noProp = 0;
		$propId = 1;
		foreach ($trekaps as $t) {
			$noProp++;
			$noKab = 0;
			
			if ($propId == $t->getPropinsiId()) {
				$noKab++;
				
			}
			
			$baris->getCell("A{$isi}")->setValue($a['RekapId']);
			$baris->getStyle("A{$isi}")->applyFromArray($styleArray);
			
			$propId = $t->getPropinsiId();
		}
	}
	
}
?>