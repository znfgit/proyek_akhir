<?
	$sekolah_id = $_REQUEST['id'] ? $_REQUEST['id'] : '';
	
	$sekolahObj = SekolahPeer::retrieveByPK($sekolah_id);
	if (!is_object($sekolahObj)) {
		die();
	}
	
	$c = new Criteria();
	$c->add(TPtkPeer::SEKOLAH_ID, $sekolah_id);
	$c->add(TPtkPeer::STATUS_DATA, NULL, Criteria::ISNULL);
	// $tPtkForSekolah = TPtkPeer::doSelectOne($c);
	$tPtkObj = TPtkPeer::doSelect($c);
	
	$namaSekolah = $sekolahObj->getNama();
	$namaKabkota = is_object($sekolahObj->getKabupatenKota()) ? $sekolahObj->getKabupatenKota()->getNama() : "-";
	$namaKecamatan = is_object($sekolahObj->getKecamatan()) ? $sekolahObj->getKecamatan()->getNama() : "-";
	
	/*
	$tPtkArr = get_object_vars($tPtkObj);
	var_dump($tPtkArr);
	$nama_sekolah = $tPtkArr[0]['nama_sekolah:protected'];
	echo $nama_sekolah; die();
	*/
	
	$expandedRowCount = TPtkPeer::doCount($c) + 100;
	
	$date = getdate();
	$this->tanggal = $date['mday']." ".getbulan($date['mon'])." ".$date['year'];
	
	echo '<?xml version="1.0"?>';
	echo '<?mso-application progid="Excel.Sheet"?>';
?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author>Abah</Author>
  <LastAuthor>Abah</LastAuthor>
  <Created>2012-11-17T04:00:23Z</Created>
  <LastSaved>2012-11-19T17:31:47Z</LastSaved>
  <Company>p2tk</Company>
  <Version>12.00</Version>
 </DocumentProperties>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>7935</WindowHeight>
  <WindowWidth>20055</WindowWidth>
  <WindowTopX>240</WindowTopX>
  <WindowTopY>75</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s16" ss:Name="Hyperlink">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#0000FF"
    ss:Underline="Single"/>
  </Style>
  <Style ss:ID="m48942568">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#C2D69A" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s29">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="14" ss:Color="#000000"
    ss:Bold="1"/>
  </Style>
  <Style ss:ID="s30">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s31">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s32">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s92">
   <Alignment ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s93" ss:Parent="s16">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Protection/>
  </Style>
  <Style ss:ID="s165">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#C2D69A" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s168">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#C2D69A" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s181">
   <Alignment ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Interior ss:Color="#DBEEF3" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s182">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#FF0000"
    ss:Italic="1"/>
   <Interior ss:Color="#DBEEF3" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s183" ss:Parent="s16">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Interior ss:Color="#DBEEF3" ss:Pattern="Solid"/>
   <Protection/>
  </Style>
  <Style ss:ID="s184">
   <Alignment ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Interior ss:Color="#DBEEF3" ss:Pattern="Solid"/>
  </Style>
 </Styles>
 <Worksheet ss:Name="Laporan Koreksi Data Tunjangan">
  <Table ss:ExpandedColumnCount="19" ss:ExpandedRowCount="<?=$expandedRowCount?>" x:FullColumns="1"
   x:FullRows="1" ss:DefaultRowHeight="15">
   <Column ss:AutoFitWidth="0" ss:Width="9.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="25.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="89.25"/>
   <Column ss:AutoFitWidth="0" ss:Width="118.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="100.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="78.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="54"/>
   <Column ss:AutoFitWidth="0" ss:Width="63"/>
   <Column ss:AutoFitWidth="0" ss:Width="146.25"/>
   <Column ss:AutoFitWidth="0" ss:Width="75"/>
   <Column ss:AutoFitWidth="0" ss:Width="150"/>
   <Column ss:AutoFitWidth="0" ss:Width="73.5"/>
   <Column ss:Index="14" ss:AutoFitWidth="0" ss:Width="137.25"/>
   <Column ss:AutoFitWidth="0" ss:Width="126.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="123.75" ss:Span="1"/>
   <Column ss:Index="18" ss:AutoFitWidth="0" ss:Width="143.25"/>
   <Column ss:AutoFitWidth="0" ss:Width="225"/>
   <Row ss:Index="2" ss:Height="15.75">
    <Cell ss:Index="2" ss:StyleID="s31"><Data ss:Type="String">Direktorat P2TK Ditjen Dikdas</Data></Cell>
   </Row>
   <Row ss:Height="18.75">
    <Cell ss:Index="2" ss:StyleID="s29"><Data ss:Type="String">Laporan Koreksi Data Tunjangan Profesi</Data></Cell>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s32"><Data ss:Type="String">per tanggal <?=$this->tanggal?></Data></Cell>
   </Row>
   <Row ss:Index="6" ss:Height="15.75">
    <Cell ss:Index="2" ss:StyleID="s30"><Data ss:Type="String">Tempat Tugas</Data></Cell>
    <Cell ss:Index="4" ss:StyleID="s30"><Data ss:Type="String">: <?=$namaSekolah?></Data></Cell>
   </Row>
   <Row ss:Height="15.75">
    <Cell ss:Index="2" ss:StyleID="s30"><Data ss:Type="String">Kabupaten/Kota</Data></Cell>
    <Cell ss:Index="4" ss:StyleID="s30"><Data ss:Type="String">: <?=$namaKabkota?></Data></Cell>
   </Row>
   <Row ss:Height="15.75">
    <Cell ss:Index="2" ss:StyleID="s30"><Data ss:Type="String">Kecamatan</Data></Cell>
    <Cell ss:Index="4" ss:StyleID="s30"><Data ss:Type="String">: <?=$namaKecamatan?></Data></Cell>
   </Row>
   <Row ss:Index="10">
    <Cell ss:Index="2" ss:StyleID="s165"/>
    <Cell ss:MergeAcross="5" ss:StyleID="s165"><Data ss:Type="String">Data Kelulusan</Data></Cell>
    <Cell ss:MergeAcross="9" ss:StyleID="s165"><Data ss:Type="String">Data Dapodik</Data></Cell>
    <Cell ss:MergeDown="1" ss:StyleID="m48942568"><Data ss:Type="String">Catatan</Data></Cell>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s165"><Data ss:Type="String">No</Data></Cell>
    <Cell ss:StyleID="s165"><Data ss:Type="String">NRG</Data></Cell>
    <Cell ss:StyleID="s165"><Data ss:Type="String">NUPTK</Data></Cell>
    <Cell ss:StyleID="s165"><Data ss:Type="String">Nama</Data></Cell>
    <Cell ss:StyleID="s165"><Data ss:Type="String">Tahun Lulus</Data></Cell>
    <Cell ss:StyleID="s165"><Data ss:Type="String">Jenjang</Data></Cell>
    <Cell ss:StyleID="s165"><Data ss:Type="String">Status</Data></Cell>
    <Cell ss:StyleID="s165"><Data ss:Type="String">Nama</Data></Cell>
    <Cell ss:StyleID="s165"><Data ss:Type="String">Status Aktif</Data></Cell>
    <Cell ss:StyleID="s165"><Data ss:Type="String">Tempat Tugas</Data></Cell>
    <Cell ss:StyleID="s165"><Data ss:Type="String">JJM sesuai</Data></Cell>
    <Cell ss:StyleID="s165"><Data ss:Type="String">JJM KTSP</Data></Cell>
    <Cell ss:StyleID="s165"><Data ss:Type="String">Tugas Tambahan</Data></Cell>
    <Cell ss:StyleID="s165"><Data ss:Type="String">JJM Tugas Tambahan</Data></Cell>
    <Cell ss:StyleID="s165"><Data ss:Type="String">Masa Kerja</Data></Cell>
    <Cell ss:StyleID="s165"><Data ss:Type="String">Golongan</Data></Cell>
    <Cell ss:StyleID="s168"><Data ss:Type="String">Email</Data></Cell>
   </Row>
   
   <?php 
   
   	$i = 0;
   	foreach ($tPtkObj as $t) {
   		$i++;
   		$styleId = ($i % 2 != 0) ? $styleId = "s92" : $styleId = "s181";
   		$catatan = "";
   		$masaKerja = "";

   		list($tahun, $bulan, $tanggal) = explode('-', $t->getTglLulusSertifikasi());
   		// options: [[1, 'Aktif'],[2,'Cuti'],[3,'Cuti di luar tanggungan negara'],[4,'Tugas Belajar'],[5,'Mutasi'],[6,'Berhenti'],[7,'Pensiun'],[8,'Wafat']]
   		switch ($t->getStatus()) {
   			case 1: $status = "Aktif"; break;
   			case 2: $status = "Cuti"; break;
   			case 3: $status = "Cuti di luar tanggungan negara"; break;
   			case 4: $status = "Tugas Belajar"; break;
   			case 5: $status = "Mutasi"; break;
   			case 6: $status = "Berhenti"; break;
   			case 7: $status = "Pensiun"; break;
   			case 8: $status = "Wafat"; break;
   			default: $status = "-"; break;
   		}
   		if ($t->getMasaKerjaTahun()) {
   			$masaKerja = $t->getMasaKerjaTahun()." Tahun ";
   		}
   		if ($t->getMasaKerjaBulan()) {
   			$masaKerja .= $t->getMasaKerjaBulan()." Bulan"; 
   		}
   		
   		if ($t->getStatus() != 1) {
   			$catatan .= "- Status PTK tidak aktif; ";
   		}
   		if (!$t->getNamaBidangStudiSertifikasi()) {
   			if ($catatan) {
   				$catatan .= "&#10;";
   			}
   			$catatan .= "- Bidang Studi Sertifikasi dinilai tidak valid; ";
   		}
   		if (!$t->getTglPensiun()) {
   			if ($catatan) {
   				$catatan .= "&#10;";
   			}
   			$catatan .= "- Tanggal Pensiun dinilai tidak valid; ";
   		}
   		if (!$t->getNamaStatusKepegawaian()) {
   			if ($catatan) {
   				$catatan .= "&#10;";
   			}
   			$catatan .= "- Status Kepegawaian dinilai tidak valid; "; // nama_status_kepegawaian
   		}
   		if (!$t->getJumlahJamMengajarSesuai()) {
   			if ($catatan) {
   				$catatan .= "&#10;";
   			}
   			$catatan .= "- JJM Sesuai kosong, data mengajar linear belum di isi"; // nama_status_kepegawaian
   		}
   	?>
   	
   	<Row>
    <Cell ss:Index="2" ss:StyleID="<?=$styleId?>"><Data ss:Type="Number"><?=$i?></Data></Cell>
    <Cell ss:StyleID="<?=$styleId?>"><Data ss:Type="String"><?=$t->getNrg()?></Data></Cell>
    <Cell ss:StyleID="<?=$styleId?>"><Data ss:Type="String"><?=$t->getNuptk()?></Data></Cell>
    <Cell ss:StyleID="<?=$styleId?>"><Data ss:Type="String"><?=$t->getNamaNrg()?></Data></Cell>
    <Cell ss:StyleID="<?=$styleId?>"><Data ss:Type="Number"><?=$tahun?></Data></Cell>
    <Cell ss:StyleID="<?=$styleId?>"><Data ss:Type="String"><?=$t->getNamaJenisSekolah()?></Data></Cell>
    <Cell ss:StyleID="<?=$styleId?>"><Data ss:Type="String"><?=$t->getNamaStatusKepegawaian()?></Data></Cell>
    <Cell ss:StyleID="<?=$styleId?>"><Data ss:Type="String"><?=$t->getNama()?></Data></Cell>
    <Cell ss:StyleID="<?=$styleId?>"><Data ss:Type="String"><?=$status?></Data></Cell>
    <Cell ss:StyleID="<?=$styleId?>"><Data ss:Type="String"><?=$namaSekolah?></Data></Cell>
    <Cell ss:StyleID="<?=$styleId?>"><Data ss:Type="Number"><?=$t->getJumlahJamMengajarSesuai()?></Data></Cell>
    <Cell ss:StyleID="<?=$styleId?>"><Data ss:Type="Number"><?=$t->getJumlahJamMengajarSesuaiKtsp()?></Data></Cell>
    <Cell ss:StyleID="<?=$styleId?>"><Data ss:Type="String"><?=$t->getNamaTugasTambahan()?></Data></Cell>
    <Cell ss:StyleID="<?=$styleId?>"><Data ss:Type="Number"><?=$t->getJumlahJamMengajar()?></Data></Cell>
    <Cell ss:StyleID="<?=$styleId?>"><Data ss:Type="String"><?=$masaKerja?></Data></Cell>
    <Cell ss:StyleID="<?=$styleId?>"><Data ss:Type="String"><?=$t->getNamaPangkatGolongan()?></Data></Cell>
    <Cell ss:StyleID="<?=$styleId?>"><Data ss:Type="String"><?=$t->getEmail()?></Data></Cell>
    <Cell ss:StyleID="<?=$styleId?>"><Data ss:Type="String" x:Ticked="1"><?=$catatan?></Data></Cell>
   </Row>
      	
   	<?php 
   	}
   ?>
      
   <!-- Row ss:Height="45">
    <Cell ss:Index="2" ss:StyleID="s181"><Data ss:Type="Number">2</Data></Cell>
    <Cell ss:StyleID="s181"><Data ss:Type="String">1089 9998 8890</Data></Cell>
    <Cell ss:StyleID="s181"><Data ss:Type="String">1298-9987-9987-0004</Data></Cell>
    <Cell ss:StyleID="s181"><Data ss:Type="String">Susanti</Data></Cell>
    <Cell ss:StyleID="s181"><Data ss:Type="Number">2010</Data></Cell>
    <Cell ss:StyleID="s181"><Data ss:Type="String">SD</Data></Cell>
    <Cell ss:StyleID="s181"><Data ss:Type="String">PNS</Data></Cell>
    <Cell ss:StyleID="s182"><Data ss:Type="String" x:Ticked="1">&#45;- Tidak match dengan dapodik</Data></Cell>
    <Cell ss:StyleID="s181"/>
    <Cell ss:StyleID="s181"/>
    <Cell ss:StyleID="s181"/>
    <Cell ss:StyleID="s181"/>
    <Cell ss:StyleID="s181"/>
    <Cell ss:StyleID="s181"/>
    <Cell ss:StyleID="s181"/>
    <Cell ss:StyleID="s181"/>
    <Cell ss:StyleID="s183" ss:Formula="=RC[-13] &amp; &quot;@gmail.com&quot;"
     ss:HRef="mailto:joni01@yahoo.com"><Data ss:Type="String">Susanti@gmail.com</Data></Cell>
    <Cell ss:StyleID="s184"><Data ss:Type="String" x:Ticked="1">-Kemungkinan Kesalahan NUPTK di dapodik&#10;- Salah NUPTK&#10;- Salah Tanggal Lahir</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="21">
    <Cell ss:Index="2" ss:StyleID="s92"><Data ss:Type="Number">3</Data></Cell>
    <Cell ss:StyleID="s92"><Data ss:Type="String">1090 9998 8889</Data></Cell>
    <Cell ss:StyleID="s92"><Data ss:Type="String">1298-9987-9987-0005</Data></Cell>
    <Cell ss:StyleID="s92"><Data ss:Type="String">Susilo</Data></Cell>
    <Cell ss:StyleID="s92"><Data ss:Type="Number">2008</Data></Cell>
    <Cell ss:StyleID="s92"><Data ss:Type="String">SMP</Data></Cell>
    <Cell ss:StyleID="s92"><Data ss:Type="String">PNS</Data></Cell>
    <Cell ss:StyleID="s92"><Data ss:Type="String">Susilo </Data></Cell>
    <Cell ss:StyleID="s92"><Data ss:Type="String">Aktif</Data></Cell>
    <Cell ss:StyleID="s92"><Data ss:Type="String">SD N Pulomas 3 Jakarta Pusat</Data></Cell>
    <Cell ss:StyleID="s92"><Data ss:Type="Number">20</Data></Cell>
    <Cell ss:StyleID="s92"><Data ss:Type="Number">12</Data></Cell>
    <Cell ss:StyleID="s92"><Data ss:Type="String">Wali Kelas</Data></Cell>
    <Cell ss:StyleID="s92"><Data ss:Type="Number">6</Data></Cell>
    <Cell ss:StyleID="s92"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s92"/>
    <Cell ss:StyleID="s93" ss:Formula="=RC[-13] &amp; &quot;@gmail.com&quot;"
     ss:HRef="mailto:joni01@yahoo.com"><Data ss:Type="String">Susilo@gmail.com</Data></Cell>
    <Cell ss:StyleID="s92"><Data ss:Type="String" x:Ticked="1">- Masa Kerja kosong (TMT Pns belum diisi)</Data></Cell>
   </Row-->
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
   </PageSetup>
   <Print>
    <ValidPrinterInfo/>
    <VerticalResolution>0</VerticalResolution>
   </Print>
   <Selected/>
   <Panes>
    <Pane>
     <Number>3</Number>
     <ActiveRow>4</ActiveRow>
     <ActiveCol>1</ActiveCol>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>
