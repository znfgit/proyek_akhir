<?php
//error_reporting(E_ALL);

class MappingModule extends XondModule {
	
	function MappingModule() {
		parent::__construct();
		$this->registerUi('sinkronisasi');
		$this->setMinifyUi(false);
		$this->setTitle('Mapping');
	}

	function execFetch() {
		//$start = $_REQUEST["start"] ? $_REQUEST["start"] : 0;
		//$limit = $_REQUEST["start"] ? $_REQUEST["start"] : 20;
		$tahun = $_REQUEST["tahun"] ? $_REQUEST["tahun"] : 2012;
		
		$c = new Criteria();
		
		$c->add(RMappingBidangStudiPeer::VERSI_TAHUN, $tahun);
		$count = RMappingBidangStudiPeer::doCount($c);
		//print_r($c);
		
		//$c->setOffset($start);
		//$c->setLimit($limit);		
		
		$bss = RMappingBidangStudiPeer::doSelect($c);
		
		
		// Get the codes as limited by the pager
		$kodes = array();
		
		foreach ($bss as $b) {
			//$b = new RMappingBidangStudi();
			$kodes[]  = $b->getKodeLama();
		}
		
		// Get the Referensi Asosiasi
		$c1 = new Criteria();
		$c1->add(RaSertifikasiTugasmengajarPeer::BIDANG_STUDI_ID, $kodes, Criteria::IN);
		$c1->add(RaSertifikasiTugasmengajarPeer::TAHUN_SERTIFIKASI, $tahun);
		$ras = RaSertifikasiTugasmengajarPeer::doSelect($c1);	
		
		// Map to Array
		$arrRa = array();
		$arrJmlCentang = array();
		
		if ($ras) {					
			foreach ($ras as $r) {
				//$r = new RaSertifikasiTugasmengajar();
				$arrRa[$r->getBidangStudiId()][$r->getMatpelId()] = $r->getSesuai();
				$arrJmlCentang[$r->getBidangStudiId()] += $r->getSesuai();
			}
		}
				/*
				$jmlCentang = 0;
				foreach ($arr as $a) {
					$jmlCentang += $a;
				}
				*/				
		//print_r($arrRa); die;
		
		// Combine both
		foreach($bss as $b) {
			//$b = new RMappingBidangStudi();
			//$arr = $b->toArray(BasePeer::TYPE_PHPNAME);
			$arr = array();
			if ($arrRa[$b->getKodeLama()]) {
				$arr = $arrRa[$b->getKodeLama()];
				//$outArr[] = array_merge($arr, $arrRa[$b->getKodeLama()]);
			} else {
				//$outArr[] = $arr;
			}
			$arr["KodeLama"] = $b->getKodeLama();
			$arr["NamaLama"] = $b->getNamaLama() ? $b->getNamaLama() : "-";
			$arr["VersiTahun"] =  $b->getVersiTahun();
			$arr["JumlahGuru"] = $b->getJumlahGuru();
			//$arr["JumlahTercentang"] = $jmlCentang;
			$arr["JumlahTercentang"] = $arrJmlCentang[$b->getKodeLama()];
			$outArr[] = $arr;
		}
		//print_r($outArr); die;
		$this->write(tableJson($outArr, $count, RMappingBidangStudiPeer::getFieldNames()));
	}
	
	function execSave() {
		
		$user_id = $this->getAuth()->getUser()->getPrimaryKey();
		
		$data = splitJsonArray($_REQUEST['rows']);
		
		$rowsAffected = 0;
		
		foreach ($data as $d) {
			
			$obj = get_object_vars(json_decode(stripcslashes($d)));
			$tahun_sertifikasi = $obj["VersiTahun"];
			$bidang_studi_id = $obj["KodeLama"];			
			
			$c = new Criteria();
			$c->add(RaSertifikasiTugasmengajarPeer::TAHUN_SERTIFIKASI, $tahun_sertifikasi);
			$c->add(RaSertifikasiTugasmengajarPeer::BIDANG_STUDI_ID, $bidang_studi_id);			
			$ras = RaSertifikasiTugasmengajarPeer::doSelect($c);
			
			//print_r($ras); die;
			
			if (!$ras) {				
				
				foreach ($obj as $key => $val) {
					if (!in_array($key, array("VersiTahun", "KodeLama", "NamaLama", "JumlahGuru"))) {
						$ra = new RaSertifikasiTugasmengajar();
						$ra->setTahunSertifikasi($tahun_sertifikasi);
						$ra->setBidangStudiId($bidang_studi_id);
						$ra->setMatpelId($key);
						$ra->setSesuai(($val == true) ? "1" : "0");
						if ($ra->save()) {
							$rowsAffected++;
						}
					}
				}
			
			} else {
				
				$rasArr = array();
				
				foreach ($ras as $ra) {
					$rasArr[$ra->getMatpelId()] = $ra;
				}				
				//print_r($rasArr);
				
				foreach ($obj as $key => $val) {
					if (!in_array($key, array("VersiTahun", "KodeLama", "NamaLama", "JumlahGuru"))) {
						//echo "$key\n";
						$ra = $rasArr[$key];
						//print_r($ra); die;
						if (is_object($ra)) {
							$ra->setSesuai(($val == true) ? "1" : "0");
							
							if ($ra->save()) {
								$rowsAffected++;
							}
						}
					}
				}
			}
			
		}
		$success = ($rowsAffected > 0) ? "true" : "false";
		$message = ($rowsAffected > 0) ? "Sejumlah $rowsAffected baris data tersimpan." : "Tidak ada perubahan";
		$this->write("{ 'success': '$success', 'message': '$message' }") ;		
	}
	
	function execRefreshJumlahGuru() {
		try {
			getDataBySql('execute dbo.updateJumlahGuruSertifikasi');
			$this->write("{ 'success': 'true', 'message': 'Berhasil mengupdate' }");
		} catch (Exception $e) {
			$this->write("{ 'success': 'true', 'message': 'Gagal: ".$e->getMessage()."' }") ;
		}
	}
	
	function execGetProgress() {
		try {
		$ps = TSinkronPropinsiPeer::doSelect(new Criteria());
		$jumlah = 0;
		$total = 0;
		$namaProg = '';
		
		foreach ($ps as $p) {
			//$p = new TSinkronPropinsiPeer();
			$progress = $p->getProgress();
			$propId = $p->getPrimaryKey();
			if (($progress > 0) && ($progress < 100)) {
				if ($p->getNamaProses() != NULL) {
					$namaProg = "[Prop $propId] " .$p->getNamaProses()."..";
				}
			}
			if ($progress == 0) {
				$namaProg = "[Prop $propId] " ."memulai proses..";
			}
			if ($progress == 100) {
				$namaProg = "[Prop $propId] " ."proses selesai.";
			}
			$jumlah += $progress;
			$total += 100;
		}
		
		$totalProg = floor(100 * ($jumlah/$total));
		$this->write("{ 'success': 'true', 'progress': $totalProg, 'nama': '$namaProg' }");
		} catch (Exception $e) {
			$this->write("{ 'success': 'true', 'progress': 80, 'nama': 'exception..building' }");
		}
	}
	
	
}
?>