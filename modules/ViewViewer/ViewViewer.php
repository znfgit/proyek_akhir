<?php

class ViewViewerModule extends XondModule {

	var $component;

	function ViewViewerModule() {
		parent::__construct();
	}
	
	function execGetData() {
		$view_id = ($_REQUEST['view_id']) ? $_REQUEST['view_id'] : 1;
		$v = TViewsPeer::retrieveByPK($view_id);
		$sql = "select * from ".$v->getNama();
		$data = getDataBySql($sql);
		foreach ($data[0] as $k=>$v) {
			$arrFields[] = $k;
		}
		
		$this->write(tableJson($data, sizeof($data), $arrFields));
		
	}
	
	function execGetStore() {
		
		$view_id = ($_REQUEST['view_id']) ? $_REQUEST['view_id'] : 1;
		$v = TViewsPeer::retrieveByPK($view_id);
		
		//print_r($v); die; 
		$db = Propel::getDB(TViewsPeer::DATABASE_NAME);
		if (get_class($db) == 'DBMySQL') {
			$sql = "select * from ".$v->getNama()." LIMIT 0,1";
		} else {
			$sql = "select top 1 * from ".$v->getNama()."";
		}
		
		//echo $sql; die;
		
		$data = getDataBySql($sql);
		//print_r($data);
		
		$arr = array();
		$totalwidth = 0;
		$i = 0;
		$id = '';
		
		foreach ($data[0] as $k=>$v) {
			
			//echo "Column name: $k<br>Column value: $v<br>Column type: ";
			//echo is_numeric($v) ? 'Integer' : '';
			//echo is_string($v) ? 'String' : '';
			//echo "<br><br>";
			if ($i == 0) {
				$id = $k;
			}
			
			$type = (is_numeric($v)) ? 'float' : 'string';
			$arr[] = array("name" => $k, "type" => $type);
			
			$len = ( strlen($k) > strlen($v) ) ? strlen($k) : strlen($v); 
			$totalwidth += $len;
						
		}
		
		foreach ($data[0] as $k=>$v) {
			
			$len = ( strlen($k) > strlen($v) ) ? strlen($k) : strlen($v);
			
			$arrCol[] = array (
				"header" => humanize($k),
				"width" => floor(300 * $len/$totalwidth),
				"align" => 'left',
				"sortable" => true,
				"hidden" => false,
				"dataIndex" => $k
			);
		}			
		
		$this->write("{ 'success' : 'true','id': '".$id."', 'record_set' : ".json_encode($arr).", 'columns' : ".json_encode($arrCol)."}");
		
	}
}
