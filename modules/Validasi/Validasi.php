<?php

//error_reporting(E_ALL);
global $tptk, $arrJP, $arrPG, $arrSK;

class DateIntervalNew {
	public $y;
	public $m;
	public $d;
	public $h;
	public $i;
	public $s;
	public $invert;
   
	public function format($format) {
		$format = str_replace('%R%y', ($this->invert ? '-' : '+') . $this->y, $format);
		$format = str_replace('%R%m', ($this->invert ? '-' : '+') . $this->m, $format);
		$format = str_replace('%R%d', ($this->invert ? '-' : '+') . $this->d, $format);
		$format = str_replace('%R%h', ($this->invert ? '-' : '+') . $this->h, $format);
		$format = str_replace('%R%i', ($this->invert ? '-' : '+') . $this->i, $format);
		$format = str_replace('%R%s', ($this->invert ? '-' : '+') . $this->s, $format);
	   
		$format = str_replace('%y', $this->y, $format);
		$format = str_replace('%m', $this->m, $format);
		$format = str_replace('%d', $this->d, $format);
		$format = str_replace('%h', $this->h, $format);
		$format = str_replace('%i', $this->i, $format);
		$format = str_replace('%s', $this->s, $format);
	   
		return $format;
	}
}

function date_diff_new(DateTime $date1, DateTime $date2) {
	$diff = new DateIntervalNew();
	if($date1 > $date2) {
		$tmp = $date1;
		$date1 = $date2;
		$date2 = $tmp;
		$diff->invert = true;
	}
   
	$diff->y = ((int) $date2->format('Y')) - ((int) $date1->format('Y'));
	$diff->m = ((int) $date2->format('n')) - ((int) $date1->format('n'));
	if($diff->m < 0) {
		$diff->y -= 1;
		$diff->m = $diff->m + 12;
	}
	$diff->d = ((int) $date2->format('j')) - ((int) $date1->format('j'));
	if($diff->d < 0) {
		$diff->m -= 1;
		$diff->d = $diff->d + ((int) $date1->format('t'));
	}
	$diff->h = ((int) $date2->format('G')) - ((int) $date1->format('G'));
	if($diff->h < 0) {
		$diff->d -= 1;
		$diff->h = $diff->h + 24;
	}
	$diff->i = ((int) $date2->format('i')) - ((int) $date1->format('i'));
	if($diff->i < 0) {
		$diff->h -= 1;
		$diff->i = $diff->i + 60;
	}
	$diff->s = ((int) $date2->format('s')) - ((int) $date1->format('s'));
	if($diff->s < 0) {
		$diff->i -= 1;
		$diff->s = $diff->s + 60;
	}
   
	return $diff;
}
		
function selisih($dateTime1, $dateTime2) {
	$interval = date_diff_new($dateTime1, $dateTime2);
	return $interval->format("%a");
}


function convertToArrayOfObjects($tableName, $keyName=NULL) {

	$peerName = $tableName."Peer";		
	//$obj = new ${$tableName}();
	$p = new ${peerName}();
	//$p = $obj->getPeer();
	$arrOfObjects = $p->doSelect(new Criteria());
	
	foreach ($arrOfObjects as $o) {	
		
		if ($keyName) {
			$key = $o->get{$keyName}();
		} else {
			$key = $o->getPrimaryKey(); 
		}		
		$arr[$key] = $o;		
	}
	
	return $arr;
	
}

$arrJP = convertToArrayOfObjects("JenjangPendidikan");
$arrPG = convertToArrayOfObjects("PangkatGolongan");
$arrSK = convertToArrayOfObjects("StatusKepegawaian");


class ValidasiModule extends XondModule {
	
	var $component;	
	
	function ValidasiModule() {
		parent::__construct();		
	}
	
	function execStart() {
		
	}	
	
	function cekIjazahTerakhir($tptk) {
		//$tptk = new TPtk();		
		global $arrJP, $arrPG, $arrSK;
		
		$ijazahTerakhir = $tptk->getIjazahTerakhirId();
		
		if ($ijazahTerakhir == "" || is_null($ijazahTerakhir)) {			
			return array("pass" => false, "reason" => "Ijazah terakhir belum diisi");
		}
		//$jp = JenjangPendidikanPeer::retrieveByPK($ijazahTerakhir);
		$jp = $arrJP[$ijazahTerakhir];
		
		if (!is_object($jp)) {
			return array("pass" => false, "reason" => "Data ijazah terakhir tidak valid");
		} else {		
			return array("pass" => true, "reason" => "Data ijazah valid");
		}
	} 
	
	function cekNuptk($tptk) {
		
		//$tptk = new TPtk();
		$fail = false;
		 		
		$nuptk = $tptk->getNuptk();
		
		if ($nuptk == "" || is_null($nuptk)) {			
			return array("pass" => false, "reason" => "NUPTK belum diisi");
		}		
		if (strlen($nuptk) != 16) {			
			return array("pass" => false, "reason" => "Jumlah digit NUPTK tidak tepat (seharusnya 16 digit)");	
		}
		if ($nuptk == "0000000000000000") {			
			return array("pass" => false, "reason" => "NUPTK tidak valid");	
		}
		return array("pass" => true, "reason" => "NUPTK dinilai valid");
	}
	
	function cekTglLahir($tptk) {
		
		//$tptk = new TPtk();	
		 		
		$tglLahir = $tptk->getTglLahir();
		
		if ($tglLahir == "" || is_null($tglLahir)) {			
			return array("pass" => false, "reason" => "Tgl lahir belum diisi");
		}
		return array("pass" => true, "reason" => "Tgl lahir dinilai valid");
	}
	
	function cekStatusKepegawaianId($tptk) {
		global $arrJP, $arrPG, $arrSK;
		
		$statusKepegawaian = $tptk->getStatusKepegawaianId();
		//echo "<br>StatKepegawaian = $statusKepegawaian<br>";
		
		if ($statusKepegawaian == "" || is_null($statusKepegawaian)) {			
			return array("pass" => false, "reason" => "Status kepegawaian belum diisi");
		}
		//$jp = StatusKepegawaianPeer::retrieveByPK($statusKepegawaian);
		$jp = $arrSK[$statusKepegawaian];
		
		if (!is_object($jp)) {
			return array("pass" => false, "reason" => "Status kepegawaian tidak valid");
		}
		return array("pass" => true, "reason" => "Status kepegawaian dinilai valid");
	}
	
	function cekPangkatGolonganId($tptk) {
		global $arrJP, $arrPG, $arrSK;
		$pangkatGolongan = $tptk->getPangkatGolonganId();
		if ($pangkatGolongan == "" || is_null($pangkatGolongan)) {			
			return array("pass" => false, "reason" => "Pangkat/Golongan belum diisi");
		}
		//$jp = PangkatGolonganPeer::retrieveByPK($pangkatGolongan);
		$jp = $arrPG[$pangkatGolongan];
		
		if (!is_object($jp)) {
			return array("pass" => false, "reason" => "Pangkat/Golongan tidak valid");
		}
		return array("pass" => true, "reason" => "Pangkat/Golongan dinilai valid");
	}
	
	function cekTmtPangkatGol($tptk) {
		
		//$tptk = new TPtk();	 		
		$tmtPangkatGol = $tptk->getTmtPangkatGol();		
		if ($tmtPangkatGol == "" || is_null($tmtPangkatGol)) {			
			return array("pass" => false, "reason" => "Tgl TMT Pangkat belum diisi");
		}
		return array("pass" => true, "reason" => "Tgl TMT Pangkat dinilai valid");
	}
	
	function cekStatus($tptk) {
		
		$status = $tptk->getStatus();
		if ($status == "" || is_null($status)) {			
			return array("pass" => false, "reason" => "Status PTK belum diisi");
		}
		return array("pass" => true, "reason" => "Status PTK dinilai valid");
	}
	
	function cekTugasTambahan($tptk) {
		//$tptk = new TPtkNew();
		$status = $tptk->getTugasTambahanId();
		if ($status == "" || is_null($status)) {			
			return array("pass" => false, "reason" => "Tugas tambahan kosong");
		}
		return array("pass" => true, "reason" => "Tugas tambahan dinilai valid");		
	}
	
	function cekJjmSesuai($tptk) {
		//$tptk = new TPtkNew();
		//$tptk->getTotalJamMengajarSesuai();
		//$tptk->getJumlahJamMengajarSesuaiKtsp() + $tptk->getJamTugasTambahan();
		//$jjmSesuai = $tptk->getJumlahJamMengajarSesuai();
		$jjmSesuai = $tptk->getTotalJamMengajarSesuai();
		
		if ($jjmSesuai == "" || is_null($jjmSesuai)) {			
			return array("pass" => false, "reason" => "JJM Sesuai kosong, data mengajar linear belum terisi");
		}
		if ($tptk->getBertugasDiWilayahTerpencil() && (($jjmSesuai > 0 && $jjmSesuai < 6))) {
			return array("pass" => false, "reason" => "JJM Sesuai (bertugas di wil.khusus) masih kurang dari 6 jam.");
		}
		if ($tptk->getBertugasDiWilayahTerpencil() && ($jjmSesuai >= 6)) {
			return array("pass" => true, "reason" => "JJM Sesuai (bertugas di wil.khusus) cukup (>= 6)");
		}		
		if ($jjmSesuai > 0 && $jjmSesuai < 24) {
			return array("pass" => false, "reason" => "JJM Sesuai masih kurang dari 24 jam.");
		}
		if ($jjmSesuai >= 24) {
			return array("pass" => true, "reason" => "JJM Sesuai cukup (>= 24)");
		}			
	}
	
	function cekJjmSesuaiKtsp($tptk) {
		//$tptk = new TPtk();		
		//$jjmSesuai = $tptk->getJumlahJamMengajarSesuaiKtsp();
		$jjmSesuai = $tptk->getJumlahJamMengajarSesuaiKtsp() + $tptk->getJamTugasTambahan();
		
		if ($jjmSesuai == "" || is_null($jjmSesuai)) {			
			return array("pass" => false, "reason" => "JJM Sesuai (dihitung dgn acuan ktsp) kosong, data mengajar linear belum terisi");
		}
		if ($jjmSesuai > 0 && $jjmSesuai < 24) {
			return array("pass" => false, "reason" => "JJM Sesuai (dihitung dgn acuan ktsp) masih kurang dari 24 jam.");
		}
		if ($jjmSesuai >= 24) {
			return array("pass" => true, "reason" => "JJM Sesuai cukup (>= 24)");
		}			
		
	}
	
	function cekMasaKerja($tptk) {
		//$tptk = new TPtk();
		
		$masaKerjaTahun= $tptk->getMasaKerjaTahun();
		$masaKerjaBulan = $tptk->getMasaKerjaBulan();
		
		if (($masaKerjaTahun || $masaKerjaBulan)){			
			return array("pass" => true, "reason" => "Masa kerja ada");			
		} else {
			return array("pass" => false, "reason" => "Masa kerja kosong");
		}		
		
	}
	
	function cekTglPensiun($tptk) {
		
		//$tptk = new TPtk();
		 		
		$tglPensiun = $tptk->getTglPensiun();
		
		if ($tglPensiun == "" || is_null($tglPensiun)) {			
			return array("pass" => false, "reason" => "Tgl pensiun kosong");
		}
		return array("pass" => true, "reason" => "Tgl pensiun kosong dinilai valid");
	}
	
	function cekSedangKuliah($tptk) {
		return array("pass" => false, "reason" => "Data sedang kuliah kosong");
	}
	
	function cekBertugasDiWilayahKhusus($tptk) {
		//$tptk = new TPtkNew();
		if ($tptk->getBertugasDiWilayahTerpencil()) {
			return array("pass" => true, "reason" => "Data bertugas di wilayah khusus valid");
		} else {
			return array("pass" => false, "reason" => "Data bertugas di wilayah khusus kosong");
		}	
	}
	
	function cekEmail($tptk) {
		$email = $tptk->getEmail();
		if (!filter_var( $email, FILTER_VALIDATE_EMAIL )) {
			return array("pass" => false, "reason" => "Alamat email kosong/tidak valid");
		} else {
			return array("pass" => true, "reason" => "Alamat email dinilai valid");
		}
	}
	
	/* Syarat T.Profesi */
	function cekStatusDapodik($tptk) {
		//$tptk = new TPtk();
		$status = $tptk->getStatus();
		if (!$status) {
			return array("pass" => false, "empty" => true, "reason" => "Empty value");
		}		
		
		if ($status == 1) {
			return array("pass" => true, "reason" => "Status ptk di sekolah tsb aktif.");
		} else {
			return array("pass" => false, "reason" => "Status ptk di sekolah tsb tidak aktif.");
		}			
	}
	
	/* Syarat Sertifikasi */
	function cekSertifikasi($tptk) {
		
		//$tptk = new TPtk();
		$nrg = $tptk->getNrg();
		//$bs = $tptk->getBidangStudiSertifikasiId();
		$bs = $tptk->getKodeBidangStudiSertifikasi();
		
		if (!$bs) {
			return array("pass" => false, "empty" => true, "reason" => "Empty value");
		}
		
		$reasonStr = "";
		
		if (!$nrg) {
			$noNrg = true;
			$reasonStr .= "NRG tidak ditemukan";	
		} 
		if (!$bs) {
			$noBs = true;
			if ($noNrg) {
				$reasonStr .= ", ";
			}
			$reasonStr .= "Bid.Studi Sertifikasi tidak ditemukan";
		}
		if ($noNrg || $noBs) {			
			return array("pass" => false, "reason" => $reasonStr);	
		} else {
			return array("pass" => true, "reason" => "Sertifikasi valid");
		}
		
	}
	

	
	function cekPensiunPadaTahunAjaran($tptk) {
		
		//$tptk = new TPtk();
		$tglPensiun = $tptk->getTglPensiun("Y-m-d");
		if (!$tglPensiun) {
			return array("pass" => false, "empty" => true, "reason" => "Empty value");
		}		
		
		$dateYear = date("Y");
		$dateMonth = date("n");
		
		// If today month is November, pension limit is June next year. 
		// 		tapi yang pensiun maret masih boleh ngajar sampai desember. 
		// If today month is March, pension limit is December THIS YEAR,  
		if ($dateMonth <= 6) {
			$tglBatas = $dateYear."-12-31";
			$tglBatasBolehLanjutMengajar =  $dateYear."-06-31";
		} else {
			$tglBatas = ($dateYear + 1)."-06-31";
			$tglBatasBolehLanjutMengajar =  $dateYear."-12-31";
		}
		
		//$timePensiun = strtotime($tglPensiun);
		//$timeBatas = strtotime($tglBatas);
		//$timeBatasBolehLanjutMengajar = strtotime($tglBatasBolehLanjutMengajar);
		
		$timePensiun = new DateTime($tglPensiun);
		$timeBatas = new DateTime($tglBatas);
		$timeBatasBolehLanjutMengajar = new DateTime($tglBatasBolehLanjutMengajar);
		
		//if ($timePensiun >= $timeBatas) {
		if (selisih($timePensiun,$timeBatas) >= 0) {
			return array("pass" => true, "reason" => "Tgl pensiun ($tglPensiun) belum melewati batas ($tglBatas).");
		}
		//if ($timePensiun < $timeBatas) {
		if (selisih($timePensiun,$timeBatas) < 0) {				
			//if ($timePensiun > $timeBatasBolehLanjutMengajar ) {
			if (selisih($timePensiun,$timeBatasBolehLanjutMengajar) > 0) {
				return array("pass" => true, "reason" => "Tgl pensiun ($tglPensiun) melewati batas pensiun ($tglBatas), namun masih boleh mengajar sampai tgl batas tsb.");
			} else {
				return array("pass" => false, "reason" => "Tgl pensiun ($tglPensiun) melewati batas pensiun semester ($tglBatas)");
			}
		}
		
	}
	
	function cekPnsAtauGty($tptk) {
		//$tptk = new TPtk();
		$s = $tptk->getStatusKepegawaianId();
		if (!$s) {
			return array("pass" => false, "empty" => true, "reason" => "Empty value");
		}		
		
		$sNama = $tptk->getNamaStatusKepegawaian();
		
		if ($s <= 4) {
			return array("pass" => true, "reason" => "Status kepegawaian ($sNama) memenuhi syarat");
		} else {
			return array("pass" => false, "reason" => "Status kepegawaian ($sNama) tidak memenuhi syarat");
		}
	}	
	
	//cekJjmSesuai sudah ada	 
}
?>