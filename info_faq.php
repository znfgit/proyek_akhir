<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Pengecekan Data Guru</title>
<link href="login-box.css" rel="stylesheet" type="text/css" />
<style>
h2 {
	background: url('/assets/images/small-unsharp-flat.png') no-repeat;
	text-indent: 60px;
}
</style>
</head>
<body>
<div style="padding: 10px 0 0 0px;" align="center">

<div class="login-box-wide">
<div class="content" align="left">

<b>Frequently Asked Question</b><br>
<ul>
<li>
<b>Pertanyaan</b>: "Kenapa Data "Total JJM Sesuai" saya tidak sama dengan yang diinput via Dapodik ??" <br>
<b>Jawaban</b>: Berikut penjelasannya: <br>(1) Data yg kami ambil menggunakan data input semester 1 tahun 2012 <br>(2) Data JJM ada 3 jenis
JJM (inputan sekolah), JJM KTSP (JJM terinput dgn batasan maksimal KTSP), dan JJM Sesuai/Linier (JJM yg dibatasi KTSP, yang sesuai dengan sertifikasinya)<br>
3) Contoh perhitungan: Jika seorang Guru mengajar pelajaran Guru Kelas SD (kelas 1) diinput mengajar 30 jam, menurut KTSP 26 jam. Maka JJMnya dianggap 26 jam, 
jika sertifikasinya Guru SD. Jika belum sertifikasi atau sertifikasinya selain Guru Kelas SD, maka JJM liniernya dianggap 0.
</li>
<br>
<li>
<b>Pertanyaan</b>: "Tolong update data saya, berikut filenya (word/excel/backup aplikasi dapodik)." <br>
<b>Jawaban</b>: Mohon update data anda langsung melalui Aplikasi Dapodik. Hubungi Kepala Sekolah/ Operator Dapodik / KK Datadik Kab/Kota anda.
Jika data telah dikirim, tunggu beberapa waktu sampai proses transfer data selesai. 
Jika pengiriman data dari aplikasi dapodik berhasil, data di web akan berubah dalam beberapa waktu sebagaimana telah terjadi pada guru2 lainnya. 
Akan percuma jika kami update data anda di sistem kami karena nanti akan tertimpa lagi oleh data dari dapodik.
</li>
<br>
<li id="kode-sertifikasi">
<b>Pertanyaan</b>: "Saya sudah input data kode sertifikasi, mengapa masih invalid di nomer 17 ?"<br>
<b>Jawaban</b>: Sesuai kebijakan, data sertifikasi kami ambil dari basis data Kelulusan Sertifikasi 
yang diterbitkan Pusbangprodik BPSDMP & PMP. Kode bidang studi yang ditampilkan diambil dari 
Nomor Peserta Kelulusan Sertifikasi (digit ke 7, 8 , 9). Kode invalid dapat terjadi jika : 
1) Pengisian Kode NUPTK di Dapodik belum benar. 2) Ada kesalahan NUPTK atau Nomor Peserta di data Kelulusan 
Solusinya adalah memperbaiki NUPTK di data Dapodik, atau memperbaiki Data Kelulusan melalui operator 
Aplikasi Tunjangan P2TK Dikdas di Dinas Pendidikan Kab/Kota setempat.


</li>
<br>
<li>
<b>Pertanyaan</b>: "Saya tidak bisa login katanya salah password" <br>
<b>Jawaban</b>: Isi password dengan format YYYYMMDD, contoh: jika tgl lahir anda 12 April 1963 passwordnya: 19630312.  
Jika masih salah, berarti data tgl lahir anda salah input di dapodik ada yang terbalik misal tgl diatas menjadi 19631203
atau masih kosong dsb. Silakan cek ke operator dapodik bapak/ibu.
</li>
<br>
<li>
<b>Pertanyaan</b>: "Saya tidak bisa login katanya NUPTK tidak ditemukan" <br>
<b>Jawaban</b>: Data dgn NUPTK tsb belum kemungkinan masuk database, atau salah input. Mohon update data anda melalui Aplikasi Dapodik. 
Hubungi kepala sekolah/ operator aplikasi dapodik /  KK Datadik Kab/Kota anda. Jika data telah dikirim, tunggu beberapa waktu 
sampai proses transfer data selesai. Jika pengiriman data dari aplikasi dapodik berhasil, data 
anda akan dapat login dengan sendirinya. 
</li>
<br>
<li>
<b>Pertanyaan</b>: "Jumlah Jam mengajar saya kosong terus/salah" <br>
<b>Jawaban</b>: Data mengajar ada di modul rombel di Aplikasi Pendataan. Mohon update data tsb. Khusus untuk kepala sekolah, diambil data
jam tugas tambahan dari jabatan PTK di kolom jabatan. Untuk jabatan lainnya sementara ini kami ambil dari daftar PTK Terdaftar, yang muncul pada
saat penambahan PTK di kolom tugas ptk.
</li>
<br>
<li>
<b>Pertanyaan</b>: "Saya sudah update data via dapodik tapi belum masuk2 di cek guru" <br>
<b>Jawaban</b>: Ada beberapa server & proses yang dilalui oleh data sebelum masuk database PTK. 
Jika data telah dikirim, mohon tunggu beberapa waktu sampai proses transfer data selesai.
Jika pengiriman data dari aplikasi dapodik berhasil, data di web akan berubah dalam beberapa waktu sebagaimana telah terjadi pada guru2 lainnya. 
</li>
<br>
<li>
<b>Pertanyaan</b>: "Masa kerja saya masih salah, sehingga gaji pokok juga salah" <br>
<b>Jawaban</b>: Karena keterbatasan formulir Dapodik, data SK berkala dan data MKG tidak bisa kami dapatkan sehingga kami menghitung dari 
TMT Pengangkatan dan/atau TMT PNS. Tentu saja ini belum memperhitungkan faktor honorer/CPNS dll ataupun sistim perhitungan masa kerja segaris. 
Ke depan kami akan bekerjasama dengan BKN sehingga dapat diperoleh data yang paling akurat. 
</li>
<br>
<li>
<b>Pertanyaan</b>: "Saya belum punya NUPTK bagaimana mendapatkannya" <br>
<b>Jawaban</b>: Sistem NUPTK saat ini dikelola oleh BPSDMP dan PMP
</li>
<br>
<li>
<b>Pertanyaan</b>: "Sekolah saya ingin terdaftar Dapodik bagaimana caranya?" <br>
<b>Jawaban</b>: Coba hubungi KK Datadik kab/kota anda untuk bantuan.
</li>
</ul>
</div>

</div>
</body>
</html>