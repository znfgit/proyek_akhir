@echo off
del /Q /S /F build
del /Q /S /F build\classes\sppsai\map
del /Q /S /F build\classes\sppsai\om
del schema.xml
del serverdapodik-schema.xml
del sqlserver-schema.xml
del buildtime-conf.xml
del runtime-conf.xml
del build.properties
copy config-singleserver\build.compile.properties build.properties
copy config-singleserver\runtime-conf.xml runtime-conf.xml
copy config-singleserver\schema-design.xml schema.xml
propel-gen 
