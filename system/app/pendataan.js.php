Ext.namespace("<?=APPNAME?>");

<?=APPNAME?>.App = new Xond.Application ({

	init : function(){
		this.addEvents({
			'create' : true
		});
		
	},
	
	beranda: new Ext.ux.Portal({			
		//region:'center',
		title:'Informasi',
		margins:'35 5 5 0',
		items:[{
			columnWidth:.50,
			style:'padding:10px 0 10px 10px',
			items:[{
				title: 'Selamat Datang',
				layout:'fit',
				movable: false,
				//tools: tools,
				html: '<img src="/assets/images/sim-rasio-logo.png"/>'
			},{
				title: 'Informasi',
				//tools: tools,
				html: '<div style="padding:10px 10px 10px 10px">Selamat datang di Aplikasi SIM Rasio PTK Dikdas.'
				  + 'Aplikasi ini melayani Manajemen & Monitoring Rasio Pendidik dan Tenaga Kependidikan '
				  + 'di lingkungan Ditjen Pendidikan Dasar. '
				  + '</div>'
			},{
				title: 'Berita',
				//tools: tools,
				html: '<div style="padding:10px 10px 10px 10px"><ul><li>Ujicoba akan dilaksanakan di 33 propinsi</li>'
				  + '<li>Target pelaksanaan pada september/oktober</li>'				  
			}]
		}]
	}),
	
	registerModules : function() {
		return [
			<?=$modcache?>			
		]
	}	
	
});