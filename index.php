<?php
/*
 * Aplikasi Pendataan Pendidikan Dasar 2011
 * 
 */

// print_r($_REQUEST); die();
require('startup.php');

if (!isset($_REQUEST["m"])) {
	header('Location: /login.php');
}

//Auth//
$auth = new XondAuth();
$auth->setAuthObject('Pengguna');
$auth->setUserColumn('Username');
$auth->setPasswordColumn('Password');
$auth->setRedirectUrl("/default.php");
$auth->addGroupMembership('PeranId');
//$auth->setTheme('xtheme-gray');
$auth->setTheme('xtheme-access');
//$auth->setAuthFace('dark');
//$auth->registerUi('examples',$auth->SYSDIR);

$config = array();
$config["custom_app_ui"] = true;

$app = new XondApplication($config);
//$app->registerNoAuthModule('PencetakanSpp');
$app->registerNoAuthModule('Validasi');
$app->registerNoAuthModule('Report');
$app->registerNoAuthModule('PerhitunganRasio');
$app->registerNoAuthAction('process');
$app->registerNoAuthAction('print');
$app->registerNoAuthAction('register');
$app->registerNoAuthAction('registerMobile');
$app->registerNoAuthAction('TunjanganProfesi');
$app->registerNoAuthAction('sendAprovalLinkEmail');
$app->registerParentMenu('Basis Data', 1);

/*$app->registerParentMenu('Basis Data Referensi', 2);
$app->registerParentMenu('Manajemen Pelaksanaan', 1);
$app->registerParentMenu('Monitoring Pelaksanaan', 2);
$app->registerParentMenu('Capacity Assessment', 3);
$app->registerParentMenu('Database Stakeholder', 4);
$app->registerParentMenu('Pelaksana', 5);
$app->registerParentMenu('Module ', 5);
*/
//print_r($auth);

if ($app->setAuth($auth))  {
	if ($app->getAuth()->getSession()) {
		$userPeranId = $app->getAuth()->getUser()->getPeranId();
		
		if ($userPeranId == 1) {
			//$app->registerModule('Progres');
			$app->registerModule('Lembaga');	
			$app->registerModule('Pengguna');
			//$app->registerModule('Tim');
			//$app->registerModule('Surveyor');
			$app->registerModule('Monitoring');
			$app->registerModule('Sekolah');	
			$app->registerModule('PerhitunganRasio');
			//$app->registerModule('PesertaDidik');	
			//$app->registerModule('Ptk');
			//$app->registerModule('RegisterPengiriman');	
		} else if ($userPeranId == 8 || $userPeranId == 9) {
			//$app->registerModule('Progres');
			$app->registerModule('Lembaga');
			$app->registerModule('Pengguna');
			//$app->registerModule('Tim');
			//$app->registerModule('Surveyor');
			//$app->registerModule('Monitoring');
			$app->registerModule('Sekolah');	
			//$app->registerModule('PesertaDidik');	
			//$app->registerModule('Ptk');
			//$app->registerModule('RegisterPengiriman');	
		} else if ($userPeranId == 10) {
			//$app->registerModule('Progres');
			//$app->registerModule('Lembaga');	
			//$app->registerModule('Pengguna');
			//$app->registerModule('Tim');
			//$app->registerModule('Surveyor');
			//$app->registerModule('Monitoring');
			$app->registerModule('Sekolah');
			//$app->registerModule('PesertaDidik');	
			//$app->registerModule('Ptk');
			//x$app->registerModule('RegisterPengiriman');	
		} else {
			$app->registerModule('Sekolah');
		}
		
	}
	
	//$app->registerModule('Worst1000smp');
	//$app->registerModule('Worst1000smu');
	//$app->registerModule('Worst1000smk');
	//$app->registerModule('ManajemenPelaksanaan');
	//$app->registerModule('MonitoringPelaksanaan');
	//$app->registerModule('CapacityAssessment');	
	//$app->registerModule('Pelaksana');
	//$app->registerModule('SekolahSmp');	
	//$app->registerModule('SekolahSmu');	
	//$app->registerModule('SekolahSmk');	
	//$app->registerModule('Rayonbysmp');
	//$app->registerModule('Rayonbysmu');
	//$app->registerModule('Rayonbysmk');
	//$app->registerModule('DayaTampung');	
	if ($app->getAuth()->getSession()) {
		$user = $app->getAuth()->getUser();	
		//print_r($user);
		$kabupatenKota = KabupatenKotaPeer::retrieveByPk($user->getKabupatenKotaId());
		$app->setUserInfo(array(
			'Id' => $user->getPenggunaId(),
			'Nama Akun' => $user->getUsername(),
			'Peran' => $user->getPeran()->getNama(),
			'Password' => str_repeat('*', strlen($user->getPassword())), 
			'Nama' => $user->getNama(),
			'Kabupaten/Kota' => ($kabupatenKota) ? $kabupatenKota->getNama() : "Administrator",
		));
	}
}

$libDir = $app->LIBDIR;
$sysDir = $app->SYSDIR; 
$cssDir = $app->CSSDIR;

$app->setTheme('xtheme-gray');
$app->registerStyle('Portal',$libDir);
$app->registerStyle('GridSummary',$libDir);
$app->registerStyle('ProgressColumn',$libDir);
$app->registerStyle('file-upload',$libDir);
$app->registerStyle('grid-examples',$libDir);
$app->registerStyle('summary',$libDir);
$app->registerStyle('pendataan',$sysDir);
$app->registerStyle('organizer',$sysDir);
//$app->registerStyle('TreeGrid',$libDir);
//$app->registerStyle('TreeGridLevels',$libDir);
$app->registerStyle('TreeGrid',$cssDir);
$app->registerStyle('TreeGridLevels',$cssDir);
$app->registerUi('Ext.ux.GMapPanel3',$libDir);
$app->registerUi('Portal',$libDir);
$app->registerUi('PortalColumn',$libDir);
$app->registerUi('Portlet',$libDir);
$app->registerUi('FileUploadField',$libDir);
$app->registerUi('GridSummary',$libDir);
$app->registerUi('ProgressColumn',$libDir);
$app->registerUi('Ext.ux.GMapPanel3',$libDir);
$app->registerUi('groupcombo',$libDir);
$app->registerUi('groupdataview',$libDir);
$app->registerUi('GroupSummary',$libDir);
$app->registerUi('example',$sysDir);
$app->registerUi('pendataan',$sysDir);
$app->registerUi('uxfusionpak',$libDir);
$app->registerUi('ux-all-debug',$libDir);
//$app->registerUi('uxmedia-min',$libDir);
//$app->registerUi('uxflash-min',$libDir);
//$app->registerUi('uxfusion',$libDir);
$app->registerUi('Ext.tree.TreeSerializer', $libDir);
$app->registerUi('TreeGrid',$libDir);

$app->registerStyle('default',$libDir);
$app->registerUi('library',$libDir);

$app->setDevelStatus(true);
$app->start();
$app->render();

//echo "m = {$_GET['m']}, a = {$_GET['a']}, t = {$_GET['t']}	"; die;



?>