<style>
td {
	font-family: Arial Narrow;
	font-size: 10px;
	border-left: thin solid #000000;
	border-top: thin solid #000000;
	padding-left: 3px;
}
</style>
<?php

	include 'startup.php';
	//phpinfo(); die;
	//error_reporting(E_ALL);
	global $filesAllowedToBeDeletedKabkota, $filesAllowedToBeDeletedPropinsi, $setLimit;
	//$setLimit = 200;
	$setLimit = false;
	
	function microtime_float()
	{
	    list($usec, $sec) = explode(" ", microtime());
	    return ((float)$usec + (float)$sec);
	}
	
	function check_zero($var) {
		return ($var === 0);
	}
	
	function getSertifikasi($p) {
		
		global $arrSekolah, $arrPtk, $arrPtkTerdaftar, $arrMengajar, $arrTerdaftar, $arrNrg, $arrBidangStudi, $arrPdt, $con;

		$nrgObj = $arrNrg[$p->getNuptk()];
		//$nrgObj = new Nrg();		
		
		if (is_object($nrgObj)) {
			$namaGuruNrg = $nrgObj->getNrgNama();
			$tglLahirNrg = $nrgObj->getNrgTgllahir();
			
			$bsNrg = $nrgObj->getNrgNuptk();			
			$bsKode = substr($nrgObj->getNrgNopeserta(), 6, 3);			 
			$bsObj = $arrBidangStudi[$bsKode];
			
			if (!$bsObj) {
				$bsKodeBaru = $arrMBS[$bsKode]; 				
				$bsObj = $arrBidangStudi[$bsKodeBaru];
			}		
			
			$bsId = is_object($bsObj) ? $bsObj->getPrimaryKey() : NULL;
			$bsNama = is_object($bsObj) ? $bsObj->getNama() : NULL;
			$bsTgl = $nrgObj->getNrgTahunlulus() ? $nrgObj->getNrgTahunlulus() : NULL; 
			unset($arrNrg[$p->getNuptk()]);
		} else {
			$bsNrg = NULL;
			$bsKode = NULL;
			$bsId = NULL;
			$bsNama = NULL;
			$bsTgl = NULL;
		}
		
		return array("nama_guru_nrg" => $namaGuruNrg, "tgl_lahir_nrg" => $tglLahirNrg, "nrg" => $bsNrg, "kode_bidang_studi_sertifikasi" => $bsKode, "nama_bidang_studi_sertifikasi" => $bsNama, "bidang_studi_sertifikasi_id" => $bsId, "tgl_lulus_sertifikasi" => $bsTgl); 
	}
	
	function getJumlahJamMengajar($p) {
		
		global $arrSekolah, $arrPtk, $arrPtkTerdaftar, $arrMengajar, $arrTerdaftar, $arrBidangStudi, $arrMBS, $arrKtsp, $arrPdt, $con, $counter;
		//$p = new TPtk();
		
		$bsId = $p->getBidangStudiSertifikasiId();
		$nuptk = $p->getNuptk();
		$mengajars = $arrMengajar[$nuptk];
		$jjmSesuai = 0;
		$jjm = 0;
		$jjmSesuaiKtsp = 0;
		
		//echo "( mengajar ";
		foreach ($mengajars as $m) {
			
			//$m = new Mengajar();		
			//$tMengajar = new TMengajar();
			//$tMengajar->setBidangStudiId();
			//echo "{$m->getBidangStudiId()} = {$arrKtsp[$m->getBidangStudiId()]} jam, ";
			
			$jjm += $m->getJamMengajarPerMinggu();
			
			if ($bsId > 0) {
				
				if ($m->getBidangStudiId() == $bsId) {
					$jjmSesuai += $m->getJamMengajarPerMinggu();
					$jjmSesuaiKtsp += $arrKtsp[$m->getBidangStudiId()];					
				}
				
			} else {
				$jjmSesuai += $m->getJamMengajarPerMinggu();
				$jjmSesuaiKtsp += $arrKtsp[$m->getBidangStudiId()];
			}
			
			
			//$countPdt = sizeof($arrPdt[$m->getRombonganBelajarId()]);
			//$pdtObj = $arrPdt[$m->getRombonganBelajarId()][0];
			
			//$pdtObj = new PesertaDidikTerdaftar();
			//$namaRombel = $pdtObj->getRombonganBelajar()->getNama();			
			/*
			try {
				
				$tMengajar = new TMengajar();
				$tMengajar->setTMengajarId($m->getMengajarId());
				$tMengajar->setTPtkId($p->getTPtkId());
				$tMengajar->setTPtkId($p->getTPtkId());
				$tMengajar->setTahunAjaranId($m->getTahunAjaranId());
				$tMengajar->setSemester($m->getSemester());
				
				//$tMengajar->setNamaRombonganBelajar($namaRombel);
				//$tMengajar->setRombonganBelajarId($m->getRombonganBelajarId());
				//$tMengajar->setRasioSiswaGuru($countPdt);			
				
				if (is_object($m->getBidangStudi())) {
					$tMengajar->setNamaBidangStudi($m->getBidangStudi()->getNama());
					$tMengajar->setBidangStudiId($m->getBidangStudiId());			
					$tMengajar->setJamMengajarPerMinggu($m->getJamMengajarPerMinggu());
					$tMengajar->setJamMengajarPerMingguMenurutKtsp($m->getBidangStudiId());
					$tMengajar->save($con);
				} else {					
					//print_r($m);
					continue;
				}
				
				$counter++;
				
			
			} catch (Exception $e) {

				//print_r($m);
				continue;
				
			}
			*/
			
			
		}
		//echo " )";
		//echo " $bsId = $jjmSesuaiKtsp jam )";
		return array("jjm" => $jjm, "jjm_sesuai" => $jjmSesuai, "jjm_sesuai_ktsp" => $jjmSesuaiKtsp);
	}
	
	
	function getTugasTambahan($p) {
		
		global $arrSekolah, $arrPtk, $arrPtkTerdaftar, $arrMengajar, $arrTerdaftar, $arrBidangStudi, $arrMBS, $arrKtsp, $arrPdt, $arrTugasPtk, $con;
		
		$terdaftars = $arrTerdaftar[$p->getTPtkId()];
		//print_r($terdaftars); die;
		
		$tugasTambahanId = NULL;
		$namaTugasTambahan = NULL;
		$jamTugasTambahan  = NULL;				
		
		foreach ($terdaftars as $t) {
			
			if (is_object($t)) {				
				
				$tugasPtk = $arrTugasPtk[$t->getTugasPtkId()];				
				//$tugasPtk = new TugasPtk();
				
				if (is_object($tugasPtk)) {				
					
					if ($tugasPtk->getTugasPtkId() != 4) {
						$tugasTambahanId = $tugasPtk->getTugasPtkId();
						$namaTugasTambahan = $tugasPtk->getNama();
						$jamTugasTambahan  = $tugasPtk->getJamDiakui();						
					}
				}
			}			
		
		}
		
		return array("tugas_tambahan_id" => $tugasTambahanId, "nama_tugas_tambahan" => $namaTugasTambahan, "jam_tugas_tambahan" => $jamTugasTambahan);
		
	}
	
	function getMasaKerja($ptk) {
		
		// Calculate Masa Kerja
		$mulaiMengajar = ($ptk->getTmtPengangkatan()) ? $ptk->getTmtPengangkatan() : $ptk->getTmtPns();
		if (!$mulaiMengajar) {
			$masaKerjaTahun = 0;
			$masaKerjaBulan = 0;
		} else {
			$hariIni = date("Y-m-d H:i:s");
			$masaKerjaTahun = datediff("yyyy", $mulaiMengajar, $hariIni, false);
			
			// Calculate Bulan
			$pasSekianTahunSejakMulaiMengajar = date("Y-m-d H:i:s", strtotime("+$masaKerjaTahun year", strtotime($mulaiMengajar)));
			$masaKerjaBulan = datediff("m", $pasSekianTahunSejakMulaiMengajar, $hariIni, false);
			
			$masaKerja = $masaKerjaTahun + $masaKerjaBulan/100;
		}
		return array("masa_kerja_tahun" => $masaKerjaTahun, "masa_kerja_bulan" => $masaKerjaBulan);		
	}
	
	function getTglPensiun($tPtk) {
				
		$tglLahir = $tPtk->getTglLahir("Y-m-d");
		$tglLahir = ($tglLahir == "1970-01-01") ? NULL : $tglLahir;			 
		$tglPensiun = date("Y-m-d", strtotime("+60 year", strtotime($tglLahir)));
		$tglPensiun = ($tglPensiun == "1970-01-01") ? NULL : $tglPensiun;
		return $tglLahir ? $tglPensiun : NULL;					
	}
	
	function getBertugasDiWilayahTerpencil($tPtk) {
		
		global $arrSekolah;
		
		/*
		$sekolah = $arrSekolah[$tPtk->getSekolahId()];
		if (is_object($sekolah)) {				
			if (($sekolah->getWilayahAdatTerpencil()==1) || ($sekolah->getWilayahBencanaAlam()==1) || ($sekolah->getWilayahBencanaSosial()==1) || ($sekolah->getWilayahDaruratLainnya()==1)) {
				return 1;
			} else {
				return NULL;
			}	
		} else {
			return NULL;
		}
		*/
		
		$sekolah = $arrSekolah[$tPtk->getSekolahId()];
		if (($sekolah)) {							
			
			if (($sekolah["wilayah_adat_terpencil"]==1) || ($sekolah["wilayah_bencana_alam"]==1) || ($sekolah["wilayah_bencana_sosial"] ==1) || ($sekolah["wilayah_darurat_lainnya"]==1)) {
				return 1;
			} else if ($sekolah["status_daerah_khusus"]) {
				return 1;
			} else {
				return NULL;
			}
				
		} else {
			return NULL;
		}
		
	}
			
	function main($kabKotaId, $date=false) {
		
		//removeKabkotaKonversiFile($kabKotaId);
		//die;
		
		$time_start = microtime_float();
		//$kabKotaId = 49;
		//$kabKotaId = 146;
		
		$kabKotaObj = KabupatenKotaPeer::retrieveByPK($kabKotaId);
		$kabKotaNama = $kabKotaObj->getNama();
		
		$propinsiObj = $kabKotaObj->getPropinsi();
		
		echo "<br><b>Mengerjakan ($kabKotaId) $kabKotaNama</b><br>";
		
		global $arrSekolah, $arrPtk, $arrPtkTerdaftar, $arrMengajar, $arrTugasPtk, $arrTerdaftar, $arrNrg, $arrBidangStudi, $arrMBS, $arrKtsp, $arrPdt, $con, $counter;
		
		//reset all vars
		$arrSekolah = $arrPtk = $arrPtkTerdaftar = $arrMengajar = $arrTugasPtk = $arrTerdaftar = $arrNrg = $arrBidangStudi = $arrKtsp = $arrPdt = $con = $counter = NULL;
		
		global $filesAllowedToBeDeletedKabkota, $filesAllowedToBeDeletedPropinsi;
		
		if (!writeKabkotaKonversiFile($kabKotaId, -1, 0)){
			return false;
		} else {
			$filesAllowedToBeDeletedKabkota[] = $kabKotaId;
		}
				
		/* Get Data Sekolah */
		
		// Versi Mpud //
		/*
		$c = new Criteria();
		$c->add(SekolahPeer::KABUPATEN_KOTA_ID, $kabKotaId);
		$a = $c->getNewCriterion(SekolahPeer::FLAG, 2, Criteria::LESS_EQUAL);
		$b = $c->getNewCriterion(SekolahPeer::FLAG, NULL, Criteria::ISNULL);
		$a->addOr($b);
		$c->add($a);
		$sekolahs = SekolahPeer::doSelect($c);
		foreach ($sekolahs as $s) {
			$arrSekolah[$s->getSekolahId()] = $s;	
		}
		*/
		
		$sekolahs = getDataBySql("
			SELECT b.sekolah_id, a.* , max( b.timestamp ) AS last_update
			FROM register_pengiriman b
			LEFT JOIN sekolah a ON a.sekolah_id = b.sekolah_id			
			WHERE a.kabupaten_kota_id = $kabKotaId
			AND (a.flag <= 2 OR a.flag is null)
			GROUP BY b.sekolah_id
		", FALSE, PtkPeer::DATABASE_NAME);
		
		foreach ($sekolahs as $s) {
			//$arrSekolah[$s->getSekolahId()] = $s;
			$arrSekolah[$s["sekolah_id"]] = $s;	
		}
		
		
		/* 
		$c = new Criteria();
		$c->add(SekolahPeer::KABUPATEN_KOTA_ID, $kabKotaId);
		$sekolahs = SekolahPeer::doSelect($c);
		foreach ($sekolahs as $s) {
			$arrSekolah[$s->getSekolahId()] = $s;	
		} Get Data Sekolah */
		
		////////////////////////
		////////////////////////
		/* Get Data PTK */
		////////////////////////
		////////////////////////
		$c = new Criteria();
		$c->addJoin(PtkPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID);
		$c->addJoin(PtkPeer::SEKOLAH_ID, TLastUpdatePeer::SEKOLAH_ID);
		$c->add(SekolahPeer::KABUPATEN_KOTA_ID, $kabKotaId);
		$c->add(PtkPeer::STATUS_DATA, NULL, Criteria::ISNULL);
		if ($date) {
			$c->add(TLastUpdatePeer::LAST_UPDATE, $date, Criteria::GREATER_EQUAL);
		}
		$c->addAscendingOrderByColumn(PtkPeer::NUPTK);
		
		if ($setLimit) {
			$c->setLimit($setLimit);
		}
		
		try {
			$ptks = PtkPeer::doSelect($c);
			if ($date) {
				echo "\n<br>Sejak tanggal $date, ditemukan ".sizeof($ptks)." ptk yang terupdate<br>\n";
			} else {
				echo "\n<br>Ditemukan ".sizeof($ptks)." yang akan diupdate<br>\n";
			}
		} catch (Exception $e) {
			print_r($e);die;
		}
		//$ptks = PtkPeer::doSelectJoin 
		
		foreach ($ptks as $p) {
			$arrPtk[$p->getPtkId()] = $p;
		}
		
		
		/* Get Data PTK Terdaftar 
		$c = new Criteria();
		$c->addJoin(PtkTerdaftarPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID);
		$c->addJoin(PtkTerdaftarPeer::PTK_ID, PtkPeer::PTK_ID); 
		$c->add(SekolahPeer::KABUPATEN_KOTA_ID, $kabKotaId);
		$c->addAscendingOrderByColumn(PtkPeer::NUPTK);
		
		if ($setLimit) {
			$c->setLimit($setLimit);
		}
		
		$ptkts = PtkTerdaftarPeer::doSelect($c);
		foreach ($ptkts as $p) {
			$arrPtkTerdaftar[$p->getPtkId()] = $p;
		}
		*/
		
		
		/* Get Data Mengajar */
		$c = new Criteria();
		$c->addJoin(MengajarPeer::PTK_ID, PtkPeer::PTK_ID);
		$c->addJoin(PtkPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID);
		$c->add(SekolahPeer::KABUPATEN_KOTA_ID, $kabKotaId);
		$c->addGroupByColumn(MengajarPeer::PTK_ID);
		$c->addGroupByColumn(MengajarPeer::ROMBONGAN_BELAJAR_ID);
		$c->addGroupByColumn(MengajarPeer::BIDANG_STUDI_ID);
		//$c->addAscendingOrderByColumn(PtkPeer::SEKOLAH_ID);
		$c->addAscendingOrderByColumn(PtkPeer::NUPTK);
		
		
		try {
			//$mengajars = MengajarPeer::doSelectJoinAllExceptRombonganBelajar($c);
			$mengajars = MengajarPeer::doSelect($c);
		} catch (Exception $e) {
			print_r($e);			
		}
		
		foreach ($mengajars as $m) {
			//$m = new Mengajar();
			//$arrMengajar[$m->getPtkId()][] = $m;
			$arrMengajar[$m->getPtk()->getNuptk()][] = $m;
		}
		
		/* Get Data Terdaftar */
		$c = new Criteria();
		$c->addJoin(PtkTerdaftarPeer::PTK_ID, PtkPeer::PTK_ID);
		$c->addJoin(PtkPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID);
		$c->add(SekolahPeer::KABUPATEN_KOTA_ID, $kabKotaId);
		$terdaftars = PtkTerdaftarPeer::doSelect($c);
		
		foreach ($terdaftars as $t) {
			$arrTerdaftar[$t->getPtkId()][] = $t;
		}
		
		/* Get Data NRG */
		$c = new Criteria();
		$c->addJoin(NrgPeer::NRG_KODEKAB, KabupatenKotaPeer::KODE_SERTIFIKASI);
		$c->add(KabupatenKotaPeer::KABUPATEN_KOTA_ID, $kabKotaId);
		$nrgs = NrgPeer::doSelect($c);	
		
		foreach ($nrgs as $n) {
			//$n = new Nrg();
			$arrNrg[$n->getNrgNuptk()] = $n;
		}
		$countNrgAwal = sizeof($arrNrg);
		
		/* Get Daftar Bidang Studi */
		$bidangStudis = BidangStudiPeer::doSelect(new Criteria());		
		foreach ($bidangStudis as $b) {
			$arrBidangStudi[$b->getKode()] = $b;			
		}
		
		/* Get Daftar Mapping Bidang Studi */
		$mbs = RMappingBidangStudiPeer::doSelect(new Criteria());		
		foreach ($mbs as $b) {
			//$b = new RMappingBidangStudi();
			$arrMBS[$b->getKodeLama()] = $b->getKodeBaru1();			
		}
		
		/* Get Daftar Jenis Sekolah */
		$bentukPendidikan = BentukPendidikanPeer::doSelect(new Criteria());		
		foreach ($bentukPendidikan as $b) {
			$arrBentukPendidikan[$b->getPrimaryKey()] = $b;			
		}		
		
		/* Get Referensi KTSP */
		$rKtsps = RJamMatpelPeer::doSelect(new Criteria());
		foreach ($rKtsps as $r) {
			//$r = new RJamMatpel();
			$arrKtsp[$r->getPrimaryKey()] = $r->getJamPelajaranMinggu(); 
		}
		
		/* Get Referensi Tugas PTK */
		$tptks = TugasPtkPeer::doSelect(new Criteria());
		foreach ($tptks as $t) {
			//$r = new RJamMatpel();
			$arrTugasPtk[$t->getPrimaryKey()] = $t;
		}
				
		/* Get Referensi Golongan */
		$golongans = PangkatGolonganPeer::doSelect(new Criteria());
		foreach ($golongans as $g) {
			$arrGolongan[$g->getPrimaryKey()] = $g;
		}
		
		/* Get Referensi Status Kepegawaian */
		$c = new Criteria();
		$statusKepegawaians = StatusKepegawaianPeer::doSelect($c);
		foreach ($statusKepegawaians as $s) {
			$arrStatusKepegawaian[$s->getPrimaryKey()] = $s;
		}
		
		/* Get PesertaDidikTerdaftars, with key Rombel  */
		$c = new Criteria();
		$c->addJoin(PesertaDidikTerdaftarPeer::ROMBONGAN_BELAJAR_ID, RombonganBelajarPeer::ROMBONGAN_BELAJAR_ID);
		$c->addJoin(RombonganBelajarPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID );
		$c->add(SekolahPeer::KABUPATEN_KOTA_ID, $kabKotaId);		
		
		$pdts = PesertaDidikTerdaftarPeer::doSelect($c);
		//$pdts = PesertaDidikTerdaftarPeer::doSelectJoinRombonganBelajar($c);
		foreach ($pdts as $p) {
			//$p = new PesertaDidikTerdaftar();
			$arrPdt[$p->getRombonganBelajarId()][] = $p;
		}
		
		/* Auto insert rekap u/ T.Profesi */
		require_once(dirname(__FILE__).D."modules".D."Validasi".D."Validasi.php");
		$vModule = new ValidasiModule();
		
		$c = new Criteria();
		$c->add(RMappingValidasiPeer::R_KEPERLUAN_ID, 3);
		$validasis = RMappingValidasiPeer::doSelectJoinAll($c);
		
		/* Prepare Last Update for Pengiriman */
		
		
		/* Anticipate IF TPtk already filled */
		$c = new Criteria();
		$c->add(TPtkPeer::KABUPATEN_KOTA_SEKOLAH_ID, $kabKotaId);
		$existingTptks = TPtkPeer::doSelect($c);
		$arrTptk = array();
		
		foreach ($existingTptks as $t) {
			$arrTptk[$t->getPrimaryKey()] = $t;
		}
		
		/* Get Ready */
		//$con = Propel::getConnection(SekolahPeer::DATABASE_NAME);
		//$con = Propel::getConnection(TPtkPeer::DATABASE_NAME);
		$con = Propel::getConnection(SekolahPeer::DATABASE_NAME);
		$con->beginTransaction();

		$ram_used = memory_get_usage()/(1024 * 1024);
		$counter = 0;
		$passCount = 0;
		$emptyCount = 0;		
		$notQualifiedCount = 0;
		$skip = array();
		
		$arrRekapSekolah = array();
		
		echo "<br>Perlu $ram_used MB memory untuk persiapan.<br>";

		writeKabkotaKonversiFile($kabKotaId, 0, sizeof($arrPtk));
		echo "<table>";
		
		/* Loop On PTK */		
		foreach ($arrPtk as $p) {
			
			if ($arrTptk[$p->getPtkId()]) {
				$tPtk = $arrTptk[$p->getPtkId()];
			} else {
				$tPtk = new TPtk();
			}
			//$tPtk = new TPtk();
			
			$ptkInArray = array_filter( $p->toArray(), "check_zero" );
			$tPtk->fromArray( $p->toArray() );
			$tPtk->setTPtkId($p->getPrimaryKey());
			$tPtk->setTPtkId($p->getPrimaryKey());
			
			//print_r($p);
			//print_r($tPtk); die;
			
			
			//$ptkId = $p->getPtkId();
			
			// Bidang Studi Sertifikasi Menurut NRG
			$bsArr = getSertifikasi($tPtk);
			
			$bsNrg = $bsArr["nrg"];
			$namaGuruNrg = $bsArr["nama_guru_nrg"];			
			$tglLahirNrg = $bsArr["tgl_lahir_nrg"];
			
			$bsNama = $bsArr["nama_bidang_studi_sertifikasi"];
			$bsId = $bsArr["bidang_studi_sertifikasi_id"];
			$bsKode = $bsArr["kode_bidang_studi_sertifikasi"];			
			$bsTgl = $bsArr["tgl_lulus_sertifikasi"];			
			
			$tPtk->setNrg($bsNrg);
			
			//$tPtk->setNamaNrg(substr($namaGuruNrg,0,11));
			$tPtk->setNamaNrg($namaGuruNrg);
			$tPtk->setTglLahirNrg($tglLahirNrg);			
			
			$tPtk->setNamaBidangStudiSertifikasi($bsNama);
			$tPtk->setKodeBidangStudiSertifikasi($bsKode);
			$tPtk->setBidangStudiSertifikasiId($bsId);
			$tPtk->setTglLulusSertifikasi($bsTgl);
			$tPtk->setTglLahirNrg($tglLahirNrg);
			
			// Nama sekolah & Wilayah 
			$sekolah = $arrSekolah[$tPtk->getSekolahId()];
			if (!($sekolah)) {
				continue;
			}
			
			if (($sekolah)) {
				
				//$namaSekolah = $sekolah->getNama();
				//$statusSekolah = $sekolah->getStatusSekolah();
				
				$namaSekolah = $sekolah["nama"];
				$statusSekolah = $sekolah["status_sekolah"];
				$lastUpdate = $sekolah["last_update"];
				
			} else {
				$namaSekolah = NULL;
				$statusSekolah = NULL;
			}
			
			//$jenisSekolahObj = $arrBentukPendidikan[$sekolah->getJenisSekolahId()];			
			$jenisSekolahObj = $arrBentukPendidikan[$sekolah["jenis_sekolah_id"]];
			
			if (is_object($jenisSekolahObj)) {
				$namaJenisSekolah = $jenisSekolahObj->getNama();
				//$jenisSekolahId = $sekolah->getJenisSekolahId();
				$jenisSekolahId = $sekolah["jenis_sekolah_id"];
			} else {
				$namaJenisSekolah = NULL;
				$jenisSekolahId = NULL;  
			}
			$tPtk->setNamaSekolah($namaSekolah);
			$tPtk->setStatusSekolah($statusSekolah);
			
			$tPtk->setNamaJenisSekolah($namaJenisSekolah);
			$tPtk->setJenisSekolahId($jenisSekolahId);
			
			$tPtk->setNamaKabupatenKota($kabKotaNama);
			$tPtk->setKabupatenKotaSekolahId($kabKotaId);
			
			$tPtk->setNamaPropinsi($propinsiObj->getNama());
			$tPtk->setPropinsiSekolahId($propinsiObj->getPrimaryKey());
			
			
			// Nama Pangkat & Status Kepegawaian
			$pangkatGolonganObj = $arrGolongan[$tPtk->getPangkatGolonganId()];
			if (is_object($pangkatGolonganObj)) {
				$tPtk->setNamaPangkatGolongan($pangkatGolonganObj->getNama());
			}
			$statusKepegawaianObj = $arrStatusKepegawaian[$tPtk->getStatusKepegawaianId()];
			if (is_object($statusKepegawaianObj)) {
				$tPtk->setNamaStatusKepegawaian($statusKepegawaianObj->getNama());
				if ($statusKepegawaianObj->getPrimaryKey() <= 3) {
					$tPtk->setIsPns(1);
				} else {
					$tPtk->setIsPns(0);
				}
			} else if (is_object($pangkatGolonganObj)) {
				if (is_object($pangkatGolonganObj)) {
					$tPtk->setIsPns(1);
				} else {
					$tPtk->setIsPns(0);
				}
			}
			
			// Jam Tambahan
			$jamTambahanArr = getTugasTambahan($tPtk);			
			$tugasTambahanId = $jamTambahanArr["tugas_tambahan_id"];
			$namaTugasTambahan = $jamTambahanArr["nama_tugas_tambahan"];
			$jamTugasTambahan = $jamTambahanArr["jam_tugas_tambahan"];
			
			//if ($tugasTambahanId != 4) {
			//print_r($jamTambahanArr); die;
			$tPtk->setTugasTambahanId($tugasTambahanId);
			$tPtk->setNamaTugasTambahan($namaTugasTambahan);
			$tPtk->setJamTugasTambahan($jamTugasTambahan);				
			//}
			
			
			// Jumlah Jam Mengajar
			$jjmArr = getJumlahJamMengajar($tPtk);
			$jjm = $jjmArr["jjm"];
			$jjmSesuai = $jjmArr["jjm_sesuai"];
			$jjmSesuaiKtsp = $jjmArr["jjm_sesuai_ktsp"];			
			
			$tPtk->setJumlahJamMengajar($jjm);
			$tPtk->setJumlahJamMengajarSesuai($jjmSesuai);
			$tPtk->setJumlahJamMengajarSesuaiKtsp($jjmSesuaiKtsp);
			$tPtk->setTotalJamMengajarSesuai($jjmSesuai + $jamTugasTambahan);
			
			// Masa Kerja			
			$arrMasaKerja = getMasaKerja($tPtk);
			$tPtk->setMasaKerjaTahun($arrMasaKerja["masa_kerja_tahun"]);
			$tPtk->setMasaKerjaBulan($arrMasaKerja["masa_kerja_bulan"]);			
			
			// Pensiun			
			$tglPensiun = getTglPensiun($tPtk);
			$tPtk->setTglPensiun($tglPensiun);
			
			// Wilayah Terpencil
			$bertugasDiWilayahTerpencil = getBertugasDiWilayahTerpencil($tPtk);
			$tPtk->setBertugasDiWilayahTerpencil($bertugasDiWilayahTerpencil);			
			
			// Riwayat Pendidikan .. sedang kuliah ndak
			$namaStatusKepegawaian = $tPtk->getNamaStatusKepegawaian();
			
			echo "<tr><td>Sekolah = ".$tPtk->getNamaSekolah()." </td><td> ID = ".$tPtk->getTPtkId()."<br>NUPTK = ".$tPtk->getNuptk()."</td><td> Nama= ".$p->getNama()."<br>".$tPtk->getTglLahir('Ymd')."</td><td> Kode = ".$bsKode."</td><td> BdStd Sertifikasi = ".$bsNama."</td><td> JJM = $jjm </td><td> JJM Sesuai = $jjmSesuai </td><td> JJM Sesuai KTSP = $jjmSesuaiKtsp </td><td> Status = $namaStatusKepegawaian </td><td> Tgl Pensiun = $tglPensiun</td>";
			//echo "ID = ".$tPtk->getTPtkId()."| Nama= ".$p->getNama()."| Kode = ".$bsKode."| BdStd Sertifikasi = ".$bsNama."| JJM = $jjm | JJM Sesuai = $jjmSesuai | JJM Sesuai KTSP = $jjmSesuaiKtsp | Tugas Tambahan = $namaTugasTambahan | Tgl Pensiun = $tglPensiun<br>";
			//echo "Nama= ".$p->getNama()."| Jam Mengajar= ".$jjm."<br>";			
			
			
			// Cek If NUPTK Dobel //
			/*
			if ($lastNuptk != $tPtk->getNuptk()) {
				// 0 : != , bikin new T_Ptk_ganda, save 0, simpan 
				//		$lastNuptk = $tptk->getNuptk
				//		$lastPtkId = $tptk->getPtkId 
				// 1 : sama dgn 0 ? bikin new T_Ptk_ganda, 
				//		setPtk->lastNuptk
				//		setDll
				//		last2 JANGAN DISET
				// 2 : sama dgn 0 ?
				//		setPtk->lastNuptk
				//		setDll
				// 3 : !=, bikin baru set $last2 baru
				
				$tg = new TPtkGanda();
				$tg->setTPtkGandaId($tPtk->getTPtkId());
				$tg->setTPtkId($tPtk->getTPtkId());
				$tg->setTPtkId($tPtk->getTPtkId());
				
				$lastPtkId = $tPtk->getTPtkId();				
				$lastNuptk = $tPtk->getNuptk();				
				$lastNama = $tPtk->getNama();				
				$lastTglLahir = $tPtk->getTglLahir(); 
				$lastNamaSekolah = $tPtk->getNamaSekolah();
				$lastSekolahId = $tPtk->getSekolahId();
				
			} else if ($lastNuptk == $tPtk->getNuptk()) {
				
				$tg = new TPtkGanda();
				$tg->setTPtkGandaId($tPtk->getTPtkId());
				$tg->setTPtkId($lastPtkId);
				$tg->setTPtkId($lastPtkId);
					
			}
					
			$tg->setNama($tPtk->getNama());			
			$tg->setTglLahir($tPtk->getTglLahir());
			$tg->setNamaSekolah($tPtk->getNamaSekolah());
			$tg->setSekolahId($tPtk->getSekolahId());
			
			$tg->setParentNama($lastNama);
			$tg->setParentTglLahir($lastTglLahir);
			$tg->setParentNamaSekolah($lastNamaSekolah);
			$tg->setParentSekolahId($lastSekolahId);
			
			$tg->setIsInduk($arrPtkTerdaftar[$tPtk->getTPtkId()]);
			$tg->save($con);
			*/
			
			try {
				
				$tPtk->save($con);
				//$tPtk->save();
			
				$counter++;
				if (($counter % 100) == 0) {
					$con->commit();	
					writeKabkotaKonversiFile($kabKotaId, $counter, sizeof($arrPtk));			
				}

			} catch (Exception $e) {
				//$skip[] = $tPtk->getTPtkId();
				//print_r($e->getMessage());
				echo ("<br><b>");
				print_r($e);
				echo ("/<b>");
				//echo ("<font color='red'>Error: {$e->getMessage()}</font>");				
				//print_r($tPtk);
				echo ("<br>");
				continue;
			}
			
			/* Check is passing syarat T.Profesi or not */ 
			$pass = true;
			$validasiEmptyCount = 0;			
			
			foreach ($validasis as $v) {				
				
				$functionName = $v->getRValidasi()->getNamaFungsi();
				$severity = $v->getRDerajatValidasiId();
				
				if ($functionName) {
					$hasilCek = $vModule->{$functionName}($tPtk);
					$passValidasi = $hasilCek["pass"];
					$empty = $hasilCek["empty"];
				}
				if ($severity == 3) {
					$pass = $pass && $passValidasi;
				}
				$hasilCekStr = $passValidasi ? "pass" : "fail";
				$styleBg = $passValidasi ? 'style="background-color:green;"' : 'style="background-color:red"';
				
				if (!$passValidasi) {
					if ($empty) {
						$validasiEmptyCount++;
					}
				}
				echo "<td $styleBg >$functionName : $hasilCekStr</td>";
				
			}
			$passStr = $pass ? "pass" : "fail";
			if ($pass) {				
				$passCount++;
				$arrRekapSekolah[$tPtk->getSekolahId()]["pass"]++; 
			} else {
				if ($validasiEmptyCount) {
					$emptyCount++;
					$arrRekapSekolah[$tPtk->getSekolahId()]["empty"]++;
				} else {
					$notQualifiedCount++;
					$arrRekapSekolah[$tPtk->getSekolahId()]["notQualified"]++;
				}
			}
			echo "<td>Verdict: <b>".$passStr."</b></td></tr>";
			
			
		}
		echo "</table>";
		
		$countNrgAkhir = sizeof($arrNrg);
		$countNrgMatch = $countNrgAwal - $countNrgAkhir;
		
		$time_end = microtime_float();
		$time = $time_end - $time_start;
		$ram_used = memory_get_usage()/(1024 * 1024);
		
		echo "Total {$counter} ptk | $emptyCount empty | {$passCount} passed | $notQualifiedCount not qualified<br>";		
		echo "Dari {$countNrgAwal} nrg | {$countNrgMatch} matching | {$countNrgAkhir} masih tersisa ";
		if (sizeof($skip) > 0) 
			echo "<br>Ada ".sizeof($skip)." data yang mengandung kesalahan, yaitu id ". implode(", ", $skip);
		echo "<br><b><i>Selesai dalam $time detik, memakan ". nf($ram_used)." MB</i></b><br>";		
		//unset ($_GLOBALS['arrSekolah'], $_GLOBALS['arrPtk'], $_GLOBALS['arrMengajar'], $_GLOBALS['arrTerdaftar'], $_GLOBALS['arrNrg'], $_GLOBALS['arrBidangStudi']);	
		unset ($sekolahs, $mengajars, $terdaftars, $nrgs, $arrSekolah, $arrPtk, $arrMengajar, $arrTerdaftar, $arrNrg, $arrBidangStudi);
		//unset ($sekolahs, $mengajars, $terdaftars, $nrgs);
		//gc_collect_cycles ();		
		//$ram_used = memory_get_usage()/(1024 * 1024);
		//echo "<br>Setelah garbage collection, memory terpakai ". nf($ram_used)." MB<br>";
		
		//foreach ($arrRekap as $a) {
		/*
		$c = new Criteria();
		$c->add(TRekapValidasiPeer::KABUPATEN_KOTA_ID, $kabKotaId);
		$tr = TRekapValidasiPeer::doSelectOne($c);
		if (!is_object($tr)) {
			$tr = new TRekapValidasi();	
		}
				
		$tr->setKabupatenKotaId($kabKotaId);
		$tr->setPropinsiId($propinsiObj->getPrimaryKey());
		$tr->setJumlahIncomplete($emptyCount);
		$tr->setJumlahPass($passCount);
		$tr->setJumlahUnqualified($notQualifiedCount);
		$tr->setRKeperluanId(3);
		//$tr->save($con);
		//}
		 */
		
		$con->commit();		
		
		removeKabkotaKonversiFile($kabKotaId);
	}
	
	function removeKabkotaKonversiFile($kabKotaId) {
		
		global $filesAllowedToBeDeletedKabkota;
		 
		$filename = "konversi-kabkota-$kabKotaId.txt";
		$path = dirname(__FILE__);
		$filepath = $path.D.'tmp'.D.$filename;
		
		$key = array_search($kabKotaId, $filesAllowedToBeDeletedKabkota);
		//print_r($filesAllowedToBeDeletedKabkota);
		//print_r($filepath);
		//print_r($key);
		
		if ($key >= 0) {
			if (file_exists($filepath)) {
				unlink($filepath);
			}
		}
	}

	function removePropinsiKonversiFile($propId) {
		global $filesAllowedToBeDeletedPropinsi;
		
		$filename = "konversi-propinsi-$propId.txt";
		$path = dirname(__FILE__);
		$filepath = $path.D.'tmp'.D.$filename;
		
		$key = array_search($propId, $filesAllowedToBeDeletedPropinsi); 
		if ($key > 0) {
			if (file_exists($filepath)) {
				unlink($filepath);
			}
		}
	}
	
	function writeKabkotaKonversiFile($kabKotaId, $progress, $total) {
		
		$filename = "konversi-kabkota-$kabKotaId.txt";
		$path = dirname(__FILE__);
		$filepath = $path.D.'tmp'.D.$filename;
		 
		if (file_exists($filepath)) {			
			$arrKonversi = unserialize(file_get_contents($filepath));
			if ($arrKonversi["progress"] > $progress) {
				return false;	
			}
		}
		$fp = fopen($filepath, 'w');
		$arrKonversi = array("kabupaten_kota_id" => $kabKotaId, "progress" => $progress, "total" => $total);
		
		//fwrite($fp, "{ 'running': true, 'progress': $progress, 'total': $total }" );
		fwrite($fp, serialize($arrKonversi));
		fclose($fp);
		
		return true;
	}
	
	function writePropinsiKonversiFile($propId, $progress, $total) {
		
		$filename = "konversi-propinsi-$propId.txt";
		$path = dirname(__FILE__);
		$filepath = $path.D.'tmp'.D.$filename;
		if (file_exists($filepath)) {			
			$arrKonversi = unserialize(file_get_contents($filepath));
			if ($arrKonversi["progress"] > $progress) {
				return false;	
			}
		}
		$fp = fopen($filepath, 'w');
		$arrKonversi = array("propinsi_id" => $propId, "progress" => $progress, "total" => $total);
		
		//fwrite($fp, "{ 'running': true, 'progress': $progress, 'total': $total }" );
		fwrite($fp, serialize($arrKonversi));				
		fclose($fp);
		return true;
	}	

	if ($argv) {
		
		$wilayah = $argv[1];
		$id = $argv[2];
		
		if ($wilayah == "kabupaten_kota_id") {
			$kabKotaId = $id;
		} else if ($wilayah == "propinsi_id") {
			$propId = $id;
		}		
	
	} else {
	
		$propId = $_REQUEST["propinsi_id"];
		$kabKotaId = $_REQUEST["kabupaten_kota_id"];
			
	}
	
	if ($kabKotaId) {		
		
		main($kabKotaId, '2012-12-01');
		
	} else if ($propId) {
		
		$prop = PropinsiPeer::retrieveByPK($propId);
		$kks = $prop->getKabupatenKotas();
		if (!writePropinsiKonversiFile($propId, 0, sizeof($kks))){
			return false;
		} else {
			$filesAllowedToBeDeletedPropinsi[] = $propId;
		}
				
		$i = 1;
		
		foreach ($kks as $k) {
			//$k = new KabupatenKota();	
			$propId = $k->getPrimaryKey();
			main($k->getPrimaryKey());
			writePropinsiKonversiFile($propId, $i, sizeof($kks));
			
			$i++;
			continue;
		}
				
	}
	
	echo "<br>Rekor pemakaian memory ". nf(memory_get_peak_usage()/(1024*1024))." MB<br>";
?>